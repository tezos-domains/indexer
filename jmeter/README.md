# JMeter Stress Tests

1. Download [Apache JMeter](https://jmeter.apache.org/). It requires [Java](https://www.java.com/en/download/).
1. Run it and open `StressTests.jmx`.
	- *HTTP Request Defaults* configures URL of the API to test.
	- *Thread Group* contains main stress testing parameters you can adjust: *Number of Threads*, *Ramp-up period*, *Loop Count*.
	- Run the tests with *Start* button in the toolbar.
	- Various views of results are available in bottom tree node e.g. *Aggregate Report*.
	- There is a *Clear All* button in the toolbar.
