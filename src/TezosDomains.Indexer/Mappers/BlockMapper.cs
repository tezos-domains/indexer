﻿using TezosDomains.Data.Models;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Mappers
{
    public static class BlockMapper
    {
        public static Block Map(TezosBlock block) => new(
            block.Header.Level.GuardNotNull(),
            block.Hash.GuardNotNull(),
            block.Header.Timestamp.GuardNotNull(),
            block.Header.Predecessor.GuardNotNull()
        );

        public static BlockSlim MapSlim(TezosBlock block) => new(
            block.Header.Level.GuardNotNull(),
            block.Hash.GuardNotNull(),
            block.Header.Timestamp.GuardNotNull()
        );
    }
}