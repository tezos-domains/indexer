﻿using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates
{
    public sealed record ReverseRecordUpdate(
        string OperationGroupHash,
        string Address,
        string Owner,
        string? Name
    ) : IBlockUpdate;
}