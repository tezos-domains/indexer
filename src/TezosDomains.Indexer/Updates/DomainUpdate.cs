﻿using System.Collections.Generic;
using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates
{
    public sealed record DomainUpdate(
        string OperationGroupHash,
        string Name,
        string? Address,
        string Owner,
        string? ValidityKey,
        IReadOnlyDictionary<string, string> Data,
        int? TokenId,
        string[] Operators
    ) : IBlockUpdate;


    public sealed record DomainTransferUpdate(
        string Owner,
        string OperationGroupHash,
        string DomainName,
        string NewOwner
    ) : IBlockUpdate;
}