﻿using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates
{
    public sealed record BidderBalancesUpdate(
        string OperationGroupHash,
        string Address,
        string TldName,
        decimal? Balance
    ) : IBlockUpdate;
}