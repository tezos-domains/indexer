﻿using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates
{
    public sealed record EventUpdate(Event Event) : IBlockUpdate;
}