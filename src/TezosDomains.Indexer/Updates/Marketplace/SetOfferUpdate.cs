﻿using System;
using TezosDomains.Data.Models.Marketplace;

namespace TezosDomains.Indexer.Updates.Marketplace
{
    public record SetOfferUpdate
        (
            OfferType Type,
            string OperationGroupHash,
            string TokenContract,
            int TokenId,
            string InitiatorAddress,
            decimal PriceWithoutFee,
            decimal Fee,
            DateTime ExpiresAtUtc
        )
        : OfferUpdate(OperationGroupHash, TokenContract, TokenId, InitiatorAddress, Type);
}