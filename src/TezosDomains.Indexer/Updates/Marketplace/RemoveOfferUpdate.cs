﻿using TezosDomains.Data.Models.Marketplace;

namespace TezosDomains.Indexer.Updates.Marketplace
{
    public record RemoveOfferUpdate(OfferType Type, string OperationGroupHash, string TokenContract, int TokenId, string InitiatorAddress)
        : OfferUpdate(OperationGroupHash, TokenContract, TokenId, InitiatorAddress, Type);
}