﻿using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates.Marketplace
{
    public abstract record OfferUpdate(string OperationGroupHash, string TokenContract, int TokenId, string InitiatorAddress, OfferType Type) : IBlockUpdate;
}