﻿using TezosDomains.Data.Models.Marketplace;

namespace TezosDomains.Indexer.Updates.Marketplace
{
    public record ExecuteOfferUpdate(
            OfferType Type,
            string OperationGroupHash,
            string TokenContract,
            int TokenId,
            string InitiatorAddress,
            string AcceptorAddress,
            decimal PriceWithoutFee,
            decimal Fee
        )
        : OfferUpdate(OperationGroupHash, TokenContract, TokenId, InitiatorAddress, Type);
}