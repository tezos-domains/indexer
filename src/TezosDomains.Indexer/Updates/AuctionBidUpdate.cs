﻿using System;
using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates
{
    public sealed record AuctionBidUpdate(
        string OperationGroupHash,
        string DomainName,
        string Bidder,
        decimal Amount,
        DateTime EndsAtUtc,
        DateTime OwnedUntilUtc
    ) : IBlockUpdate;
}