﻿using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates
{
    public sealed record AuctionSettledUpdate(
        string OperationGroupHash,
        string DomainName
    ) : IBlockUpdate;
}