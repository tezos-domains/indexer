﻿using System;
using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Updates
{
    public sealed record ValidityUpdate(
        string OperationGroupHash,
        string ValidityKey,
        DateTime ExpiresAtUtc
    ) : IBlockUpdate;
}