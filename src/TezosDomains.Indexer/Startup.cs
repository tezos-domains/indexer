using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Prometheus;
using Serilog;
using System;
using System.Net;
using TezosDomains.Common;
using TezosDomains.Common.Configuration;
using TezosDomains.Common.Health;
using TezosDomains.Common.Metrics;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Services;
using TezosDomains.Indexer.Health;
using TezosDomains.Indexer.Metrics;
using TezosDomains.Indexer.Services;
using TezosDomains.Indexer.Services.Update;
using TezosDomains.Indexer.Services.Update.Events;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Services.Update.Records;
using TezosDomains.Indexer.Subscribers;
using TezosDomains.Indexer.Subscribers.Events;
using TezosDomains.Indexer.Subscribers.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Utils;

namespace TezosDomains.Indexer
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _env;

        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddControllers();
            services.AddMemoryCache();
            services.AddTezosDomainsCommon();

            services.AddTezos();

            services.AddMongoDb();
            services.AddTransient<SellOfferExtractionStrategy>();
            services.AddTransient<BuyOfferExtractionStrategy>();
            services.AddTransient(typeof(OfferEntrypointSubscriber<>));
            services.AddTransient<AuctionBidderBalancesSubscriber>();
            services.AddTransient<AuctionSubscriber>();
            services.AddTransient<NameRegistrySubscriber>();
            services.AddTransient<AuctionBidEntrypointSubscriber>();
            services.AddTransient<AuctionSettleEntrypointSubscriber>();
            services.AddTransient(typeof(OfferPlacedEntrypointSubscriber<>));
            services.AddTransient(typeof(OfferExecutedEntrypointSubscriber<>));
            services.AddTransient(typeof(OfferRemovedEntrypointSubscriber<>));
            services.AddTransient<DomainClaimEntrypointSubscriber>();
            services.AddTransient<DomainBuyEntrypointSubscriber>();
            services.AddTransient<DomainRenewEntrypointSubscriber>();
            services.AddTransient<DomainCommitEntrypointSubscriber>();
            services.AddTransient<DomainSetChildRecordEntrypointSubscriber>();
            services.AddTransient<DomainUpdateEntrypointSubscriber>();
            services.AddTransient<ReverseRecordUpdateEntrypointSubscriber>();
            services.AddTransient<ReverseRecordClaimEntrypointSubscriber>();
            services.AddTransient<DefaultUpdateService>();
            services.AddTransient<IUpdateService>(p => p.Create<UpdateServiceMetricsDecorator>(p.GetRequiredService<DefaultUpdateService>()));

            // Order of IUpdateProcessor-s is significant e.g. domains must be updated before reverse records.
            // First is executed DomainUpdatePreProcessor - creates DomainTransferEvents, updates or discards DomainUpdates/SetChildRecords
            services.AddTransient<IUpdatePreProcessor, DomainUpdatePreProcessor>();
            // After IUpdateProcessors are executed sequentially
            services.AddTransient<IEventProcessor, AuctionSettleEventProcessor>();
            services.AddTransient<IEventProcessor, AuctionBidEventProcessor>();
            services.AddTransient<IEventProcessor, AuctionEndEventProcessor>();
            services.AddTransient<IEventProcessor, AuctionWithdrawEventProcessor>();
            services.AddTransient<IEventProcessor, DomainGrantEventProcessor>();
            services.AddTransient<IEventProcessor, OfferUpdateEventProcessor>();
            services.AddTransient<IUpdateProcessor, EventUpdateProcessor>();
            services.AddTransient<IUpdateSubProcessor<ReverseRecord>, ValidityUpdateSubProcessor<ReverseRecord>>();
            services.AddTransient<IUpdateSubProcessor<Domain>, ValidityUpdateSubProcessor<Domain>>();
            services.AddTransient<IUpdateSubProcessor<Domain>, DomainUpdateParentOwnerSubProcessor>();
            services.AddTransient<IUpdateSubProcessor<Domain>, DomainUpdateLastAuctionSubProcessor>();
            //order is important - AuctionUpdateProcessor must be before DomainUpdateProcess to read the updated auction data from db
            services.AddTransient<IUpdateProcessor, AuctionUpdateProcessor>();
            services.AddTransient<IUpdateProcessor, DomainUpdateProcessor>();
            services.AddTransient<IUpdateProcessor, ReverseRecordUpdateProcessor>();
            services.AddTransient<IUpdateProcessor, BidderBalancesUpdateProcessor>();
            services.AddTransient<IUpdateProcessor, BlockUpdateProcessor>();
            //order is important - OfferUpdateProcessor must be after DomainUpdateProcess to read the updated data from db
            services.AddTransient<IUpdateProcessor, OfferUpdateProcessor>();

            services.AddTezosDomainsConfigurationWithDiagnostics<MarketplaceConfiguration>("Marketplace");
            services.AddTezosDomainsConfigurationWithDiagnostics<AffiliateConfiguration>("Affiliate");


            services.AddHostedService<IndexerService>();

            services.AddTezosDomainsConfigurationWithDiagnostics<IndexingProgressHealthConfiguration>(sectionName: "IndexingProgressHealth");
            services.AddSingleton<IndexingProgressHealthCheck>();

            services.AddHealthChecks()
                .AddCheck<IndexingProgressHealthCheck>("IndexingProgress")
                .ForwardToPrometheus()
                ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, MetricsConfiguration metricsConfiguration)
        {
            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();
            app.UseSecurityHeaders(_configuration);
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseHttpMetrics();
            app.UseHealth();
            app.UseConfigurationDiagnostics();

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapMetricsIfEnabled(metricsConfiguration);
                    endpoints.MapControllers();

                    //http route for loadBalancer health check - only pick the indexer with acquired db distributed lock
                    endpoints.MapGet(
                        "/lock",
                        async context =>
                        {
                            var isLockAcquired = app.ApplicationServices.GetService<IndexerDistributedLockService>()?.DbLockInfo?.MachineName
                                                 == Environment.MachineName;
                            await context.Response.WriteAsync($"db lock acquired:{isLockAcquired}");
                            context.Response.StatusCode =
                                isLockAcquired
                                    ? (int)HttpStatusCode.OK
                                    : (int)HttpStatusCode.NotFound;
                        }
                    );
                }
            );
        }
    }
}