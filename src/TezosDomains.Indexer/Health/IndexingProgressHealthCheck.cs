﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Services;

namespace TezosDomains.Indexer.Health
{
    public sealed class IndexingProgressHealthCheck : IHealthCheck
    {
        private readonly IndexingProgressHealthConfiguration _configuration;
        private readonly IBlockQueryService _blockQueryService;
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly ILogger _logger;
        private readonly IndexerDistributedLockService _distributedLockService;

        private DateTime _lastProgressTime = DateTime.UtcNow;
        private volatile BlockSlim? _lastBlock;

        public IndexingProgressHealthCheck(
            IndexingProgressHealthConfiguration configuration,
            IBlockQueryService blockQueryService,
            IHostApplicationLifetime appLifetime,
            ILogger<IndexingProgressHealthCheck> logger,
            IndexerDistributedLockService distributedLockService
        )
        {
            _configuration = configuration;
            _blockQueryService = blockQueryService;
            _appLifetime = appLifetime;
            _logger = logger;
            _distributedLockService = distributedLockService;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellation)
        {
            BlockSlim? newBlock = await _blockQueryService.GetLatestOrNullAsync(cancellation);
            var data = new Dictionary<string, object>() { { "DistributedLock", _distributedLockService.DbLockInfo?.ToString() ?? "null" } };

            if (newBlock?.Hash != _lastBlock?.Hash)
            {
                var result = HealthCheckResult.Healthy(
                    $"Block indexing recently progressed from {_lastBlock} to {newBlock}.",
                    data: data
                );
                _lastProgressTime = DateTime.UtcNow;
                _lastBlock = newBlock;
                return result;
            }

            var progressAge = DateTime.UtcNow - _lastProgressTime;

            if (progressAge < _configuration.ProgressAge.Degraded)
                return HealthCheckResult.Healthy(
                    $"Last block was indexed {progressAge} ago which is still fine - less than {_configuration.ProgressAge.Degraded}. It indexed {_lastBlock}.",
                    data: data
                );

            if (progressAge < _configuration.ProgressAge.Unhealthy)
                return HealthCheckResult.Degraded(
                    $"Last block was indexed {progressAge} ago which may be much - between {_configuration.ProgressAge.Degraded} and {_configuration.ProgressAge.Unhealthy}. It indexed {_lastBlock}.",
                    data: data
                );

            if (progressAge < _configuration.ProgressAge.Shutdown)
                return HealthCheckResult.Unhealthy(
                    $"Last block was indexed {progressAge} ago which is too much - more than or equal to {_configuration.ProgressAge.Unhealthy}. It indexed {_lastBlock}.",
                    data: data
                );

            _logger.LogCritical(
                "No block has been indexed for {progressAge} with {@lastBlock}. It is more than {configAge} so shutting the app down.",
                progressAge,
                _lastBlock,
                _configuration.ProgressAge.Shutdown
            );
            Program.AfterShutdownDelay = _configuration.AfterShutdownDelay;
            _appLifetime.StopApplication();

            return HealthCheckResult.Unhealthy(
                $"No block has been indexed for {progressAge} with last block {_lastBlock}. It is more than configured {_configuration.ProgressAge.Shutdown} so shutting the app down.",
                data: data
            );
        }
    }
}