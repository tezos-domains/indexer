﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using TezosDomains.Common.Configuration;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Health
{
    public sealed class IndexingProgressHealthConfiguration : IConfigurationInstance, IValidatableObject
    {
        [Required]
        public ProgressAges ProgressAge { get; init; } = null!;

        public TimeSpan AfterShutdownDelay { get; init; }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            if (ProgressAge.Degraded >= ProgressAge.Unhealthy)
                yield return new ValidationResult($"{GetProperty(c => c.ProgressAge.Degraded)} must be less than {GetProperty(c => c.ProgressAge.Unhealthy)}.");

            if (ProgressAge.Unhealthy >= ProgressAge.Shutdown)
                yield return new ValidationResult($"{GetProperty(c => c.ProgressAge.Unhealthy)} must be less than {GetProperty(c => c.ProgressAge.Shutdown)}.");
        }

        object IConfigurationInstance.GetDiagnosticInfo() => this;

        private string GetProperty<T>(Expression<Func<IndexingProgressHealthConfiguration, T>> expression)
            => $"{expression.GetFieldName()} {expression.Compile().Invoke(this)}";
    }

    public sealed class ProgressAges
    {
        public TimeSpan Degraded { get; init; }
        public TimeSpan Unhealthy { get; init; }
        public TimeSpan Shutdown { get; init; }
    }
}