﻿using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Indexer.Services
{
    public interface IIndexingCommand
    {
        Task ExecuteAsync(CancellationToken cancellation);
    }
}