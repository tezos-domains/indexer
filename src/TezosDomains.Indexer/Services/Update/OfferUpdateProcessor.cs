﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Updates;
using TezosDomains.Indexer.Updates.Marketplace;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update
{
    public sealed class OfferUpdateProcessor : HistorizationProcessor<Offer>
    {
        private readonly ILogger _logger;
        private readonly MarketplaceConfiguration _configuration;
        private readonly IMongoCollection<Domain> _domainDbCollection;

        public OfferUpdateProcessor(MongoDbContext dbContext, ILogger<OfferUpdateProcessor> logger, MarketplaceConfiguration configuration) : base(dbContext)
        {
            _logger = logger;
            _configuration = configuration;
            _domainDbCollection = dbContext.Get<Domain>();
        }

        public override async Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation)
        {
            var updates = new List<IBlockUpdate>();
            var tokenIds = new List<int>();
            var validityKeys = new List<string>();

            foreach (var update in args.GetUpdates<IBlockUpdate>())
            {
                switch (update)
                {
                    case OfferUpdate o:
                        updates.Add(o);
                        tokenIds.Add(o.TokenId);
                        break;
                    case DomainUpdate { TokenId: { } } d:
                        tokenIds.Add(d.TokenId.Value);
                        updates.Add(d);
                        break;
                    case ValidityUpdate v:
                        validityKeys.Add(v.ValidityKey);
                        updates.Add(v);
                        break;
                }
            }

            if (updates.Count == 0)
                return;

            var domainFilter = DomainFilters
                .GetAllWithTokenId(tokenIds, validityKeys)
                .ExcludeHistory();
            var domainsByTokenId = await _domainDbCollection.Find(session, domainFilter)
                .ToDictionaryAsync(d => d.TokenId.GuardNotNull(), cancellation);
            var tokenIdsByValidityKey = domainsByTokenId.Values
                .Where(d => d.ValidityKey != null && d.TokenId != null)
                .GroupBy(d => d.ValidityKey.GuardNotNull())
                .ToDictionary(d => d.Key, group => group.Select(d => d.TokenId.GuardNotNull()));

            var allUpdatedTokenIds = tokenIds.Concat(tokenIdsByValidityKey.Values.SelectMany(v => v)).Distinct();
            var filter = OfferFilters
                .ByTokenId(allUpdatedTokenIds)
                .ExcludeHistory();
            var offersById = await DbCollection.Find(session, filter)
                .ToDictionaryAsync(a => a.OfferId, cancellation);

            var possibleVersionIdsToHistorize =
                offersById.Values.ToDictionary(a => a.OfferId, a => new Historize(a.VersionId));

            foreach (var u in updates)
            {
                switch (u)
                {
                    case OfferUpdate o when !_configuration.AllowedNftContracts.Contains(o.TokenContract):
                        _logger.LogWarning(
                            "Skipping update: {update} because tokenContract: {tokenContract} is not among allowed NFT contracts({allowedNftContracts}).",
                            o,
                            o.TokenContract,
                            _configuration.AllowedNftContracts
                        );
                        break;


                    case SetOfferUpdate so:
                        var (soOffer, soOfferId) = GetPresentOffer(so);
                        var soTokenId = so.GetKey().TokenId;
                        var domain = domainsByTokenId.GetValueOrDefault(soTokenId);

                        if (domain is null)
                            _logger.LogWarning("tokenId: {tokenId} doesn't correspond to any domain.", soTokenId);

                        var domainData = domain is not null ? DomainData.Create(domain) : null;
                        var acceptorAddress = so.Type == OfferType.BuyOffer ? domainData?.Owner : null;

                        offersById[soOfferId] = new Offer(
                            OfferId: soOfferId,
                            Block: args.Block,
                            OperationGroupHash: so.OperationGroupHash,
                            Type: so.Type,
                            InitiatorAddress: so.InitiatorAddress,
                            TokenContract: so.TokenContract,
                            TokenId: soTokenId,
                            CreatedAtUtc: soOffer?.CreatedAtUtc ?? args.Block.Timestamp,
                            ExpiresAtUtc: so.ExpiresAtUtc,
                            Domain: domainData,
                            PriceWithoutFee: so.PriceWithoutFee,
                            Fee: so.Fee,
                            Presence: OfferBlockchainPresence.Present,
                            Validity: Offer.CalculateValidity(_configuration.MarketplaceContract, so.InitiatorAddress, acceptorAddress, domainData, so.Type),
                            AcceptorAddress: acceptorAddress,
                            ValidUntilBlockLevel: int.MaxValue,
                            ValidUntilTimestamp: DateTime.MaxValue
                        );
                        SetVersionIdToHistorize(possibleVersionIdsToHistorize, soOfferId);
                        break;


                    case ExecuteOfferUpdate eo:
                        var (eoOffer, eoOfferId) = GetPresentOffer(eo);

                        offersById[eoOfferId] = eoOffer.GuardNotNull($"Unable to process {nameof(ExecuteOfferUpdate)}({eo}). No existing offer found.") with
                        {
                            Block = args.Block,
                            OperationGroupHash = eo.OperationGroupHash,
                            Presence = OfferBlockchainPresence.Executed,
                            AcceptorAddress = eo.AcceptorAddress
                        };
                        SetVersionIdToHistorize(possibleVersionIdsToHistorize, eoOfferId);
                        break;


                    case RemoveOfferUpdate ro:
                        var (roOffer, roOfferId) = GetPresentOffer(ro);

                        offersById[roOfferId] = roOffer.GuardNotNull($"Unable to process {nameof(RemoveOfferUpdate)}({ro}). No existing offer found.") with
                        {
                            Block = args.Block,
                            OperationGroupHash = ro.OperationGroupHash,
                            Presence = OfferBlockchainPresence.Removed
                        };
                        SetVersionIdToHistorize(possibleVersionIdsToHistorize, roOfferId);
                        break;


                    case DomainUpdate { TokenId: { } tokenId } d:
                        foreach (var o in GetOffers(tokenId))
                        {
                            DomainData data = new(d.Name, d.Owner, d.Operators, d.Data, (o.Domain?.ExpiresAtUtc).ConvertNullToMax());
                            //update buyOffer.sellerAddress when domain owner changes (only applicable for open offer)
                            var acceptor = o.Type == OfferType.BuyOffer && o.Presence == OfferBlockchainPresence.Present ? d.Owner : o.AcceptorAddress;

                            offersById[o.OfferId] = o with
                            {
                                Block = args.Block,
                                OperationGroupHash = d.OperationGroupHash,
                                Domain = data,
                                Validity = Offer.CalculateValidity(_configuration.MarketplaceContract, o.InitiatorAddress, acceptor, data, o.Type),
                                AcceptorAddress = acceptor
                            };
                            SetVersionIdToHistorize(possibleVersionIdsToHistorize, o.OfferId);
                        }

                        break;


                    case ValidityUpdate v:
                        foreach (var tokenId in tokenIdsByValidityKey.GetValueOrDefault(v.ValidityKey).NullToEmpty())
                        {
                            foreach (var o in GetOffers(tokenId))
                            {
                                offersById[o.OfferId] = o with
                                {
                                    Block = args.Block,
                                    OperationGroupHash = v.OperationGroupHash,
                                    Domain = o.Domain.GuardNotNull() with { ExpiresAtUtc = v.ExpiresAtUtc }
                                };
                                SetVersionIdToHistorize(possibleVersionIdsToHistorize, o.OfferId);
                            }
                        }

                        break;


                    default:
                        throw new InvalidOperationException($"Unable to process {u}.");

                        IEnumerable<Offer> GetOffers(int tokenId) => offersById.Values.Where(o => o.TokenId == tokenId);

                        (Offer? Offer, string OfferId) GetPresentOffer(OfferUpdate update)
                        {
                            var key = update.GetKey();
                            var offer = offersById.Values.SingleOrDefault(o => o.GetKey() == key && o.Presence == OfferBlockchainPresence.Present);
                            return (offer, offer?.OfferId ?? Offer.GenerateId(key.Type, key.Address, key.TokenId, args.Block.Level));
                        }
                }
            }

            var historize = possibleVersionIdsToHistorize.Where(v => v.Value.State == Historize.EntryState.Updated)
                .ToDictionary(h => h.Key, h => h.Value.VersionId);
            var changedOfferKeys = possibleVersionIdsToHistorize
                .Where(h => h.Value.State != Historize.EntryState.Untouched)
                .Select(h => h.Key)
                .ToHashSet();
            var updatedOffers = offersById.Values.Where(o => changedOfferKeys.Contains(o.OfferId)).ToArray();

            await WriteUpdatesToDbAsync(updatedOffers, historize.Values, args.Block, session, cancellation);
        }

        private static void SetVersionIdToHistorize(Dictionary<string, Historize> possibleVersionIdsToHistorize, string id)
        {
            if (possibleVersionIdsToHistorize.TryGetValue(id, out var previous))
                previous.State = Historize.EntryState.Updated;
            else
                possibleVersionIdsToHistorize[id] = new Historize(string.Empty) { State = Historize.EntryState.Created };
        }

        private sealed record Historize(string VersionId)
        {
            public EntryState State { get; set; }

            public enum EntryState
            {
                Untouched,
                Updated,
                Created
            }
        }
    }
}