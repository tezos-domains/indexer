﻿using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Updates.Marketplace;

namespace TezosDomains.Indexer.Services.Update
{
    public static class OfferExtensions
    {
        public static OfferKey GetKey(this OfferPlacedEvent offer) => new(offer.SourceAddress, offer.TokenId, OfferType.SellOffer);
        public static OfferKey GetKey(this OfferExecutedEvent offer) => new(offer.InitiatorAddress, offer.TokenId, OfferType.SellOffer);
        public static OfferKey GetKey(this BuyOfferPlacedEvent offer) => new(offer.SourceAddress, offer.TokenId, OfferType.BuyOffer);
        public static OfferKey GetKey(this BuyOfferExecutedEvent offer) => new(offer.InitiatorAddress, offer.TokenId, OfferType.BuyOffer);
        public static OfferKey GetKey(this Offer offer) => new(offer.InitiatorAddress, offer.TokenId, offer.Type);
        public static OfferKey GetKey(this OfferUpdate offer) => new(offer.InitiatorAddress, offer.TokenId, offer.Type);
    }
}