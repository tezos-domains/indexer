﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public abstract class GenerateEventProcessor<TEvent> : IEventProcessor
        where TEvent : Event
    {
        public async Task<IEnumerable<Event>> ProcessEventsAsync(
            IReadOnlyList<Event> events,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var generatedEvents = await GenerateNewEventsAsync(events, args, session, cancellation);
            return events.Concat(generatedEvents);
        }

        protected abstract Task<IEnumerable<TEvent>> GenerateNewEventsAsync(
            IReadOnlyList<Event> originalEvents,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        );
    }
}