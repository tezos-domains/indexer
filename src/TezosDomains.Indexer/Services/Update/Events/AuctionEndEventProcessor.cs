﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public sealed class AuctionEndEventProcessor : GenerateEventProcessor<AuctionEndEvent>
    {
        private readonly MongoDbContext _dbContext;

        public AuctionEndEventProcessor(MongoDbContext dbContext)
            => _dbContext = dbContext;

        protected override async Task<IEnumerable<AuctionEndEvent>> GenerateNewEventsAsync(
            IReadOnlyList<Event> originalEvents,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            if (args.PreviousBlock == null)
                return Array.Empty<AuctionEndEvent>();

            var filter = AuctionFilters.EndedBetween(
                    exclusiveFromUtc: args.PreviousBlock.Timestamp,
                    inclusiveToUtc: args.Block.Timestamp
                )
                .ExcludeHistory();

            var endedAuctions = await _dbContext.Get<Auction>()
                .Find(session, filter)
                .Project(a => new { a.DomainName, a.Bids })
                .ToListAsync(cancellation);

            if (endedAuctions.Count == 0)
                return Array.Empty<AuctionEndEvent>();

            var highestBidIndex = ^1;
            return endedAuctions.Select(
                auction => new AuctionEndEvent(
                    Id: Event.GenerateId(),
                    Block: args.Block,
                    SourceAddress: auction.Bids[highestBidIndex].Bidder,
                    DomainName: auction.DomainName,
                    WinningBid: auction.Bids[highestBidIndex].Amount,
                    Participants: auction.Bids.Select(b => b.Bidder).ToList()
                )
            );
        }
    }
}