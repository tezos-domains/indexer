﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public sealed class DomainGrantEventProcessor : GenerateEventProcessor<DomainGrantEvent>
    {
        private readonly MongoDbContext _dbContext;
        private readonly ILogger _logger;

        public DomainGrantEventProcessor(MongoDbContext dbContext, ILogger<DomainGrantEventProcessor> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        protected override async Task<IEnumerable<DomainGrantEvent>> GenerateNewEventsAsync(
            IReadOnlyList<Event> originalEvents,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var auctionedDomainNames = originalEvents.OfType<Event, AuctionSettleEvent>().Select(b => b.DomainName);
            var boughtDomainNames = originalEvents.OfType<Event, DomainBuyEvent>().Select(b => b.DomainName);
            var claimedDomainNames = originalEvents.OfType<Event, DomainClaimEvent>().Select(b => b.DomainName);
            var changedDomainNames = auctionedDomainNames.Concat(boughtDomainNames).Concat(claimedDomainNames).ToHashSet();

            var grantedDomainsOrUpdates = args
                .GetUpdates<DomainUpdate>()
                .Where(u => !changedDomainNames.Contains(u.Name) && DomainNameUtils.GetDomainLevel(u.Name) == 2)
                .GroupBy(
                    g => g.Name,
                    g => g,
                    (name, g) =>
                    {
                        if (g.Count() > 1)
                            _logger.LogWarning("Multiple domainUpdates {domainName} occurred in the same block.", name);

                        return g.Last();
                    }
                )
                .ToDictionary(d => d.Name);

            if (grantedDomainsOrUpdates.Count == 0)
                return Array.Empty<DomainGrantEvent>();

            var filter = DomainFilters
                .ExistingAtLatestBlock(domainNames: grantedDomainsOrUpdates.Keys)
                .ExcludeHistory();
            var existingDomainNames = await _dbContext.Get<Domain>()
                .Find(session, filter)
                .Project(d => d.Name)
                .ToListAsync(cancellation);

            var validityMap = args.GetUpdates<ValidityUpdate>().ToDictionary(u => u.ValidityKey, u => u.ExpiresAtUtc);
            return grantedDomainsOrUpdates.Keys
                .Except(existingDomainNames)
                .Select(
                    domainName =>
                    {
                        var domainsExpiresAtUtc = validityMap.GetValueOrDefault(domainName);
                        var update = grantedDomainsOrUpdates[domainName];
                        return new DomainGrantEvent(
                            Id: Event.GenerateId(),
                            Block: args.Block,
                            SourceAddress: update.Owner,
                            OperationGroupHash: update.OperationGroupHash,
                            DomainName: domainName,
                            DurationInDays: domainsExpiresAtUtc != default ? (domainsExpiresAtUtc - args.Block.Timestamp).Days : null,
                            DomainOwnerAddress: update.Owner,
                            DomainForwardRecordAddress: update.Address,
                            Data: update.Data
                        );
                    }
                );
        }
    }
}