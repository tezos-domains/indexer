﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public abstract class UpdateEventProcessor<TEvent> : IEventProcessor
        where TEvent : Event
    {
        public async Task<IEnumerable<Event>> ProcessEventsAsync(
            IReadOnlyList<Event> events,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var eventsToUpdate = events.OfType<TEvent>().ToList();
            if (eventsToUpdate.Count == 0)
                return events;

            var updatedEvents = await UpdateEventsAsync(eventsToUpdate, args.Block, session, cancellation);
            return events
                .Except(eventsToUpdate)
                .Concat(updatedEvents);
        }

        protected abstract Task<IEnumerable<TEvent>> UpdateEventsAsync(
            IReadOnlyList<TEvent> events,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        );
    }
}