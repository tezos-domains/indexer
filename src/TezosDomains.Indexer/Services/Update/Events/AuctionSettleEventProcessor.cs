﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public sealed class AuctionSettleEventProcessor : UpdateEventProcessor<AuctionSettleEvent>
    {
        private readonly MongoDbContext _dbContext;

        public AuctionSettleEventProcessor(MongoDbContext dbContext)
            => _dbContext = dbContext;

        protected override async Task<IEnumerable<AuctionSettleEvent>> UpdateEventsAsync(
            IReadOnlyList<AuctionSettleEvent> events,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var eventsByDomainName = events.ToDictionary(e => e.DomainName);
            var filter = AuctionFilters.CanBeSettled(
                    atUtc: block.Timestamp,
                    domainNames: eventsByDomainName.Keys
                )
                .ExcludeHistory();

            var auctions = await _dbContext.Get<Auction>()
                .Find(session, filter)
                .Project(a => new { a.DomainName, a.OwnedUntilUtc, a.EndsAtUtc, a.HighestBid.Amount })
                .ToListAsync(cancellation);

            if (auctions.Count != events.Count)
                throw new InvalidDataException(
                    $"AuctionSettleEvents parsed from block {block.Level} ({block.Hash}), but {auctions.Count} corresponding auctions in CanBeSettled state exists in db."
                );

            return auctions.Select(
                auction => eventsByDomainName[auction.DomainName] with
                {
                    RegistrationDurationInDays = (auction.OwnedUntilUtc - auction.EndsAtUtc).Days,
                    WinningBid = auction.Amount,
                }
            );
        }
    }
}