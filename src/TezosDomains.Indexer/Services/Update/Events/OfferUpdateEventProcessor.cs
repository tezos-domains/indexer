﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public sealed class OfferUpdateEventProcessor : UpdateEventProcessor<OfferEvent>
    {
        private readonly MongoDbContext _dbContext;
        private readonly ILogger<OfferUpdateEventProcessor> _logger;

        public OfferUpdateEventProcessor(MongoDbContext dbContext, ILogger<OfferUpdateEventProcessor> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        protected override async Task<IEnumerable<OfferEvent>> UpdateEventsAsync(
            IReadOnlyList<OfferEvent> events,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var tokenIds = events.Select(e => e.TokenId).ToArray();
            var domainDataByTokenId = await _dbContext.Get<Domain>()
                .Find(
                    DomainFilters
                        .GetAllWithTokenId(tokenIds: tokenIds)
                        .ExcludeHistory()
                )
                .Project(d => new { d.TokenId, d.Name, d.Owner })
                .ToDictionaryAsync(d => d.TokenId.GuardNotNull(), d => new DomainData(d.Name, d.Owner), cancellation);

            var tokenIdsPlaced = events.Where(e => e is OfferPlacedEvent or OfferExecutedEvent).Select(o => o.TokenId).ToHashSet();
            var activeOffersFilter = OfferFilters
                .ByPresence(OfferBlockchainPresence.Present, tokenIds: tokenIdsPlaced)
                .ExcludeHistory();
            var activeOffers = await _dbContext.Get<Offer>()
                .Find(session, activeOffersFilter)
                .Project(o => new OfferPrice(new OfferKey(o.InitiatorAddress, o.TokenId, o.Type), o.PriceWithoutFee, o.Fee))
                .ToDictionaryAsync(o => o.Key, cancellation);

            return events
                .Where(e => domainDataByTokenId.ContainsKey(e.TokenId)) // drop all offer events without an existing domain
                .Select(
                    e =>
                    {
                        var eventWithoutDomainName = e switch
                        {
                            OfferPlacedEvent placed when activeOffers.ContainsKey(placed.GetKey()) => new OfferUpdatedEvent(
                                Id: placed.Id,
                                Block: placed.Block,
                                SourceAddress: placed.SourceAddress,
                                OperationGroupHash: placed.OperationGroupHash,
                                TokenId: placed.TokenId,
                                PriceWithoutFee: placed.PriceWithoutFee,
                                Fee: placed.Fee,
                                ExpiresAtUtc: placed.ExpiresAtUtc
                            ),
                            BuyOfferPlacedEvent pe => pe with { DomainOwner = domainDataByTokenId[e.TokenId].Owner },
                            BuyOfferExecutedEvent { } ee => ee with
                            {
                                //in exceptionally rare case when the executeEvent is in same block as placed event => offer will not be in the activeOffers dictionary
                                //indexer should not break in this case => do not fill price and fee
                                PriceWithoutFee = TryGetActiveOfferOrWarn(ee)?.PriceWithoutFee ?? default,
                                Fee = activeOffers.GetValueOrDefault(ee.GetKey())?.Fee ?? default,
                            },
                            _ => e
                        };

                        return eventWithoutDomainName with { DomainName = domainDataByTokenId[e.TokenId].Name };

                        OfferPrice? TryGetActiveOfferOrWarn(BuyOfferExecutedEvent ee)
                        {
                            var offerOrNull = activeOffers.GetValueOrDefault(ee.GetKey());
                            if (offerOrNull == null)
                                _logger.LogError(
                                    "No active offer found for {BuyOfferExecutedEvent}. Unable to update {PriceWithoutFee} and {Fee} fields.",
                                    ee,
                                    nameof(BuyOfferExecutedEvent.PriceWithoutFee),
                                    nameof(BuyOfferExecutedEvent.Fee)
                                );

                            return offerOrNull;
                        }
                    }
                );
        }

        private record DomainData(string Name, string Owner);

        private record OfferPrice(OfferKey Key, decimal PriceWithoutFee, decimal Fee);
    }
}