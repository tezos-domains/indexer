﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public interface IEventProcessor
    {
        Task<IEnumerable<Event>> ProcessEventsAsync(
            IReadOnlyList<Event> events,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        );
    }
}