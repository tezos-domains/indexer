﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Events
{
    /// <summary>
    /// AuctionWithdrawEvent is only created when there are 3 consecutive operations in block (WithdrawEntrypoint,BidderBalanceUpdate,OutgoingTransaction)
    /// </summary>
    public class AuctionWithdrawEventProcessor : IEventProcessor
    {
        private readonly ILogger _logger;

        public AuctionWithdrawEventProcessor(ILogger<AuctionWithdrawEventProcessor> logger)
        {
            _logger = logger;
        }


        public Task<IEnumerable<Event>> ProcessEventsAsync(
            IReadOnlyList<Event> events,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var state = new WithdrawStateMachine(_logger);

            var removeEvents = new List<Event>();
            var processedEvents = new List<Event>();
            foreach (var update in args.Updates)
            {
                switch (update)
                {
                    case EventUpdate { Event: AuctionWithdrawEvent withdrawEntrypoint }:
                        removeEvents.Add(withdrawEntrypoint);
                        state.WaitForBigMapChange(withdrawEntrypoint);
                        break;
                    case BidderBalancesUpdate balancesUpdate:
                        state.WaitForOutgoingTransaction(balancesUpdate);
                        break;
                    case EventUpdate { Event: OutgoingMoneyTransactionEvent transfer }:
                        removeEvents.Add(transfer);
                        if (state.TryCreateValidWithdrawEvent(transfer, out var updatedWithdrawEvent))
                        {
                            processedEvents.Add(updatedWithdrawEvent.GuardNotNull());
                        }

                        break;
                    default:
                        state.Reset();
                        break;
                }
            }

            return Task.FromResult(
                events
                    .Except(removeEvents)
                    .Concat(processedEvents)
            );
        }


        /// <summary>
        /// State machine to track withdrawal updates
        /// </summary>
        private sealed class WithdrawStateMachine
        {
            private AuctionWithdrawEvent? _withdrawEvent;
            private BidderBalancesUpdate? _bigMapUpdate;
            private readonly ILogger _logger;

            public WithdrawStateMachine(ILogger logger)
            {
                _logger = logger;
                Reset();
            }

            public void Reset()
            {
                _withdrawEvent = null;
                _bigMapUpdate = null;
            }


            public void WaitForBigMapChange(AuctionWithdrawEvent withdrawEntrypoint)
            {
                if (_withdrawEvent != null || _bigMapUpdate != null)
                {
                    _logger.LogInformation(
                        "WithdrawEntrypoint event arrived after another {withdrawEvent} or {bigMapUpdate}. This event/update is being discarded.",
                        _withdrawEvent,
                        _bigMapUpdate
                    );
                    Reset();
                }

                _withdrawEvent = withdrawEntrypoint;
            }

            public void WaitForOutgoingTransaction(BidderBalancesUpdate bigMapUpdate)
            {
                if (_withdrawEvent != null
                    && _bigMapUpdate == null
                    && _withdrawEvent.SourceAddress == bigMapUpdate.Address
                    && _withdrawEvent.TldName == bigMapUpdate.TldName
                    && _withdrawEvent.OperationGroupHash == bigMapUpdate.OperationGroupHash
                )
                {
                    //Withdrawal event data match bidder_balance bigMap change
                    _bigMapUpdate = bigMapUpdate;
                    return;
                }

                _logger.LogInformation(
                    "BigMap bidder_balance update arrived and doesn't match to previous withdrawal entrypoint. Prior data ({storedWithdrawEvent}, {storedBigMapUpdate}) and current update ({bigMapUpdate}) are discarded.",
                    _withdrawEvent,
                    _bigMapUpdate,
                    bigMapUpdate
                );
                Reset();
            }

            public bool TryCreateValidWithdrawEvent(OutgoingMoneyTransactionEvent transfer, out AuctionWithdrawEvent? updatedWithdrawEvent)
            {
                bool isEventValid;
                if (_withdrawEvent != null
                    && _bigMapUpdate != null
                    && _withdrawEvent.OperationGroupHash == transfer.OperationGroupHash
                    && _withdrawEvent.DestinationAddress == transfer.DestinationAddress
                )
                {
                    //Valid withdrawal event is created by consecutive WithdrawalEntrypoint, bidder_balances bigMap change and outgoingTransactionEvent
                    //Data across these three updates must match
                    updatedWithdrawEvent = _withdrawEvent with { WithdrawnAmount = transfer.WithdrawnAmount };
                    isEventValid = true;
                }
                else
                {
                    _logger.LogInformation(
                        "Outgoing money transfer happened, but it doesn't match to previous withdrawal entrypoint and bigMap update. Prior data ({storedWithdrawEvent}, {storedBigMapUpdate}) and current update ({transfer}) are discarded.",
                        _withdrawEvent,
                        _bigMapUpdate,
                        transfer
                    );
                    updatedWithdrawEvent = default;
                    isEventValid = false;
                }


                Reset();
                return isEventValid;
            }
        }
    }
}