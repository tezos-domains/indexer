﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Indexer.Services.Update.Events
{
    public sealed class AuctionBidEventProcessor : UpdateEventProcessor<AuctionBidEvent>
    {
        private readonly MongoDbContext _dbContext;

        public AuctionBidEventProcessor(MongoDbContext dbContext)
            => _dbContext = dbContext;

        protected override async Task<IEnumerable<AuctionBidEvent>> UpdateEventsAsync(
            IReadOnlyList<AuctionBidEvent> events,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var domainGroups = events.GroupBy(
                b => b.DomainName,
                (_, bids) => bids.OrderBy(b => b.BidAmount)
            );
            var bidsWithoutPrevious = new List<AuctionBidEvent>();
            var processedBids = new List<AuctionBidEvent>();

            foreach (var group in domainGroups)
            {
                AuctionBidEvent? previousBid = null;
                foreach (var bid in group)
                {
                    if (previousBid == null)
                    {
                        // This part generates outbids to bids from previous blocks (bids that are stored in the DB)
                        bidsWithoutPrevious.Add(bid);
                    }
                    else
                    {
                        // This part generates outbids to other bids in this block
                        processedBids.Add(bid.WithPreviousBid(previousBid.SourceAddress, previousBid.BidAmount));
                    }

                    previousBid = bid;
                }
            }

            var filter = AuctionFilters.InProgress(
                    atUtc: block.Timestamp,
                    domainNames: bidsWithoutPrevious.Select(b => b.DomainName).Distinct()
                )
                .ExcludeHistory();

            var auctionPerDomain = await _dbContext.Get<Auction>()
                .Find(session, filter)
                .Project(a => new { a.DomainName, a.HighestBid.Bidder, a.HighestBid.Amount })
                .ToDictionaryAsync(a => a.DomainName, cancellation);

            return bidsWithoutPrevious
                .Select(
                    bid => auctionPerDomain.TryGetValue(bid.DomainName, out var auction)
                        ? bid.WithPreviousBid(bidderAddress: auction.Bidder, amount: auction.Amount)
                        : bid
                )
                .Concat(processedBids);
        }
    }
}