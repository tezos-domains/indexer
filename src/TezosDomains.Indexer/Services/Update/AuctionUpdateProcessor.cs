﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update
{
    public sealed class AuctionUpdateProcessor : HistorizationProcessor<Auction>
    {
        public AuctionUpdateProcessor(MongoDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation)
        {
            var bidUpdates = args.GetUpdates<AuctionBidUpdate>();
            var settleUpdates = args.GetUpdates<AuctionSettledUpdate>();

            if (bidUpdates.Count == 0 && settleUpdates.Count == 0)
                return;

            var filter = AuctionFilters.Active(
                    atUtc: args.Block.Timestamp,
                    inProgressDomainNames: bidUpdates.Select(u => u.DomainName).Distinct(),
                    canBeSettledDomainNames: settleUpdates.Select(u => u.DomainName).Distinct()
                )
                .ExcludeHistory();

            var auctionsByDomainName = await DbCollection.Find(session, filter).ToDictionaryAsync(a => a.DomainName, cancellation);
            var versionIdsToHistorize = auctionsByDomainName.Values.Select(a => a.VersionId).ToList();

            foreach (var bidUpdate in bidUpdates)
            {
                var activeAuction = auctionsByDomainName.GetValueOrDefault(bidUpdate.DomainName);
                var bid = new Bid(
                    Block: args.Block,
                    OperationGroupHash: bidUpdate.OperationGroupHash,
                    Bidder: bidUpdate.Bidder,
                    Amount: bidUpdate.Amount
                );
                var bids = (activeAuction?.Bids).NullToEmpty().Append(bid).ToList();
                auctionsByDomainName[bidUpdate.DomainName] = new Auction(
                    AuctionId: activeAuction?.AuctionId ?? Auction.GenerateId(bidUpdate.DomainName, args.Block.Level),
                    Block: args.Block,
                    OperationGroupHash: bidUpdate.OperationGroupHash,
                    DomainName: bidUpdate.DomainName,
                    Bids: bids,
                    EndsAtUtc: bidUpdate.EndsAtUtc,
                    OwnedUntilUtc: bidUpdate.OwnedUntilUtc,
                    IsSettled: false,
                    FirstBidAtUtc: activeAuction?.FirstBidAtUtc ?? args.Block.Timestamp,
                    BidCount: (activeAuction?.BidCount ?? 0) + 1,
                    HighestBid: bid,
                    CountOfUniqueBidders: bids.Select(b => b.Bidder).Distinct().Count(),
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );
            }

            foreach (var settleUpdate in settleUpdates)
            {
                var activeAuction = auctionsByDomainName[settleUpdate.DomainName];
                auctionsByDomainName[settleUpdate.DomainName] = activeAuction with
                {
                    Block = args.Block,
                    OperationGroupHash = settleUpdate.OperationGroupHash,
                    IsSettled = true,
                };
            }

            await WriteUpdatesToDbAsync(auctionsByDomainName.Values, versionIdsToHistorize, args.Block, session, cancellation);
        }
    }
}