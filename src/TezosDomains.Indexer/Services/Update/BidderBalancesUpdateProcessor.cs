﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update
{
    public sealed class BidderBalancesUpdateProcessor : HistorizationProcessor<BidderBalances>
    {
        private readonly ILogger _logger;

        public BidderBalancesUpdateProcessor(MongoDbContext dbContext, ILogger<BidderBalancesUpdateProcessor> logger) : base(dbContext)
            => _logger = logger;

        public override Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation)
        {
            var bidderUpdates = args.GetUpdates<BidderBalancesUpdate>();
            if (bidderUpdates.Count == 0)
                return Task.CompletedTask;

            var processedUpdates = EnsureSingleUpdatePerAddress(bidderUpdates, args.Block);
            return StoreUpdatesAsync(processedUpdates, args.Block, session, cancellation);
        }

        private async Task StoreUpdatesAsync(
            IReadOnlyCollection<BidderBalancesUpdate> updates,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var filter = FilterBuilder
                .In(b => b.Address, updates.Select(u => u.Address).Distinct())
                .ExcludeHistory();
            var biddersByAddress = await DbCollection.Find(session, filter).ToDictionaryAsync(b => b.Address, cancellation);
            var versionIdsToHistorize = biddersByAddress.Values.Select(b => b.VersionId).ToList();

            foreach (var update in updates)
            {
                var existingBidder = biddersByAddress.GetValueOrDefault(update.Address);
                var balances = new Dictionary<string, decimal>((existingBidder?.Balances).NullToEmpty());

                if (update.Balance.HasValue)
                    balances[update.TldName] = update.Balance.Value;
                else
                    balances.Remove(update.TldName);

                biddersByAddress[update.Address] = new BidderBalances(
                    Block: block,
                    OperationGroupHash: update.OperationGroupHash,
                    Address: update.Address,
                    Balances: balances,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );
            }

            await WriteUpdatesToDbAsync(biddersByAddress.Values, versionIdsToHistorize, block, session, cancellation);
        }

        private IReadOnlyList<BidderBalancesUpdate> EnsureSingleUpdatePerAddress(IReadOnlyList<BidderBalancesUpdate> updates, Block block)
        {
            var updatesPerAddress = updates.Select((u, i) => (Index: i, Update: u))
                .GroupBy(u => (u.Update.Address, u.Update.TldName), (_, changes) => changes.OrderByDescending(c => c.Index).First().Update)
                .ToList();

            if (updates.Count != updatesPerAddress.Count)
                _logger.LogWarning("The block has multiple 'bidder_balances' updates for same address.");

            return updatesPerAddress;
        }
    }
}