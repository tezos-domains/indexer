﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Indexer.Services.Update.Events;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update
{
    public sealed class EventUpdateProcessor : UpdateProcessorBase<Event>
    {
        private readonly IReadOnlyList<IEventProcessor> _eventProcessors;

        public EventUpdateProcessor(MongoDbContext dbContext, IEnumerable<IEventProcessor> eventProcessors) : base(dbContext)
            => _eventProcessors = eventProcessors.ToArray();

        public override async Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation)
        {
            var events = args.GetUpdates<EventUpdate>().ConvertAll(u => u.Event);

            foreach (var processor in _eventProcessors)
                events = (await processor.ProcessEventsAsync(events, args, session, cancellation)).ToList();

            if (events.Count > 0)
                await WriteToDbAsync(BuildInserts(events), session, cancellation);
        }

        protected override IEnumerable<WriteModel<Event>> GetRollbackWrites(int levelToRollback)
            => new[] { new DeleteManyModel<Event>(FilterBuilder.CreatedAtBlock(levelToRollback)) };
    }
}