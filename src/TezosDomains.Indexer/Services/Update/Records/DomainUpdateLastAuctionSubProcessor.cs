﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Records
{
    /// <summary>
    /// DomainUpdateLastAuctionSubProcessor is dependent on AuctionUpdateProcessor, which updates Auctions to current state.
    /// Current Auctions changed in this block are retrieved from DB and stored in their respective Domain object (if any - first auctions didn't have any domain object).
    /// </summary>
    public class DomainUpdateLastAuctionSubProcessor : IUpdateSubProcessor<Domain>
    {
        private readonly MongoDbContext _dbContext;
        private HashSet<string>? _auctionedDomains;

        public DomainUpdateLastAuctionSubProcessor(MongoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool HasUpdates(StoreUpdatesArguments args) => args.Updates.Any(u => u is AuctionBidUpdate or AuctionSettledUpdate);

        public FilterDefinition<Domain>? FilterExistingRecords(FilterDefinitionBuilder<Domain> builder, StoreUpdatesArguments args)
        {
            _auctionedDomains = args.Updates.Select(
                    u => u switch
                    {
                        AuctionBidUpdate ab => ab.DomainName,
                        AuctionSettledUpdate ab => ab.DomainName,
                        _ => null
                    }
                )
                .WhereNotNull()
                .ToHashSet();


            return _auctionedDomains.Any()
                ? builder.In(d => d.Name, _auctionedDomains)
                : null;
        }

        public async Task ApplyUpdates(
            Dictionary<string, Domain> recordsByKey,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var auctions = await _dbContext.Get<Auction>()
                .Find(session, Builders<Auction>.Filter.In(a => a.DomainName, _auctionedDomains.GuardNotNull()).ExcludeHistory())
                .ToListAsync(cancellation);

            foreach (var auction in auctions)
            {
                if (!recordsByKey.ContainsKey(auction.DomainName))
                    continue;

                recordsByKey[auction.DomainName] = recordsByKey[auction.DomainName] with
                {
                    Block = args.Block,
                    OperationGroupHash = auction.OperationGroupHash,
                    LastAuction = auction
                };
            }
        }
    }
}