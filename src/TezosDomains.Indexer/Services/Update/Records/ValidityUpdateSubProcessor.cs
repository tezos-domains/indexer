﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Utilities;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Services.Update.Records
{
    public class ValidityUpdateSubProcessor<TRecord> : IUpdateSubProcessor<TRecord> where TRecord : Record<TRecord>
    {
        public bool HasUpdates(StoreUpdatesArguments args) => args.GetUpdates<ValidityUpdate>().Any();

        public FilterDefinition<TRecord>? FilterExistingRecords(FilterDefinitionBuilder<TRecord> builder, StoreUpdatesArguments args)
        {
            var validityKeys = args.GetUpdates<ValidityUpdate>().Select(u => u.ValidityKey).Distinct().ToArray();
            return validityKeys.Any()
                ? builder.In(d => d.ValidityKey, validityKeys)
                : null;
        }

        public Task ApplyUpdates(
            Dictionary<string, TRecord> recordsByKey,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            foreach (var validityUpdate in args.GetUpdates<ValidityUpdate>())
            {
                var recordsToUpdate = recordsByKey.Values.Where(d => d.ValidityKey == validityUpdate.ValidityKey).ToList();
                foreach (var record in recordsToUpdate)
                    recordsByKey[record.GetKey()] = record with
                    {
                        Block = args.Block,
                        OperationGroupHash = validityUpdate.OperationGroupHash,
                        ExpiresAtUtc = validityUpdate.ExpiresAtUtc
                    };
            }

            return Task.CompletedTask;
        }
    }
}