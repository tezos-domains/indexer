﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Records
{
    public sealed class DomainUpdateProcessor : RecordUpdateProcessor<Domain, DomainUpdate>
    {
        public DomainUpdateProcessor(MongoDbContext dbContext, IEnumerable<IUpdateSubProcessor<Domain>> subProcessors) : base(dbContext, subProcessors)
        {
        }

        protected override string GetDocumentKey(DomainUpdate update) => update.Name;

        protected override async Task ApplyUpdates(
            Dictionary<string, Domain> domainsByName,
            IReadOnlyList<DomainUpdate> domainUpdates,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var validityKeyToExpiresAtUtc = await ResolveValidityKeyToExpiresAtUtcAsync(domainUpdates, domainsByName.Values, session, cancellation);

            var ownersByDomainName = await GetParentOwners(domainsByName, domainUpdates, session, cancellation);

            foreach (var update in domainUpdates)
            {
                var calculated = DomainNameUtils.Parse(update.Name);
                domainsByName[update.Name] = new Domain(
                    Block: block,
                    OperationGroupHash: update.OperationGroupHash,
                    Name: update.Name,
                    Address: update.Address,
                    Owner: update.Owner,
                    Data: update.Data,
                    TokenId: update.TokenId,
                    Label: calculated.Label,
                    Ancestors: calculated.Ancestors,
                    ParentName: calculated.ParentName,
                    ParentOwner: ResolveParentOwner(),
                    ExpiresAtUtc: ResolveExpiresAtUtc(),
                    ValidityKey: update.ValidityKey,
                    Level: calculated.Level,
                    NameForOrdering: calculated.NameForOrdering,
                    Operators: update.Operators,
                    LastAuction: domainsByName.GetValueOrDefault(update.Name)?.LastAuction,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );

                DateTime ResolveExpiresAtUtc()
                {
                    if (update.ValidityKey == null)
                        return DateTime.MaxValue;

                    if (update.ValidityKey == update.Name)
                        return validityKeyToExpiresAtUtc.GetValueOrDefault(update.ValidityKey);

                    return validityKeyToExpiresAtUtc.TryGetValue(update.ValidityKey, out var expiresAtUtc)
                        ? expiresAtUtc
                        : throw new Exception($"Domain '{update.Name}' has ValidityKey '{update.ValidityKey}' which does not correspond to any other domain.");
                }

                string? ResolveParentOwner() => calculated.ParentName != null
                    ? ownersByDomainName.GetValueOrDefault(calculated.ParentName)
                    : null;
            }
        }

        private async Task<Dictionary<string, string>> GetParentOwners(
            Dictionary<string, Domain> domainsByName,
            IReadOnlyList<DomainUpdate> domainUpdates,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var parentDomains = domainUpdates.Select(d => DomainNameUtils.Parse(d.Name).ParentName)
                .WhereNotNull()
                .Where(d => !domainsByName.ContainsKey(d))
                .ToHashSet();

            var filter = FilterBuilder
                .In(d => d.Name, parentDomains)
                .ExcludeHistory();
            var ownerDomains = await DbContext.Get<Domain>()
                .Find(session, filter)
                .Project(d => new { d.Name, d.Owner })
                .ToListAsync(cancellation);
            ownerDomains.AddRange(domainsByName.Select(d => new { Name = d.Key, d.Value.Owner }));
            //load owners by domain name from db
            var ownerDomainsByDomainName = ownerDomains.ToDictionary(d => d.Name, d => d.Owner);

            //update owners by domain updates
            foreach (var domainUpdate in domainUpdates)
            {
                ownerDomainsByDomainName[domainUpdate.Name] = domainUpdate.Owner;
            }

            return ownerDomainsByDomainName;
        }

        private async Task<IReadOnlyDictionary<string, DateTime>> ResolveValidityKeyToExpiresAtUtcAsync(
            IEnumerable<DomainUpdate> domainUpdates,
            IEnumerable<Domain> existingDomains,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var validityKeyToExpiresAtUtc = new Dictionary<string, DateTime>();

            foreach (var domain in existingDomains)
                if (domain.ValidityKey != null)
                    validityKeyToExpiresAtUtc[domain.ValidityKey] = domain.ExpiresAtUtc;

            var namesToLoad = domainUpdates
                .Select(u => u.ValidityKey)
                .WhereNotNull()
                .Where(k => !validityKeyToExpiresAtUtc.ContainsKey(k))
                .ToHashSet();

            if (namesToLoad.Count > 0)
            {
                var filter = FilterBuilder
                    .In(d => d.Name, namesToLoad)
                    .ExcludeHistory();
                var additionalDomains = await DbContext.Get<Domain>()
                    .Find(session, filter)
                    .Project(d => new { d.Name, d.ExpiresAtUtc })
                    .ToListAsync(cancellation);

                foreach (var domain in additionalDomains)
                    validityKeyToExpiresAtUtc.Add(domain.Name, domain.ExpiresAtUtc);
            }

            return validityKeyToExpiresAtUtc;
        }
    }
}