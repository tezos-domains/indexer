﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.Utilities;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Records
{
    public abstract class RecordUpdateProcessor<TRecord, TUpdate> : HistorizationProcessor<TRecord>
        where TRecord : Record<TRecord>
        where TUpdate : IBlockUpdate
    {
        private readonly IReadOnlyCollection<IUpdateSubProcessor<TRecord>> _subProcessors;

        protected RecordUpdateProcessor(MongoDbContext dbContext, IEnumerable<IUpdateSubProcessor<TRecord>> subProcessors) : base(dbContext)
        {
            _subProcessors = subProcessors.ToArray();
        }

        public override async Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation)
        {
            var recordUpdates = args.GetUpdates<TUpdate>();

            if (recordUpdates.Count == 0 && _subProcessors.All(s => !s.HasUpdates(args)))
                return;

            var filter = FilterExistingRecords(recordUpdates, _subProcessors.Select(p => p.FilterExistingRecords(FilterBuilder, args)).WhereNotNull());
            var recordsByKey = await DbCollection.Find(session, filter).ToDictionaryAsync(r => r.GetKey(), cancellation);
            var versionIdsToHistorize = recordsByKey.Values.Select(r => r.VersionId).ToList();

            await ApplyUpdates(recordsByKey, recordUpdates, args.Block, session, cancellation);
            foreach (var subProcessor in _subProcessors)
            {
                await subProcessor.ApplyUpdates(recordsByKey, args, session, cancellation);
            }

            await WriteUpdatesToDbAsync(recordsByKey.Values, versionIdsToHistorize, args.Block, session, cancellation);
        }

        private FilterDefinition<TRecord> FilterExistingRecords(
            IReadOnlyList<TUpdate> recordUpdates,
            IEnumerable<FilterDefinition<TRecord>> subProcessorFilters
        )
            => FilterBuilder
                .Or(subProcessorFilters.Prepend(FilterBuilder.ByKeys(recordUpdates.Select(GetDocumentKey).Distinct())).ToArray())
                .ExcludeHistory();

        protected abstract string GetDocumentKey(TUpdate update);

        protected abstract Task ApplyUpdates(
            Dictionary<string, TRecord> recordsByKey,
            IReadOnlyList<TUpdate> recordUpdates,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        );
    }
}