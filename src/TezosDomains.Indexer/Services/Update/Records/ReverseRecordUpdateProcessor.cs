﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Records
{
    internal sealed class ReverseRecordUpdateProcessor : RecordUpdateProcessor<ReverseRecord, ReverseRecordUpdate>
    {
        public ReverseRecordUpdateProcessor(MongoDbContext dbContext, IEnumerable<IUpdateSubProcessor<ReverseRecord>> subProcessors) : base(
            dbContext,
            subProcessors
        )
        {
        }

        protected override string GetDocumentKey(ReverseRecordUpdate update) => update.Address;

        protected override async Task ApplyUpdates(
            Dictionary<string, ReverseRecord> reverseRecordsByAddress,
            IReadOnlyList<ReverseRecordUpdate> reverseRecordUpdates,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var nameToValidity = await ResolveNameToValidityAsync(reverseRecordUpdates, reverseRecordsByAddress.Values, session, cancellation);

            foreach (var update in reverseRecordUpdates)
            {
                var validity = update.Name != null
                    ? nameToValidity.GetValueOrDefault(update.Name)
                      ?? throw new Exception($"Reverse record '{update.Address}' has Name '{update.Name}' which does not correspond to any domain.")
                    : null;

                reverseRecordsByAddress[update.Address] = new ReverseRecord(
                    Block: block,
                    OperationGroupHash: update.OperationGroupHash,
                    Address: update.Address,
                    Name: update.Name,
                    Owner: update.Owner,
                    ExpiresAtUtc: (validity?.ExpiresAtUtc).ConvertNullToMax(),
                    ValidityKey: validity?.ValidityKey,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );
            }
        }

        private async Task<IReadOnlyDictionary<string, RecordValidity>> ResolveNameToValidityAsync(
            IEnumerable<ReverseRecordUpdate> reverseRecordUpdates,
            IEnumerable<ReverseRecord> existingReverseRecords,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var nameToValidity = new Dictionary<string, RecordValidity>();

            foreach (var reverseRecord in existingReverseRecords)
                if (reverseRecord.Name != null)
                    nameToValidity[reverseRecord.Name] = new RecordValidity(reverseRecord.ValidityKey, reverseRecord.ExpiresAtUtc);

            var namesToLoad = reverseRecordUpdates
                .Select(u => u.Name)
                .WhereNotNull()
                .Where(n => !nameToValidity.ContainsKey(n))
                .ToHashSet();

            if (namesToLoad.Count > 0)
            {
                var filter = DomainFilters
                    .ExistingAtLatestBlock(namesToLoad)
                    .ExcludeHistory();
                var domains = await DbContext.Get<Domain>()
                    .Find(session, filter)
                    .Project(d => new { d.Name, d.ValidityKey, d.ExpiresAtUtc }) // We assume that domains are up-to-date.
                    .ToListAsync(cancellation);

                foreach (var domain in domains)
                    nameToValidity[domain.Name] = new RecordValidity(domain.ValidityKey, domain.ExpiresAtUtc);
            }

            return nameToValidity;
        }

        private sealed record RecordValidity(string? ValidityKey, DateTime? ExpiresAtUtc);
    }
}