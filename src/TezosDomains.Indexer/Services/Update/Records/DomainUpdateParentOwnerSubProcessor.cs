﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Records
{
    /// <summary>
    /// DomainUpdateParentOwnerSubProcessor is dependent on DomainUpdatePreProcessor, which generates DomainTransferEvents.
    /// DomainUpdatePreProcessor must be executed prior DomainUpdateProcessor which executes this DomainUpdateParentOwnerSubProcessor.
    /// </summary>
    public class DomainUpdateParentOwnerSubProcessor : IUpdateSubProcessor<Domain>
    {
        public bool HasUpdates(StoreUpdatesArguments args) => args.GetEventUpdates<DomainTransferEvent>().Any();

        public FilterDefinition<Domain>? FilterExistingRecords(FilterDefinitionBuilder<Domain> builder, StoreUpdatesArguments args)
        {
            var newOwnerDomains = args.GetEventUpdates<DomainTransferEvent>().Select(u => u.DomainName).Distinct().ToArray();
            return newOwnerDomains.Any()
                ? builder.In(d => d.ParentName, newOwnerDomains)
                : null;
        }

        public Task ApplyUpdates(
            Dictionary<string, Domain> recordsByKey,
            StoreUpdatesArguments args,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var transfersByDomainName = args.GetEventUpdates<DomainTransferEvent>()
                .GroupBy(e => e.DomainName, (_, group) => group.Last())
                .ToDictionary(e => e.DomainName);

            var parentWithSubdomains = recordsByKey.Values.Where(r => r.ParentName != null && transfersByDomainName.Keys.Contains(r.ParentName))
                .GroupBy(r => r.ParentName.GuardNotNull());
            foreach (var parentSubdomains in parentWithSubdomains)
            {
                foreach (var subdomain in parentSubdomains)
                {
                    recordsByKey[subdomain.Name] = subdomain with
                    {
                        Block = args.Block,
                        OperationGroupHash = transfersByDomainName[parentSubdomains.Key].OperationGroupHash,
                        ParentOwner = transfersByDomainName[parentSubdomains.Key].NewOwner
                    };
                }
            }

            return Task.CompletedTask;
        }
    }
}