﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Indexer.Services.Update.Generic;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services.Update.Records
{
    /// <summary>
    /// DomainUpdatePreProcessor is modifying StoreUpdatesArguments.Updates collection.
    /// It generates DomainTransferEvent, when domain owner changes. New EventUpdate is inserted at position right after DomainUpdate.
    /// It discards DomainUpdate/SetChildRecord when only owner changed.
    /// It replaces SetChildRecordEvent with a copy which has IsNewRecord flag set for newly created domain.
    /// </summary>
    public class DomainUpdatePreProcessor : IUpdatePreProcessor
    {
        private readonly MongoDbContext _dbContext;

        public DomainUpdatePreProcessor(MongoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<StoreUpdatesArguments> ProcessUpdatesAsync(StoreUpdatesArguments args, CancellationToken cancellation)
        {
            var domainUpdates = args.GetUpdates<DomainUpdate>();
            var filter = DomainFilters
                .ExistingAtLatestBlock(domainNames: domainUpdates.Select(e => e.Name).Distinct())
                .ExcludeHistory();
            var existingDomains = await _dbContext.Get<Domain>()
                .Find(filter)
                .Project(d => new DomainState(d.Name, d.Owner, d.Address, d.Data, d.Operators))
                .ToListAsync(cancellation);
            var currentStateByDomain = existingDomains.ToDictionary(d => d.Name);

            var updates = args.Updates.SelectMany(ProcessUpdate);

            return args with { Updates = updates.ToList() };

            IEnumerable<IBlockUpdate> ProcessUpdate(IBlockUpdate update, int index)
            {
                switch (update)
                {
                    case DomainUpdate d:
                        yield return d;

                        if (currentStateByDomain.TryGetValue(d.Name, out var current))
                        {
                            if (current.Owner != d.Owner)
                            {
                                //generate transfer event when domain's owner changes
                                yield return new EventUpdate(
                                    new DomainTransferEvent(
                                        Id: Event.GenerateId(),
                                        Block: args.Block,
                                        SourceAddress: currentStateByDomain[d.Name].Owner,
                                        OperationGroupHash: d.OperationGroupHash,
                                        DomainName: d.Name,
                                        NewOwner: d.Owner
                                    )
                                );
                            }
                            else if (!current.Operators.OrderBy(o => o).SequenceEqual(d.Operators.OrderBy(o => o)))
                            {
                                //generate domainUpdateOperators event when domain's operators change is the only change
                                yield return new EventUpdate(
                                    new DomainUpdateOperatorsEvent(
                                        Id: Event.GenerateId(),
                                        Block: args.Block,
                                        SourceAddress: currentStateByDomain[d.Name].Owner,
                                        OperationGroupHash: d.OperationGroupHash,
                                        DomainName: d.Name,
                                        Operators: d.Operators
                                    )
                                );
                            }
                        }

                        currentStateByDomain[d.Name] = new DomainState(d.Name, d.Owner, d.Address, d.Data, d.Operators);
                        break;
                    case EventUpdate { Event: DomainSetChildRecordEvent @event }:
                        if (TryAddToUpdates(
                                @event: @event with { IsNewRecord = !currentStateByDomain.ContainsKey(@event.DomainName) },
                                update: out var addSetChildRecordUpdate,
                                claimUpdate: args.Updates.ElementAtOrDefault(index - 1)
                            ))
                        {
                            yield return addSetChildRecordUpdate.GuardNotNull();
                        }

                        break;
                    case EventUpdate { Event: DomainUpdateEvent @event }:
                        if (TryAddToUpdates(@event, out var addDomainUpdateEvent, claimUpdate: args.Updates.ElementAtOrDefault(index - 2)))
                        {
                            yield return addDomainUpdateEvent.GuardNotNull();
                        }

                        break;

                    case EventUpdate { Event: DomainClaimEvent @event }:
                        // Enrich domainClaim event with forwardRecord from DomainUpdateEvent if present - Domain claim often contains claim succeeded with domainUpdate
                        if (args.Updates.ElementAtOrDefault(index + 2) is EventUpdate { Event: DomainUpdateEvent updateEvent }
                            && updateEvent.DomainName == @event.DomainName
                            && updateEvent.SourceAddress == @event.SourceAddress)
                        {
                            var updatedClaimEvent = @event with { DomainForwardRecordAddress = updateEvent.DomainForwardRecordAddress };
                            yield return new EventUpdate(updatedClaimEvent);
                            break;
                        }

                        yield return update;
                        break;
                    default:
                        yield return update;
                        break;
                }
            }


            bool TryAddToUpdates<TDomainUpdateEvent>(TDomainUpdateEvent @event, out IBlockUpdate? update, IBlockUpdate? claimUpdate = null)
                where TDomainUpdateEvent : Event, IHasDomainDetails
            {
                //Discard event
                // - if update only changes domain's owner
                // - if previous event is DomainClaim (SetChildRecordEvent is generated because, DNSRegistrar is calling SetChildRecord proxy contract and not directly NameRegistry)
                if ((currentStateByDomain.TryGetValue(@event.DomainName, out var current) && IsOnlyOwnerAndOperatorsUpdate(@event, current))
                    || (claimUpdate is EventUpdate { Event: DomainClaimEvent claimEvent } && claimEvent.DomainName == @event.DomainName))
                {
                    update = null;
                    return false;
                }

                //Otherwise add
                update = new EventUpdate(@event);
                return true;
            }
        }

        private static bool IsOnlyOwnerAndOperatorsUpdate(IHasDomainDetails updateEvent, DomainState currentState)
            => updateEvent.DomainForwardRecordAddress == currentState.Address
               && updateEvent.Data.Count == currentState.Data.Count
               && !updateEvent.Data.Except(currentState.Data).Any()
               && updateEvent.DomainOwnerAddress != currentState.Owner;

        record DomainState(string Name, string Owner, string? Address, IReadOnlyDictionary<string, string> Data, IReadOnlyList<string> Operators);
    }
}