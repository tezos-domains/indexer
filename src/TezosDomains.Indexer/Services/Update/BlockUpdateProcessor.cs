﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Indexer.Services.Update.Generic;

namespace TezosDomains.Indexer.Services.Update
{
    public sealed class BlockUpdateProcessor : UpdateProcessorBase<Block>
    {
        public BlockUpdateProcessor(MongoDbContext dbContext) : base(dbContext)
        {
        }

        public override Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation)
            => DbCollection.InsertOneAsync(session, args.Block, options: null, cancellation);

        protected override IEnumerable<WriteModel<Block>> GetRollbackWrites(int levelToRollback)
            => new[] { new DeleteOneModel<Block>(FilterBuilder.Eq(b => b.Level, levelToRollback)) };
    }
}