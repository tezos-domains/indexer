﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;

namespace TezosDomains.Indexer.Services.Update.Generic
{
    public abstract class HistorizationProcessor<TDocument> : UpdateProcessorBase<TDocument>
        where TDocument : IHasHistory
    {
        protected HistorizationProcessor(MongoDbContext dbContext) : base(dbContext)
        {
        }

        protected Task WriteUpdatesToDbAsync(
            IReadOnlyCollection<TDocument> newItems,
            IReadOnlyCollection<string> versionIdsToHistorize,
            Block block,
            IClientSessionHandle session,
            CancellationToken cancellation
        )
        {
            var models = newItems.Select<TDocument, WriteModel<TDocument>>(i => new InsertOneModel<TDocument>(i)).ToList();

            if (versionIdsToHistorize.Count > 0)
                models.Add(
                    new UpdateManyModel<TDocument>(
                        FilterBuilder.In(d => d.VersionId, versionIdsToHistorize),
                        Builders<TDocument>.Update
                            .Set(d => d.ValidUntilBlockLevel, block.Level)
                            .Set(d => d.ValidUntilTimestamp, block.Timestamp)
                    )
                );

            return models.Count > 0
                ? WriteToDbAsync(models, session, cancellation)
                : Task.CompletedTask;
        }

        protected override IEnumerable<WriteModel<TDocument>> GetRollbackWrites(int levelToRollback)
        {
            var deleteCurrent = new DeleteManyModel<TDocument>(FilterBuilder.CreatedAtBlock(levelToRollback));
            var updatePreviousToCurrent = new UpdateManyModel<TDocument>(
                FilterBuilder.Eq(d => d.ValidUntilBlockLevel, levelToRollback),
                Builders<TDocument>.Update
                    .Set(d => d.ValidUntilBlockLevel, int.MaxValue)
                    .Set(d => d.ValidUntilTimestamp, DateTime.MaxValue)
            );
            return new WriteModel<TDocument>[] { deleteCurrent, updatePreviousToCurrent };
        }
    }
}