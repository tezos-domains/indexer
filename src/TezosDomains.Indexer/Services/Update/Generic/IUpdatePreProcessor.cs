﻿using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Indexer.Services.Update.Generic
{
    public interface IUpdatePreProcessor
    {
        Task<StoreUpdatesArguments> ProcessUpdatesAsync(StoreUpdatesArguments args, CancellationToken cancellation);
    }
}