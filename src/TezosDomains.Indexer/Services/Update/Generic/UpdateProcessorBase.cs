﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;

namespace TezosDomains.Indexer.Services.Update.Generic
{
    public abstract class UpdateProcessorBase<TDocument> : IUpdateProcessor
        where TDocument : IHasDbCollection
    {
        protected static readonly FilterDefinitionBuilder<TDocument> FilterBuilder = Builders<TDocument>.Filter;
        protected readonly MongoDbContext DbContext;
        protected readonly IMongoCollection<TDocument> DbCollection;

        protected UpdateProcessorBase(MongoDbContext dbContext)
        {
            DbContext = dbContext;
            DbCollection = dbContext.Get<TDocument>();
        }

        public abstract Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation);

        public Task RollbackAsync(int levelToRollback, IClientSessionHandle session, CancellationToken cancellation)
            => WriteToDbAsync(GetRollbackWrites(levelToRollback), session, cancellation);

        protected abstract IEnumerable<WriteModel<TDocument>> GetRollbackWrites(int levelToRollback);

        protected Task WriteToDbAsync(IEnumerable<WriteModel<TDocument>> models, IClientSessionHandle session, CancellationToken cancellation)
            => DbCollection.BulkWriteAsync(session, models, new BulkWriteOptions { IsOrdered = false }, cancellation);

        protected IEnumerable<WriteModel<TDocument>> BuildInserts(IEnumerable<TDocument> documents)
            => documents.Select<TDocument, WriteModel<TDocument>>(i => new InsertOneModel<TDocument>(i));
    }
}