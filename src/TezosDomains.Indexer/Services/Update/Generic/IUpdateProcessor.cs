﻿using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Indexer.Services.Update.Generic
{
    public interface IUpdateProcessor
    {
        Task StoreUpdatesAsync(StoreUpdatesArguments args, IClientSessionHandle session, CancellationToken cancellation);
        Task RollbackAsync(int levelToRollback, IClientSessionHandle session, CancellationToken cancellation);
    }
}