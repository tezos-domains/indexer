﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;

namespace TezosDomains.Indexer.Services.Update.Generic
{
    public interface IUpdateSubProcessor<TRecord>
        where TRecord : Record<TRecord>
    {
        bool HasUpdates(StoreUpdatesArguments args);

        FilterDefinition<TRecord>? FilterExistingRecords(FilterDefinitionBuilder<TRecord> builder, StoreUpdatesArguments args);

        Task ApplyUpdates(
            Dictionary<string, TRecord> recordsByKey,
            StoreUpdatesArguments args,
            IClientSessionHandle clientSessionHandle,
            CancellationToken cancellation
        );
    }
}