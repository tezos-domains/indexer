﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using TezosDomains.Data.MongoDb.Initialization;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Services;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Metrics;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services
{
    public class IndexerService : BackgroundService
    {
        private static readonly TimeSpan CacheExpiration = TimeSpan.FromHours(1);

        private readonly ILogger _logger;
        private readonly IBlockQueryService _blockQueryService;
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly IUpdateService _updateService;
        private readonly IndexerDistributedLockService _indexerDistributedLockService;
        private readonly IMemoryCache _blockCache;
        private readonly IBlockProcessor _blockProcessor;
        private readonly IBlockReader _blockReader;
        private readonly MongoDbInitializer _dbInitializer;
        private readonly TezosContractConfiguration _contractConfiguration;

        public IndexerService(
            ILogger<IndexerService> logger,
            IBlockQueryService blockQueryService,
            IHostApplicationLifetime appLifetime,
            IUpdateService updateService,
            IndexerDistributedLockService indexerDistributedLockService,
            IMemoryCache blockCache,
            IBlockProcessor blockProcessor,
            IBlockReader blockReader,
            MongoDbInitializer dbInitializer,
            TezosContractConfiguration contractConfiguration
        )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _blockQueryService = blockQueryService ?? throw new ArgumentNullException(nameof(blockQueryService));
            _appLifetime = appLifetime ?? throw new ArgumentNullException(nameof(appLifetime));
            _updateService = updateService ?? throw new ArgumentNullException(nameof(updateService));
            _indexerDistributedLockService = indexerDistributedLockService ?? throw new ArgumentNullException(nameof(indexerDistributedLockService));
            _blockCache = blockCache ?? throw new ArgumentNullException(nameof(blockCache));
            _blockProcessor = blockProcessor;
            _blockReader = blockReader;
            _dbInitializer = dbInitializer;
            _contractConfiguration = contractConfiguration;
        }

        protected override async Task ExecuteAsync(CancellationToken parentCancellation)
        {
            _logger.LogInformation("Indexer Service is starting.");

            try
            {
                var cts = CancellationTokenSource.CreateLinkedTokenSource(parentCancellation);
                var cancellation = cts.Token;

                await _blockProcessor.InitAsync(cancellation);

                await _indexerDistributedLockService.AcquireAsync(cts);

                await _dbInitializer.InitializeAsync(_contractConfiguration.ChainId, cancellation);
                var settings = _dbInitializer.GetSettings();

                var lastIndexedBlock = await _blockQueryService.GetLatestOrNullAsync(cancellation);
                _blockReader.Reset(lastIndexedBlock?.Level, settings.TezosChainId);

                var channel = Channel.CreateBounded<IIndexingCommand>(100);

                var producerTask = RunTaskWithCancellationOrError(
                    cts,
                    async (ct) =>
                    {
                        var block = await _blockReader.NextBlockAsync(ct);
                        using (_logger.BeginScope("Processing {@block}.", new { block.Hash, block.Header.Level }))
                        {
                            var blockLevel = block.Header.Level.GuardNotNull();
                            var previousBlockLevel = blockLevel - 1;
                            var knownPredecessor = await _blockCache.GetOrCreateAsync(
                                previousBlockLevel,
                                async cacheEntry =>
                                {
                                    cacheEntry.SlidingExpiration = CacheExpiration;
                                    var previousBlock = await _blockQueryService.FindAsync(previousBlockLevel, ct);
                                    return previousBlock;
                                }
                            );

                            if (knownPredecessor == null || block.Header.Predecessor == knownPredecessor.Hash)
                            {
                                var updates = _blockProcessor.Process(block);
                                var blockData = BlockMapper.Map(block);
                                await channel.Writer.WriteAsync(
                                    new WriteBlockCommand(
                                        _updateService,
                                        _logger,
                                        new StoreUpdatesArguments(updates, blockData, knownPredecessor)
                                    ),
                                    ct
                                );
                                IndexerMetrics.CommandQueueSizeGauge.Inc();
                                _logger.LogInformation("The block added to processing queue.");
                                _blockCache.Set(blockLevel, blockData, CacheExpiration);
                            }
                            else
                            {
                                await channel.Writer.WriteAsync(new RollbackBlockCommand(_updateService, _logger, previousBlockLevel), ct);
                                IndexerMetrics.CommandQueueSizeGauge.Inc();
                                _logger.LogWarning(
                                    "Rollback of {blockHash} (level:{level}) added to processing queue. Caused by the block {block} being processed.",
                                    knownPredecessor,
                                    previousBlockLevel,
                                    block
                                );
                                _blockReader.Reset(blockLevel - 2, settings.TezosChainId);
                            }
                        }
                    },
                    "producer"
                );

                var consumerTask = RunTaskWithCancellationOrError(
                    cts,
                    async (ct) =>
                    {
                        var command = await channel.Reader.ReadAsync(ct);
                        IndexerMetrics.CommandQueueSizeGauge.Dec();

                        _indexerDistributedLockService.EnsureLockActive();
                        await command.ExecuteAsync(ct);
                    },
                    "consumer"
                );

                await Task.WhenAll(producerTask, consumerTask);
            }
            catch (Exception ex)
            {
                if (parentCancellation.IsCancellationRequested)
                    _logger.LogDebug("Indexer stopping. Cancellation requested from ASP.NET Core.");
                else
                    _logger.LogCritical(ex, "Indexing failed. Tezos Monitor Service is stopping.");
            }
            finally
            {
                _appLifetime.StopApplication();
                _logger.LogInformation("Indexer Service stopped.");
            }
        }

        private Task RunTaskWithCancellationOrError(CancellationTokenSource cts, Func<CancellationToken, Task> function, string taskName)
        {
            return Task.Run(
                async () =>
                {
                    try
                    {
                        while (!cts.Token.IsCancellationRequested)
                            await function(cts.Token);
                    }
                    catch (Exception e)
                    {
                        if (cts.IsCancellationRequested)
                        {
                            _logger.LogDebug($"Task {taskName} cancelled");
                        }
                        else
                        {
                            _logger.LogError(e, $"Task {taskName} failed. Cancelling cancellation");
                            cts.Cancel();
                        }

                        throw;
                    }
                }
            );
        }
    }
}