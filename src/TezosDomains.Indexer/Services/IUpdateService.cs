﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Services
{
    public interface IUpdateService
    {
        Task StoreUpdatesAsync(StoreUpdatesArguments args, CancellationToken cancellation);
        Task RollbackAsync(int levelToRollback, CancellationToken cancellation);
    }

    public sealed record StoreUpdatesArguments(
        IReadOnlyList<IBlockUpdate> Updates,
        Block Block,
        Block? PreviousBlock
    )
    {
        public IReadOnlyList<TUpdate> GetUpdates<TUpdate>()
            where TUpdate : IBlockUpdate
            => Updates.OfType<IBlockUpdate, TUpdate>().ToList();

        public IEnumerable<TEventUpdate> GetEventUpdates<TEventUpdate>()
            where TEventUpdate : Event
            => Updates.OfType<IBlockUpdate, EventUpdate>()
                .Select(u => u.Event as TEventUpdate)
                .WhereNotNull();
    }
}