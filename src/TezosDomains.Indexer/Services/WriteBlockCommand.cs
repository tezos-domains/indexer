﻿using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Indexer.Services
{
    public class WriteBlockCommand : IIndexingCommand
    {
        private readonly IUpdateService _updateService;
        private readonly ILogger _logger;
        private readonly StoreUpdatesArguments _args;

        public WriteBlockCommand(IUpdateService updateService, ILogger logger, StoreUpdatesArguments args)
        {
            _updateService = updateService;
            _logger = logger;
            _args = args;
        }

        public async Task ExecuteAsync(CancellationToken cancellation)
        {
            using (_logger.BeginScope("Storing {@block} in db.", _args.Block))
            {
                _logger.LogInformation("Storing STARTED.");
                await _updateService.StoreUpdatesAsync(_args, cancellation);
                _logger.LogInformation("Storing FINISHED.");
            }
        }
    }
}