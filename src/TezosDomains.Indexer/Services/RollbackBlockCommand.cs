﻿using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Indexer.Services
{
    public class RollbackBlockCommand : IIndexingCommand
    {
        private readonly IUpdateService _updateService;
        private readonly int _level;
        private readonly ILogger _logger;

        public RollbackBlockCommand(IUpdateService updateService, ILogger logger, int level)
        {
            _updateService = updateService;
            _logger = logger;
            _level = level;
        }

        public async Task ExecuteAsync(CancellationToken ct)
        {
            using (_logger.BeginScope("Rolling back block with {blockLevel} in db.", _level))
            {
                _logger.LogInformation("Rollback STARTED.");
                await _updateService.RollbackAsync(_level, ct);
                _logger.LogInformation("Rollback FINISHED.");
            }
        }
    }
}