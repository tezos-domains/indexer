﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Indexer.Services.Update.Generic;

namespace TezosDomains.Indexer.Services
{
    public class DefaultUpdateService : IUpdateService
    {
        private readonly MongoDbContext _dbContext;
        private readonly IUpdatePreProcessor _preProcessor;
        private readonly IReadOnlyList<IUpdateProcessor> _updateProcessors;

        public DefaultUpdateService(MongoDbContext dbContext, IEnumerable<IUpdateProcessor> updateProcessors, IUpdatePreProcessor preProcessor)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _preProcessor = preProcessor;
            _updateProcessors = updateProcessors.ToArray();
        }

        private async Task ExecuteInTransaction(Func<IClientSessionHandle, Task> actions, CancellationToken cancellation)
        {
            using var session = await _dbContext.Database.Client.StartSessionAsync(options: null, cancellation);
            session.StartTransaction(new TransactionOptions(ReadConcern.Local, ReadPreference.Primary, WriteConcern.WMajority));

            await actions(session);

            await session.CommitTransactionAsync(cancellation);
        }

        public async Task StoreUpdatesAsync(StoreUpdatesArguments args, CancellationToken cancellation)
        {
            await ValidateLatestStoredBlockAsync(args.Block.Level - 1, cancellation);

            args = await _preProcessor.ProcessUpdatesAsync(args, cancellation);

            await ExecuteInTransaction(
                async session =>
                {
                    foreach (var processor in _updateProcessors) // Order is significant e.g. domains must be updated before reverse records.
                        await processor.StoreUpdatesAsync(args, session, cancellation);
                },
                cancellation
            );
        }

        public async Task RollbackAsync(int levelToRollback, CancellationToken cancellation)
        {
            await ValidateLatestStoredBlockAsync(levelToRollback, cancellation);

            await ExecuteInTransaction(
                async session =>
                {
                    foreach (var processor in _updateProcessors) // Order is significant e.g. domains must be updated before reverse records.
                        await processor.RollbackAsync(levelToRollback, session, cancellation);
                },
                cancellation
            );
        }

        private async Task ValidateLatestStoredBlockAsync(int expectedBlockLevel, CancellationToken cancellation)
        {
            var actualBlockLevel = await _dbContext.Get<Block>()
                .Find(FilterDefinition<Block>.Empty)
                .Sort(Builders<Block>.Sort.Descending(b => b.Level))
                .Project(b => (int?) b.Level)
                .Limit(1)
                .FirstOrDefaultAsync(cancellation);

            if (actualBlockLevel.HasValue && actualBlockLevel != expectedBlockLevel)
                throw new InvalidOperationException(
                    $"ValidationError: Unable to store update. Expected block level {expectedBlockLevel} is not the latest stored block level {actualBlockLevel} in the database."
                );
        }
    }
}