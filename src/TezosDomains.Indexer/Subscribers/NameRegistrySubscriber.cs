﻿using System.Collections.Generic;
using TezosDomains.Common;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Subscribers
{
    public sealed class NameRegistrySubscriber : IContractSubscriber
    {
        IEnumerable<IBlockUpdate> IContractSubscriber.OnBigMapDiff(OnBigMapDiffArguments args)
        {
            IBlockUpdate? update = args.BigMapName switch
            {
                "records" => new DomainUpdate(
                    OperationGroupHash: args.OperationGroupHash,
                    Name: args.KeyProperty.GetValueAsString(),
                    Address: args.ValueProperty["address"].GetValueAsNullableAddress(),
                    Owner: args.ValueProperty["owner"].GetValueAsAddress(),
                    ValidityKey: args.ValueProperty["expiry_key"].GetValueAsNullableString(),
                    Data: args.ValueProperty["data"].GetValueAsMap(),
                    TokenId: args.ValueProperty["tzip12_token_id"].GetValueAsNullableInt(),
                    Operators: args.ValueProperty["internal_data"].GetAndUnpackValueFromMapAsAddressArray("operators")
                ),
                "reverse_records" => new ReverseRecordUpdate(
                    OperationGroupHash: args.OperationGroupHash,
                    Address: args.KeyProperty.GetValueAsAddress(),
                    Owner: args.ValueProperty["owner"].GetValueAsAddress(),
                    Name: args.ValueProperty["name"].GetValueAsNullableString()
                ),
                "expiry_map" => new ValidityUpdate(
                    OperationGroupHash: args.OperationGroupHash,
                    ValidityKey: args.KeyProperty.GetValueAsString(),
                    ExpiresAtUtc: (args.ValueProperty.GetValueOrDefault("__value__")?.GetValueAsDateTime()).ConvertNullToMax()
                ),
                _ => null,
            };

            if (update != null)
                yield return update;
        }
    }
}