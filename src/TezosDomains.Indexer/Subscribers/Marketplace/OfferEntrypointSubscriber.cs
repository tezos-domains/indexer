﻿using System.Collections.Generic;
using TezosDomains.Common;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates.Marketplace;

namespace TezosDomains.Indexer.Subscribers.Marketplace
{
    public class OfferEntrypointSubscriber<TConfiguration> : IContractSubscriber
        where TConfiguration : IOfferExtractionStrategy
    {
        private readonly IOfferExtractionStrategy _strategy;

        public OfferEntrypointSubscriber(TConfiguration strategy)
        {
            _strategy = strategy;
        }

        IEnumerable<IBlockUpdate> IContractSubscriber.OnEntrypoint(OnEntrypointArguments args)
        {
            IBlockUpdate? update = args switch
            {
                { EntrypointName: "place_offer" } => new SetOfferUpdate(
                    _strategy.Type,
                    OperationGroupHash: args.OperationGroupHash,
                    TokenContract: args.PropertiesByName[key: "token_contract"].GetValueAsAddress(),
                    TokenId: args.PropertiesByName[key: "token_id"].GetValueAsInt(),
                    InitiatorAddress: args.SourceAddress,
                    PriceWithoutFee: _strategy.GetOfferPlacePriceWithoutFee(args.Amount, args.PropertiesByName),
                    Fee: _strategy.GetOfferPlaceFee(args.Amount, args.PropertiesByName),
                    ExpiresAtUtc: args.PropertiesByName[key: "expiration"].GetValueAsNullableDateTime().ConvertNullToMax()
                ),
                { EntrypointName: "remove_offer" } => new RemoveOfferUpdate(
                    _strategy.Type,
                    OperationGroupHash: args.OperationGroupHash,
                    TokenContract: args.PropertiesByName[key: "token_contract"].GetValueAsAddress(),
                    TokenId: args.PropertiesByName[key: "token_id"].GetValueAsInt(),
                    InitiatorAddress: args.SourceAddress
                ),
                { EntrypointName: "execute_offer" } => new ExecuteOfferUpdate(
                    _strategy.Type,
                    OperationGroupHash: args.OperationGroupHash,
                    TokenContract: args.PropertiesByName[key: "token_contract"].GetValueAsAddress(),
                    TokenId: args.PropertiesByName[key: "token_id"].GetValueAsInt(),
                    InitiatorAddress: args.PropertiesByName[key: _strategy.Initiator].GetValueAsAddress(),
                    AcceptorAddress: args.SourceAddress,
                    PriceWithoutFee: _strategy.GetOfferExecutedPriceWithoutFee(args.Amount, args.PropertiesByName),
                    Fee: _strategy.GetOfferExecutedFee(args.Amount, args.PropertiesByName)
                ),
                _ => null
            };

            if (update != null)
                yield return update;
        }
    }
}