﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Subscribers.Marketplace;

public class SellOfferExtractionStrategy : IOfferExtractionStrategy
{
    private readonly MarketplaceConfiguration _configuration;

    public SellOfferExtractionStrategy(MarketplaceConfiguration configuration)
    {
        _configuration = configuration;
    }

    public OfferType Type => OfferType.SellOffer;
    public string Initiator => "seller";

    public decimal GetOfferPlaceFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => propertiesByName[key: "price"].GetValueAsMutezDecimal() * _configuration.MarketplaceFee;

    public decimal GetOfferPlacePriceWithoutFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => propertiesByName[key: "price"].GetValueAsMutezDecimal() * (1 - _configuration.MarketplaceFee);

    public decimal GetOfferExecutedFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => amount * _configuration.MarketplaceFee;

    public decimal GetOfferExecutedPriceWithoutFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => amount * (1 - _configuration.MarketplaceFee);
}