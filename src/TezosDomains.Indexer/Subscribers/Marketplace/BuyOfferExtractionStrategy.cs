﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Subscribers.Marketplace;

public class BuyOfferExtractionStrategy : IOfferExtractionStrategy
{
    public OfferType Type => OfferType.BuyOffer;
    public string Initiator => "buyer";

    public decimal GetOfferPlaceFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => amount - propertiesByName[key: "price"].GetValueAsMutezDecimal();

    public decimal GetOfferPlacePriceWithoutFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => propertiesByName[key: "price"].GetValueAsMutezDecimal();

    public decimal GetOfferExecutedFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => -1;

    public decimal GetOfferExecutedPriceWithoutFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName)
        => -1;
}