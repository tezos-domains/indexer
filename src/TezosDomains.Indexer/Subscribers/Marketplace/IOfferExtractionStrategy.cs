﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Subscribers.Marketplace;

public interface IOfferExtractionStrategy
{
    OfferType Type { get; }
    string Initiator { get; }
    decimal GetOfferPlaceFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName);
    decimal GetOfferPlacePriceWithoutFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName);
    decimal GetOfferExecutedFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName);
    decimal GetOfferExecutedPriceWithoutFee(decimal amount, IReadOnlyDictionary<string, IMichelsonValue> propertiesByName);
}