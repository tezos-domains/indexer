﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Subscribers.Events;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Subscribers
{
    /// <summary>
    /// AuctionWithdrawEvent is generated from outgoing transaction from TlDRegistrar contact
    /// It is the only outgoing transaction in TezosDomains contracts
    /// </summary>
    public sealed class AuctionBidderBalancesSubscriber : EventEntrypointSubscriber, IContractSubscriber
    {
        IEnumerable<IBlockUpdate> IContractSubscriber.OnBigMapDiff(OnBigMapDiffArguments args)
        {
            if (args.BigMapName == "bidder_balances")
                yield return new BidderBalancesUpdate(
                    OperationGroupHash: args.OperationGroupHash,
                    Address: args.KeyProperty.GetValueAsAddress(),
                    TldName: TldRegistrarUtils.ExtractTldName(args.StoragePropertiesByName),
                    Balance: args.ValueProperty.GetValueOrDefault("__value__")?.GetValueAsMutezDecimal()
                );
        }

        IEnumerable<IBlockUpdate> IContractSubscriber.OnOutgoingMoneyTransaction(OnOutgoingTransactionArguments args)
        {
            var withdrawEvent = new OutgoingMoneyTransactionEvent(
                Id: Event.GenerateId(),
                Block: BlockMapper.MapSlim(args.Block),
                SourceAddress: args.SourceAddress,
                DestinationAddress: args.DestinationAddress,
                OperationGroupHash: args.OperationGroupHash,
                WithdrawnAmount: args.Amount
            );
            yield return new EventUpdate(withdrawEvent);
        }

        protected override string EntrypointName => "withdraw";

        protected override Event MapEvent(MapEventArguments args) =>
            new AuctionWithdrawEvent(
                Id: args.Id,
                Block: args.Block,
                SourceAddress: args.SourceAddress,
                DestinationAddress: args.PropertiesByName["withdraw"].GetValueAsAddress(),
                OperationGroupHash: args.OperationGroupHash,
                TldName: TldRegistrarUtils.ExtractTldName(args.StoragePropertiesByName),
                WithdrawnAmount: 0
            );
    }
}