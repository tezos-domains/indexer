﻿using System.Collections.Generic;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Subscribers
{
    public sealed class AuctionSubscriber : IContractSubscriber
    {
        IEnumerable<IBlockUpdate> IContractSubscriber.OnBigMapDiff(OnBigMapDiffArguments args)
        {
            if (args.BigMapName != "auctions")
                yield break;

            var domainName = TldRegistrarUtils.CreateDomainName(args.KeyProperty.GetValueAsString(), args.StoragePropertiesByName);

            if (args.ValueProperty.Count == 0)
            {
                // auction removed from the bigmap means it has been settled
                yield return new AuctionSettledUpdate(
                    OperationGroupHash: args.OperationGroupHash,
                    DomainName: domainName
                );
            }
            else
            {
                //auction bid update
                var endsAtUtc = args.ValueProperty["ends_at"].GetValueAsDateTime();
                yield return new AuctionBidUpdate(
                    OperationGroupHash: args.OperationGroupHash,
                    DomainName: domainName,
                    Bidder: args.ValueProperty["last_bidder"].GetValueAsAddress(),
                    Amount: args.ValueProperty["last_bid"].GetValueAsMutezDecimal(),
                    EndsAtUtc: endsAtUtc,
                    OwnedUntilUtc: endsAtUtc.AddDays(args.ValueProperty["ownership_period"].GetValueAsInt())
                );
            }
        }
    }
}