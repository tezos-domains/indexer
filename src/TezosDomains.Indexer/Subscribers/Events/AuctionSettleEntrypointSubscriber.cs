﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class AuctionSettleEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "settle";

        protected override Event MapEvent(MapEventArguments args)
            => new AuctionSettleEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                DomainName: TldRegistrarUtils.ExtractDomainName(args.PropertiesByName, args.StoragePropertiesByName),
                DomainOwnerAddress: args.PropertiesByName["owner"].GetValueAsAddress(),
                DomainForwardRecordAddress: args.PropertiesByName["address"].GetValueAsNullableAddress(),
                Data: args.PropertiesByName["data"].GetValueAsMap(),
                RegistrationDurationInDays: 0,
                WinningBid: 0
            );
    }
}