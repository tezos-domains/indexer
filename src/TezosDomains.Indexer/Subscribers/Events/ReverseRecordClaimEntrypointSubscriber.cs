﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class ReverseRecordClaimEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "claim_reverse_record";

        protected override Event MapEvent(MapEventArguments args)
            => new ReverseRecordClaimEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                Name: args.PropertiesByName["name"].GetValueAsNullableString(),
                ReverseRecordOwnerAddress: args.PropertiesByName["owner"].GetValueAsAddress()
            );
    }
}