﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class DomainSetChildRecordEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "set_child_record";

        protected override Event MapEvent(MapEventArguments args)
            => new DomainSetChildRecordEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                DomainName: $"{args.PropertiesByName["label"].GetValueAsString()}.{args.PropertiesByName["parent"].GetValueAsString()}",
                DomainOwnerAddress: args.PropertiesByName["owner"].GetValueAsAddress(),
                DomainForwardRecordAddress: args.PropertiesByName["address"].GetValueAsNullableAddress(),
                Data: args.PropertiesByName["data"].GetValueAsMap(),
                IsNewRecord: true
            );
    }
}