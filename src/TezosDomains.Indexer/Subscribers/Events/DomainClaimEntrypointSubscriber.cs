﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class DomainClaimEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "claim";

        protected override Event MapEvent(MapEventArguments args)
            => new DomainClaimEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                DomainName: $"{args.PropertiesByName["label"].GetValueAsString()}.{args.PropertiesByName["tld"].GetValueAsString()}",
                DomainOwnerAddress: args.PropertiesByName["owner"].GetValueAsAddress(),
                DomainForwardRecordAddress: null
            );
    }
}