﻿using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Configuration;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class DomainRenewEntrypointSubscriber : EventEntrypointSubscriber
    {
        private readonly AffiliateConfiguration _affiliateConfiguration;

        public DomainRenewEntrypointSubscriber(AffiliateConfiguration affiliateConfiguration)
        {
            _affiliateConfiguration = affiliateConfiguration;
        }

        protected override string EntrypointName => "renew";

        protected override Event MapEvent(MapEventArguments args)
            => new DomainRenewEvent(
                args.Id,
                args.Block,
                _affiliateConfiguration.ReplaceSourceAddressWhenAffiliateContract(args.SourceAddress, args.RootOperationSource),
                args.OperationGroupHash,
                DomainName: TldRegistrarUtils.ExtractDomainName(args.PropertiesByName, args.StoragePropertiesByName),
                Price: args.Amount,
                DurationInDays: args.PropertiesByName["duration"].GetValueAsInt()
            );
    }
}