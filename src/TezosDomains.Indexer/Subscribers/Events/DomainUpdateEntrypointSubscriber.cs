﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class DomainUpdateEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "update_record";

        protected override Event MapEvent(MapEventArguments args)
            => new DomainUpdateEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                DomainName: args.PropertiesByName["name"].GetValueAsString(),
                DomainOwnerAddress: args.PropertiesByName["owner"].GetValueAsAddress(),
                DomainForwardRecordAddress: args.PropertiesByName["address"].GetValueAsNullableAddress(),
                Data: args.PropertiesByName["data"].GetValueAsMap()
            );
    }
}