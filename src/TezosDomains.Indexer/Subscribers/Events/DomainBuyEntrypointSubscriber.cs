﻿using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Configuration;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class DomainBuyEntrypointSubscriber : EventEntrypointSubscriber
    {
        private readonly AffiliateConfiguration _affiliateConfiguration;

        public DomainBuyEntrypointSubscriber(AffiliateConfiguration affiliateConfiguration)
        {
            _affiliateConfiguration = affiliateConfiguration;
        }

        protected override string EntrypointName => "buy";

        protected override Event MapEvent(MapEventArguments args)
            => new DomainBuyEvent(
                args.Id,
                args.Block,
                _affiliateConfiguration.ReplaceSourceAddressWhenAffiliateContract(args.SourceAddress, args.RootOperationSource),
                args.OperationGroupHash,
                DomainName: TldRegistrarUtils.ExtractDomainName(args.PropertiesByName, args.StoragePropertiesByName),
                Price: args.Amount,
                DurationInDays: args.PropertiesByName["duration"].GetValueAsInt(),
                DomainOwnerAddress: args.PropertiesByName["owner"].GetValueAsAddress(),
                DomainForwardRecordAddress: args.PropertiesByName["address"].GetValueAsNullableAddress(),
                Data: args.PropertiesByName["data"].GetValueAsMap()
            );
    }
}