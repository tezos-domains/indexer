﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Models.ParsedData;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public abstract class EventEntrypointSubscriber : IContractSubscriber
    {
        protected abstract string EntrypointName { get; }

        IEnumerable<IBlockUpdate> IContractSubscriber.OnEntrypoint(OnEntrypointArguments args)
        {
            if (args.EntrypointName != EntrypointName)
                yield break;

            var mapEventArgs = new MapEventArguments(
                Id: Event.GenerateId(),
                Block: BlockMapper.MapSlim(args.Block),
                args.SourceAddress,
                args.OperationGroupHash,
                args.Amount,
                args.StoragePropertiesByName,
                args.PropertiesByName,
                args.RootOperationSource
            );
            var @event = MapEvent(mapEventArgs);
            yield return new EventUpdate(@event);
        }

        protected abstract Event MapEvent(MapEventArguments args);
    }

    public sealed record MapEventArguments(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        decimal Amount,
        IReadOnlyDictionary<string, IMichelsonValue> StoragePropertiesByName,
        IReadOnlyDictionary<string, IMichelsonValue> PropertiesByName,
        string? RootOperationSource = null
    );
}