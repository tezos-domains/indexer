﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class DomainCommitEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "commit";

        protected override Event MapEvent(MapEventArguments args)
            => new DomainCommitEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                CommitmentHash: args.PropertiesByName["commit"].GetValueAsHexBytesString()
            );
    }
}