﻿using System;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;

namespace TezosDomains.Indexer.Subscribers.Events.Marketplace
{
    public class OfferExecutedEntrypointSubscriber<TOfferExtractionStrategy> : EventEntrypointSubscriber
        where TOfferExtractionStrategy : IOfferExtractionStrategy
    {
        private readonly TOfferExtractionStrategy _strategy;

        public OfferExecutedEntrypointSubscriber(TOfferExtractionStrategy strategy)
        {
            _strategy = strategy;
        }

        protected override string EntrypointName => "execute_offer";

        protected override Event MapEvent(MapEventArguments args)
            => _strategy.Type switch
            {
                OfferType.SellOffer => new OfferExecutedEvent(
                    args.Id,
                    args.Block,
                    args.SourceAddress,
                    args.OperationGroupHash,
                    TokenId: args.PropertiesByName["token_id"].GetValueAsInt(),
                    InitiatorAddress: args.PropertiesByName[_strategy.Initiator].GetValueAsAddress(),
                    PriceWithoutFee: _strategy.GetOfferExecutedPriceWithoutFee(args.Amount, args.PropertiesByName),
                    Fee: _strategy.GetOfferExecutedFee(args.Amount, args.PropertiesByName)
                ),
                OfferType.BuyOffer => new BuyOfferExecutedEvent(
                    args.Id,
                    args.Block,
                    args.SourceAddress,
                    args.OperationGroupHash,
                    TokenId: args.PropertiesByName["token_id"].GetValueAsInt(),
                    InitiatorAddress: args.PropertiesByName[_strategy.Initiator].GetValueAsAddress(),
                    PriceWithoutFee: _strategy.GetOfferExecutedPriceWithoutFee(args.Amount, args.PropertiesByName),
                    Fee: _strategy.GetOfferExecutedFee(args.Amount, args.PropertiesByName)
                ),
                _ => throw new ArgumentOutOfRangeException()
            };
    }
}