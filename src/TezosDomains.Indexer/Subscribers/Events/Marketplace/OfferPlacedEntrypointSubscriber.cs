﻿using System;
using TezosDomains.Common;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;

namespace TezosDomains.Indexer.Subscribers.Events.Marketplace
{
    public class OfferPlacedEntrypointSubscriber<TConfiguration> : EventEntrypointSubscriber
        where TConfiguration : IOfferExtractionStrategy
    {
        private readonly IOfferExtractionStrategy _strategy;

        public OfferPlacedEntrypointSubscriber(TConfiguration strategy)
        {
            _strategy = strategy;
        }

        protected override string EntrypointName => "place_offer";

        protected override Event MapEvent(MapEventArguments args)
            => _strategy.Type switch
            {
                OfferType.SellOffer => new OfferPlacedEvent(
                    args.Id,
                    args.Block,
                    args.SourceAddress,
                    args.OperationGroupHash,
                    TokenId: args.PropertiesByName["token_id"].GetValueAsInt(),
                    PriceWithoutFee: _strategy.GetOfferPlacePriceWithoutFee(args.Amount, args.PropertiesByName),
                    Fee: _strategy.GetOfferPlaceFee(args.Amount, args.PropertiesByName),
                    ExpiresAtUtc: args.PropertiesByName["expiration"].GetValueAsNullableDateTime().ConvertNullToMax()
                ),
                OfferType.BuyOffer => new BuyOfferPlacedEvent(
                    args.Id,
                    args.Block,
                    args.SourceAddress,
                    args.OperationGroupHash,
                    TokenId: args.PropertiesByName["token_id"].GetValueAsInt(),
                    PriceWithoutFee: _strategy.GetOfferPlacePriceWithoutFee(args.Amount, args.PropertiesByName),
                    Fee: _strategy.GetOfferPlaceFee(args.Amount, args.PropertiesByName),
                    ExpiresAtUtc: args.PropertiesByName["expiration"].GetValueAsNullableDateTime().ConvertNullToMax()
                ),
                _ => throw new ArgumentOutOfRangeException()
            };
    }
}