﻿using System;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;

namespace TezosDomains.Indexer.Subscribers.Events.Marketplace
{
    public class OfferRemovedEntrypointSubscriber<TConfiguration> : EventEntrypointSubscriber
        where TConfiguration : IOfferExtractionStrategy

    {
        private readonly IOfferExtractionStrategy _strategy;

        public OfferRemovedEntrypointSubscriber(TConfiguration strategy)
        {
            _strategy = strategy;
        }

        protected override string EntrypointName => "remove_offer";

        protected override Event MapEvent(MapEventArguments args)
            => _strategy.Type switch
            {
                OfferType.SellOffer => new OfferRemovedEvent(
                    args.Id,
                    args.Block,
                    args.SourceAddress,
                    args.OperationGroupHash,
                    TokenId: args.PropertiesByName["token_id"].GetValueAsInt()
                ),
                OfferType.BuyOffer => new BuyOfferRemovedEvent(
                    args.Id,
                    args.Block,
                    args.SourceAddress,
                    args.OperationGroupHash,
                    TokenId: args.PropertiesByName["token_id"].GetValueAsInt()
                ),
                _ => throw new ArgumentOutOfRangeException()
            };
    }
}