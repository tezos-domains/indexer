﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class AuctionBidEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "bid";

        protected override Event MapEvent(MapEventArguments args)
            => new AuctionBidEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                DomainName: TldRegistrarUtils.ExtractDomainName(args.PropertiesByName, args.StoragePropertiesByName),
                BidAmount: args.PropertiesByName["bid"].GetValueAsMutezDecimal(),
                TransactionAmount: args.Amount,
                PreviousBidderAddress: null,
                PreviousBidAmount: null
            );
    }
}