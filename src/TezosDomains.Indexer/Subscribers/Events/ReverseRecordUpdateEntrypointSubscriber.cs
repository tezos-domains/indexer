﻿using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.Subscribers.Events
{
    public sealed class ReverseRecordUpdateEntrypointSubscriber : EventEntrypointSubscriber
    {
        protected override string EntrypointName => "update_reverse_record";

        protected override Event MapEvent(MapEventArguments args)
            => new ReverseRecordUpdateEvent(
                args.Id,
                args.Block,
                args.SourceAddress,
                args.OperationGroupHash,
                Name: args.PropertiesByName["name"].GetValueAsNullableString(),
                ReverseRecordOwnerAddress: args.PropertiesByName["owner"].GetValueAsAddress(),
                ReverseRecordAddress: args.PropertiesByName["address"].GetValueAsAddress()
            );
    }
}