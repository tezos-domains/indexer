using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading;
using TezosDomains.Common;
using TezosDomains.Common.Configuration;
using TezosDomains.Common.Logging;
using TezosDomains.Utils;

namespace TezosDomains.Indexer
{
    public class Program
    {
        public static TimeSpan AfterShutdownDelay { get; set; }

        public static void Main(string[] args)
        {
            LoggerInit.CreateLogger("TezosDomains.Indexer");
            var logger = Log.ForContext<Program>();

            try
            {
                logger.Information("Starting up. Version: {version}", AppVersionInfo.Version);
                CreateHostBuilder(logger, args).Build().Run();
                logger.Information("Shutting down");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                if (AfterShutdownDelay > TimeSpan.Zero)
                    logger.Information("Waiting additional {delay} after shutdown (before restart by Docker).", AfterShutdownDelay);

                Log.CloseAndFlush();

                if (AfterShutdownDelay > TimeSpan.Zero)
                    Thread.Sleep(AfterShutdownDelay);
            }
        }

        public static IHostBuilder CreateHostBuilder(ILogger logger, params string[] args)
            => Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(
                    (hostingContext, config) =>
                    {
                        var network = HostEnvironmentConfiguration.GetHostTezosNetwork(hostingContext.Configuration);
                        logger.Information("Using {network} with respective appsettings.", network);

                        if (network != null)
                        {
                            var defaultAppSettingsIndex = config.Sources.RequiredIndexOf(s => s is JsonConfigurationSource);
                            var networkAppSettings = new JsonConfigurationSource { Path = $"appsettings.{network}.json", Optional = true };
                            config.Sources.Insert(defaultAppSettingsIndex + 1, networkAppSettings);
                        }
                    }
                )
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}