﻿using Prometheus;
using TezosDomains.Common.Metrics;

namespace TezosDomains.Indexer.Metrics
{
    public static class IndexerMetrics
    {
        public static readonly Gauge CommandQueueSizeGauge = Prometheus.Metrics.CreateGauge(
            "td_indexer_command_queue_size",
            "Number of commands (update, rollback) to be executed by UpdateService."
        );

        public static readonly Histogram StoredUpdatesHistogram = MetricsCollectors.CreateRequestDurationHistogram(
            "td_mongodb_updateservice_storeupdates_duration_seconds",
            "Histogram of storing block updates processing durations."
        );

        public static readonly Counter BlockProcessedCounter = MetricsCollectors.CreateRequestCounter(
            "td_mongodb_updateservice_block_processed_counter",
            "Last processed block level."
        );

        public static readonly Gauge LastBlockLevel = Prometheus.Metrics.CreateGauge(
            "td_mongodb_updateservice_blocklevel",
            "Last processed block level."
        );
    }
}