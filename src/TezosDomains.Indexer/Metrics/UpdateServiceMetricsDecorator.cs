﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Common.Metrics;
using TezosDomains.Indexer.Services;

namespace TezosDomains.Indexer.Metrics
{
    public class UpdateServiceMetricsDecorator : MetricsDecorator<IUpdateService>, IUpdateService
    {
        public UpdateServiceMetricsDecorator(IUpdateService decorated) : base(
            decorated,
            IndexerMetrics.BlockProcessedCounter,
            IndexerMetrics.StoredUpdatesHistogram
        )
        {
        }

        public async Task StoreUpdatesAsync(StoreUpdatesArguments args, CancellationToken cancellation)
        {
            await UpdateCounters(s => s.StoreUpdatesAsync(args, cancellation));

            IndexerMetrics.LastBlockLevel.Set(args.Block.Level);
        }

        public async Task RollbackAsync(int levelToRollback, CancellationToken cancellation)
        {
            await UpdateCounters(s => s.RollbackAsync(levelToRollback, cancellation));

            IndexerMetrics.LastBlockLevel.Set(levelToRollback - 1);
        }
    }
}