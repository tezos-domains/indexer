using AspNetCoreRateLimit;
using GraphQL;
using GraphQL.DI;
using GraphQL.MicrosoftDI;
using GraphQL.Server;
using GraphQL.Server.Ui.Playground;
using GraphQL.SystemTextJson;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using Prometheus;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.Configuration;
using TezosDomains.Api.GraphQL;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Infrastructure;
using TezosDomains.Api.GraphQL.Ordering;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Api.GraphQL.Scalars;
using TezosDomains.Api.Health;
using TezosDomains.Common;
using TezosDomains.Common.Configuration;
using TezosDomains.Common.Health;
using TezosDomains.Common.Metrics;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _env;
        private const string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMongoDb();
            services.AddHttpContextAccessor();
            services.AddTezosDomainsCommon();

            AddGraphQLTypes(services);
            AddIpRateLimit(services);
            AddCors(services);

            services.AddGraphQL(ConfigureGraphQL);
            services.AddConfigurationDiagnostics((ExecutionOptions o) => new { o.ComplexityConfiguration, o.MaxParallelExecutionCount, o.EnableMetrics });

            services.AddTezosDomainsConfigurationWithDiagnostics<IndexerConfiguration>("Indexer");
            services.AddTezosDomainsConfigurationWithDiagnostics<MarketplaceConfiguration>("Marketplace");
            services.AddTransient<IndexerHealthCheck>();
            services.AddHealthChecks()
                .AddCheck<IndexerHealthCheck>("Indexer")
                .ForwardToPrometheus()
                ;
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, MetricsConfiguration metricsConfiguration)
        {
            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var defaultPath = "/playground";
            var graphQLOptions = new PlaygroundOptions
            {
                // See https://github.com/prisma-labs/graphql-playground#settings
                PlaygroundSettings = new Dictionary<string, object> { { "schema.polling.enable", false } }
            };
            app.UseHttpMetrics();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseIpRateLimiting();
            app.UseSecurityHeaders(_configuration, contentSecurityPolicyExcludedPaths: new[] { defaultPath });
            app.UseGraphQLPlayground(graphQLOptions, defaultPath);
            app.UseGraphQLWithMetrics();
            app.UseHealth();
            app.UseConfigurationDiagnostics();
            //add default redirect to playground
            app.UseRewriter(
                new RewriteOptions()
                    .AddRedirect("^$", defaultPath)
            );

            if (metricsConfiguration.IsEnabled)
            {
                app.UseMetricServer(url: metricsConfiguration.Endpoint);
            }
        }

        private void AddCors(IServiceCollection services)
        {
            var corsConfiguration = _configuration.GetValid<CorsConfiguration>("TezosDomains:Cors");
            services.AddConfigurationWithDiagnostics(corsConfiguration);
            services.AddCors(
                options =>
                {
                    options.AddPolicy(
                        name: MyAllowSpecificOrigins,
                        builder =>
                        {
                            if (corsConfiguration.AllowedOrigins.Contains("*"))
                                builder.AllowAnyOrigin();
                            else
                                builder.WithOrigins(corsConfiguration.AllowedOrigins.ToArray());

                            builder.WithHeaders(
                                HeaderNames.ContentType,
                                "X-CSRF-Token",
                                "X-Requested-With",
                                HeaderNames.Accept,
                                HeaderNames.AcceptEncoding,
                                HeaderNames.AcceptLanguage,
                                HeaderNames.ContentLength,
                                HeaderNames.Origin,
                                HeaderNames.Referer,
                                HeaderNames.UserAgent,
                                "apollographql-client-name",
                                "apollographql-client-version"
                            );
                            builder.WithMethods("POST", "OPTIONS");
                        }
                    );
                }
            );
        }

        private static void AddGraphQLTypes(IServiceCollection services)
        {
            services.AddScoped<CurrentTimeService>();
            services.AddScoped<IFilterContextResolver, FilterContextResolver>();

            services.AddSingleton<ISchema, TezosDomainsSchema>();
            services.AddSingleton(typeof(AutoRegisteringInterfaceGraphType<>));
            services.AddSingleton(typeof(FixedAutoRegisteringObjectGraphType<>));
            services.AddSingleton(typeof(NoUnderscoreEnumerationGraphType<>));

            // Scalars:
            services.AddSingleton<AddressScalarType>();
            services.AddSingleton<MutezScalarType>();
            services.AddSingleton<JsonScalarType>();

            // DocumentOrderField:
            services.AddSingleton(new DocumentOrderFieldGraphType<Domain>(DomainOrder.Fields));
            services.AddSingleton(new DocumentOrderFieldGraphType<ReverseRecord>(ReverseRecordOrder.Fields));
            services.AddSingleton(new DocumentOrderFieldGraphType<Auction>(AuctionOrder.Fields));
            services.AddSingleton(new DocumentOrderFieldGraphType<Event>(EventOrder.Fields));
            services.AddSingleton(new DocumentOrderFieldGraphType<Offer>(OfferOrder.Fields));

            // Generic paging
            services.AddSingleton(typeof(IRelayConnectionService<>), typeof(RelayConnectionService<>));
            services.AddSingleton<IPageSizeCalculator, PageSizeCalculator>();
            services.AddConfigurationWithDiagnostics<GraphQLPageSizeConfiguration>("GraphQLPageSize");
        }

        private void ConfigureGraphQL(IGraphQLBuilder builder)
        {
            builder
                .AddSystemTextJson()
                .AddHttpMiddleware<ISchema, GraphQLHttpWithMetricsMiddleware>()
                .ConfigureExecutionOptions(
                    options =>
                    {
                        _configuration.Bind("GraphQL", options);

                        options.Schema.GuardNotNull().Comparer = new ImplicitAlphabeticalSchemaComparer();
                        options.UnhandledExceptionDelegate = unhandledContext =>
                        {
                            var services = unhandledContext.Context?.RequestServices ?? options.RequestServices;
                            if (services != null)
                            {
                                var errorHandler = ActivatorUtilities.CreateInstance<TezosErrorHandler>(services);
                                errorHandler.Handle(unhandledContext);
                            }

                            return Task.CompletedTask;
                        };
                    }
                );
        }

        private void AddIpRateLimit(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.Configure<IpRateLimitOptions>(_configuration.GetSection("IpRateLimiting"));
            services.Configure<IpRateLimitPolicies>(_configuration.GetSection("IpRateLimitPolicies"));
            services.AddInMemoryRateLimiting();
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IProcessingStrategy, AsyncKeyLockProcessingStrategy>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            services.AddConfigurationDiagnostics((IpRateLimitOptions o) => o);
            services.AddConfigurationDiagnostics((IpRateLimitPolicies o) => o);
        }
    }
}