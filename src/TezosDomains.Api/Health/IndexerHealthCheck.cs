﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.Configuration;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.Health
{
    public sealed class IndexerHealthCheck : IHealthCheck
    {
        private readonly IndexerConfiguration _indexerConfiguration;
        private readonly IBlockQueryService _blockQueryService;

        public IndexerHealthCheck(IndexerConfiguration indexerConfiguration, IBlockQueryService blockQueryService)
        {
            _indexerConfiguration = indexerConfiguration;
            _blockQueryService = blockQueryService;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellation = default)
        {
            try
            {
                var block = await _blockQueryService.GetLatestOrNullAsync(cancellation);
                return block != null && (DateTime.UtcNow - block.Timestamp) < _indexerConfiguration.LatestBlockDelay
                    ? HealthCheckResult.Healthy($"Indexer (block ingestion) is working fine. Latest indexed: {block}.")
                    : HealthCheckResult.Degraded($"Indexer (block ingestion) is behind. Latest indexed: {block?.ToString() ?? "no block indexed yet"}.");
            }
            catch (Exception e)
            {
                return HealthCheckResult.Unhealthy("Error in querying latest indexed block.", e);
            }
        }
    }
}