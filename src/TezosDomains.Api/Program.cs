using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using TezosDomains.Common;
using TezosDomains.Common.Logging;

namespace TezosDomains.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LoggerInit.CreateLogger("TezosDomains.Api");
            var logger = Log.ForContext<Program>();

            try
            {
                logger.Information("Starting up. Version: {version}", AppVersionInfo.Version);
                CreateHostBuilder(args).Build().Run();
                logger.Information("Shutting down");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(params string[] args)
            => Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}