﻿using Prometheus;

namespace TezosDomains.Api.Metrics
{
    public static class GraphQLMetrics
    {
        public static readonly Counter RequestsCounter = Prometheus.Metrics.CreateCounter(
            name: "td_api_graphql_requests_counter",
            help: "Request counter of graphql queries.",
            labelNames: new[] { "operations" }
        );

        public static readonly Histogram RequestsDuration = Prometheus.Metrics.CreateHistogram(
            name: "td_api_graphql_requests_duration_seconds",
            help: "A graphql requests duration.",
            labelNames: new[] { "operations" }
        );
    }
}