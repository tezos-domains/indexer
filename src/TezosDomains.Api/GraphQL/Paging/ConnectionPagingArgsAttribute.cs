﻿using GraphQL;
using GraphQL.Types;
using System;

namespace TezosDomains.Api.GraphQL.Paging
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class ConnectionPagingArgsAttribute : GraphQLAttribute
    {
        public override void Modify(FieldType fieldType, bool isInputType)
        {
            fieldType.Arguments ??= new QueryArguments();

            foreach (var arg in ConnectionPagingArgs.Definitions)
                fieldType.Arguments.Add(
                    new QueryArgument(arg.GraphType)
                    {
                        Name = arg.Name,
                        Description = arg.Description,
                    }
                );
        }
    }
}