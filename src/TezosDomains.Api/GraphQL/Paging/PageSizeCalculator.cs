﻿namespace TezosDomains.Api.GraphQL.Paging
{
    public interface IPageSizeCalculator
    {
        int Calculate(int? value, string argName);
    }

    public sealed class PageSizeCalculator : IPageSizeCalculator
    {
        private readonly GraphQLPageSizeConfiguration _configuration;

        public PageSizeCalculator(GraphQLPageSizeConfiguration configuration)
            => _configuration = configuration;

        public int Calculate(int? value, string argName)
        {
            if (value == null)
                return _configuration.Default;
            if (value <= 0)
                throw new GraphQLInputException(argName, value, mustBe: "greater than 0");
            if (value > _configuration.Max)
                throw new GraphQLInputException(argName, value, mustBe: $"less than or equal to max {_configuration.Max}");

            return value.Value;
        }
    }
}