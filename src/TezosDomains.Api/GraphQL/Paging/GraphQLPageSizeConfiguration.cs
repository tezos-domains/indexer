﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Api.GraphQL.Paging
{
    public sealed class GraphQLPageSizeConfiguration : IValidatableObject, IConfigurationInstance
    {
        public int Default { get; init; } = 10;
        public int Max { get; init; } = 50;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Default <= 0)
                yield return new ValidationResult($"Default must be greater than 0 but it's {Default}.", new[] { nameof(Default) });

            if (Max < Default)
                yield return new ValidationResult($"Max must be greater than or equal to Default {Default} but it's {Max}.", new[] { nameof(Max) });
        }

        object IConfigurationInstance.GetDiagnosticInfo() => this;
    }
}