﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;

namespace TezosDomains.Api.GraphQL.Paging
{
    [NameFromGenericArgument("{0}Connection")]
    [Description(
        "A relay connection from an object to a list of objects of a particular type."
        + " For more details, see: `https://relay.dev/graphql/connections.htm`"
    )]
    public sealed class Connection<TNode>
        where TNode : class
    {
        public Connection(List<Edge<TNode>> edges, int totalCount, PageInfo pageInfo)
        {
            Edges = edges;
            TotalCount = totalCount;
            PageInfo = pageInfo;
        }

        [Description(
            "A count of the total number of objects in this connection, ignoring pagination."
            + " This allows a client to fetch the first five objects by passing `5` as the argument to `first`,"
            + " then fetch the total count so it could display `5 of 83`, for example."
        )]
        public int TotalCount { get; set; }

        [Description("Information to aid in pagination.")]
        public PageInfo PageInfo { get; set; }

        [Description("A list of all of the edges returned in the connection.")]
        public List<Edge<TNode>> Edges { get; set; }

        [Description(
            "A list of all of the objects returned in the connection. This is a convenience field provided for quickly exploring the API;"
            + " rather than querying for `{ edges { node } }` when no edge data is needed, this field can be used instead."
            + " Note that when clients like Relay need to fetch the `cursor` field on edge to enable efficient pagination,"
            + " this shortcut cannot be used, and the full `{ edges { node } }` version should be used instead."
        )]
        public List<TNode> Items => Edges.ConvertAll(e => e.Node);
    }

    [NameFromGenericArgument("{0}Edge")]
    [Description("An edge in a connection from an object to another object of a particular type.")]
    public sealed class Edge<TNode>
        where TNode : class
    {
        public Edge(TNode node, string cursor)
        {
            Node = node;
            Cursor = cursor;
        }

        [Description("The item at the end of the edge")]
        public TNode Node { get; set; }

        [Description("A cursor for use in pagination")]
        public string Cursor { get; set; }
    }

    [Description("Information about pagination in a connection.")]
    public sealed class PageInfo
    {
        public PageInfo(bool hasNextPage, bool hasPreviousPage, string? startCursor, string? endCursor)
        {
            HasNextPage = hasNextPage;
            HasPreviousPage = hasPreviousPage;
            StartCursor = startCursor;
            EndCursor = endCursor;
        }

        [Description("When paginating forwards, are there more items?")]
        public bool HasNextPage { get; set; }

        [Description("When paginating backwards, are there more items?")]
        public bool HasPreviousPage { get; set; }

        [Description("When paginating backwards, the cursor to continue.")]
        public string? StartCursor { get; set; }

        [Description("When paginating forwards, the cursor to continue.")]
        public string? EndCursor { get; set; }
    }
}