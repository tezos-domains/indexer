﻿using GraphQL;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Paging
{
    public interface IRelayConnectionService<TDocument>
        where TDocument : IHasDbCollection
    {
        Task<Connection<TGraphDto>> LoadPageAsync<TGraphDto>(
            IResolveFieldContext graphQLContext,
            FilterDefinition<TDocument> filter,
            DocumentOrder<TDocument> order,
            Func<TDocument, TGraphDto> graphDtoFactory
        )
            where TGraphDto : class;
    }

    public sealed class RelayConnectionService<TDocument> : IRelayConnectionService<TDocument>
        where TDocument : IHasDbCollection
    {
        private readonly IQueryService<TDocument> _queryService;
        private readonly IPageSizeCalculator _pageSizeCalculator;

        public RelayConnectionService(IQueryService<TDocument> queryService, IPageSizeCalculator pageSizeCalculator)
        {
            _queryService = queryService;
            _pageSizeCalculator = pageSizeCalculator;
        }

        public Task<Connection<TGraphDto>> LoadPageAsync<TGraphDto>(
            IResolveFieldContext graphQLContext,
            FilterDefinition<TDocument> filter,
            DocumentOrder<TDocument> order,
            Func<TDocument, TGraphDto> graphDtoFactory
        )
            where TGraphDto : class
        {
            var pagingArgs = ConnectionPagingArgs.Resolve(graphQLContext);

            return pagingArgs.Before == null && pagingArgs.Last == null
                ? ResolveUnidirectionalAsync(pagingArgs.After, pagingArgs.First, ConnectionPagingArgs.Names.Ascending)
                : ResolveReversedAsync();

            async Task<Connection<TGraphDto>> ResolveReversedAsync()
            {
                order = ReverseDocumentOrder(order); // Reverse all fields order direction.
                var connection = await ResolveUnidirectionalAsync(pagingArgs.Before, pagingArgs.Last, ConnectionPagingArgs.Names.Descending);

                connection.Edges.Reverse();
                var pageInfo = connection.PageInfo;
                (pageInfo.StartCursor, pageInfo.EndCursor) = (pageInfo.EndCursor, pageInfo.StartCursor);
                (pageInfo.HasPreviousPage, pageInfo.HasNextPage) = (pageInfo.HasNextPage, pageInfo.HasPreviousPage);

                return connection;
            }

            async Task<Connection<TGraphDto>> ResolveUnidirectionalAsync(string? after, int? first, (string After, string First) argNames)
            {
                var afterIndex = after != null ? ParsePageIndex(after, argNames.After, order) : null;
                var requestedCount = _pageSizeCalculator.Calculate(first, argNames.First);
                var query = new DocumentQuery<TDocument>(filter, order, afterIndex, TakeCount: requestedCount + 1);

                var documentsTask = _queryService.FindAllAsync(query, graphQLContext.CancellationToken);
                var totalCount = await _queryService.CountAsync(query.Filter, graphQLContext.CancellationToken);
                var documents = await documentsTask; // Runs in parallel with totalCount task

                var hasNextPage = false;
                if (documents.Count > requestedCount)
                {
                    hasNextPage = true;
                    documents.RemoveAt(documents.Count - 1);
                }

                var edges = documents.ConvertAll(
                    document =>
                    {
                        var node = graphDtoFactory(document);
                        var fieldValues = order.Field.DbFields.ConvertAll(
                            field =>
                            {
                                try
                                {
                                    return field.Accessor(document);
                                }
                                catch (NullReferenceException)
                                {
                                    //field expression might point through nullable object (ex: OfferOrder.DOMAIN_NAME)
                                    return null;
                                }
                            }
                        );
                        var cursor = CursorConverter.ToCursorId(new PagingIndex<TDocument>(order.Field.Name, fieldValues));
                        return new Edge<TGraphDto>(node, cursor);
                    }
                );
                var pageInfo = new PageInfo(
                    hasNextPage: hasNextPage,
                    hasPreviousPage: after != null,
                    startCursor: edges.FirstOrDefault()?.Cursor,
                    endCursor: edges.LastOrDefault()?.Cursor
                );
                return new Connection<TGraphDto>(edges, (int)totalCount, pageInfo);
            }
        }

        private static DocumentOrder<TDocument> ReverseDocumentOrder(DocumentOrder<TDocument> order)
            => order with
            {
                Field = order.Field with
                {
                    DbFields = order.Field.DbFields.ConvertAll(f => f with { DirectionOverride = f.DirectionOverride?.Reverse() })
                },
                Direction = order.Direction?.Reverse()
            };

        private static PagingIndex<TDocument> ParsePageIndex(string cursorId, string argName, DocumentOrder<TDocument> order)
        {
            try
            {
                var index = CursorConverter.ToPagingIndex<TDocument>(cursorId);

                if (index.OrderFieldName != order.Field.Name)
                    throw new GraphQLInputException(
                        $"its order '{index.OrderFieldName}' must be as same as '{order.Field.Name}' which is specified in 'order' argument (may be default)"
                    );

                if (index.FieldValues.Count != order.Field.DbFields.Count)
                    throw new NullReferenceException();

                for (var i = 0; i < index.FieldValues.Count; i++)
                    if (index.FieldValues[i] == null && !order.Field.DbFields[i].IsNullable)
                        throw new NullReferenceException();

                return index;
            }
            catch (Exception ex)
            {
                var reason = (ex as GraphQLInputException)?.Message ?? "it can't be deserialized";
                throw new GraphQLInputException($"Invalid cursor '{cursorId}' specified in argument '{argName}' because {reason}. Just don't use it anymore.");
            }
        }
    }
}