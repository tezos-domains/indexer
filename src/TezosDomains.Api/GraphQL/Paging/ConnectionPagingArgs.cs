﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;

namespace TezosDomains.Api.GraphQL.Paging
{
    public record ConnectionPagingArgs(string? After, string? Before, int? First, int? Last)
    {
        public static IReadOnlyList<(Type GraphType, string Name, string Description)> Definitions => new[]
        {
            (typeof(StringGraphType), Names.After, "Only return edges after the specified cursor."),
            (typeof(StringGraphType), Names.Before, "Only return edges prior to the specified cursor."),
            (typeof(IntGraphType), Names.First,
                "Specifies the maximum number of edges to return, starting after the cursor specified by `after`, or the first number of edges if `after` is not specified."),
            (typeof(IntGraphType), Names.Last,
                "Specifies the maximum number of edges to return, starting prior to the cursor specified by `before`, or the last number of edges if `before` is not specified."),
        };

        public static ConnectionPagingArgs Resolve(IResolveFieldContext graphQLContext)
        {
            var args = new ConnectionPagingArgs(
                After: graphQLContext.GetArgument<string?>(Names.After),
                Before: graphQLContext.GetArgument<string?>(Names.Before),
                First: graphQLContext.GetArgument<int?>(Names.First),
                Last: graphQLContext.GetArgument<int?>(Names.Last)
            );

            if (args.After != null && args.Before != null)
                throw new GraphQLInputException($"Arguments '{Names.After}' and '{Names.Before}' cannot be specified at the same time.");
            if (args.After != null && args.Last != null)
                throw new GraphQLInputException(
                    $"If argument '{Names.After}' is specified, then '{Names.Last}' cannot be specified. Use '{Names.First}' instead."
                );
            if (args.Before != null && args.First != null)
                throw new GraphQLInputException(
                    $"If argument '{Names.Before}' is specified, then '{Names.First}' cannot be specified. Use '{Names.Last}' instead."
                );

            return args;
        }

        public static class Names
        {
            public const string After = "after";
            public const string Before = "before";
            public const string First = "first";
            public const string Last = "last";

            public static readonly (string, string) Ascending = (After, First);
            public static readonly (string, string) Descending = (Before, Last);
        }
    }
}