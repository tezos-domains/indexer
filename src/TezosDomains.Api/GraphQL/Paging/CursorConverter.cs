﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Paging
{
    public static class CursorConverter
    {
        private static readonly JsonSerializerOptions JsonOptions = new() { DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull };

        public static string ToCursorId<TDocument>(PagingIndex<TDocument> index)
            where TDocument : IHasDbCollection
        {
            var dto = new PageIndexDto(
                index.OrderFieldName,
                index.FieldValues.ConvertAll(SerializeFieldValue)
            );
            var bytes = JsonSerializer.SerializeToUtf8Bytes(dto, JsonOptions);
            return Convert.ToBase64String(bytes);
        }

        public static PagingIndex<TDocument> ToPagingIndex<TDocument>(string cursorId)
            where TDocument : IHasDbCollection
        {
            var bytes = Convert.FromBase64String(cursorId);
            var dto = JsonSerializer.Deserialize<PageIndexDto?>(bytes).GuardNotNull();

            return new PagingIndex<TDocument>(
                OrderFieldName: dto.F.GuardNotNull(),
                FieldValues: dto.V.GuardNotNull().ConvertAll(d => DeserializeFieldValue(d.GuardNotNull()))
            );
        }

        private static FieldValueDto SerializeFieldValue(object? value)
            => value switch
            {
                null => new FieldValueDto(),
                bool b => new FieldValueDto { B = b },
                string s => new FieldValueDto { S = s },
                int i => new FieldValueDto { I = i },
                decimal d => new FieldValueDto { D = d },
                DateTime dt => new FieldValueDto { T = dt },
                _ => throw new Exception($"Unsupported field value {value} of {value.GetType()}."),
            };

        private static object? DeserializeFieldValue(FieldValueDto dto)
            => new object?[] { dto.B, dto.S, dto.I, dto.D, dto.T }
                .SingleOrDefault(v => v != null);

        // Nulls so that asserts are enforced, short names to minimize JSON size.
        private sealed record PageIndexDto(string? F, IReadOnlyList<FieldValueDto?>? V);

        private sealed record FieldValueDto
        {
            public bool? B { get; init; }
            public string? S { get; init; }
            public int? I { get; init; }
            public decimal? D { get; init; }
            public DateTime? T { get; init; }
        }
    }
}