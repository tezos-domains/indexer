﻿using System;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Scalars
{
    public sealed class MutezScalarType : ScalarGraphType<decimal, string>
    {
        public MutezScalarType()
            => Description = "Micro TEZ, the smallest monetary unit in Tezos.";

        protected override decimal Deserialize(string value)
            => decimal.Parse(value);

        protected override string Serialize(decimal value)
        {
            var floored = Math.Floor(value); // Smaller than mutez is only for internal calculations -> floor
            return floored.ToInvariantString();
        }
    }
}