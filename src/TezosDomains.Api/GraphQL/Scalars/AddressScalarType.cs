﻿using TezosDomains.Data.Models;
using TezosDomains.Indexer.Tezos.Encoding;

namespace TezosDomains.Api.GraphQL.Scalars
{
    public sealed class AddressScalarType : ScalarGraphType<Address, string>
    {
        public AddressScalarType()
            => Description = $"Address identifier - {TezosEncoding.Address.Description}";

        protected override Address Deserialize(string value)
            => ValidateAddress(value);

        protected override string Serialize(Address value)
            => ValidateAddress(value);

        public static string ValidateAddress(string value)
        {
            TezosEncoding.Address.ConvertFromBase58(value);
            return value;
        }
    }
}