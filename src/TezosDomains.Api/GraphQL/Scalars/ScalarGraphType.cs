﻿using GraphQL.Types;
using GraphQLParser.AST;
using System;
using System.Linq;
using System.Text.Json;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Scalars
{
    public abstract class ScalarGraphType<TValue, TInput> : ScalarGraphType
        where TValue : notnull
        where TInput : notnull
    {
        protected ScalarGraphType()
            => Name = GetType().Name.RemoveRequiredSuffix("ScalarType");

        public sealed override object? ParseLiteral(GraphQLValue graphQLValue)
        {
            try
            {
                var clrValue = ConvertToClr(graphQLValue);
                return ParseValue(clrValue);
            }
            catch (Exception ex)
            {
                throw new GraphQLInputException($"Invalid value {JsonSerializer.Serialize(graphQLValue)} at {graphQLValue.Location}: {ex.Message}");
            }
        }

        public sealed override object? ParseValue(object? value)
            => value switch
            {
                null => null,
                TInput input => Deserialize(input),
                _ => throw new GraphQLInputException($"Value must be {typeof(TInput).Name} but it is {value.GetType().Name}."),
            };

        public sealed override object? Serialize(object? value)
            => value != null ? Serialize((TValue)value) : null;

        protected abstract TValue Deserialize(TInput value);
        protected abstract object Serialize(TValue value);

        /// <summary>It should return same value as GraphQL passes to <see cref="ParseValue" />.</summary>
        private static object? ConvertToClr(GraphQLValue graphQLValue)
            => graphQLValue switch
            {
                GraphQLBooleanValue b => bool.Parse(b.Value),
                GraphQLFloatValue f => float.Parse(f.Value),
                GraphQLIntValue i => int.Parse(i.Value),
                GraphQLListValue l => l.Values?.ConvertAll(ConvertToClr),
                GraphQLNullValue => null,
                GraphQLObjectValue o => o.Fields?.ToDictionary(f => f.Name.StringValue, f => ConvertToClr(f.Value)),
                GraphQLStringValue s => s.Value.ToString(),
                _ => throw new NotSupportedException($"${graphQLValue.GetType()} is not supported as a scalar."),
            };
    }
}