﻿using System.Text.Json;

namespace TezosDomains.Api.GraphQL.Scalars
{
    public sealed class JsonScalarType : ScalarGraphType<JsonElement, object>
    {
        public JsonScalarType()
            => Description = "JSON value.";

        protected override JsonElement Deserialize(object value)
        {
            var jsonStr = JsonSerializer.Serialize(value); // Value from GraphQL usually is a dictionary.
            return JsonSerializer.Deserialize<JsonElement>(jsonStr);
        }

        protected override object Serialize(JsonElement value)
            => value;
    }
}