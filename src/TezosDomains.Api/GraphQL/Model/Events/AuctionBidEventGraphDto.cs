﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class AuctionBidEventGraphDto : EventGraphDtoBase
    {
        private readonly AuctionBidEvent _event;
        private readonly RecordFilterContext _recordContext;

        public AuctionBidEventGraphDto(AuctionBidEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
            _event = @event;
            _recordContext = recordContext;
        }

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The bid amount.")]
        public decimal BidAmount => _event.BidAmount;

        [Description("The amount spent in this transaction.")]
        public decimal TransactionAmount => _event.TransactionAmount;

        [Description("The last bid amount before this bid.")]
        public decimal? PreviousBidAmount => _event.PreviousBidAmount;

        [Description("The last bidder address before this bid.")]
        public Address? PreviousBidderAddress => _event.PreviousBidderAddress;

        [ReverseRecordDescription(sourceAddressProperty: nameof(PreviousBidderAddress))]
        public Task<ReverseRecordGraphDto?> PreviousBidderAddressReverseRecord(
            [FromServices] IQueryService<ReverseRecord> queryService,
            CancellationToken cancellation
        )
            => queryService.FindSingleDtoAsync(_event.PreviousBidderAddress, _recordContext, cancellation);
    }
}