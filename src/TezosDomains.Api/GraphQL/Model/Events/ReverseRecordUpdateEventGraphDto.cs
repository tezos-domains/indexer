﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public class ReverseRecordUpdateEventGraphDto : EventGraphDtoBase
    {
        private readonly ReverseRecordUpdateEvent _event;

        public ReverseRecordUpdateEventGraphDto(ReverseRecordUpdateEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string? Name => _event.Name;

        [Description("The owner of the bought domain.")]
        public Address ReverseRecordOwnerAddress => _event.ReverseRecordOwnerAddress;

        [Description("The forward record for the bought domain.")]
        public Address ReverseRecordAddress => _event.ReverseRecordAddress;
    }
}