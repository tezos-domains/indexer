﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public class DomainTransferEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainTransferEvent _event;
        private readonly RecordFilterContext _recordContext;

        public DomainTransferEventGraphDto(DomainTransferEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
            _event = @event;
            _recordContext = recordContext;
        }

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The new owner of the transferred domain.")]
        public Address NewOwner => _event.NewOwner;

        [ReverseRecordDescription(sourceAddressProperty: nameof(NewOwner))]
        public Task<ReverseRecordGraphDto?> NewOwnerReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_event.NewOwner, _recordContext, cancellation);
    }
}