﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    [NameFromGraphDtoType]
    [Description("An action in Tezos Domains describing an interaction with a domain or an auction.")]
    public abstract class TezosDocumentGraphDto
    {
        [DescriptionFormat("{0} in which the last change happened.", nameof(Block))]
        public abstract BlockGraphDto Block { get; }
    }
}