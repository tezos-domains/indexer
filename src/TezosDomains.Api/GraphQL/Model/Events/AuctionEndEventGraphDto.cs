﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class AuctionEndEventGraphDto : EventGraphDtoBase
    {
        private readonly AuctionEndEvent _event;

        public AuctionEndEventGraphDto(AuctionEndEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The winning bid amount.")]
        public decimal WinningBid => _event.WinningBid;

        [Description("The auction bidders' addresses.")]
        public List<AuctionParticipantGraphDto> Participants => _event.Participants.ConvertAll(p => new AuctionParticipantGraphDto(p, _event));
    }

    [Description("An auction participant.")]
    public sealed class AuctionParticipantGraphDto : NodeGraphDto
    {
        private readonly string _address;
        private readonly AuctionEndEvent _event;

        public AuctionParticipantGraphDto(string address, AuctionEndEvent @event)
        {
            _address = address;
            _event = @event;
        }

        protected override string LocalId => $"{_event.Id}:{_address}";

        [Description("Participant's address")]
        public Address Address => _address;
    }
}