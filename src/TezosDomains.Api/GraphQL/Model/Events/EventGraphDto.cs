﻿using GraphQL;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Events.Marketplace;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    [Description("An action in Tezos Domains describes an interaction with a domain, a reverse record, or an auction.")]
    public abstract class EventGraphDto : NodeGraphDto
    {
        private readonly Event _event;
        private readonly RecordFilterContext _recordContext;

        protected EventGraphDto(Event @event, RecordFilterContext recordContext)
        {
            _event = @event;
            _recordContext = recordContext;
        }

        protected override string LocalId => _event.Id;

        [Description("Block in which last change happened.")]
        public BlockGraphDto Block => new(_event.Block);

        [Description("Event type.")]
        public EventType Type => EventTypeResolver.Get(GetType());

        [Description("An address that triggered the event.")]
        public Address SourceAddress => _event.SourceAddress;

        [ReverseRecordDescription(sourceAddressProperty: nameof(SourceAddress))]
        public Task<ReverseRecordGraphDto?> SourceAddressReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_event.SourceAddress, _recordContext, cancellation);

        internal static EventGraphDto Create(Event @event, RecordFilterContext recordContext)
            => @event switch
            {
                AuctionBidEvent e => new AuctionBidEventGraphDto(e, recordContext),
                AuctionEndEvent e => new AuctionEndEventGraphDto(e, recordContext),
                AuctionSettleEvent e => new AuctionSettleEventGraphDto(e, recordContext),
                AuctionWithdrawEvent e => new AuctionWithdrawEventGraphDto(e, recordContext),
                DomainBuyEvent e => new DomainBuyEventGraphDto(e, recordContext),
                DomainClaimEvent e => new DomainClaimEventGraphDto(e, recordContext),
                DomainCommitEvent e => new DomainCommitEventGraphDto(e, recordContext),
                DomainGrantEvent e => new DomainGrantEventGraphDto(e, recordContext),
                DomainRenewEvent e => new DomainRenewEventGraphDto(e, recordContext),
                DomainSetChildRecordEvent e => new DomainSetChildRecordEventGraphDto(e, recordContext),
                DomainTransferEvent e => new DomainTransferEventGraphDto(e, recordContext),
                DomainUpdateEvent e => new DomainUpdateEventGraphDto(e, recordContext),
                DomainUpdateOperatorsEvent e => new DomainUpdateOperatorsEventGraphDto(e, recordContext),
                BuyOfferExecutedEvent e => new BuyOfferExecutedEventGraphDto(e, recordContext),
                BuyOfferPlacedEvent e => new BuyOfferPlacedEventGraphDto(e, recordContext),
                BuyOfferRemovedEvent e => new BuyOfferRemovedEventGraphDto(e, recordContext),
                OfferExecutedEvent e => new OfferExecutedEventGraphDto(e, recordContext),
                // OfferUpdatedEvent Must be before OfferPlacedEvent b/c of inheritance.
                OfferUpdatedEvent e => new OfferUpdatedEventGraphDto(e, recordContext),
                OfferPlacedEvent e => new OfferPlacedEventGraphDto(e, recordContext),
                OfferRemovedEvent e => new OfferRemovedEventGraphDto(e, recordContext),
                ReverseRecordClaimEvent e => new ReverseRecordClaimEventGraphDto(e, recordContext),
                ReverseRecordUpdateEvent e => new ReverseRecordUpdateEventGraphDto(e, recordContext),
                _ => throw new Exception($"Unsupported event type: {@event.GetType()}."),
            };
    }
}