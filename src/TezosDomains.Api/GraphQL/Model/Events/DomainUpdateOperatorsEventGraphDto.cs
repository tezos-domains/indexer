﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public class DomainUpdateOperatorsEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainUpdateOperatorsEvent _event;

        public DomainUpdateOperatorsEventGraphDto(DomainUpdateOperatorsEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The domain TZIP-12 operators.")]
        public List<Address> Operators => _event.Operators.ConvertAll(o => new Address(o));
    }
}