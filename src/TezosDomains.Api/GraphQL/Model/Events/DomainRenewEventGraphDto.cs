﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class DomainRenewEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainRenewEvent _event;

        public DomainRenewEventGraphDto(DomainRenewEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The duration of the renewed domain.")]
        public int DurationInDays => _event.DurationInDays;

        [Description("The price paid for the renewal.")]
        public decimal Price => _event.Price;
    }
}