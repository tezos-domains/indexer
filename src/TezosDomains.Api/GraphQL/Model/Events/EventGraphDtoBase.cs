﻿using GraphQL.Types;
using System;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    [Interfaces(typeof(EventGraphDto), typeof(NodeGraphDto), typeof(TezosDocumentGraphDto))]
    [DescriptionFromEventType]
    public abstract class EventGraphDtoBase : EventGraphDto
    {
        protected EventGraphDtoBase(Event @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
        }

        private class DescriptionFromEventTypeAttribute : ModifyGraphTypeAttribute
        {
            protected override void Modify(IGraphType graphType, Type dtoType)
            {
                var eventType = EventTypeResolver.Get(dtoType);
                graphType.Description = eventType.GetDescription();
            }
        }
    }
}