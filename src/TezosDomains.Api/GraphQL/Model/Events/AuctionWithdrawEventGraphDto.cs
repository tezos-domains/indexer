﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class AuctionWithdrawEventGraphDto : EventGraphDtoBase
    {
        private readonly AuctionWithdrawEvent _event;

        public AuctionWithdrawEventGraphDto(AuctionWithdrawEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The tld domain name.")]
        public string TldName => _event.TldName;

        [Description("The withdrawn amount.")]
        public decimal WithdrawnAmount => _event.WithdrawnAmount;
    }
}