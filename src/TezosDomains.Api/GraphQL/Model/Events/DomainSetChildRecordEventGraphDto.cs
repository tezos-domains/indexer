﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public class DomainSetChildRecordEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainSetChildRecordEvent _event;

        public DomainSetChildRecordEventGraphDto(DomainSetChildRecordEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("When `true`, a new subdomain is created. When `false`, a subdomain was updated by the owner of the parent domain.")]
        public bool IsNewRecord => _event.IsNewRecord;

        [Description("The owner of the bought domain.")]
        public Address DomainOwnerAddress => _event.DomainOwnerAddress;

        [Description("The forward record for the bought domain.")]
        public Address? DomainForwardRecordAddress => _event.DomainForwardRecordAddress;

        [Description("The data for the bought domain.")]
        public List<DataItemGraphDto> Data => DataItemGraphDto.Create(_event.Data, this);
    }
}