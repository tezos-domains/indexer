﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class DomainCommitEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainCommitEvent _event;

        public DomainCommitEventGraphDto(DomainCommitEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("Commitment hash which allows for the domain to be bought.")]
        public string CommitmentHash => _event.CommitmentHash;
    }
}