﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class AuctionSettleEventGraphDto : EventGraphDtoBase
    {
        private readonly AuctionSettleEvent _event;

        public AuctionSettleEventGraphDto(AuctionSettleEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The owner of the won domain.")]
        public Address DomainOwnerAddress => _event.DomainOwnerAddress;

        [Description("The forward record for the won domain.")]
        public Address? DomainForwardRecordAddress => _event.DomainForwardRecordAddress;

        [Description("The data for the won domain.")]
        public List<DataItemGraphDto> Data => DataItemGraphDto.Create(_event.Data, this);

        [Description("The price paid for the won domain.")]
        public decimal WinningBid => _event.WinningBid;

        [Description(
            "The period in days during which the auction winner owns this domain. This includes the period after the auction ended but was not settled yet."
        )]
        public int RegistrationDurationInDays => _event.RegistrationDurationInDays;
    }
}