﻿using System;
using System.ComponentModel;
using TezosDomains.Common;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events.Marketplace
{
    public abstract class OfferPlacedEventGraphDtoBase : OfferEventGraphDto
    {
        private readonly OfferPlacedEvent _event;

        protected OfferPlacedEventGraphDtoBase(OfferPlacedEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [Description("The price part which goes to the seller.")]
        public decimal PriceWithoutFee => _event.PriceWithoutFee;

        [Description("The domain selling fee.")]
        public decimal Fee => _event.Fee;

        [Description("The price to buy the domain.")]
        public decimal Price => _event.Price;

        [Description("The offer expiration date.")]
        public DateTime? ExpiresAtUtc => _event.ExpiresAtUtc.ConvertMaxToNull();
    }

    public sealed class OfferPlacedEventGraphDto : OfferPlacedEventGraphDtoBase
    {
        public OfferPlacedEventGraphDto(OfferPlacedEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
        }
    }

    public class OfferUpdatedEventGraphDto : OfferPlacedEventGraphDtoBase
    {
        public OfferUpdatedEventGraphDto(OfferUpdatedEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
        }
    }

    public sealed class BuyOfferPlacedEventGraphDto : OfferPlacedEventGraphDtoBase
    {
        private readonly BuyOfferPlacedEvent _event;

        public BuyOfferPlacedEventGraphDto(BuyOfferPlacedEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
            _event = @event;
        }

        [Description("The domain owner.")]
        public string DomainOwner => _event.DomainOwner;
    }
}