﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events.Marketplace
{
    public abstract class OfferEventGraphDto : EventGraphDtoBase
    {
        private readonly OfferEvent _event;

        protected OfferEventGraphDto(OfferEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain token id.")]
        public int TokenId => _event.TokenId;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;
    }
}