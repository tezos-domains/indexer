﻿using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events.Marketplace
{
    public sealed class OfferRemovedEventGraphDto : OfferEventGraphDto
    {
        public OfferRemovedEventGraphDto(OfferRemovedEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
        }
    }

    public sealed class BuyOfferRemovedEventGraphDto : OfferEventGraphDto
    {
        public BuyOfferRemovedEventGraphDto(BuyOfferRemovedEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
        }
    }
}