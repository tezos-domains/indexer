﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events.Marketplace
{
    public sealed class OfferExecutedEventGraphDto : OfferExecutedEventGraphDtoBase
    {
        public OfferExecutedEventGraphDto(OfferExecutedEvent @event, RecordFilterContext recordContext) : base(@event, recordContext)
        {
        }

        [Description("The seller of the bought domain.")]
        public Address SellerAddress => Event.InitiatorAddress;

        [ReverseRecordDescription(sourceAddressProperty: nameof(SellerAddress))]
        public Task<ReverseRecordGraphDto?> SellerAddressReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(Event.InitiatorAddress, RecordContext, cancellation);
    }

    public sealed class BuyOfferExecutedEventGraphDto : OfferExecutedEventGraphDtoBase
    {
        public BuyOfferExecutedEventGraphDto(BuyOfferExecutedEvent @event, RecordFilterContext recordContext) : base(@event, recordContext)
        {
        }

        [Description("The buyer of the bought domain.")]
        public Address BuyerAddress => Event.InitiatorAddress;

        [ReverseRecordDescription(sourceAddressProperty: nameof(BuyerAddress))]
        public Task<ReverseRecordGraphDto?> BuyerAddressReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(Event.InitiatorAddress, RecordContext, cancellation);
    }

    public abstract class OfferExecutedEventGraphDtoBase : OfferEventGraphDto
    {
        protected readonly OfferExecutedEvent Event;
        protected readonly RecordFilterContext RecordContext;

        protected OfferExecutedEventGraphDtoBase(OfferExecutedEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
        {
            Event = @event;
            RecordContext = recordContext;
        }


        [Description("The sold domain price without the fee.")]
        public decimal PriceWithoutFee => Event.PriceWithoutFee;

        [Description("The domain selling fee.")]
        public decimal Fee => Event.Fee;

        [Description("The sold domain price.")]
        public decimal Price => Event.Price;
    }
}