﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class DomainClaimEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainClaimEvent _event;

        public DomainClaimEventGraphDto(DomainClaimEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The owner of the claimed domain.")]
        public Address DomainOwnerAddress => _event.DomainOwnerAddress;

        [Description("The forward record of the claimed domain.")]
        public Address? DomainForwardRecordAddress => _event.DomainForwardRecordAddress;
    }
}