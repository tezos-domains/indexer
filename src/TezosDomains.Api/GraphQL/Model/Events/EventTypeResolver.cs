﻿using System;
using System.Collections.Concurrent;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public static class EventTypeResolver
    {
        private static readonly ConcurrentDictionary<Type, EventType> Types = new();

        public static EventType Get(Type dtoType)
            => Types.GetOrAdd(dtoType, ResolveType);

        private static EventType ResolveType(Type dtoType)
        {
            var graphName = GraphDtoGraphTypeName.Determine(dtoType);
            var rawValue = typeof(EventType).GetField(graphName)?.GetValue(null);
            return (EventType?)rawValue ?? throw new Exception($"DTO type {dtoType} doesn't correspond to any {typeof(EventType)}.");
        }
    }
}