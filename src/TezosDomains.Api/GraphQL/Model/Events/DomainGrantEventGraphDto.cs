﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public sealed class DomainGrantEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainGrantEvent _event;

        public DomainGrantEventGraphDto(DomainGrantEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The owner of the granted domain.")]
        public Address DomainOwnerAddress => _event.DomainOwnerAddress;

        [Description("The forward record of the granted domain.")]
        public Address? DomainForwardRecordAddress => _event.DomainForwardRecordAddress;

        [Description("The data of the granted domain.")]
        public List<DataItemGraphDto> Data => DataItemGraphDto.Create(_event.Data, this);

        [Description("The duration of the granted domain. If null, then granted forever.")]
        public int? DurationInDays => _event.DurationInDays;
    }
}