﻿using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public class ReverseRecordClaimEventGraphDto : EventGraphDtoBase
    {
        private readonly ReverseRecordClaimEvent _event;

        public ReverseRecordClaimEventGraphDto(ReverseRecordClaimEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string? Name => _event.Name;

        [Description("The owner of the bought domain.")]
        public Address ReverseRecordOwnerAddress => _event.ReverseRecordOwnerAddress;
    }
}