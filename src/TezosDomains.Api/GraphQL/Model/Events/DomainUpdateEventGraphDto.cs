﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Events
{
    public class DomainUpdateEventGraphDto : EventGraphDtoBase
    {
        private readonly DomainUpdateEvent _event;

        public DomainUpdateEventGraphDto(DomainUpdateEvent @event, RecordFilterContext recordContext)
            : base(@event, recordContext)
            => _event = @event;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _event.OperationGroupHash;

        [Description("The domain name.")]
        public string DomainName => _event.DomainName;

        [Description("The owner of the bought domain.")]
        public Address DomainOwnerAddress => _event.DomainOwnerAddress;

        [Description("The forward record for the bought domain.")]
        public Address? DomainForwardRecordAddress => _event.DomainForwardRecordAddress;

        [Description("The data for the bought domain.")]
        public List<DataItemGraphDto> Data => DataItemGraphDto.Create(_event.Data, this);
    }
}