﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Marketplace
{
    [Description("Domain sell offer.")]
    public sealed class OfferGraphDto : OfferBaseGraphDto
    {
        public OfferGraphDto(Offer offer, RecordFilterContext recordContext) : base(offer, recordContext)
        {
        }

        [Description("Domain seller address.")]
        public Address SellerAddress => Offer.InitiatorAddress;

        [ReverseRecordDescription(sourceAddressProperty: nameof(SellerAddress))]
        public Task<ReverseRecordGraphDto?> SellerAddressReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(Offer.InitiatorAddress, RecordContext, cancellation);

        [Description("Domain buyer address.")]
        public Address? BuyerAddress => Offer.AcceptorAddress;

        [ReverseRecordDescription(sourceAddressProperty: nameof(BuyerAddress))]
        public Task<ReverseRecordGraphDto?> BuyerAddressReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(Offer.AcceptorAddress, RecordContext, cancellation);
    }
}