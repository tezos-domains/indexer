﻿using GraphQL;
using System;
using System.ComponentModel;
using TezosDomains.Api.Configuration;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Common;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model.Marketplace;

public abstract class OfferBaseGraphDto : NodeGraphDto
{
    protected readonly Offer Offer;
    protected readonly RecordFilterContext RecordContext;

    protected OfferBaseGraphDto(Offer offer, RecordFilterContext recordContext)
    {
        Offer = offer;
        RecordContext = recordContext;
    }

    protected override string LocalId => Offer.OfferId;

    [Description("Domain NFT token id.")]
    public int TokenId => Offer.TokenId;

    [Description("NFT token contract.")]
    public string TokenContract => Offer.TokenContract;

    [Description("Domain data.")]
    public DomainDataGraphDto? Domain => Offer.Domain != null ? new DomainDataGraphDto(Offer.Domain) : null;

    [Description("Offer creation.")]
    public DateTime CreatedAtUtc => Offer.CreatedAtUtc;

    [Description("Offer expiration.")]
    public DateTime? ExpiresAtUtc => Offer.ExpiresAtUtc.ConvertMaxToNull();

    [Description("Amount to buy the domain without a fee.")]
    public decimal PriceWithoutFee => Offer.PriceWithoutFee;

    [Description("Domain's offer fee.")]
    public decimal Fee => Offer.Fee;

    [Description("Total price for the domain.")]
    public decimal Price => Offer.Price;

    [OperationGroupHashDescription]
    public string OperationGroupHash => Offer.OperationGroupHash;

    [Description("Offer state.")]
    public OfferState State([FromServices] MarketplaceConfiguration config)
        => Offer.GetState(RecordContext.Timestamp, config.DomainIsExpiringThreshold);
}