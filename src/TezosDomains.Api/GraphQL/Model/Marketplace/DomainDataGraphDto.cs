﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Marketplace
{
    [Description("Offer's domain data.")]
    public sealed class DomainDataGraphDto : NodeGraphDto
    {
        private readonly DomainData _domainData;

        public DomainDataGraphDto(DomainData domainData) => _domainData = domainData;

        protected override string LocalId => _domainData.Name;

        [Description("Domain name.")]
        public string Name => _domainData.Name;

        [Description("Domain owner.")]
        public Address Owner => _domainData.Owner;

        [Description("Domain operators.")]
        public List<Address> Operators => _domainData.Operators.ConvertAll(o => new Address(o));

        [Description("Data associated with an offered domain.")]
        public List<DataItemGraphDto> Data => DataItemGraphDto.Create(_domainData.Data, this);

        [Description("Domain expiration.")]
        public DateTime? ExpiresAtUtc => _domainData.ExpiresAtUtc.ConvertMaxToNull();
    }
}