﻿using GraphQL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Auctions;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model
{
    [Description("Domain - forward record.")]
    public sealed class DomainGraphDto : NodeGraphDto
    {
        private readonly Domain _domain;
        private readonly RecordFilterContext _recordContext;

        public DomainGraphDto(Domain domain, RecordFilterContext recordContext)
        {
            _domain = domain;
            _recordContext = recordContext;
        }

        protected override string LocalId => _domain.Name;

        [Description("Domain name.")]
        public string Name => _domain.Name;

        [Description("First segment of domain name.")]
        public string Label => _domain.Label;

        [Description("Domain address.")]
        public Address? Address => _domain.Address;

        [ReverseRecordDescription(sourceAddressProperty: nameof(Address))]
        public Task<ReverseRecordGraphDto?> AddressReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_domain.Address, _recordContext, cancellation);

        [Description("Owner address.")]
        public Address Owner => _domain.Owner;

        [ReverseRecordDescription(sourceAddressProperty: nameof(Owner))]
        public Task<ReverseRecordGraphDto?> OwnerReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_domain.Owner, _recordContext, cancellation);

        [Description("Domain validity.")]
        public DateTime? ExpiresAtUtc => _domain.ExpiresAtUtc.ConvertMaxToNull();

        [DescriptionFormat("Domain level, which is a number of segments in its {0:camel}.", nameof(Name))]
        public int Level => _domain.Level;

        [Description("Data associated with this domain.")]
        public List<DataItemGraphDto> Data => DataItemGraphDto.Create(_domain.Data, this);

        [OperationGroupHashDescription]
        public string OperationGroupHash => _domain.OperationGroupHash;

        [Description("TZIP-12 NFT Token id.")]
        public int? TokenId => _domain.TokenId;

        [DescriptionFormat("Parent domain {0:camel}.", nameof(Name))]
        public string? ParentName => _domain.ParentName;

        [DescriptionFormat("Parent domain {0:camel} address.", nameof(Owner))]
        public string? ParentOwner => _domain.ParentOwner;

        [DescriptionFormat("Last {0} of the domain.", nameof(Auction))]
        public AuctionGraphDto? LastAuction => _domain.LastAuction != null ? new AuctionGraphDto(_domain.LastAuction, _recordContext) : null;

        [ReverseRecordDescription(sourceAddressProperty: nameof(ParentOwner))]
        public Task<ReverseRecordGraphDto?> ParentOwnerReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_domain.ParentOwner, _recordContext, cancellation);

        [ConnectionPagingArgs]
        [Description("Subdomains of the domain e.g. `foo.bar.tez` is subdomain of `bar.tez`.")]
        public Task<Connection<DomainGraphDto>> Subdomains(
            [FromServices] IRelayConnectionService<Domain> connectionService,
            IResolveFieldContext graphQLContext,
            [Description("If `true`, only direct subdomains with level +1 are returned. Otherwise, all subdomains are returned. Default is `false`.")]
            bool? onlyDirectChildren = null,
            DomainOrder? order = null
        )
        {
            order ??= DomainOrder.Default;
            var graphQLFilter = new DomainsFilter
            {
                Ancestors = new StringArrayFilter { Include = _domain.Name },
                Level = onlyDirectChildren == true ? new IntFilter { EqualTo = _domain.Level + 1 } : null,
                Validity = _recordContext.Validity,
            };
            var dbFilter = graphQLFilter.ToDbFilter(_recordContext);

            return connectionService.LoadPageAsync(graphQLContext, dbFilter, order, d => new DomainGraphDto(d, _recordContext));
        }

        // Different from addressReverseRecord below b/c it's null if name doesn't correspond.
        [DescriptionFormat(
            "{0} record corresponding to both the {1:camel} and the {2:camel} of this domain.",
            nameof(TezosDomains.Data.Models.ReverseRecord),
            nameof(Address),
            nameof(Name)
        )]
        public async Task<ReverseRecordGraphDto?> ReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
        {
            var recordDto = await queryService.FindSingleDtoAsync(_domain.Address, _recordContext, cancellation);
            return recordDto?.Name == _domain.Name ? recordDto : null;
        }

        [Description("Domain TZIP-12 operators.")]
        public List<DomainOperatorGraphDto> Operators => _domain.Operators.ConvertAll(o => new DomainOperatorGraphDto(o, _recordContext));
    }
}