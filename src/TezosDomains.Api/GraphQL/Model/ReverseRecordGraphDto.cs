﻿using GraphQL;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model
{
    [Description("Reverse record.")]
    public sealed class ReverseRecordGraphDto : NodeGraphDto
    {
        private readonly ReverseRecord _record;
        private readonly RecordFilterContext _recordContext;

        public ReverseRecordGraphDto(ReverseRecord record, RecordFilterContext recordContext)
        {
            _record = record;
            _recordContext = recordContext;
        }

        protected override string LocalId => _record.Address;

        [Description("Reverse record address.")]
        public Address Address => _record.Address;

        internal string? Name => _record.Name;

        [Description("Reverse record owner.")]
        public Address Owner => _record.Owner;

        [ReverseRecordDescription(sourceAddressProperty: nameof(Owner))]
        public Task<ReverseRecordGraphDto?> OwnerReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_record.Owner, _recordContext, cancellation);

        [Description("Reverse record validity.")]
        public DateTime? ExpiresAtUtc => _record.ExpiresAtUtc.ConvertMaxToNull();

        [OperationGroupHashDescription]
        public string OperationGroupHash => _record.OperationGroupHash;

        [DescriptionFormat("{0} corresponding to the {1:camel} of this record.", nameof(Data.Models.Domain), nameof(Name))]
        public Task<DomainGraphDto?> Domain([FromServices] IQueryService<Domain> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_record.Name, _recordContext, cancellation);
    }
}