﻿using System;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.GraphQL.Model
{
    [Description("Block in the chain.")]
    public sealed class BlockGraphDto : NodeGraphDto
    {
        private readonly BlockSlim _block;

        public BlockGraphDto(BlockSlim block) => _block = block;

        protected override string LocalId => _block.Hash;

        [Description("Block level.")]
        public int Level => _block.Level;

        [Description("Block hash.")]
        public string Hash => _block.Hash;

        [Description("Block timestamp.")]
        public DateTime Timestamp => _block.Timestamp;
    }
}