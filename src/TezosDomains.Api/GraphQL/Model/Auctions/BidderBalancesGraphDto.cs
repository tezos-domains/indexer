﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Auctions
{
    [Description("Bidder balances from unused auctions bids.")]
    public sealed class BidderBalancesGraphDto : NodeGraphDto
    {
        private readonly string _address;
        private readonly BidderBalances? _bidder;

        public BidderBalancesGraphDto(string address, BidderBalances? bidder)
        {
            _address = address;
            _bidder = bidder;
        }

        protected override string LocalId => _address;

        [Description("Reverse record address.")]
        public Address Address => _address;

        [OperationGroupHashDescription]
        public string? OperationGroupHash => _bidder?.OperationGroupHash;

        [Description("User's withdrawable balances by tld registrars.")]
        public List<BalanceGraphDto> Balances => _bidder?.Balances.ConvertAll(b => new BalanceGraphDto(_address, b)) ?? new();
    }
}