﻿using System.Collections.Generic;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Model.Generic;

namespace TezosDomains.Api.GraphQL.Model.Auctions
{
    [Description("A user balance for the specific TLD registrar.")]
    public sealed class BalanceGraphDto : NodeGraphDto
    {
        private readonly string _bidderAddress;
        private readonly KeyValuePair<string, decimal> _balance;

        public BalanceGraphDto(string bidderAddress, KeyValuePair<string, decimal> balance)
        {
            _bidderAddress = bidderAddress;
            _balance = balance;
        }

        protected override string LocalId => $"{_bidderAddress}:{_balance.Key}";

        [Description("The tld registrar's name.")]
        public string TldName => _balance.Key;

        [Description("The withdrawable balance.")]
        public decimal Balance => _balance.Value;
    }
}