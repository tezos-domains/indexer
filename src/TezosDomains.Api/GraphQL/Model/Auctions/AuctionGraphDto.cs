﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Auctions
{
    [Description("Domain auction.")]
    public sealed class AuctionGraphDto : NodeGraphDto
    {
        private readonly Auction _auction;
        private readonly RecordFilterContext _recordContext;

        public AuctionGraphDto(Auction auction, RecordFilterContext recordContext)
        {
            _auction = auction;
            _recordContext = recordContext;
        }

        protected override string LocalId => _auction.AuctionId;

        [Description("Domain name.")]
        public string DomainName => _auction.DomainName;

        [DescriptionFormat("Auction end {0}.", nameof(DateTime))]
        public DateTime EndsAtUtc => _auction.EndsAtUtc;

        [DescriptionFormat("{0} until an auction winner can claim the domain.", nameof(DateTime))]
        public DateTime OwnedUntilUtc => _auction.OwnedUntilUtc;

        [Description("Number of bids for this auction.")]
        public int BidCount => _auction.BidCount;

        [Description("The sum of bids' amounts.")]
        public decimal BidAmountSum => _auction.BidAmountSum;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _auction.OperationGroupHash;

        [Description("The auction state.")]
        public AuctionState State => _auction.GetState(_recordContext.Timestamp);

        [Description("Block level of the auction's first bid.")]
        public int StartedAtLevel => _auction.StartedAtLevel;

        [Description("The current highest bid.")]
        public BidGraphDto HighestBid => new(_auction.HighestBid, _auction, _recordContext);

        [Description("The count of unique bidders.")]
        public int CountOfUniqueBidders => _auction.CountOfUniqueBidders;

        [Description("Auction bids.")]
        public List<BidGraphDto> Bids => _auction.Bids.Reverse().Select(b => new BidGraphDto(b, _auction, _recordContext)).ToList();
    }
}