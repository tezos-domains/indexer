﻿using GraphQL;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Auctions
{
    [Description("Auction bid.")]
    public sealed class BidGraphDto : NodeGraphDto
    {
        private readonly Bid _bid;
        private readonly Auction _auction;
        private readonly RecordFilterContext _recordContext;

        public BidGraphDto(Bid bid, Auction auction, RecordFilterContext recordContext)
        {
            _bid = bid;
            _auction = auction;
            _recordContext = recordContext;
        }

        protected override string LocalId
        {
            get
            {
                var index = _auction.Bids.RequiredIndexOf(b => b == _bid);
                return $"{_auction.AuctionId}:{index}";
            }
        }

        [Description("Bidder address.")]
        public Address Bidder => _bid.Bidder;

        [ReverseRecordDescription(sourceAddressProperty: nameof(Bidder))]
        public Task<ReverseRecordGraphDto?> BidderReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_bid.Bidder, _recordContext, cancellation);

        [Description("Bid amount.")]
        public decimal Amount => _bid.Amount;

        [Description("Bid timestamp.")]
        public DateTime Timestamp => _bid.Block.Timestamp;

        [OperationGroupHashDescription]
        public string OperationGroupHash => _bid.OperationGroupHash;
    }
}