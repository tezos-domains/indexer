﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Model
{
    [Description("Domain TZIP-12 operator.")]
    public sealed class DomainOperatorGraphDto : NodeGraphDto
    {
        private readonly string _address;
        private readonly RecordFilterContext _recordContext;

        public DomainOperatorGraphDto(string address, RecordFilterContext recordContext)
        {
            _address = address;
            _recordContext = recordContext;
        }

        protected override string LocalId => _address;

        [Description("Operator's address.")]
        public string Address => _address;

        [ReverseRecordDescription(sourceAddressProperty: nameof(Address))]
        public Task<ReverseRecordGraphDto?> ReverseRecord([FromServices] IQueryService<ReverseRecord> queryService, CancellationToken cancellation)
            => queryService.FindSingleDtoAsync(_address, _recordContext, cancellation);
    }
}