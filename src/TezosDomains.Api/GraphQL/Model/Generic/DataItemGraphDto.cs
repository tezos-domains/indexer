﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Encoding;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Model.Generic
{
    [Description("An item of arbitrary data associated with the record.")]
    public sealed class DataItemGraphDto : NodeGraphDto
    {
        private readonly KeyValuePair<string, string> _item;
        private readonly NodeGraphDto _parent;

        public DataItemGraphDto(KeyValuePair<string, string> item, NodeGraphDto parent)
        {
            _item = item;
            _parent = parent;
        }

        protected override string LocalId => $"{_parent.Id}:{_item.Key}";

        [Description("The unique key of the data item.")]
        public string Key => _item.Key;

        [Description("The raw value of the data item.")]
        public string RawValue => _item.Value;

        [Description("The parsed value of the data item. The value is `null` if the on-chain bytes don't represent a valid JSON.")]
        public JsonElement? Value
        {
            get
            {
                // Hex conversion fails only if invalid db -> global error.
                var jsonStr = TezosEncoding.String.ConvertFromHex(_item.Value);
                try
                {
                    return JsonSerializer.Deserialize<JsonElement>(jsonStr);
                }
                catch
                {
                    return null;
                }
            }
        }

        internal static List<DataItemGraphDto> Create(IEnumerable<KeyValuePair<string, string>> items, NodeGraphDto parent)
            => items.ConvertAll(i => new DataItemGraphDto(i, parent));
    }
}