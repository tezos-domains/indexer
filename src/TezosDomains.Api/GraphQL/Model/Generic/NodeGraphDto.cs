﻿using GraphQL;
using System.ComponentModel;
using TezosDomains.Api.GraphQL.Building;

namespace TezosDomains.Api.GraphQL.Model.Generic
{
    [NameFromGraphDtoType]
    [Description("Common interface with a globally unique ID.")]
    [Interfaces(typeof(NodeGraphDto))]
    public abstract class NodeGraphDto
    {
        [Id]
        [Description("Unique global ID of the object.")]
        public string Id
        {
            get
            {
                var graphTypeName = GraphDtoGraphTypeName.Determine(GetType());
                return $"{graphTypeName}:{LocalId}";
            }
        }

        protected abstract string LocalId { get; }
    }
}