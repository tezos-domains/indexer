﻿using GraphQL;
using GraphQL.Execution;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace TezosDomains.Api.GraphQL.Infrastructure
{
    public sealed class TezosErrorHandler
    {
        private readonly ILogger _logger;

        public TezosErrorHandler(ILogger<TezosErrorHandler> logger)
        {
            _logger = logger;
        }

        public void Handle(UnhandledExceptionContext context)
        {
            if (context.Exception is GraphQLInputException)
            {
                var messageForUser = context.Exception.Message;

                if (context.FieldContext != null)
                    context.FieldContext.Errors.Add(new ExecutionError(messageForUser));
                else if (context.Context != null)
                    context.Context.Errors.Add(new ExecutionError(messageForUser));
                else
                    context.ErrorMessage = messageForUser;
            }
            else
            {
                _logger.LogError(
                    context.Exception,
                    "Unhandled GraphQL error with {graphqlErrorMessage} for {graphqlQuery} with variables ({variables}).",
                    context.ErrorMessage,
                    context.Context?.Document.Source,
                    string.Join(", ", context.Context?.Variables?.Select(v => $"{v.Name}:{v.Value}") ?? new[] { "EMPTY" })
                );
            }
        }
    }
}