﻿using GraphQL;
using GraphQL.Server.Transports.AspNetCore;
using GraphQL.Types;
using GraphQLParser.AST;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.Metrics;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Infrastructure
{
    public class GraphQLHttpWithMetricsMiddleware : GraphQLHttpMiddleware<ISchema>
    {
        private readonly ILogger<GraphQLHttpWithMetricsMiddleware> _logger;

        public GraphQLHttpWithMetricsMiddleware(IGraphQLTextSerializer deserializer, ILogger<GraphQLHttpWithMetricsMiddleware> logger) : base(deserializer)
        {
            _logger = logger;
        }

        protected override Task RequestExecutedAsync(in GraphQLRequestExecutionResult requestExecutionResult)
        {
            UpdateMetrics(requestExecutionResult);
            return base.RequestExecutedAsync(requestExecutionResult);
        }


        private void UpdateMetrics(GraphQLRequestExecutionResult result)
        {
            try
            {
                var operations = result.Result.Document?.Definitions
                    .Where(d => d is GraphQLOperationDefinition)
                    .OfType<GraphQLOperationDefinition>()
                    .SelectMany(o => o.SelectionSet.Selections)
                    .OfType<GraphQLField>()
                    .Select(f => f.Name)
                    .ToList();

                if (operations == null || operations.Count == 0)
                    return;

                var operationNamesStr = operations.Join(",");
                GraphQLMetrics.RequestsCounter.WithLabels(operationNamesStr).Inc();
                GraphQLMetrics.RequestsDuration.WithLabels(operationNamesStr).Observe(result.Elapsed.TotalSeconds);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unable to log qraphql query metrics");
            }
        }
    }

    public static class GraphQLExtensions
    {
        public static IApplicationBuilder UseGraphQLWithMetrics(this IApplicationBuilder builder) =>
            builder.UseGraphQL<ISchema, GraphQLHttpWithMetricsMiddleware>();
    }
}