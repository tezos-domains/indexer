﻿using GraphQL.Introspection;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Infrastructure
{
    public sealed class ImplicitAlphabeticalSchemaComparer : ISchemaComparer
    {
        private readonly AlphabeticalSchemaComparer _alphabeticalComparer = new();

        public IComparer<IGraphType> TypeComparer => _alphabeticalComparer.TypeComparer;
        public IComparer<Directive> DirectiveComparer => _alphabeticalComparer.DirectiveComparer;

        public IComparer<IFieldType>? FieldComparer(IGraphType parent)
            => _alphabeticalComparer.FieldComparer(parent);

        public IComparer<QueryArgument>? ArgumentComparer(IFieldType field)
            => _alphabeticalComparer.ArgumentComparer(field);

        public IComparer<EnumValueDefinition>? EnumValueComparer(EnumerationGraphType graphType)
        {
            var enumType = ExtractGenericArgument(graphType.GetType(), typeof(EnumerationGraphType<>));

            return enumType?.IsDefined(typeof(GraphQLOrderAsDeclaredAttribute)) == true
                ? null
                : _alphabeticalComparer.EnumValueComparer(graphType);
        }

        private static Type? ExtractGenericArgument(Type reflectedClass, Type genericClassToExtract)
        {
            var genericDefinition = reflectedClass.IsGenericType ? reflectedClass.GetGenericTypeDefinition() : null;

            if (genericDefinition == genericClassToExtract)
                return reflectedClass.GetGenericArguments().Single();

            return reflectedClass.BaseType != null ? ExtractGenericArgument(reflectedClass.BaseType, genericClassToExtract) : null;
        }
    }
}