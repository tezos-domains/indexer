﻿using GraphQL.Types;
using System;
using System.Text.Json;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model;
using TezosDomains.Api.GraphQL.Model.Auctions;
using TezosDomains.Api.GraphQL.Model.Events;
using TezosDomains.Api.GraphQL.Model.Events.Marketplace;
using TezosDomains.Api.GraphQL.Model.Generic;
using TezosDomains.Api.GraphQL.Model.Marketplace;
using TezosDomains.Api.GraphQL.Ordering;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Api.GraphQL.RootQueries;
using TezosDomains.Api.GraphQL.Scalars;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Auctions;
using TezosDomains.Data.MongoDb.Query.Filtering.Domains;
using TezosDomains.Data.MongoDb.Query.Filtering.Events;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Filtering.Marketplace;
using TezosDomains.Data.MongoDb.Query.Ordering;

namespace TezosDomains.Api.GraphQL
{
    public class TezosDomainsSchema : Schema
    {
        public TezosDomainsSchema(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
            Query = new AutoRegisteringObjectGraphType<Query>();

            RegisterModelTypes();
            RegisterCommonTypes();
        }

        private void RegisterModelTypes()
        {
            // Auction:
            AutoRegisterOutputMappingWithConnection<AuctionGraphDto>();
            AutoRegisterOutputMapping<BidGraphDto>();
            AutoRegisterInputMapping<AuctionsFilter>();
            AutoRegisterInputMapping<AuctionStateFilter>();
            AutoRegisterInputMapping<AuctionOrder>();
            RegisterTypeMapping(typeof(DocumentOrderField<Auction>), typeof(DocumentOrderFieldGraphType<Auction>));

            // BidderBalances:
            AutoRegisterOutputMapping<BidderBalancesGraphDto>();
            AutoRegisterOutputMapping<BalanceGraphDto>();

            // Block:
            AutoRegisterOutputMapping<BlockGraphDto>();

            // Domain:
            AutoRegisterOutputMappingWithConnection<DomainGraphDto>();
            AutoRegisterOutputMapping<DomainOperatorGraphDto>();
            AutoRegisterInputMapping<DomainsFilter>();
            AutoRegisterInputMapping<DomainLabelFilter>();
            AutoRegisterInputMapping<LabelCompositionFilter>();
            AutoRegisterInputMapping<DomainOrder>();
            RegisterTypeMapping(typeof(DocumentOrderField<Domain>), typeof(DocumentOrderFieldGraphType<Domain>));

            // Event common:
            RegisterTypeMapping(typeof(EventGraphDto), typeof(AutoRegisteringInterfaceGraphType<EventGraphDto>));
            AutoRegisterOutputMapping<Connection<EventGraphDto>>();
            AutoRegisterOutputMapping<Edge<EventGraphDto>>();
            AutoRegisterInputMapping<EventsFilter>();
            AutoRegisterInputMapping<EventTypeFilter>();
            AutoRegisterInputMapping<EventOrder>();
            RegisterTypeMapping(typeof(DocumentOrderField<Event>), typeof(DocumentOrderFieldGraphType<Event>));

            // Event types:
            AutoRegisterOutputMapping<AuctionBidEventGraphDto>();
            AutoRegisterOutputMapping<AuctionEndEventGraphDto>();
            AutoRegisterOutputMapping<AuctionParticipantGraphDto>();
            AutoRegisterOutputMapping<AuctionSettleEventGraphDto>();
            AutoRegisterOutputMapping<AuctionWithdrawEventGraphDto>();
            AutoRegisterOutputMapping<DomainBuyEventGraphDto>();
            AutoRegisterOutputMapping<DomainClaimEventGraphDto>();
            AutoRegisterOutputMapping<DomainCommitEventGraphDto>();
            AutoRegisterOutputMapping<DomainGrantEventGraphDto>();
            AutoRegisterOutputMapping<DomainRenewEventGraphDto>();
            AutoRegisterOutputMapping<DomainSetChildRecordEventGraphDto>();
            AutoRegisterOutputMapping<DomainTransferEventGraphDto>();
            AutoRegisterOutputMapping<DomainUpdateEventGraphDto>();
            AutoRegisterOutputMapping<DomainUpdateOperatorsEventGraphDto>();
            AutoRegisterOutputMapping<OfferExecutedEventGraphDto>();
            AutoRegisterOutputMapping<BuyOfferExecutedEventGraphDto>();
            AutoRegisterOutputMapping<OfferUpdatedEventGraphDto>();
            AutoRegisterOutputMapping<OfferPlacedEventGraphDto>();
            AutoRegisterOutputMapping<BuyOfferPlacedEventGraphDto>();
            AutoRegisterOutputMapping<OfferRemovedEventGraphDto>();
            AutoRegisterOutputMapping<BuyOfferRemovedEventGraphDto>();
            AutoRegisterOutputMapping<ReverseRecordClaimEventGraphDto>();
            AutoRegisterOutputMapping<ReverseRecordUpdateEventGraphDto>();

            // Interfaces:
            RegisterTypeMapping(typeof(NodeGraphDto), typeof(AutoRegisteringInterfaceGraphType<NodeGraphDto>));
            RegisterTypeMapping(typeof(TezosDocumentGraphDto), typeof(AutoRegisteringInterfaceGraphType<TezosDocumentGraphDto>));

            // Offer:
            AutoRegisterOutputMappingWithConnection<OfferGraphDto>();
            AutoRegisterOutputMappingWithConnection<BuyOfferGraphDto>();
            AutoRegisterOutputMapping<DomainDataGraphDto>();
            AutoRegisterInputMapping<OffersFilter>();
            AutoRegisterInputMapping<BuyOffersFilter>();
            AutoRegisterInputMapping<OfferStateFilter>();
            AutoRegisterInputMapping<OfferOrder>();
            RegisterTypeMapping(typeof(DocumentOrderField<Offer>), typeof(DocumentOrderFieldGraphType<Offer>));

            // ReverseRecord:
            AutoRegisterOutputMappingWithConnection<ReverseRecordGraphDto>();
            AutoRegisterInputMapping<ReverseRecordsFilter>();
            AutoRegisterInputMapping<ReverseRecordOrder>();
            RegisterTypeMapping(typeof(DocumentOrderField<ReverseRecord>), typeof(DocumentOrderFieldGraphType<ReverseRecord>));
        }

        private void RegisterCommonTypes()
        {
            // Scalars:
            RegisterTypeMapping(typeof(Address), typeof(AddressScalarType));
            RegisterTypeMapping(typeof(decimal), typeof(MutezScalarType));
            RegisterTypeMapping(typeof(JsonElement), typeof(JsonScalarType));

            // Common input:
            AutoRegisterInputMapping<AddressArrayFilter>();
            AutoRegisterInputMapping<AddressFilter>();
            RegisterTypeMapping(typeof(AddressPrefix), typeof(NoUnderscoreEnumerationGraphType<AddressPrefix>));
            AutoRegisterInputMapping<AssociatedBlockFilter>();
            AutoRegisterInputMapping<BlockHistoryFilter>();
            AutoRegisterInputMapping<DateTimeFilter>();
            AutoRegisterInputMapping<IntFilter>();
            AutoRegisterInputMapping<MutezFilter>();
            AutoRegisterInputMapping<NullableAddressFilter>();
            AutoRegisterInputMapping<NullableDateTimeFilter>();
            AutoRegisterInputMapping<NullableStringFilter>();
            AutoRegisterInputMapping<StringArrayFilter>();
            AutoRegisterInputMapping<StringEqualityFilter>();
            AutoRegisterInputMapping<StringFilter>();
            AutoRegisterInputMapping<StringFilter.StringLengthFilter>();
            AutoRegisterInputMapping<IdEqualityFilter>();

            // Common output:
            AutoRegisterOutputMapping<DataItemGraphDto>();
            AutoRegisterOutputMapping<PageInfo>();
        }

        private void AutoRegisterOutputMappingWithConnection<TNode>()
            where TNode : class
        {
            AutoRegisterOutputMapping<TNode>();
            AutoRegisterOutputMapping<Connection<TNode>>();
            AutoRegisterOutputMapping<Edge<TNode>>();
        }

        private void AutoRegisterOutputMapping<T>()
        {
            RegisterTypeMapping(typeof(T), typeof(FixedAutoRegisteringObjectGraphType<T>));

            // The type may not be on public CLR API (e.g. AuctionBidEventGraphDto) => inform GraphQL framework about it.
            RegisterType(typeof(FixedAutoRegisteringObjectGraphType<T>));
        }

        private void AutoRegisterOutputType<T>()
            => RegisterType(typeof(FixedAutoRegisteringObjectGraphType<T>));

        private void AutoRegisterInputMapping<T>()
            => RegisterTypeMapping(typeof(T), typeof(AutoRegisteringInputObjectGraphType<T>));
    }
}