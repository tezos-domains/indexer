﻿using System;

namespace TezosDomains.Api.GraphQL
{
    /// <summary>
    /// Represents client input error so its message is returned to client and it's not logged.
    /// </summary>
    public sealed class GraphQLInputException : Exception
    {
        public GraphQLInputException(string message) : base(message)
        {
        }

        public GraphQLInputException(string argName, object? actualValue, string mustBe)
            : this($"Argument '{argName}' must be {mustBe} but value {Format(actualValue)} was specified.")
        {
        }

        private static string Format(object? value)
            => value switch
            {
                null => "null",
                string s => $"'{s}'",
                _ => value.ToString() ?? "",
            };
    }
}