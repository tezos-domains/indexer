﻿using GraphQL.Types;
using System.Collections.Generic;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Api.GraphQL.Ordering
{
    public sealed class DocumentOrderFieldGraphType<TDocument> : EnumerationGraphType
    {
        public DocumentOrderFieldGraphType(IEnumerable<DocumentOrderField<TDocument>> fields)
        {
            var documentName = typeof(TDocument).Name;
            Name = $"{documentName}OrderField";
            Description = $"Properties by which {documentName}s are ordered.";

            foreach (var field in fields)
                Add(field.Name, field, field.Description);
        }
    }
}