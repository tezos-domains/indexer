﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Model;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL
{
    public static class QueryServiceExtensions
    {
        public static Task<DomainGraphDto?> FindSingleDtoAsync(
            this IQueryService<Domain> queryService,
            string? name,
            RecordFilterContext context,
            CancellationToken cancellation
        )
            => queryService.FindInternalAsync(name, context, cancellation, d => new DomainGraphDto(d, context));

        public static Task<ReverseRecordGraphDto?> FindSingleDtoAsync(
            this IQueryService<ReverseRecord> queryService,
            string? address,
            RecordFilterContext context,
            CancellationToken cancellation
        )
            => queryService.FindInternalAsync(address, context, cancellation, r => new ReverseRecordGraphDto(r, context));

        private static async Task<TDto?> FindInternalAsync<TRecord, TDto>(
            this IQueryService<TRecord> queryService,
            string? key,
            RecordFilterContext context,
            CancellationToken cancellation,
            Func<TRecord, TDto> dtoFactory
        )
            where TRecord : class, IRecord
            where TDto : class
        {
            if (string.IsNullOrWhiteSpace(key))
                return null;

            var filter = new SingleRecordFilter<TRecord>(key, context.Validity).ToDbFilter(context);
            var record = await queryService.FindSingleAsync(filter, cancellation);
            return record != null ? dtoFactory(record) : null;
        }
    }
}