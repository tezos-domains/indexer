﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public partial class Query
    {
        [DescriptionFormat(
            $"Finds a single {nameof(ReverseRecord)} by its unique {{0:camel}} and specified filters.",
            nameof(ReverseRecordGraphDto.Address)
        )]
        public static async Task<ReverseRecordGraphDto?> ReverseRecord(
            [FromServices] IQueryService<ReverseRecord> reverseRecordQueryService,
            [FromServices] IFilterContextResolver filterContextResolver,
            CancellationToken cancellation,
            [Description("Unique address of reverse record to find.")]
            string address,
            [RecordValidityDescription] RecordValidity? validity = null,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            address.GuardNotEmptyGraphQLArg();
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, cancellation, validity ?? default);

            return await reverseRecordQueryService.FindSingleDtoAsync(address, recordContext, cancellation);
        }

        [ConnectionPagingArgs]
        [Description($"Finds all `{nameof(ReverseRecord)}`-s corresponding to specified filters.")]
        public static async Task<Connection<ReverseRecordGraphDto>> ReverseRecords(
            [FromServices] IRelayConnectionService<ReverseRecord> connectionService,
            [FromServices] IFilterContextResolver filterContextResolver,
            IResolveFieldContext graphQLContext,
            [DocumentFilterDescription(typeof(ReverseRecord))]
            ReverseRecordsFilter? where = null,
            [DocumentOrderDescription(typeof(ReverseRecord))]
            ReverseRecordOrder? order = null,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            where ??= new();
            order ??= ReverseRecordOrder.Default;
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, graphQLContext.CancellationToken, where.Validity ?? default);
            var filter = where.ToDbFilter(recordContext);


            return await connectionService.LoadPageAsync(graphQLContext, filter, order, r => new ReverseRecordGraphDto(r, recordContext));
        }
    }
}