﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Auctions;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public partial class Query
    {
        [Description("Finds bidder balances for the given address.")]
        public static async Task<BidderBalancesGraphDto> BidderBalances(
            [FromServices] IQueryService<BidderBalances> queryService,
            [FromServices] IFilterContextResolver filterContextResolver,
            CancellationToken cancellation,
            [Description("The address for which current bidder balances should be obtained.")]
            string address,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            address.GuardNotEmptyGraphQLArg();
            var filterContext = await filterContextResolver.ResolveAsync(atBlock, cancellation);
            var filter = new SingleDocumentWithHistoryFilter<BidderBalances>(address).ToDbFilter(filterContext);

            var bidder = await queryService.FindSingleAsync(filter, cancellation);
            return new BidderBalancesGraphDto(address, bidder);
        }
    }
}