﻿using GraphQL;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Events;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Events;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public partial class Query
    {
        [ConnectionPagingArgs]
        [DescriptionFormat("Finds all {0}-s corresponding to specified filters.", nameof(Event))]
        public static async Task<Connection<EventGraphDto>> Events(
            [FromServices] IRelayConnectionService<Event> connectionService,
            [FromServices] IFilterContextResolver filterContextResolver,
            IResolveFieldContext graphQLContext,
            [DocumentFilterDescription(typeof(Event))]
            EventsFilter? where = null,
            [DocumentOrderDescription(typeof(Event))]
            EventOrder? order = null
        )
        {
            var recordContext = filterContextResolver.GetCurrent();
            var filter = (where ?? new()).ToDbFilter(new EmptyFilterContext());
            order ??= EventOrder.Default;

            return await connectionService.LoadPageAsync(graphQLContext, filter, order, e => EventGraphDto.Create(e, recordContext));
        }
    }
}