﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.Configuration;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Marketplace;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Filtering.Marketplace;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public partial class Query
    {
        [DescriptionFormat(
            "Finds a single {0} by its {1:camel}, {2:camel}, and offer creation block level.",
            nameof(Data.Models.Marketplace.Offer),
            nameof(OfferGraphDto.SellerAddress),
            nameof(OfferGraphDto.TokenId)
        )]
        public static async Task<OfferGraphDto?> Offer(
            [FromServices] IQueryService<Offer> offerQueryService,
            [FromServices] IFilterContextResolver filterContextResolver,
            CancellationToken cancellation,
            [Description("Seller address corresponding to the offer.")]
            Address sellerAddress,
            [Description("The NFT token id corresponding to a domain.")]
            int tokenId,
            [Description("The offer's creation block level.")]
            int startedAtLevel,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, cancellation);
            var id = Data.Models.Marketplace.Offer.GenerateId(OfferType.SellOffer, sellerAddress, tokenId, startedAtLevel);
            var filter = new SingleDocumentWithHistoryFilter<Offer>(id).ToDbFilter(recordContext);

            var offer = await offerQueryService.FindSingleAsync(filter, cancellation);
            return offer != null ? new OfferGraphDto(offer, recordContext) : null;
        }

        [ConnectionPagingArgs]
        [DescriptionFormat("Finds all {0}-s corresponding to specified filters.", nameof(Data.Models.Marketplace.Offer))]
        public static async Task<Connection<OfferGraphDto>> Offers(
            [FromServices] IRelayConnectionService<Offer> connectionService,
            [FromServices] IFilterContextResolver filterContextResolver,
            [FromServices] MarketplaceConfiguration config,
            IResolveFieldContext graphQLContext,
            [DocumentFilterDescription(typeof(Offer))]
            OffersFilter? where = null,
            [DocumentOrderDescription(typeof(Offer))]
            OfferOrder? order = null,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, graphQLContext.CancellationToken);
            var offersContext = new OffersFilterContext(recordContext.Timestamp, recordContext.ExcludeHistory, config.DomainIsExpiringThreshold);

            var filter = (where ?? new()).ToDbFilter(offersContext);
            order ??= OfferOrder.Default;

            return await connectionService.LoadPageAsync(graphQLContext, filter, order, o => new OfferGraphDto(o, recordContext));
        }

        [DescriptionFormat("Finds a single valid {0} by its {1:camel} name.", nameof(Data.Models.Marketplace.Offer), nameof(OfferGraphDto.Domain))]
        public static async Task<OfferGraphDto?> CurrentOffer(
            [FromServices] IQueryService<Offer> offerQueryService,
            [FromServices] IFilterContextResolver filterContextResolver,
            CancellationToken cancellation,
            [Description("The domain name corresponding to the offer.")]
            string domainName,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, cancellation);
            var filter = new CurrentOfferFilter(domainName).ToDbFilter(recordContext);

            var offer = await offerQueryService.FindSingleAsync(filter, cancellation);
            return offer != null ? new OfferGraphDto(offer, recordContext) : null;
        }
    }
}