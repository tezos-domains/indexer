﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    [Description("Root node with entry queries.")]
    public partial class Query
    {
        [DescriptionFormat(
            "Finds a single {0}` by its unique {1:camel} and specified filters.",
            nameof(Data.Models.Domain),
            nameof(DomainGraphDto.Name)
        )]
        public static async Task<DomainGraphDto?> Domain(
            [FromServices] IQueryService<Domain> domainQueryService,
            [FromServices] IFilterContextResolver filterContextResolver,
            CancellationToken cancellation,
            [Description("Unique name of the domain to find.")]
            string name,
            [RecordValidityDescription] RecordValidity? validity = null,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            name.GuardNotEmptyGraphQLArg();
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, cancellation, validity ?? default);

            return await domainQueryService.FindSingleDtoAsync(name, recordContext, cancellation);
        }

        [ConnectionPagingArgs]
        [DescriptionFormat("Finds all {0}-s corresponding to specified filters.", nameof(Data.Models.Domain))]
        public static async Task<Connection<DomainGraphDto>> Domains(
            [FromServices] IRelayConnectionService<Domain> connectionService,
            [FromServices] IFilterContextResolver filterContextResolver,
            IResolveFieldContext graphQLContext,
            [DocumentFilterDescription(typeof(Domain))]
            DomainsFilter? where = null,
            [DocumentOrderDescription(typeof(Domain))]
            DomainOrder? order = null,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            where ??= new();
            order ??= DomainOrder.Default;
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, graphQLContext.CancellationToken, where.Validity ?? default);
            var filter = where.ToDbFilter(recordContext);

            return await connectionService.LoadPageAsync(graphQLContext, filter, order, d => new DomainGraphDto(d, recordContext));
        }
    }
}