﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public partial class Query
    {
        [DescriptionFormat(
            "Finds the latest indexed {0} (head) or a specific {0} by its {1:camel} or {2:camel} when supplied."
            + " Note: Only blocks starting with the origination of the smart contracts are available.",
            nameof(Data.Models.Block),
            nameof(BlockGraphDto.Hash),
            nameof(BlockGraphDto.Level)
        )]
        public static async Task<BlockGraphDto?> Block(
            [FromServices] IBlockQueryService queryService,
            CancellationToken cancellation,
            [Description("The hash of the block to find.")]
            string? hash = null,
            [Description("The level of the block to find.")]
            int? level = null
        )
        {
            hash.GuardNotEmptyGraphQLArg(allowNull: true);

            var fullBlock = await ((level, hash) switch
            {
                (level: null, hash: not null) => queryService.FindAsync(hash, cancellation),
                (level: not null, _) => FindByLevelAndHashAsync(),
                _ => queryService.GetLatestOrNullAsync(cancellation),
            });
            return fullBlock != null ? new BlockGraphDto(fullBlock) : null;

            async Task<Block?> FindByLevelAndHashAsync()
            {
                var blockStartLevel = await queryService.GetStartLevelOrNullAsync(cancellation);
                if (level < blockStartLevel)
                {
                    var mustBe = $"greater than or equal to {blockStartLevel} because older blocks are not indexed";
                    throw new GraphQLInputException(nameof(level), level, mustBe);
                }

                var block = await queryService.FindAsync(level.Value, cancellation);
                return hash == null || hash == block?.Hash ? block : null;
            }
        }
    }
}