﻿using GraphQL;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Model.Auctions;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Auctions;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public partial class Query
    {
        [DescriptionFormat(
            "Finds a single {0} by its unique {1:camel} and {2:camel}.",
            nameof(Data.Models.Auctions.Auction),
            nameof(AuctionGraphDto.DomainName),
            nameof(AuctionGraphDto.StartedAtLevel)
        )]
        public static async Task<AuctionGraphDto?> Auction(
            [FromServices] IQueryService<Auction> auctionQueryService,
            [FromServices] IFilterContextResolver filterContextResolver,
            CancellationToken cancellation,
            [Description("The domain name corresponding to the auction.")]
            string domainName,
            [Description("The auction's first bid block level.")]
            int startedAtLevel,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            domainName.GuardNotEmptyGraphQLArg();
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, cancellation);
            var id = Data.Models.Auctions.Auction.GenerateId(domainName, startedAtLevel);
            var filter = new SingleDocumentWithHistoryFilter<Auction>(id).ToDbFilter(recordContext);

            var auction = await auctionQueryService.FindSingleAsync(filter, cancellation);
            return auction != null ? new AuctionGraphDto(auction, recordContext) : null;
        }

        [ConnectionPagingArgs]
        [DescriptionFormat("Finds all {0}-s corresponding to specified filters.", nameof(Data.Models.Auctions.Auction))]
        public static async Task<Connection<AuctionGraphDto>> Auctions(
            [FromServices] IRelayConnectionService<Auction> connectionService,
            [FromServices] IFilterContextResolver filterContextResolver,
            IResolveFieldContext graphQLContext,
            [DocumentFilterDescription(typeof(Auction))]
            AuctionsFilter? where = null,
            [DocumentOrderDescription(typeof(Auction))]
            AuctionOrder? order = null,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, graphQLContext.CancellationToken);
            var filter = (where ?? new()).ToDbFilter(recordContext);
            order ??= AuctionOrder.Default;

            return await connectionService.LoadPageAsync(graphQLContext, filter, order, a => new AuctionGraphDto(a, recordContext));
        }

        [DescriptionFormat(
            "Finds a single {0} by its {1:camel} in current/specified time.",
            nameof(Data.Models.Auctions.Auction),
            nameof(AuctionGraphDto.DomainName)
        )]
        public static async Task<AuctionGraphDto?> CurrentAuction(
            [FromServices] IQueryService<Auction> auctionQueryService,
            [FromServices] IFilterContextResolver filterContextResolver,
            CancellationToken cancellation,
            [Description("The domain name corresponding to the auction.")]
            string domainName,
            [BlockHistoryDescription] BlockHistoryFilter? atBlock = null
        )
        {
            domainName.GuardNotEmptyGraphQLArg();
            var recordContext = await filterContextResolver.ResolveAsync(atBlock, cancellation);
            var filter = new CurrentAuctionFilter(domainName).ToDbFilter(recordContext);

            var auction = await auctionQueryService.FindSingleAsync(filter, cancellation);
            return auction != null ? new AuctionGraphDto(auction, recordContext) : null;
        }
    }
}