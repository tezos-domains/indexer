﻿using System;

namespace TezosDomains.Api.GraphQL.Building
{
    public sealed class CurrentTimeService
    {
        // Single constant value per HTTP request.
        public DateTime UtcNow { get; } = DateTime.UtcNow;
    }
}