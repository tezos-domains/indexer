﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace TezosDomains.Api.GraphQL.Building
{
    // https://github.com/graphql-dotnet/graphql-dotnet/issues/3204"
    public sealed class FixedAutoRegisteringObjectGraphType<T> : AutoRegisteringObjectGraphType<T>
    {
        protected override IEnumerable<MemberInfo> GetRegisteredMembers()
            => GetRegisteredMembers(typeof(T));

        private static IEnumerable<MemberInfo> GetRegisteredMembers(Type type)
        {
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static).Where(x => x.CanRead);
            var methods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)
                .Where(
                    x =>
                        x.DeclaringType != typeof(object)
                        && !x.ContainsGenericParameters // exclude methods with open generics
                        && !x.IsSpecialName // exclude methods generated for properties
                        && x.ReturnType != typeof(void) // exclude methods which do not return a value
                        && x.ReturnType != typeof(Task) // exclude methods which do not return a value
                        && x.GetBaseDefinition() == x // exclude methods which override an inherited class' method (e.g. GetHashCode)
                        && !(x.Name == "Equals"
                             && !x.IsStatic
                             && x.GetParameters().Length == 1
                             && x.GetParameters()[0].ParameterType == type
                             && x.ReturnType == typeof(bool)) // exclude methods generated for record types: bool Equals(TSourceType)
                        && x.Name != "<Clone>$" // exclude methods generated for record types
                );

            return properties.Concat<MemberInfo>(methods);
        }
    }
}