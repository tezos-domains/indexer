﻿using GraphQL.Types;
using System;
using System.Linq;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Building
{
    public static class GraphDtoGraphTypeName
    {
        public static string Determine(Type dtoType)
            => dtoType.Name.RemoveRequiredSuffix("GraphDto");
    }

    public abstract class NameFromSourceTypeAttribute : ModifyGraphTypeAttribute
    {
        protected override void Modify(IGraphType graphType, Type dtoType)
            => graphType.Name = DetermineName(dtoType);

        protected abstract string DetermineName(Type dtoType);
    }

    public sealed class NameFromGraphDtoTypeAttribute : NameFromSourceTypeAttribute
    {
        protected override string DetermineName(Type dtoType)
            => GraphDtoGraphTypeName.Determine(dtoType);
    }

    public sealed class NameFromGenericArgumentAttribute : NameFromSourceTypeAttribute
    {
        private readonly string _format;

        public NameFromGenericArgumentAttribute(string format)
            => _format = format;

        protected override string DetermineName(Type sourceType)
        {
            var dtoType = sourceType.GetGenericArguments().Single();
            var dtoGraphTypeName = GraphDtoGraphTypeName.Determine(dtoType);
            return string.Format(_format, dtoGraphTypeName);
        }
    }
}