﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TezosDomains.Api.GraphQL.Building
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class InterfacesAttribute : GraphQLAttribute
    {
        private readonly IReadOnlyCollection<Type> _interfaceDtoTypes;

        public InterfacesAttribute(Type interfaceDtoType, params Type[] interfaceDtoTypes)
            => _interfaceDtoTypes = interfaceDtoTypes.Append(interfaceDtoType).ToList();

        public override void Modify(IGraphType graphType)
        {
            if (!(graphType is IObjectGraphType objGraphType))
                throw new Exception($"{GetType()} supports only output types but it was put on {graphType.GetType()} with Name '{graphType.Name}'.");

            foreach (var interfaceDtoType in _interfaceDtoTypes)
                objGraphType.Interfaces.Add(typeof(AutoRegisteringInterfaceGraphType<>).MakeGenericType(interfaceDtoType));
        }
    }
}