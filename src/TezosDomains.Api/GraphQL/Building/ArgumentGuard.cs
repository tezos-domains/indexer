﻿using System.Runtime.CompilerServices;

namespace TezosDomains.Api.GraphQL.Building
{
    public static class ArgumentGuard
    {
        public static void GuardNotEmptyGraphQLArg(this string? arg, bool allowNull = false, [CallerArgumentExpression("arg")] string argExpression = "")
        {
            if (string.IsNullOrWhiteSpace(arg) && (!allowNull || arg != null))
                throw new GraphQLInputException(argExpression, arg, mustBe: "a non-empty string");
        }
    }
}