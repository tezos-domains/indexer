﻿using GraphQL.Types;
using System;

namespace TezosDomains.Api.GraphQL.Building
{
    /// <summary>Transforms "tz1" to "TZ1" instead of "TZ_1".</summary>
    public sealed class NoUnderscoreEnumerationGraphType<TEnum> : EnumerationGraphType<TEnum>
        where TEnum : Enum
    {
        protected override string ChangeEnumCase(string value)
            => value.ToUpperInvariant();
    }
}