﻿using GraphQL.Types;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Building
{
    public sealed class AutoRegisteringInterfaceGraphType<TDto> : InterfaceGraphType<TDto>
    {
        public AutoRegisteringInterfaceGraphType()
        {
            var helperType = new FixedAutoRegisteringObjectGraphType<TDto>();

            Name = helperType.Name;
            Description = helperType.Description;

            foreach (var field in helperType.Fields)
                Field(field.Type.GuardNotNull(), field.Name, field.Description);
        }
    }
}