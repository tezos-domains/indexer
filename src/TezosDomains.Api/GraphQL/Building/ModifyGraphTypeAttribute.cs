﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Linq;

namespace TezosDomains.Api.GraphQL.Building
{
    [AttributeUsage(AttributeTargets.Class)]
    public abstract class ModifyGraphTypeAttribute : GraphQLAttribute
    {
        public sealed override void Modify(IGraphType graphType)
        {
            var graphTypeType = graphType.GetType();
            try
            {
                var dtoType = graphType.GetType().GetGenericArguments().Single();
                Modify(graphType, dtoType);
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType()} failed to modify graph type {graphTypeType}.", ex);
            }
        }

        protected abstract void Modify(IGraphType graphType, Type dtoType);
    }
}