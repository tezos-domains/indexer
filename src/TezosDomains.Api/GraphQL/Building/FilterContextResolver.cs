﻿using GraphQL;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Building
{
    public interface IFilterContextResolver
    {
        Task<RecordFilterContext> ResolveAsync(BlockHistoryFilter? atBlock, CancellationToken cancellation, RecordValidity validity = default);

        RecordFilterContext GetCurrent(RecordValidity validity = default);
    }

    public sealed class FilterContextResolver : IFilterContextResolver
    {
        private readonly IBlockQueryService _blockQueryService;
        private readonly CurrentTimeService _timeService;

        public FilterContextResolver(IBlockQueryService blockQueryService, CurrentTimeService timeService)
        {
            _blockQueryService = blockQueryService;
            _timeService = timeService;
        }

        public async Task<RecordFilterContext> ResolveAsync(BlockHistoryFilter? atBlock, CancellationToken cancellation, RecordValidity validity)
        {
            switch (atBlock)
            {
                case null:
                case { Level: null, Timestamp: null }:
                    return GetCurrent(validity);

                case { Level: not null, Timestamp: not null }:
                    throw new GraphQLInputException(
                        $"Filter '{nameof(atBlock)}' cannot contain both '{nameof(atBlock.Level).ToCamelCase()}' and '{nameof(atBlock.Timestamp).ToCamelCase()}'."
                    );

                case { Timestamp: not null }:
                    var excludeHistory = _timeService.UtcNow <= atBlock.Timestamp.Value; //only include for historic queries
                    return new(atBlock.Timestamp.Value, excludeHistory, validity);

                case { Level: not null }:
                    var block = await _blockQueryService.FindAsync(atBlock.Level.Value, cancellation);
                    if (block == null)
                        throw new GraphQLInputException(
                            $"Filter value '{nameof(atBlock)}.{nameof(atBlock.Level).ToCamelCase()}' = {atBlock.Level} is invalid. It is either too far in past or in future."
                        );

                    return new(block.Timestamp, ExcludeHistory: false, validity);
            }
        }

        public RecordFilterContext GetCurrent(RecordValidity validity)
            => new(_timeService.UtcNow, ExcludeHistory: true, validity);
    }
}