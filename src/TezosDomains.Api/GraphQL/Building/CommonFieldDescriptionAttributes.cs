﻿using System.ComponentModel;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Building
{
    public sealed class OperationGroupHashDescriptionAttribute : DescriptionAttribute
    {
        public OperationGroupHashDescriptionAttribute()
            : base("The hash of the operation group that contained this/last change.")
        {
        }
    }

    public sealed class ReverseRecordDescriptionAttribute : DescriptionFormatAttribute
    {
        public ReverseRecordDescriptionAttribute(string sourceAddressProperty)
            : base("{0} corresponding to {1:camel}.", nameof(ReverseRecord), sourceAddressProperty)
        {
        }
    }
}