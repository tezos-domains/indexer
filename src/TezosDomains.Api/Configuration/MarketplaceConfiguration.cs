﻿using System;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Api.Configuration
{
    public sealed class MarketplaceConfiguration : IConfigurationInstance
    {
        public TimeSpan DomainIsExpiringThreshold { get; init; }

        object IConfigurationInstance.GetDiagnosticInfo() => this;
    }
}