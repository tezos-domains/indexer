﻿using System;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Api.Configuration
{
    public sealed class IndexerConfiguration : IConfigurationInstance
    {
        public TimeSpan LatestBlockDelay { get; init; }

        object IConfigurationInstance.GetDiagnosticInfo() => this;
    }
}