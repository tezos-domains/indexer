﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Api.Configuration
{
    public sealed class CorsConfiguration : IConfigurationInstance
    {
        [Required]
        public IReadOnlyList<string> AllowedOrigins { get; init; } = null!; // If non-null readonly -> ASP.NET binder doesn't set it.

        object IConfigurationInstance.GetDiagnosticInfo() => this;
    }
}