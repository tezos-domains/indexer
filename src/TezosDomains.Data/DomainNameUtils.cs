﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TezosDomains.Data
{
    public static class DomainNameUtils
    {
        private const char LabelSeparator = '.';

        public static int GetDomainLevel(string domain) => domain.Count(c => c == LabelSeparator) + 1;

        public static (string Label, string? ParentName, string[] Ancestors, int Level, string NameForOrdering) Parse(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Value cannot be null or empty.", nameof(name));

            var ancestors = new List<string>();
            var labels = name.Split(LabelSeparator);

            ValidateLabels(labels);

            var label = labels[0];
            var reversedLabels = labels.Reverse().ToArray();
            reversedLabels
                .Aggregate(
                    (parent, currentLabel) =>
                    {
                        ancestors.Add(parent);
                        return $"{currentLabel}{LabelSeparator}{parent}";
                    }
                );

            var nameForOrdering = string.Join(LabelSeparator, ChangeLabelsPositionForOrdering(reversedLabels));
            return (label, ancestors.LastOrDefault(), ancestors.ToArray(), labels.Length, nameForOrdering);
        }

        // For correct domain ordering we need to switch the labels position
        // we want to order alphabetically based on the 2nd level domain, ie.: alice.org, page.alice.org, wiki.page.alice.org, bob.com, page.bob.com
        // to get desired ordering we reverse labels and switch the last two positions, ie: 3[alice].4[org].2[page].1[wiki]
        private static string[] ChangeLabelsPositionForOrdering(string[] reversedLabels)
            => reversedLabels.Length switch
            {
                1 => reversedLabels,
                2 => new[] { reversedLabels[1], reversedLabels[0] },
                _ => new[] { reversedLabels[1], reversedLabels[0] }.Concat(reversedLabels[2..]).ToArray()
            };

        private static void ValidateLabels(string[] labels)
        {
            if (labels.Length == 0)
                throw new ArgumentException("Name must contain at least one label.");

            if (labels.Any(string.IsNullOrEmpty))
                throw new ArgumentException("Name must only contain non-empty labels.");
        }
    }
}