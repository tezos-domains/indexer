﻿using System;
using System.Collections.Generic;

namespace TezosDomains.Data.Models.Marketplace
{
    public sealed record DomainData(
        string Name,
        string Owner,
        IReadOnlyList<string> Operators,
        IReadOnlyDictionary<string, string> Data,
        DateTime ExpiresAtUtc
    )
    {
        public static DomainData Create(Domain domain) => new(domain.Name, domain.Owner, domain.Operators, domain.Data, domain.ExpiresAtUtc);
    }
}