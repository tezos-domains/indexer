﻿namespace TezosDomains.Data.Models.Marketplace
{
    public sealed record OfferKey(string Address, int TokenId, OfferType Type);
}