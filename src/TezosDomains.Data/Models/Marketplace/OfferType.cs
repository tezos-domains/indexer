﻿namespace TezosDomains.Data.Models.Marketplace;

public enum OfferType
{
    SellOffer, // proposed by a seller.
    BuyOffer // proposed by buyer to the seller.
}