﻿namespace TezosDomains.Data.Models.Marketplace
{
    /// <summary>
    /// Offer presence in offer contract storage
    /// Present - offer is stored in the big map
    /// Executed - offer was removed from big map using Execute entrypoint
    /// Removed - offer was removed from big map using Remove entrypoint
    /// </summary>
    public enum OfferBlockchainPresence
    {
        Present,
        Removed,
        Executed,
    }
}