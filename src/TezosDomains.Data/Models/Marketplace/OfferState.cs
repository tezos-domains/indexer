﻿namespace TezosDomains.Data.Models.Marketplace
{
    public enum OfferState
    {
        Active,
        Removed,
        Executed,
        DomainExpired,
        OfferExpired,
        OfferSellerDomainOwnerMismatch,
        DomainOperatorsContractMissing,
        DomainDoesNotExist,
        DomainIsExpiringSoon
    }
}