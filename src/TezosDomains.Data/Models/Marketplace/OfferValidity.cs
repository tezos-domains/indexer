﻿namespace TezosDomains.Data.Models.Marketplace
{
    public enum OfferValidity
    {
        Valid,
        DomainDoesNotExist,
        DomainOperatorsContractMissing,
        OfferSellerDomainOwnerMismatch,
    }
}