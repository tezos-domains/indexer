﻿using System;
using System.Linq;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models.Marketplace
{
    public sealed record Offer(
        [property: DocumentKey] string OfferId,
        BlockSlim Block,
        string OperationGroupHash,
        OfferType Type,
        string InitiatorAddress,
        string TokenContract,
        int TokenId,
        DateTime CreatedAtUtc,
        DateTime ExpiresAtUtc,
        decimal PriceWithoutFee,
        decimal Fee,
        OfferBlockchainPresence Presence,
        DomainData? Domain,
        OfferValidity Validity,
        string? AcceptorAddress,
        int ValidUntilBlockLevel,
        DateTime ValidUntilTimestamp
    ) : HasHistory<Offer>
    {
        public decimal Price => PriceWithoutFee + Fee;

        public static string GenerateId(OfferType type, string initiatorAddress, int tokenId, int startedAtLevel)
        {
            var prefix = type == OfferType.BuyOffer ? "b:" : string.Empty;
            return $"{prefix}{tokenId}:{initiatorAddress}:{startedAtLevel}";
        }

        public static OfferValidity CalculateValidity(
            string marketplaceContract,
            string initiatorAddress,
            string? acceptorAddress,
            DomainData? domain,
            OfferType offerType
        )
            => (offerType, domain) switch
            {
                (_, null) => OfferValidity.DomainDoesNotExist,
                (OfferType.SellOffer, domain: { Owner: var owner }) when owner != initiatorAddress => OfferValidity.OfferSellerDomainOwnerMismatch,
                (OfferType.BuyOffer, domain: { Owner: var owner }) when owner != acceptorAddress => OfferValidity.OfferSellerDomainOwnerMismatch,
                (OfferType.SellOffer, domain: { Operators: var ops }) when !ops.Contains(marketplaceContract) => OfferValidity.DomainOperatorsContractMissing,
                _ => OfferValidity.Valid
            };

        public OfferState GetState(DateTime atTimestamp, TimeSpan domainIsExpiringThreshold)
            => (Presence, Validity) switch
            {
                (OfferBlockchainPresence.Removed, _) => OfferState.Removed,
                (OfferBlockchainPresence.Executed, _) => OfferState.Executed,

                (OfferBlockchainPresence.Present, _) when atTimestamp > ExpiresAtUtc => OfferState.OfferExpired,
                (OfferBlockchainPresence.Present, _) when atTimestamp > Domain?.ExpiresAtUtc => OfferState.DomainExpired,
                (OfferBlockchainPresence.Present, OfferValidity.Valid)
                    when Domain?.ExpiresAtUtc != DateTime.MaxValue && atTimestamp > Domain?.ExpiresAtUtc.Add(-domainIsExpiringThreshold)
                    => OfferState.DomainIsExpiringSoon,

                (OfferBlockchainPresence.Present, OfferValidity.DomainDoesNotExist) => OfferState.DomainDoesNotExist,
                (OfferBlockchainPresence.Present, OfferValidity.OfferSellerDomainOwnerMismatch) => OfferState.OfferSellerDomainOwnerMismatch,
                (OfferBlockchainPresence.Present, OfferValidity.DomainOperatorsContractMissing) => OfferState.DomainOperatorsContractMissing,

                (OfferBlockchainPresence.Present, _) => OfferState.Active,

                _ => throw new InvalidOperationException($"Inconsistent offer state ({this})")
            };
    }
}