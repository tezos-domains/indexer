﻿using System;
using System.Collections.Generic;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models.Auctions
{
    public sealed record BidderBalances(
        BlockSlim Block,
        string OperationGroupHash,
        [property: DocumentKey] string Address,
        IReadOnlyDictionary<string, decimal> Balances,
        int ValidUntilBlockLevel,
        DateTime ValidUntilTimestamp
    ) : HasHistory<BidderBalances>;
}