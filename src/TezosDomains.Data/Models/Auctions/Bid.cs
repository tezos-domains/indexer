﻿namespace TezosDomains.Data.Models.Auctions
{
    public sealed record Bid(
        BlockSlim Block,
        string OperationGroupHash,
        string Bidder,
        decimal Amount
    ) : IHasBlock, IHasOperationGroupHash;
}