﻿using System;
using System.Collections.Generic;
using System.Linq;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models.Auctions
{
    public sealed record Auction(
        [property: DocumentKey] string AuctionId,
        BlockSlim Block,
        string OperationGroupHash,
        string DomainName,
        IReadOnlyList<Bid> Bids,
        DateTime EndsAtUtc,
        DateTime OwnedUntilUtc,
        bool IsSettled,
        DateTime FirstBidAtUtc,
        int BidCount,
        Bid HighestBid,
        int CountOfUniqueBidders,
        int ValidUntilBlockLevel,
        DateTime ValidUntilTimestamp
    ) : HasHistory<Auction>
    {
        public decimal BidAmountSum => Bids.Sum(b => b.Amount);

        public static string GenerateId(string domainName, int startedAtLevel)
            => $"{domainName}:{startedAtLevel}";

        public int StartedAtLevel
        {
            get
            {
                var segments = AuctionId.Split(":");
                return segments.Length == 2 && int.TryParse(segments[1], out var startedAtLevel)
                    ? startedAtLevel
                    : throw new InvalidOperationException($"AuctionId '{AuctionId}' is not in correct format: {{domainName}}:{{startedAtLevel}}");
            }
        }

        public AuctionState GetState(DateTime atUtc)
        {
            if (IsSettled)
                return AuctionState.Settled;

            if (atUtc < EndsAtUtc)
                return AuctionState.InProgress;

            if (atUtc < OwnedUntilUtc)
                return AuctionState.CanBeSettled;

            return AuctionState.SettlementExpired;
        }
    }
}