﻿using System.ComponentModel;
using TezosDomains.Utils;

namespace TezosDomains.Data.Models.Auctions
{
    [GraphQLOrderAsDeclared]
    [Description("The auction state is based on its launch.")]
    public enum AuctionState
    {
        [Description("The auction is in progress, and users can make a bid.")]
        InProgress,

        [Description("The auction has its winner who can settle it within a predefined period.")]
        CanBeSettled,

        [Description("The auction has been settled by its winner.")]
        Settled,

        [Description("The auction has not been settled by its winner within a predefined period.")]
        SettlementExpired
    }
}