﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models
{
    public interface IHasDomainDetails
    {
        string DomainName { get; }
        string DomainOwnerAddress { get; }
        string? DomainForwardRecordAddress { get; }
        IReadOnlyDictionary<string, string> Data { get; }
    }
}