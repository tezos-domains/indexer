﻿using System;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models
{
    public interface IHasHistory : IHasDbCollection, IHasBlock, IHasOperationGroupHash
    {
        // <summary>Generated, unique regardless of historical version.</summary>
        string VersionId { get; }

        /// <summary>EXCLUSIVE - Valid until this block level.</summary>
        int ValidUntilBlockLevel { get; }

        /// <summary>EXCLUSIVE - Valid until this block timestamp.</summary>
        DateTime ValidUntilTimestamp { get; }
    }

    public abstract record HasHistory : IHasHistory
    {
        public abstract string VersionId { get; }

        public abstract BlockSlim Block { get; init; }
        public abstract int ValidUntilBlockLevel { get; init; }
        public abstract DateTime ValidUntilTimestamp { get; init; }
        public abstract string OperationGroupHash { get; init; }
    }

    public abstract record HasHistory<TThis> : HasHistory
        where TThis : HasHistory<TThis>
    {
        public sealed override string VersionId
        {
            get
            {
                var key = DocumentHelper<TThis>.GetKey((TThis)this);
                return $"{key}:{Block.Level}";
            }
        }
    }
}