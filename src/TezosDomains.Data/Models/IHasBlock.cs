﻿namespace TezosDomains.Data.Models
{
    public interface IHasBlock
    {
        BlockSlim Block { get; }
    }
}