﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TezosDomains.Data.Models
{
    public static class UtilityExtensions
    {
        public static Exception CreateInvalidError<TEnum>(this TEnum value)
            where TEnum : Enum
            => new InvalidEnumArgumentException($"Value {value} of enum {typeof(TEnum)} is invalid for particular use case.");

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Task<T?> AsNullableResult<T>(this Task<T> task)
#pragma warning disable CS8619 // Nullability of reference types in value doesn't match target type.
            => task;
#pragma warning restore CS8619
    }
}