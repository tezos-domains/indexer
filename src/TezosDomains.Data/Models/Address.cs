﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace TezosDomains.Data.Models
{
    public sealed class Address
    {
        public string Value { get; }

        public Address(string value)
        {
            // TODO validate address
            Value = value;
        }

        public override string ToString() => Value;

        [return: NotNullIfNotNull("value")]
        public static implicit operator Address?(string? value)
            => value != null ? new Address(value) : null;

        [return: NotNullIfNotNull("address")]
        public static implicit operator string?(Address? address)
            => address?.Value;
    }

    [Description("Address prefix which specifies its kind.")]
    public enum AddressPrefix
    {
        [Description("Prefix `tz1` used for user addresses.")]
        tz1,

        [Description("Prefix `tz2` used for user addresses.")]
        tz2,

        [Description("Prefix `tz3` used for user addresses.")]
        tz3,

        [Description("Prefix `KT1` used for smart contract addresses.")]
        KT1,

        [Description("Prefix `txr1` used for originated tx rollup.")]
        txr1,

        [Description("Prefix `sr1` used for smart rollup addresses.")]
        sr1,
    }
}