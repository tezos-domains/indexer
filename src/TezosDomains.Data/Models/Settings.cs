﻿namespace TezosDomains.Data.Models
{
    public sealed record Settings(
        string TezosChainId
    ) : IHasDbCollection;
}