﻿using System;

namespace TezosDomains.Data.Models
{
    public interface IRecord : IHasHistory
    {
        DateTime ExpiresAtUtc { get; }
        string? ValidityKey { get; }
    }

    public abstract record Record<TThis> : HasHistory<TThis>, IRecord
        where TThis : Record<TThis>
    {
        public abstract DateTime ExpiresAtUtc { get; init; }
        public abstract string? ValidityKey { get; init; }
    }
}