﻿using System;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models
{
    public sealed record ReverseRecord(
        BlockSlim Block,
        string OperationGroupHash,
        [property: DocumentKey] string Address,
        string? Name,
        string Owner,
        DateTime ExpiresAtUtc,
        string? ValidityKey,
        int ValidUntilBlockLevel,
        DateTime ValidUntilTimestamp
    ) : Record<ReverseRecord>;
}