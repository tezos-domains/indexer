﻿using System;

namespace TezosDomains.Data.Models
{
    public sealed record BlockSlim(
        int Level,
        string Hash,
        DateTime Timestamp
    );
}