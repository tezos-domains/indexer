﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record AuctionWithdrawEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string DestinationAddress,
        string OperationGroupHash,
        string TldName,
        decimal WithdrawnAmount
    ) : Event, IHasOperationGroupHash;
}