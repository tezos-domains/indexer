﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record DomainUpdateOperatorsEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        string[] Operators
    ) : Event, IHasOperationGroupHash;
}