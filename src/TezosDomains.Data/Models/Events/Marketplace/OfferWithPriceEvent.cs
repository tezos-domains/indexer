﻿namespace TezosDomains.Data.Models.Events.Marketplace;

public abstract record OfferWithPriceEvent(
    string Id,
    BlockSlim Block,
    string SourceAddress,
    string OperationGroupHash,
    int TokenId,
    decimal PriceWithoutFee,
    decimal Fee
) : OfferEvent(Id, Block, SourceAddress, OperationGroupHash, TokenId)
{
    public decimal Price => PriceWithoutFee + Fee;
}