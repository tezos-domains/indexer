﻿namespace TezosDomains.Data.Models.Events.Marketplace;

public sealed record BuyOfferExecutedEvent(
    string Id,
    BlockSlim Block,
    string SourceAddress, //SellerAddress for OfferType.BuyOffer
    string OperationGroupHash,
    int TokenId,
    string InitiatorAddress, //BuyerAddress for OfferType.BuyOffer
    decimal PriceWithoutFee,
    decimal Fee
) : OfferExecutedEvent(Id, Block, SourceAddress, OperationGroupHash, TokenId, InitiatorAddress, PriceWithoutFee, Fee);