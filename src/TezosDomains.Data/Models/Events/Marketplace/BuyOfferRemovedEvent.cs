﻿namespace TezosDomains.Data.Models.Events.Marketplace;

public sealed record BuyOfferRemovedEvent(
    string Id,
    BlockSlim Block,
    string SourceAddress,
    string OperationGroupHash,
    int TokenId
) : OfferRemovedEvent(Id, Block, SourceAddress, OperationGroupHash, TokenId);