﻿namespace TezosDomains.Data.Models.Events.Marketplace
{
    public record OfferRemovedEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        int TokenId
    ) : OfferEvent(Id, Block, SourceAddress, OperationGroupHash, TokenId);
}