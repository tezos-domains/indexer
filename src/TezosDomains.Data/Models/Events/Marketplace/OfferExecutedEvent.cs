﻿namespace TezosDomains.Data.Models.Events.Marketplace
{
    public record OfferExecutedEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress, //BuyerAddress
        string OperationGroupHash,
        int TokenId,
        string InitiatorAddress, //SellerAddress
        decimal PriceWithoutFee,
        decimal Fee
    ) : OfferWithPriceEvent(Id, Block, SourceAddress, OperationGroupHash, TokenId, PriceWithoutFee, Fee);
}