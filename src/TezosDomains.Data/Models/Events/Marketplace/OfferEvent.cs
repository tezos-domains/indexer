﻿namespace TezosDomains.Data.Models.Events.Marketplace
{
    public abstract record OfferEvent
    (
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        int TokenId,
        string DomainName = ""
    ) : Event, IHasOperationGroupHash;
}