﻿using System;

namespace TezosDomains.Data.Models.Events.Marketplace
{
    public record OfferUpdatedEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress, //SellerAddress
        string OperationGroupHash,
        int TokenId,
        decimal PriceWithoutFee,
        decimal Fee,
        DateTime ExpiresAtUtc
    ) : OfferPlacedEvent(Id, Block, SourceAddress, OperationGroupHash, TokenId, PriceWithoutFee, Fee, ExpiresAtUtc);
}