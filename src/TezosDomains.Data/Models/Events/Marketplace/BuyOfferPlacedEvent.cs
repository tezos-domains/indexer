﻿using System;

namespace TezosDomains.Data.Models.Events.Marketplace;

public record BuyOfferPlacedEvent(
    string Id,
    BlockSlim Block,
    string SourceAddress, //BuyerAddress
    string OperationGroupHash,
    int TokenId,
    decimal PriceWithoutFee,
    decimal Fee,
    DateTime ExpiresAtUtc,
    string DomainOwner = "" //is filled in OfferUpdateEventProcessor
) : OfferPlacedEvent(Id, Block, SourceAddress, OperationGroupHash, TokenId, PriceWithoutFee, Fee, ExpiresAtUtc);