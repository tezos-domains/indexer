﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record ReverseRecordClaimEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string? Name,
        string ReverseRecordOwnerAddress
    ) : Event, IHasOperationGroupHash;
}