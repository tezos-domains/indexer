﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record DomainRenewEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        decimal Price,
        int DurationInDays
    ) : Event, IHasOperationGroupHash;
}