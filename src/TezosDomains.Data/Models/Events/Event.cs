﻿using System;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models.Events
{
    public abstract record Event : IHasDbCollection, IHasBlock
    {
        [DocumentKey]
        public abstract string Id { get; init; }

        public abstract BlockSlim Block { get; init; }
        public abstract string SourceAddress { get; init; }

        public static string GenerateId() => Guid.NewGuid().ToString("N");
    }
}