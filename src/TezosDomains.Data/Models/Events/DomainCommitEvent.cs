﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record DomainCommitEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string CommitmentHash
    ) : Event, IHasOperationGroupHash;
}