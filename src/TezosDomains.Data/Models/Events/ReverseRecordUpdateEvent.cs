﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record ReverseRecordUpdateEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string? Name,
        string ReverseRecordOwnerAddress,
        string ReverseRecordAddress
    ) : Event, IHasOperationGroupHash;
}