﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record DomainTransferEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        string NewOwner
    ) : Event, IHasOperationGroupHash;
}