﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public sealed record AuctionSettleEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        string DomainOwnerAddress,
        string? DomainForwardRecordAddress,
        IReadOnlyDictionary<string, string> Data,
        int RegistrationDurationInDays,
        decimal WinningBid
    ) : Event, IHasOperationGroupHash;
}