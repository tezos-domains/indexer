﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public sealed record DomainSetChildRecordEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        string DomainOwnerAddress,
        string? DomainForwardRecordAddress,
        IReadOnlyDictionary<string, string> Data,
        bool IsNewRecord
    ) : Event, IHasOperationGroupHash, IHasDomainDetails;
}