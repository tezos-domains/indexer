﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public sealed record AuctionEndEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress, // WinnerAddress
        string DomainName,
        decimal WinningBid,
        IReadOnlyList<string> Participants
    ) : Event;
}