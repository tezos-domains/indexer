﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record OutgoingMoneyTransactionEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string DestinationAddress,
        string OperationGroupHash,
        decimal WithdrawnAmount
    ) : Event, IHasOperationGroupHash;
}