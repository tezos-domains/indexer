﻿namespace TezosDomains.Data.Models.Events
{
    /// <summary>
    /// Claimed event using a DNSRegistrar contract
    /// </summary>
    public sealed record DomainClaimEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        string DomainOwnerAddress,
        string? DomainForwardRecordAddress
    ) : Event, IHasOperationGroupHash;
}