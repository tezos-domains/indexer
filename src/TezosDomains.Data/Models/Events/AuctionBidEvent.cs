﻿namespace TezosDomains.Data.Models.Events
{
    public sealed record AuctionBidEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        decimal BidAmount,
        decimal TransactionAmount,
        string? PreviousBidderAddress,
        decimal? PreviousBidAmount
    ) : Event, IHasOperationGroupHash
    {
        public AuctionBidEvent WithPreviousBid(string bidderAddress, decimal amount)
            => this with { PreviousBidderAddress = bidderAddress, PreviousBidAmount = amount };
    }
}