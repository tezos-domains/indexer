﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public sealed record DomainBuyEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        decimal Price,
        int DurationInDays,
        string DomainOwnerAddress,
        string? DomainForwardRecordAddress,
        IReadOnlyDictionary<string, string> Data
    ) : Event, IHasOperationGroupHash;
}