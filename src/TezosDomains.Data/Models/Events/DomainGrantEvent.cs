﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public record DomainGrantEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        int? DurationInDays,
        string DomainOwnerAddress,
        string? DomainForwardRecordAddress,
        IReadOnlyDictionary<string, string> Data
    ) : Event, IHasOperationGroupHash;
}