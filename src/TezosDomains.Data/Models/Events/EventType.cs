﻿using System.ComponentModel;

namespace TezosDomains.Data.Models.Events
{
    [Description("Event types.")]
    public enum EventType
    {
        [Description("An event triggered by a user's bid to an auction.")]
        AuctionBidEvent,

        [Description("An event triggered by a block with a timestamp greater or equal to an auction's end.")]
        AuctionEndEvent,

        [Description("An event triggered by a user's auction settlement action.")]
        AuctionSettleEvent,

        [Description("An event triggered by a user's withdrawal action.")]
        AuctionWithdrawEvent,

        [Description("An event triggered by a user buying a domain.")]
        DomainBuyEvent,

        [Description("An event triggered by a user renewing a domain.")]
        DomainRenewEvent,

        [Description("An event triggered by a user updating domain's data.")]
        DomainUpdateEvent,

        [Description("An event triggered by a user creating a subdomain.")]
        DomainSetChildRecordEvent,

        [Description("An event triggered by a user's commit transaction during the domain buying process.")]
        DomainCommitEvent,

        [Description("An event triggered by a user claiming a reverse record.")]
        ReverseRecordClaimEvent,

        [Description("An event triggered by a user updating a reverse record.")]
        ReverseRecordUpdateEvent,

        [Description("An event triggered by a sunrise period granting a domain.")]
        DomainGrantEvent,

        [Description("An event triggered by a user transferring a domain.")]
        DomainTransferEvent,

        [Description("An event triggered by a user updating operators for a domain.")]
        DomainUpdateOperatorsEvent,

        [Description("An event triggered by a user creating an offer to sell a domain.")]
        OfferPlacedEvent,

        [Description("An event triggered by a user updating an offer to sell a domain.")]
        OfferUpdatedEvent,

        [Description("An event triggered by a user accepting an existing offer to buy a domain.")]
        OfferExecutedEvent,

        [Description("An event triggered by a user removing his offer to sell a domain.")]
        OfferRemovedEvent,

        [Description("An event triggered by a user claiming a DNS domain.")]
        DomainClaimEvent,

        [Description("An event triggered by a user creating an offer to buy a domain.")]
        BuyOfferPlacedEvent,

        [Description("An event triggered by a user accepting an existing offer to buy a domain.")]
        BuyOfferExecutedEvent,

        [Description("An event triggered by a user removing his offer to buy a domain.")]
        BuyOfferRemovedEvent,
    }
}