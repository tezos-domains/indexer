﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public record DomainUpdateEvent(
        string Id,
        BlockSlim Block,
        string SourceAddress,
        string OperationGroupHash,
        string DomainName,
        string DomainOwnerAddress,
        string? DomainForwardRecordAddress,
        IReadOnlyDictionary<string, string> Data
    ) : Event, IHasOperationGroupHash, IHasDomainDetails;
}