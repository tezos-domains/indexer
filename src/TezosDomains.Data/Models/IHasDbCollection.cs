﻿namespace TezosDomains.Data.Models
{
    /// <summary>
    /// Marks a MongoDB document which has a collection.
    /// </summary>
    public interface IHasDbCollection
    {
    }
}