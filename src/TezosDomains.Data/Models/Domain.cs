﻿using System;
using System.Collections.Generic;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models
{
    public sealed record Domain(
        BlockSlim Block,
        string OperationGroupHash,
        [property: DocumentKey] string Name,
        string? Address,
        string Owner,
        IReadOnlyDictionary<string, string> Data,
        int? TokenId,
        string Label,
        IReadOnlyList<string> Ancestors,
        string? ParentName,
        string? ParentOwner,
        DateTime ExpiresAtUtc,
        string? ValidityKey,
        int Level,
        string NameForOrdering,
        IReadOnlyList<string> Operators,
        Auction? LastAuction,
        int ValidUntilBlockLevel,
        DateTime ValidUntilTimestamp
    ) : Record<Domain>;
}