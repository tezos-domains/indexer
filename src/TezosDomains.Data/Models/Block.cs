﻿using System;
using System.Diagnostics.CodeAnalysis;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Models
{
    // intentionally not inherited from BlockSlim - we only want to store Block in Block collection
    public sealed record Block(
        int Level,
        [property: DocumentKey] string Hash,
        DateTime Timestamp,
        string Predecessor
    ) : IHasDbCollection
    {
        public BlockSlim ToSlim() => new BlockSlim(Level, Hash, Timestamp);

        [return: NotNullIfNotNull(parameterName: "block")]
        public static implicit operator BlockSlim?(Block? block) => block?.ToSlim();
    }
}