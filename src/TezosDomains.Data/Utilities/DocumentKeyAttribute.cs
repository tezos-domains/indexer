﻿using System;

namespace TezosDomains.Data.Utilities
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DocumentKeyAttribute : Attribute
    {
    }
}