﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Data.Utilities
{
    public static class DocumentHelper
    {
        public static string GetKey<TDocument>(this TDocument document)
            where TDocument : IHasDbCollection
            => DocumentHelper<TDocument>.GetKey(document);
    }

    // Values are cached only once per document type.
    public static class DocumentHelper<TDocument>
        where TDocument : IHasDbCollection
    {
        public static readonly Expression<Func<TDocument, string>> KeyExpression;
        public static readonly Func<TDocument, string> GetKey;
        public static readonly string KeyPropertyName;

        static DocumentHelper()
        {
            try
            {
                var keyProperties = typeof(TDocument).GetProperties().Where(p => p.GetCustomAttribute<DocumentKeyAttribute>() != null).ToList();
                if (keyProperties.Count != 1 || keyProperties[0].PropertyType != typeof(string))
                    throw new Exception(
                        $"Expected single string property with ${nameof(DocumentKeyAttribute)} but there are [{keyProperties.Join()}]."
                        + $" Is {typeof(TDocument)} correct final document class?"
                    );

                var param = Expression.Parameter(typeof(TDocument));
                KeyExpression = Expression.Lambda<Func<TDocument, string>>(Expression.Property(param, keyProperties[0]), param);
                GetKey = KeyExpression.Compile();
                KeyPropertyName = keyProperties[0].Name;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed init of {typeof(TDocument)}.", ex);
            }
        }
    }
}