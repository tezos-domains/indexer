﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Linq;
using System.Net.Mime;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using TezosDomains.Common.OneTimePassword;
using TezosDomains.Utils.Json;

namespace TezosDomains.Common.Health
{
    internal sealed class HealthResponseBuilder
    {
        private static readonly JsonSerializerOptions JsonOptions = new()
        {
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
            WriteIndented = true,
            Converters = { new JsonStringEnumConverter(), new SimpleExceptionConverter(), new TimeSpanJsonConverter() },
        };

        private readonly OneTimePasswordGuard _oneTimePasswordGuard;

        public HealthResponseBuilder(OneTimePasswordGuard oneTimePasswordGuard)
            => _oneTimePasswordGuard = oneTimePasswordGuard;

        public async Task WriteAsync(HttpContext context, HealthReport report)
        {
            var allowDetails = _oneTimePasswordGuard.IsValid(context.Request.Query);
            Func<object?, object?> sanitizeDetails = d => allowDetails ? d : "NOT AVAILABLE";

            var resultObj = new
            {
                status = report.Status,
                duration = sanitizeDetails(report.TotalDuration),
                entries = report.Entries.ToDictionary(
                    entry => entry.Key,
                    entry => new
                    {
                        status = entry.Value.Status,
                        description = sanitizeDetails(entry.Value.Description),
                        duration = sanitizeDetails(entry.Value.Duration),
                        data = sanitizeDetails(entry.Value.Data),
                        error = sanitizeDetails(entry.Value.Exception),
                    }
                )
            };
            var resultJsonBytes = JsonSerializer.SerializeToUtf8Bytes(resultObj, JsonOptions);

            context.Response.SetUtf8ContentType(MediaTypeNames.Application.Json);
            await context.Response.Body.WriteAsync(resultJsonBytes, context.RequestAborted);
        }
    }
}