﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.DependencyInjection;

namespace TezosDomains.Common.Health
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseHealth(this IApplicationBuilder builder)
        {
            var responseBuilder = builder.ApplicationServices.GetRequiredService<HealthResponseBuilder>();
            return builder.UseHealthChecks(
                path: "/health",
                new HealthCheckOptions { ResponseWriter = responseBuilder.WriteAsync }
            );
        }
    }
}