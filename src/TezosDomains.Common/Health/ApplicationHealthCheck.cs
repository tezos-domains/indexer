﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Common.Health
{
    public class ApplicationHealthCheck : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellation)
        {
            return Task.FromResult(
                HealthCheckResult.Healthy(
                    data: new Dictionary<string, object>
                    {
                        ["version"] = AppVersionInfo.Version,
                        ["commit"] = AppVersionInfo.ShortGitHash,
                        ["hostname"] = Environment.MachineName
                    }
                )
            );
        }
    }
}