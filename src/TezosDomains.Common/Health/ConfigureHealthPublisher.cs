﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

namespace TezosDomains.Common.Health
{
    public sealed class ConfigureHealthPublisher : IConfigureOptions<HealthCheckPublisherOptions>
    {
        private readonly IConfiguration _configuration;

        public ConfigureHealthPublisher(IConfiguration configuration)
            => _configuration = configuration;

        public void Configure(HealthCheckPublisherOptions options)
        {
            _configuration.Bind("Health", options);
        }
    }
}