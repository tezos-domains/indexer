﻿namespace TezosDomains.Common;

public static class IntExtensions
{
    public static int? ConvertMaxToNull(this int value) => value == int.MaxValue ? null : value;
    public static int ConvertNullToMax(this int? value) => value ?? int.MaxValue;
}