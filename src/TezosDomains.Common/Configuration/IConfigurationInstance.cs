﻿namespace TezosDomains.Common.Configuration
{
    public interface IConfigurationInstance
    {
        string Name => GetType().Name;

        object GetDiagnosticInfo();
    }

    public sealed record ConfigurationWrapper(
        string Name,
        object DiagnosticInfo
    ) : IConfigurationInstance
    {
        string IConfigurationInstance.Name => Name;
        object IConfigurationInstance.GetDiagnosticInfo() => DiagnosticInfo;
    }
}