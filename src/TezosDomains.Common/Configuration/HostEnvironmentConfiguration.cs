﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using TezosDomains.Utils;

namespace TezosDomains.Common.Configuration
{
    public sealed class HostEnvironmentConfiguration : IConfigurationInstance
    {
        private const string TezosNetworkArgName = "TEZOS_NETWORK";

        private readonly object _info;

        public HostEnvironmentConfiguration(IWebHostEnvironment env, IConfiguration configuration)
            => _info = new
            {
                env.ApplicationName,
                env.EnvironmentName,
                env.WebRootPath,
                env.ContentRootPath,
                TezosNetwork = configuration.GetSection(TezosNetworkArgName).Get<string?>().WhiteSpaceToNull()
            };

        public object GetDiagnosticInfo() => _info;

        public static string? GetHostTezosNetwork(IConfiguration configuration)
            => configuration.GetSection(TezosNetworkArgName).Get<string?>().WhiteSpaceToNull();
    }
}