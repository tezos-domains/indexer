﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net.Mime;
using System.Text.Json;
using System.Text.Json.Serialization;
using TezosDomains.Common.OneTimePassword;

namespace TezosDomains.Common.Configuration
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseConfigurationDiagnostics(this IApplicationBuilder app)
        {
            var configList = app.ApplicationServices.GetServices<IConfigurationInstance>().ToList();
            var configDictionary = configList.ToDictionary(c => c.Name, c => c.GetDiagnosticInfo());
            var logger = app.ApplicationServices.GetRequiredService<ILogger<IApplicationBuilder>>();

            foreach (var (type, config) in configDictionary)
                logger.LogInformation("Using {@configuration} of type {type}.", config, type);

            var jsonOptions = new JsonSerializerOptions
            {
                WriteIndented = true,
                Converters = { new JsonStringEnumConverter() },
            };
            var jsonBytes = JsonSerializer.SerializeToUtf8Bytes(configDictionary, jsonOptions);

            app.Map(
                pathMatch: (PathString)"/config",
                b => b
                    .UseOneTimePassword()
                    .Use(
                        async (context, next) =>
                        {
                            context.Response.SetUtf8ContentType(MediaTypeNames.Application.Json);
                            context.Response.ContentLength = jsonBytes.LongLength;
                            await context.Response.Body.WriteAsync(jsonBytes, context.RequestAborted);

                            await next(context);
                        }
                    )
            );
        }
    }
}