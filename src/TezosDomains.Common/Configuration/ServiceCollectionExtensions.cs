﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace TezosDomains.Common.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static void AddConfigurationWithDiagnostics<TConfiguration>(this IServiceCollection services, string sectionName)
            where TConfiguration : class, IConfigurationInstance, new()
        {
            services.AddSingleton(sp => sp.GetRequiredService<IConfiguration>().GetValid<TConfiguration>(sectionName));
            services.AddSingleton<IConfigurationInstance>(sp => sp.GetRequiredService<TConfiguration>());
        }

        public static void AddTezosDomainsConfigurationWithDiagnostics<TConfiguration>(this IServiceCollection services, string sectionName)
            where TConfiguration : class, IConfigurationInstance, new()
            => services.AddConfigurationWithDiagnostics<TConfiguration>($"TezosDomains:{sectionName}");

        public static void AddConfigurationWithDiagnostics<TConfiguration>(this IServiceCollection services, TConfiguration configuration)
            where TConfiguration : class, IConfigurationInstance
        {
            services.AddSingleton(configuration);
            services.AddSingleton<IConfigurationInstance>(configuration);
        }

        public static void AddConfigurationDiagnostics<TOptions>(this IServiceCollection services, Func<TOptions, object> getDiagnosticInfo)
            where TOptions : class, new()
            => services.AddSingleton<IConfigurationInstance>(
                serviceProvider =>
                {
                    var options = serviceProvider.GetRequiredService<IOptions<TOptions>>().Value;
                    return new ConfigurationWrapper(
                        Name: typeof(TOptions).Name,
                        DiagnosticInfo: getDiagnosticInfo(options)
                    );
                }
            );
    }
}