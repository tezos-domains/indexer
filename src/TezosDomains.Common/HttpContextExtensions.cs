﻿using Microsoft.AspNetCore.Http;

namespace TezosDomains.Common
{
    public static class HttpContextExtensions
    {
        public static void SetUtf8ContentType(this HttpResponse response, string contentType)
            => response.ContentType = $"{contentType}; charset=UTF-8";
    }
}