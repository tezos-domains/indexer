﻿using OtpNet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TezosDomains.Common.Configuration;
using TezosDomains.Utils;

namespace TezosDomains.Common.OneTimePassword
{
    public sealed class OneTimePasswordConfiguration : IValidatableObject, IConfigurationInstance
    {
        public bool IsEnabled { get; init; }
        public string Secret { get; init; } = "";
        public OtpHashMode Mode { get; init; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!IsEnabled)
                yield break;

            if (string.IsNullOrWhiteSpace(Secret))
                yield return new ValidationResult($"{nameof(Secret)} must be specified if {nameof(IsEnabled)} is true.");

            if (!Enum.IsDefined(Mode))
                yield return new ValidationResult($"{nameof(Mode)} is {Mode} but it must be one of: {Enum.GetValues<OtpHashMode>().Join()}.");
        }

        public object GetDiagnosticInfo() => new { IsEnabled, Mode };
    }
}