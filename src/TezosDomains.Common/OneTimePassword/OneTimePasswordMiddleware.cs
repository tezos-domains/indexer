﻿using Microsoft.AspNetCore.Http;
using System.Net;
using System.Threading.Tasks;

namespace TezosDomains.Common.OneTimePassword
{
    public sealed class OneTimePasswordMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly OneTimePasswordGuard _guard;

        public OneTimePasswordMiddleware(RequestDelegate next, OneTimePasswordGuard guard)
        {
            _next = next;
            _guard = guard;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (!_guard.IsValid(context.Request.Query))
            {
                context.Response.StatusCode = (int) HttpStatusCode.Forbidden;
                await context.Response.WriteAsync("Invalid OTP query parameter.", context.RequestAborted);
            }
            else
            {
                await _next(context);
            }
        }
    }
}