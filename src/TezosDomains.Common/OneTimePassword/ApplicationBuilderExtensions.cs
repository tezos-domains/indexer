﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace TezosDomains.Common.OneTimePassword
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseOneTimePassword(this IApplicationBuilder app)
        {
            var configuration = app.ApplicationServices.GetRequiredService<OneTimePasswordConfiguration>();

            return configuration.IsEnabled
                ? app.UseMiddleware<OneTimePasswordMiddleware>()
                : app;
        }
    }
}