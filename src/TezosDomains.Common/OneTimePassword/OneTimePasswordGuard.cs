﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using OtpNet;

namespace TezosDomains.Common.OneTimePassword
{
    public sealed class OneTimePasswordGuard
    {
        private readonly Totp? _totp;

        public OneTimePasswordGuard(OneTimePasswordConfiguration configuration)
            => _totp = configuration.IsEnabled
                ? new Totp(Base32Encoding.ToBytes(configuration.Secret), mode: configuration.Mode)
                : null;

        public bool IsValid(IQueryCollection query)
        {
            if (_totp == null)
                return true;

            var otp = query["otp"];
            return !StringValues.IsNullOrEmpty(otp) && _totp.VerifyTotp(otp, out _, VerificationWindow.RfcSpecifiedNetworkDelay);
        }
    }
}