﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using TezosDomains.Common.Configuration;
using TezosDomains.Common.Health;
using TezosDomains.Common.Metrics;
using TezosDomains.Common.OneTimePassword;

namespace TezosDomains.Common
{
    public static class ServiceCollectionExtensions
    {
        public static void AddTezosDomainsCommon(this IServiceCollection services)
        {
            // Configuration.
            services.AddSingleton<IConfigurationInstance, HostEnvironmentConfiguration>();

            // OneTimePassword.
            services.AddTezosDomainsConfigurationWithDiagnostics<OneTimePasswordConfiguration>("OneTimePassword");
            services.AddSingleton<OneTimePasswordGuard>();

            // Metrics.
            services.AddTezosDomainsConfigurationWithDiagnostics<MetricsConfiguration>("Metrics");

            // Health.
            services.AddSingleton<IConfigureOptions<HealthCheckPublisherOptions>, ConfigureHealthPublisher>();
            services.AddSingleton<HealthResponseBuilder>();
            services.AddHealthChecks()
                .AddCheck<ApplicationHealthCheck>("Application");
        }
    }
}