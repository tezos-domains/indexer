﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Common.Metrics
{
    public sealed class MetricsConfiguration : IValidatableObject, IConfigurationInstance
    {
        /// <summary>
        /// Only enables/disables http endpoint.
        /// The metrics are collected always.
        /// </summary>
        public bool IsEnabled { get; init; }

        public string Endpoint { get; init; } = "";

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!IsEnabled)
                yield break;

            if (string.IsNullOrWhiteSpace(Endpoint))
                yield return new ValidationResult($"{nameof(Endpoint)} must be specified if {nameof(IsEnabled)} is true.");
        }

        public object GetDiagnosticInfo() => this;
    }
}