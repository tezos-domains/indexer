﻿using Microsoft.AspNetCore.Routing;
using Prometheus;

namespace TezosDomains.Common.Metrics
{
    public static class EndpointRouteBuilderExtensions
    {
        public static void MapMetricsIfEnabled(
            this IEndpointRouteBuilder endpoints,
            MetricsConfiguration configuration
        )
        {
            if (configuration.IsEnabled)
                endpoints.MapMetrics(configuration.Endpoint);
        }
    }
}