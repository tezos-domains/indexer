﻿using Prometheus;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TezosDomains.Utils;

namespace TezosDomains.Common.Metrics
{
    public abstract class MetricsDecorator<TService>
        where TService : notnull
    {
        private readonly TService _decorated;
        private readonly Counter _requestsCounter;
        private readonly Histogram _requestsDurationHistogram;

        protected MetricsDecorator(TService decorated, Counter requestsCounter, Histogram requestsDurationHistogram)
        {
            _decorated = decorated;
            _requestsCounter = requestsCounter;
            _requestsDurationHistogram = requestsDurationHistogram;
        }

        private string[] GetLabels<TDelegate>(Expression<TDelegate> action)
        {
            var type = _decorated.GetType().GetFormattedName();
            var method = action.ExtractMethodName();

            return new[] { type, method };
        }

        protected async Task<TResult> UpdateCounters<TResult>(Expression<Func<TService, Task<TResult>>> action)
        {
            var labels = GetLabels(action);
            _requestsCounter.WithLabels(labels).Inc();
            using (_requestsDurationHistogram.WithLabels(labels).NewTimer())
            {
                return await action.Compile()(_decorated);
            }
        }

        protected async Task UpdateCounters(Expression<Func<TService, Task>> action)
        {
            var labels = GetLabels(action);
            _requestsCounter.WithLabels(labels).Inc();
            using (_requestsDurationHistogram.WithLabels(labels).NewTimer())
            {
                await action.Compile()(_decorated);
            }
        }
    }
}