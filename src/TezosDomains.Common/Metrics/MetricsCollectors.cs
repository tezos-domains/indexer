﻿using Prometheus;

namespace TezosDomains.Common.Metrics
{
    public static class MetricsCollectors
    {
        public static Counter CreateRequestCounter(string name, string help)
            => Prometheus.Metrics.CreateCounter(name, help, labelNames: new[] { "type", "method" });

        public static Histogram CreateRequestDurationHistogram(string name, string help, double[]? buckets = null)
            => Prometheus.Metrics.CreateHistogram(name, help, new HistogramConfiguration() { Buckets = buckets, LabelNames = new[] { "type", "method" } });
    }
}