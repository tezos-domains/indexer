﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace TezosDomains.Common
{
    internal sealed class SimpleExceptionConverter : JsonConverter<Exception>
    {
        public override Exception? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            => throw new NotSupportedException();

        public override void Write(Utf8JsonWriter writer, Exception exception, JsonSerializerOptions options)
        {
            writer.WriteStartObject();

            writer.WriteString("message", exception.Message);
            writer.WriteString("type", exception.GetType().ToString());
            writer.WriteString("stackTrace", exception.StackTrace);
            writer.WritePropertyName("innerException");

            if (exception.InnerException != null)
                Write(writer, exception.InnerException, options);
            else
                writer.WriteNullValue();

            writer.WritePropertyName("data");
            JsonSerializer.Serialize(writer, exception.Data, options);

            writer.WriteEndObject();
        }
    }
}