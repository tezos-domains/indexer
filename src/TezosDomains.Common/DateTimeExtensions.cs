﻿using System;

namespace TezosDomains.Common;

public static class DateTimeExtensions
{
    public static DateTime? ConvertMaxToNull(this DateTime dateTime) => dateTime == DateTime.MaxValue ? null : dateTime;
    public static DateTime ConvertNullToMax(this DateTime? dateTime) => dateTime ?? DateTime.MaxValue;
}