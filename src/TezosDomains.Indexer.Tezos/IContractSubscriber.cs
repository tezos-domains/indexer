﻿using System;
using System.Collections.Generic;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos
{
    public interface IContractSubscriber
    {
        IEnumerable<IBlockUpdate> OnEntrypoint(OnEntrypointArguments args) => Array.Empty<IBlockUpdate>();
        IEnumerable<IBlockUpdate> OnBigMapDiff(OnBigMapDiffArguments args) => Array.Empty<IBlockUpdate>();
        IEnumerable<IBlockUpdate> OnOutgoingMoneyTransaction(OnOutgoingTransactionArguments args) => Array.Empty<IBlockUpdate>();
    }

    public sealed record OnEntrypointArguments(
        TezosBlock Block,
        string OperationGroupHash,
        string EntrypointName,
        string SourceAddress,
        decimal Amount,
        IReadOnlyDictionary<string, IMichelsonValue> PropertiesByName,
        IReadOnlyDictionary<string, IMichelsonValue> StoragePropertiesByName,
        string? RootOperationSource = null
    );

    public sealed record OnBigMapDiffArguments(
        TezosBlock Block,
        string OperationGroupHash,
        string BigMapName,
        IMichelsonValue KeyProperty,
        IReadOnlyDictionary<string, IMichelsonValue> ValueProperty,
        IReadOnlyDictionary<string, IMichelsonValue> StoragePropertiesByName
    );

    public sealed record OnOutgoingTransactionArguments(
        TezosBlock Block,
        string OperationGroupHash,
        string SourceAddress,
        string DestinationAddress,
        decimal Amount,
        IReadOnlyDictionary<string, IMichelsonValue> StoragePropertiesByName
    );
}