﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Services
{
    internal sealed class BlockProcessor : IBlockProcessor
    {
        private readonly IContractDefinitionProvider _contractProvider;
        private readonly ISubscriberProvider _subscriberProvider;

        public BlockProcessor(IContractDefinitionProvider contractProvider, ISubscriberProvider subscriberProvider)
        {
            _contractProvider = contractProvider;
            _subscriberProvider = subscriberProvider;
        }

        public Task InitAsync(CancellationToken cancellation)
            => _contractProvider.InitAsync(cancellation);

        public IReadOnlyList<IBlockUpdate> Process(TezosBlock tezosBlock)
        {
            var updates = new List<IBlockUpdate>();
            foreach (var operationGroup in tezosBlock.OperationGroups)
            {
                var operationGroupHash = operationGroup.Hash.GuardNotNull();
                foreach (var operation in operationGroup.Operations)
                {
                    if (operation.Kind != Michelson.Const.Operation.Kind.Transaction
                        || operation.Metadata.GuardNotNull().OperationResult.GuardNotNull().Status != Michelson.Const.OperationResultStatus.Applied)
                        continue;

                    NotifySubscribers(operation, operationGroupHash, operation.Metadata.GuardNotNull().OperationResult.GuardNotNull());

                    foreach (var internalOperation in operation.Metadata.GuardNotNull().InternalOperations)
                    {
                        if (internalOperation.Kind != Michelson.Const.Operation.Kind.Transaction
                            || internalOperation.Result.GuardNotNull().Status != Michelson.Const.OperationResultStatus.Applied)
                            continue;

                        NotifySubscribers(internalOperation, operationGroupHash, internalOperation.Result.GuardNotNull(), operation.Source);
                    }
                }
            }

            return updates;

            void NotifySubscribers(Content operation, string operationGroupHash, OperationResult result, string? rootOperationSource = null)
            {
                var amount = decimal.Parse(operation.Amount.GuardNotNull());
                if (_contractProvider.SubscribedContracts.TryGetValue(operation.Destination.GuardNotNull(), out var contract))
                {
                    var entrypointName = operation.Parameters.GuardNotNull().Entrypoint.GuardNotNull();
                    var entrypointLayout = contract.EntrypointLayouts[entrypointName];

                    foreach (var subscriber in _subscriberProvider.Subscribers[contract.Address])
                    {
                        var args = new OnEntrypointArguments(
                            Block: tezosBlock,
                            OperationGroupHash: operationGroupHash,
                            EntrypointName: entrypointName,
                            SourceAddress: operation.Source.GuardNotNull(),
                            Amount: amount,
                            PropertiesByName: entrypointLayout.Apply(operation.Parameters.GuardNotNull().Value, enumerateValueWhenArray: true),
                            StoragePropertiesByName: contract.StorageProperties,
                            rootOperationSource
                        );
                        updates.AddRange(subscriber.OnEntrypoint(args));
                    }

                    foreach (var lazyStorageDiff in result.LazyStorageDiffs.NullToEmpty())
                    {
                        if (lazyStorageDiff.Kind != Michelson.Const.LazyStorageDiffKind.BigMap
                            || lazyStorageDiff.BigMapDiff.GuardNotNull().Action != Michelson.Const.BigMapDiffAction.Update)
                            continue;

                        var bigMapLayout = contract.BigMapLayouts[lazyStorageDiff.Id.GuardNotNull()];
                        foreach (var bigMapUpdate in lazyStorageDiff.BigMapDiff.GuardNotNull().Updates.NullToEmpty())
                        {
                            var (key, value) = bigMapLayout.Apply(bigMapUpdate);
                            foreach (var subscriber in _subscriberProvider.Subscribers[contract.Address])
                            {
                                var args = new OnBigMapDiffArguments(
                                    Block: tezosBlock,
                                    OperationGroupHash: operationGroupHash,
                                    BigMapName: bigMapLayout.Name,
                                    KeyProperty: key,
                                    ValueProperty: value,
                                    StoragePropertiesByName: contract.StorageProperties
                                );
                                updates.AddRange(subscriber.OnBigMapDiff(args));
                            }
                        }
                    }
                }

                if (amount > 0 && _contractProvider.SubscribedContracts.TryGetValue(operation.Source.GuardNotNull(), out contract))
                {
                    foreach (var subscriber in _subscriberProvider.Subscribers[contract.Address])
                    {
                        var args = new OnOutgoingTransactionArguments(
                            Block: tezosBlock,
                            OperationGroupHash: operationGroupHash,
                            SourceAddress: operation.Source.GuardNotNull(),
                            DestinationAddress: operation.Destination.GuardNotNull(),
                            Amount: amount,
                            StoragePropertiesByName: contract.StorageProperties
                        );
                        updates.AddRange(subscriber.OnOutgoingMoneyTransaction(args));
                    }
                }
            }
        }
    }
}