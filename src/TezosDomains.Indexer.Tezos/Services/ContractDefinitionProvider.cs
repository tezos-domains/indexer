﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos.Services
{
    internal interface IContractDefinitionProvider
    {
        IReadOnlyDictionary<string, Contract> SubscribedContracts { get; }

        Task InitAsync(CancellationToken cancellation);
    }

    internal sealed record Contract(
        IReadOnlyDictionary<string, BigMapLayout> BigMapLayouts,
        IReadOnlyDictionary<string, RecordLayout> EntrypointLayouts,
        string Address,
        IReadOnlyDictionary<string, IMichelsonValue> StorageProperties
    );

    internal sealed class ContractDefinitionProvider : IContractDefinitionProvider
    {
        private readonly TezosContractConfiguration _configuration;
        private readonly ITezosClient _tezosClient;
        private readonly ILayoutFactory _layoutFactory;
        private readonly ILogger _logger;
        private IReadOnlyDictionary<string, Contract>? _subscribedContracts;

        public ContractDefinitionProvider(
            TezosContractConfiguration configuration,
            ITezosClient tezosClient,
            ILayoutFactory layoutFactory,
            ILogger<ContractDefinitionProvider> logger
        )
        {
            _configuration = configuration;
            _tezosClient = tezosClient;
            _layoutFactory = layoutFactory;
            _logger = logger;
        }

        public IReadOnlyDictionary<string, Contract> SubscribedContracts
            => _subscribedContracts ?? throw new InvalidOperationException($"{nameof(InitAsync)} must be called first.");

        public async Task InitAsync(CancellationToken cancellation)
        {
            _logger.LogInformation("Contracts initialization ... STARTED");

            var storageByContracts = ParseLocalStorageConfiguration(_configuration.Storage);
            var subscribedContracts = await LoadContractDefinitionsAsync(storageByContracts, cancellation);

            _logger.LogInformation("Contracts initialization ... FINISHED");
            _subscribedContracts = subscribedContracts;
        }

        private async Task<Dictionary<string, Contract>?> LoadContractDefinitionsAsync(
            IReadOnlyDictionary<string, IReadOnlyDictionary<string, IMichelsonValue>> storageByContracts,
            CancellationToken cancellation
        )
        {
            var subscribedContracts = new Dictionary<string, Contract>();

            foreach (var (contractAddress, subscriptions) in _configuration.Subscriptions)
            {
                var contract = await LoadContractDefinition(
                    contractAddress,
                    storageByContracts.GetValueOrDefault(contractAddress, new Dictionary<string, IMichelsonValue>()),
                    cancellation
                );

                if (subscribedContracts.ContainsKey(contract.Address))
                {
                    var message = $"Contract configuration error. Contract {contract.Address} is already initialized.";
                    throw new InvalidOperationException(message);
                }

                subscribedContracts[contract.Address] = contract;
            }

            return subscribedContracts;
        }

        private IReadOnlyDictionary<string, IReadOnlyDictionary<string, IMichelsonValue>> ParseLocalStorageConfiguration(
            IReadOnlyList<ContractStorageItemConfiguration> contractStorageItemConfigurations
        )
        {
            var itemsByContracts = new Dictionary<string, Dictionary<string, IMichelsonValue>>();
            foreach (var itemConfiguration in contractStorageItemConfigurations)
            {
                var value = new MichelsonValue(itemConfiguration.Definition, itemConfiguration.Value);
                foreach (var contractAddress in itemConfiguration.Contracts)
                {
                    if (!itemsByContracts.TryGetValue(contractAddress, out var contractStorageProperties))
                    {
                        contractStorageProperties = new Dictionary<string, IMichelsonValue>();
                        itemsByContracts[contractAddress] = contractStorageProperties;
                    }

                    contractStorageProperties[itemConfiguration.Name] = value;
                }
            }

            _logger.LogInformation("Contract storage loaded from local configuration.");

            return itemsByContracts.ToDictionary(i => i.Key, i => (IReadOnlyDictionary<string, IMichelsonValue>) i.Value);
        }

        async Task<Contract> LoadContractDefinition(string address, IReadOnlyDictionary<string, IMichelsonValue> storage, CancellationToken cancellation)
        {
            _logger.LogInformation("Loading contract definition {contract} ... STARTED", address);

            // load the initial contract
            var contractScript = await _tezosClient.GetContractScriptAsync(address, cancellation);

            // create contract instance
            var contract = new Contract(
                Address: contractScript.Address,
                BigMapLayouts: _layoutFactory.CreateBigMapLayouts(contractScript),
                EntrypointLayouts: _layoutFactory.CreateEntrypointLayouts(contractScript.Address, contractScript),
                StorageProperties: storage
            );


            _logger.LogInformation("Loading contract definition {contract} ... FINISHED", address);
            return contract;
        }
    }
}