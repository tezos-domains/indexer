﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using TezosDomains.Indexer.Tezos.Configuration;

namespace TezosDomains.Indexer.Tezos.Services
{
    internal interface ISubscriberProvider
    {
        IReadOnlyDictionary<string, IReadOnlyList<IContractSubscriber>> Subscribers { get; }
    }

    internal sealed class SubscriberProvider : ISubscriberProvider
    {
        public IReadOnlyDictionary<string, IReadOnlyList<IContractSubscriber>> Subscribers { get; }

        public SubscriberProvider(TezosContractConfiguration configuration, IServiceProvider serviceProvider)
        {
            Subscribers = configuration.Subscriptions.ToDictionary(
                s => s.Key,
                s => (IReadOnlyList<IContractSubscriber>) s.Value
                    .Select(
                        subscriberClassName =>
                        {
                            try
                            {
                                var subscriberType = Type.GetType(subscriberClassName, throwOnError: true)!;
                                return (IContractSubscriber) serviceProvider.GetRequiredService(subscriberType);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception($"Invalid subscriber '{subscriberClassName}'.", ex);
                            }
                        }
                    )
                    .ToArray()
            );
        }
    }
}