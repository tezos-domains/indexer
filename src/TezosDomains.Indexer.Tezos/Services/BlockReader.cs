﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Services
{
    internal sealed class BlockReader : IBlockReader
    {
        private readonly IBlockMonitor _blockMonitor;
        private readonly ITezosClient _tezosClient;
        private readonly ILogger _logger;
        private readonly TezosContractConfiguration _configuration;
        private volatile int _currentLevel;
        private volatile int _lastKnownHead;
        private volatile string? _chainId;

        public BlockReader(IBlockMonitor blockMonitor, ITezosClient tezosClient, ILogger<BlockReader> logger, TezosContractConfiguration configuration)
        {
            _blockMonitor = blockMonitor ?? throw new ArgumentNullException(nameof(blockMonitor));
            _tezosClient = tezosClient ?? throw new ArgumentNullException(nameof(tezosClient));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configuration = configuration;
        }

        public async Task<TezosBlock> NextBlockAsync(CancellationToken cancellation)
        {
            if (_currentLevel <= 0)
            {
                throw new InvalidOperationException("BlockReader set to an invalid level");
            }

            if (!_blockMonitor.IsOpen && IsSynced())
            {
                var head = await _tezosClient.GetHeadAsync(cancellation);
                _lastKnownHead = head.Level.GuardNotNull();
                _logger.LogDebug("Fetched new head {newLevel}", _lastKnownHead);
            }

            if (IsSynced())
            {
                // use monitor to fetch the new head
                TezosBlockHeader newHead;
                do
                {
                    newHead = await _blockMonitor.WaitForNextAsync(cancellation);
                } while (newHead.Level <= _lastKnownHead && !cancellation.IsCancellationRequested);

                _lastKnownHead = newHead.Level.GuardNotNull();
            }

            return await AdvanceAsync(cancellation);
        }

        private bool IsSynced() => _currentLevel >= _lastKnownHead - _configuration.NumberOfBlockConfirmations;

        private async Task<TezosBlock> AdvanceAsync(CancellationToken cancellation)
        {
            var newLevel = _currentLevel + 1;
            _logger.LogDebug("Advancing level to {newLevel}", newLevel);
            var block = await _tezosClient.GetBlockAsync(newLevel, cancellation);

            if (block.ChainId.GuardNotNull() != _chainId.GuardNotNull())
                throw new InvalidOperationException(
                    $"Block chainId mismatch. Last block chainId {_chainId} is not the same as latest fetched block with chainId {block.ChainId}"
                );

            // only advance when successful
            _currentLevel = newLevel;
            return block;
        }

        public void Reset(int? currentBlockLevel, string? chainId)
        {
            if (chainId != null && chainId != _configuration.ChainId.GuardNotNull())
                throw new InvalidOperationException(
                    $"Block chainId mismatch. Configured 'TezosDomains:TezosContract:ChainId' {_configuration.ChainId} is not the same as latest stored block with chainId {chainId}"
                );

            _chainId = chainId ?? _configuration.ChainId;
            _currentLevel = _lastKnownHead = currentBlockLevel ?? _configuration.StartFromLevel - 1;
        }
    }
}