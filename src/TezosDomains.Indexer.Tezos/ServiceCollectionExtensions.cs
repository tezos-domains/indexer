﻿using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Net.Http;
using TezosDomains.Common.Configuration;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Health;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Metrics;
using TezosDomains.Indexer.Tezos.Services;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos
{
    public static class ServiceCollectionExtensions
    {
        public static void AddTezos(this IServiceCollection services)
        {
            services
                .AddHealthChecks()
                .AddCheck<TezosNodeHealthCheck>("TezosNode");

            services.AddTezosDomainsConfigurationWithDiagnostics<TezosNodeConfiguration>("TezosNode");
            services.AddTezosDomainsConfigurationWithDiagnostics<TezosContractConfiguration>("TezosContract");

            services.AddSingleton<IBlockReader, BlockReader>();
            services.AddSingleton<IBlockProcessor, BlockProcessor>();
            services.AddSingleton<ILayoutFactory, LayoutFactory>();
            services.AddSingleton<IContractDefinitionProvider, ContractDefinitionProvider>();
            services.AddSingleton<ISubscriberProvider, SubscriberProvider>();
            services.AddSingleton<ITezosClient>(sp => sp.Create<TezosClientMetricsDecorator>(sp.GetRequiredService<TezosClient>()));
            services.AddHttpClient<TezosClient>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5))
                .AddPolicyHandler(GetRetryPolicy);
            services.AddHttpClient<IBlockMonitor, BlockMonitor>();
        }

        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy(IServiceProvider serviceProvider, HttpRequestMessage request)
        {
            var config = serviceProvider.GetRequiredService<TezosNodeConfiguration>();
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .WaitAndRetryAsync(config.RetryDelays);
        }
    }
}