﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    [JsonConverter(typeof(LazyStorageDiffJsonConverter))]
    public sealed class LazyStorageDiff
    {
        public string? Kind { get; set; }
        public string? Id { get; set; }
        public LazyBigMapDiff? BigMapDiff { get; set; }

        private sealed class LazyStorageDiffJsonConverter : JsonConverter<LazyStorageDiff>
        {
            public override LazyStorageDiff? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                var dto = JsonSerializer.Deserialize<Dto>(ref reader, options);
                var bigMapDiff = dto?.Kind == Michelson.Const.LazyStorageDiffKind.BigMap
                    ? dto.Diff.Deserialize<LazyBigMapDiff>(options)
                    : null;

                return new LazyStorageDiff
                {
                    Kind = dto?.Kind,
                    Id = dto?.Id,
                    BigMapDiff = bigMapDiff,
                };
            }

            public override void Write(Utf8JsonWriter writer, LazyStorageDiff value, JsonSerializerOptions options)
                => throw new NotSupportedException();

            private sealed class Dto
            {
                public string? Kind { get; set; }
                public string? Id { get; set; }
                public JsonElement Diff { get; set; }
            }
        }
    }
}