﻿using System.Text.Json.Serialization;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class OperationMetadata
    {
        public OperationResult? OperationResult { get; set; }

        [JsonPropertyName("internal_operation_results")]
        public InternalOperation[] InternalOperations { get; set; } = new InternalOperation[0];
    }
}