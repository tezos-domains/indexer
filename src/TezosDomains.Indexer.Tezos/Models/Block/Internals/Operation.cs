﻿namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class Operation : Content
    {
        public OperationMetadata? Metadata { get; set; }
    }
}