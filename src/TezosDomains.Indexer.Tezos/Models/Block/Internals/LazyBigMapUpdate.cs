﻿using System.Text.Json;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public sealed class LazyBigMapUpdate
    {
        public JsonElement Key { get; set; }
        public JsonElement? Value { get; set; }
    }
}