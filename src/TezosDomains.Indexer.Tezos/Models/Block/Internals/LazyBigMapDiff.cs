﻿namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public sealed class LazyBigMapDiff
    {
        public string? Action { get; set; }
        public LazyBigMapUpdate[]? Updates { get; set; }
    }
}