﻿namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class InternalOperation : Content
    {
        public OperationResult? Result { get; set; }
    }
}