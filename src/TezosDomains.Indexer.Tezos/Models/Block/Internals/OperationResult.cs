﻿using System.Text.Json.Serialization;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public sealed class OperationResult
    {
        public string? Status { get; set; }

        [JsonPropertyName("lazy_storage_diff")]
        public LazyStorageDiff[]? LazyStorageDiffs { get; set; }
    }
}