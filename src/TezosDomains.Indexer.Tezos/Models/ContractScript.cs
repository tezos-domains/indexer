﻿using System.Text.Json;

namespace TezosDomains.Indexer.Tezos.Models
{
    public record ContractScript(string Address, JsonElement Code, JsonElement Storage)
    {
        public static ContractScript Create(string address, JsonElement contract)
        {
            var scriptElement = contract.GetProperty("script");
            return new ContractScript(address, scriptElement.GetProperty("code"), scriptElement.GetProperty("storage"));
        }
    }
}