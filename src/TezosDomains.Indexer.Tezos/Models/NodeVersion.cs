﻿using System.Text.Json;

namespace TezosDomains.Indexer.Tezos.Models
{
    public class NodeVersion
    {
        public class VersionInfo
        {
            public int Major { get; set; }
            public int Minor { get; set; }
            public JsonElement? AdditionalInfo { get; set; }

            public override string ToString() => $"{Major}.{Minor} ({AdditionalInfo})";
        }

        public class NetworkVersionInfo
        {
            public int Major { get; set; }
            public int Minor { get; set; }
            public string? ChainName { get; set; }
        }

        public class VersionCommitInfo
        {
            public string? CommitHash { get; set; }
            public string? CommitDate { get; set; }
        }

        public VersionInfo? Version { get; set; }
        public NetworkVersionInfo? NetworkVersion { get; set; }
        public VersionCommitInfo? CommitInfo { get; set; }
    }
}