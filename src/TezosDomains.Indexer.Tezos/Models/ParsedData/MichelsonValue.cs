﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Encoding;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Models.ParsedData
{
    public interface IMichelsonValue
    {
        string GetValueAsString();
        string? GetValueAsNullableString();
        string GetValueAsAddress();
        string? GetValueAsNullableAddress();
        DateTime GetValueAsDateTime();
        int GetValueAsInt();
        decimal GetValueAsMutezDecimal();
        string GetValueAsHexBytesString();
        Dictionary<string, string> GetValueAsMap();
        string[] GetAndUnpackValueFromMapAsAddressArray(string key);
        string GetValueAsBigMapId();
        JsonElement Definition { get; }
        int? GetValueAsNullableInt();
        DateTime? GetValueAsNullableDateTime();
    }

    public sealed record MichelsonValue(
        JsonElement Definition,
        JsonElement Value
    ) : IMichelsonValue
    {
        public string GetValueAsString() => ParseString(Definition, Value);


        public string[] GetAndUnpackValueFromMapAsAddressArray(string key)
        {
            if (GetValueAsMap().TryGetValue(key, out var value))
            {
                var addressArray = TezosEncoding.PackedData.Unpack(value);
                if (addressArray.ValueKind == JsonValueKind.Array)
                {
                    return addressArray.EnumerateArray().Select(ParseAddressWithoutDefinition).ToArray();
                }

                throw CreateInvalidOperationException(Definition, Value);
            }

            return Array.Empty<string>();
        }

        public string GetValueAsBigMapId() => ParseBigMapId(Definition, Value);

        public string GetValueAsAddress() => ParseAddress(Definition, Value);


        public DateTime GetValueAsDateTime() => ParseDateTime(Definition, Value);

        public int GetValueAsInt() => ParseInt(Definition, Value);

        public string? GetValueAsNullableString() => ParseNullable(Definition, Value, ParseString);

        public string? GetValueAsNullableAddress() => ParseNullable(Definition, Value, ParseAddress);

        public int? GetValueAsNullableInt()
            //IMPORTANT - cast to (int?) necessary, otherwise ParseNullable return 0 for default
            => ParseNullable(Definition, Value, (element, jsonElement) => (int?) ParseInt(element, jsonElement));

        public DateTime? GetValueAsNullableDateTime()
            //IMPORTANT - cast to (DateTime?) necessary, otherwise ParseNullable return default(DateTime) instead of default(DateTime?)
            => ParseNullable(Definition, Value, (element, jsonElement) => (DateTime?) ParseDateTime(element, jsonElement));

        public decimal GetValueAsMutezDecimal() => ParseMutezDecimal(Definition, Value);

        public string GetValueAsHexBytesString() => ParseHexBytesString(Definition, Value);

        public Dictionary<string, string> GetValueAsMap() => ParseMap(Definition, Value);

        private static decimal ParseMutezDecimal(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Money.Mutez)
                && Michelson.TryGetValue(value, Michelson.Const.Type.Int, out var intString))
                return decimal.Parse(intString);

            throw CreateInvalidOperationException(definition, value);
        }

        private static int ParseInt(JsonElement definition, JsonElement value)
        {
            if ((Michelson.IsPrim(definition, Michelson.Const.Prim.Int) || Michelson.IsPrim(definition, Michelson.Const.Prim.Nat))
                && Michelson.TryGetValue(value, Michelson.Const.Type.Int, out var intString))
                return int.Parse(intString);

            throw CreateInvalidOperationException(definition, value);
        }

        private static DateTime ParseDateTime(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Timestamp))
            {
                if (Michelson.TryGetValue(value, Michelson.Const.Type.Int, out var intString))
                    return DateTimeOffset.FromUnixTimeSeconds(long.Parse(intString)).UtcDateTime;

                if (Michelson.TryGetValue(value, Michelson.Const.Type.String, out var @string))
                    return DateTime.Parse(@string).ToUniversalTime();
            }

            throw CreateInvalidOperationException(definition, value);
        }

        private static Dictionary<string, string> ParseMap(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Map) && value.ValueKind == JsonValueKind.Array)
            {
                return value.EnumerateArray()
                    .Select(
                        v =>
                        {
                            if (!Michelson.IsPrim(v, Michelson.Const.Prim.Elt))
                                throw CreateInvalidOperationException(definition, value);

                            return (Key: ParseString(Michelson.GetArg(definition, 0), Michelson.GetArg(v, 0)),
                                Value: ParseHexBytesString(Michelson.GetArg(definition, 1), Michelson.GetArg(v, 1)));
                        }
                    )
                    .ToDictionary(v => v.Key, v => v.Value);
            }

            throw CreateInvalidOperationException(definition, value);
        }

        private static string ParseString(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Bytes) && Michelson.TryGetValue(value, Michelson.Const.Type.Bytes, out var hexBytesString))
                return TezosEncoding.String.ConvertFromHex(hexBytesString);

            if (Michelson.IsPrim(definition, Michelson.Const.Prim.String) && Michelson.TryGetValue(value, Michelson.Const.Type.String, out var @string))
                return @string;

            throw CreateInvalidOperationException(definition, value);
        }

        private static string ParseHexBytesString(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Bytes) && Michelson.TryGetValue(value, Michelson.Const.Type.Bytes, out var hexBytesString))
                return hexBytesString;

            throw CreateInvalidOperationException(definition, value);
        }

        private static string ParseBigMapId(JsonElement definition, JsonElement value)
        {
            var id = Michelson.IsPrim(definition, Michelson.Const.Prim.BigMap) && value.TryGetProperty("int", out var @int)
                ? @int.GetString()
                : null;

            return id.WhiteSpaceToNull()
                   ?? throw CreateInvalidOperationException(definition, value);
        }

        private static T? ParseNullable<T>(JsonElement definition, JsonElement value, Func<JsonElement, JsonElement, T> parseNonNull)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Option))
            {
                if (Michelson.IsPrim(value, Michelson.Const.Prim.Some))
                    return parseNonNull(Michelson.GetArg(definition, 0), Michelson.GetArg(value, 0));

                if (Michelson.IsPrim(value, Michelson.Const.Prim.None))
                    return default;
            }

            throw CreateInvalidOperationException(definition, value);
        }

        private static string ParseAddress(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Address))
            {
                return ParseAddressWithoutDefinition(value);
            }

            throw CreateInvalidOperationException(definition, value);
        }

        private static string ParseAddressWithoutDefinition(JsonElement value)
        {
            if (Michelson.TryGetValue(value, Michelson.Const.Type.Bytes, out var hexBytesString))
            {
                return TezosEncoding.Address.ConvertFromHex(hexBytesString);
            }

            if (Michelson.TryGetValue(value, Michelson.Const.Type.String, out var @string))
            {
                return @string;
            }

            throw CreateInvalidOperationException("Address", value);
        }

        private static Exception CreateInvalidOperationException(JsonElement definition, JsonElement value)
            => new InvalidOperationException(
                string.Join(
                    Environment.NewLine,
                    "Data doesn't correspond to requested value type.",
                    $"(Definition:{definition},",
                    $"Value:{value})"
                )
            );

        private static Exception CreateInvalidOperationException(string type, JsonElement value)
            => new InvalidOperationException(
                string.Join(
                    Environment.NewLine,
                    $"Data doesn't correspond to requested value type ({type}).",
                    $"Value:{value})"
                )
            );
    }
}