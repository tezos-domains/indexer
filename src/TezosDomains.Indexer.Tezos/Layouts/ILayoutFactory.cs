﻿using System.Collections.Generic;
using TezosDomains.Indexer.Tezos.Models;

namespace TezosDomains.Indexer.Tezos.Layouts
{
    internal interface ILayoutFactory
    {
        RecordLayout CreateStorageLayout(ContractScript contractScript);
        IReadOnlyDictionary<string, BigMapLayout> CreateBigMapLayouts(ContractScript contractScript);
        IReadOnlyDictionary<string, RecordLayout> CreateEntrypointLayouts(string address, ContractScript contractScript);
    }
}