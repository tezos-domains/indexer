﻿using System.Collections.Generic;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos.Layouts
{
    internal sealed class BigMapLayout
    {
        public string Name { get; }
        public JsonElement KeyDefinition { get; }
        public RecordLayout ValueLayout { get; }

        public BigMapLayout(string name, JsonElement keyDefinition, RecordLayout valueLayout)
        {
            Name = name;
            KeyDefinition = keyDefinition;
            ValueLayout = valueLayout;
        }

        public (MichelsonValue Key, IReadOnlyDictionary<string, IMichelsonValue> Value) Apply(LazyBigMapUpdate bigMapUpdate)
        {
            var key = new MichelsonValue(KeyDefinition, bigMapUpdate.Key);
            var valueProperties = bigMapUpdate.Value.HasValue
                ? ValueLayout.Apply(bigMapUpdate.Value.Value)
                : new Dictionary<string, IMichelsonValue>();
            return (key, valueProperties);
        }
    }
}