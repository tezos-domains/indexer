﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos.Layouts
{
    internal sealed class RecordLayout
    {
        private readonly JsonElement[] _definition;

        public RecordLayout(JsonElement definition) => _definition = Parse(definition).ToArray();

        public IReadOnlyDictionary<string, IMichelsonValue> Apply(JsonElement valueElement, bool enumerateValueWhenArray = false)
        {
            var values = (_definition.Length != 1 && valueElement.ValueKind == JsonValueKind.Array && enumerateValueWhenArray
                ? valueElement.EnumerateArray()
                : Parse(valueElement)).ToArray();

            if (_definition.Length != values.Length)
                throw new ArgumentException("Error in matching element's value to its definition.", nameof(valueElement));

            var valuesByName = values.Select(
                    (value, idx) =>
                    {
                        var propertyDefinition = _definition[idx];
                        //simple key/value pair bigMap doesn't contain annotation for value (use '__value__' instead)
                        var name = Michelson.GetAnnotationOrNull(propertyDefinition) ?? "__value__";
                        var valueWithDefinition = new MichelsonValue(propertyDefinition, value);

                        return (Name: name, Value: valueWithDefinition);
                    }
                )
                .ToDictionary(o => o.Name, o => (IMichelsonValue) o.Value);

            return valuesByName;
        }

        public static IEnumerable<JsonElement> Parse(JsonElement element, string prim = Michelson.Const.Prim.Pair)
            => Michelson.IsPrim(element, prim)
                ? Michelson.EnumerateArgs(element).SelectMany(e => Parse(e, prim))
                : new[] { element };
    }
}