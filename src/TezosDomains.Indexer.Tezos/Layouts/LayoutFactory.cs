﻿using System;
using System.Collections.Generic;
using System.Linq;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Layouts
{
    internal class LayoutFactory : ILayoutFactory
    {
        public RecordLayout CreateStorageLayout(ContractScript contractScript)
        {
            var typeProperty = contractScript.Code.EnumerateArray()
                .Single(el => Michelson.IsPrim(el, Michelson.Const.Prim.Storage));
            var storageType = Michelson.GetArg(typeProperty, 0);

            return new RecordLayout(storageType);
        }

        public IReadOnlyDictionary<string, BigMapLayout> CreateBigMapLayouts(ContractScript contractScript)
        {
            var bigMapLayouts = new Dictionary<string, BigMapLayout>();

            foreach (var (_, prop) in CreateStorageLayout(contractScript).Apply(contractScript.Storage))
            {
                if (Michelson.IsPrim(prop.Definition, Michelson.Const.Prim.BigMap))
                {
                    var key = Michelson.GetArg(prop.Definition, 0);
                    var value = Michelson.GetArg(prop.Definition, 1);
                    var bigMapName = Michelson.GetAnnotationOrNull(prop.Definition).GuardNotNull();
                    bigMapLayouts[prop.GetValueAsBigMapId()] = new BigMapLayout(bigMapName, key, new RecordLayout(value));
                }
            }

            return bigMapLayouts;
        }

        public IReadOnlyDictionary<string, RecordLayout> CreateEntrypointLayouts(string address, ContractScript contractScript)
        {
            var parameterProperty = contractScript.Code.EnumerateArray()
                .Single(el => Michelson.IsPrim(el, Michelson.Const.Prim.Parameter));
            var parameterType = Michelson.GetArg(parameterProperty, index: 0);

            var entrypointLayouts = RecordLayout.Parse(parameterType, Michelson.Const.Prim.Or)
                .ToDictionary(
                    element => Michelson.GetAnnotationOrNull(element) ?? throw new InvalidOperationException($"null entrypoint at {address}"),
                    element => new RecordLayout(element)
                );

            return entrypointLayouts;
        }
    }
}