﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;

namespace TezosDomains.Indexer.Tezos.Configuration
{
    public sealed class ContractStorageItemConfiguration : IValidatableObject
    {
        [Required]
        public string Name { get; init; } = null!;

        [Required]
        public string[] Contracts { get; init; } = null!;

        private readonly string? _definitionString;

        [Required]
        public string? DefinitionString
        {
            get => _definitionString;
            init
            {
                _definitionString = value;
                Definition = ParseJson(value, nameof(DefinitionString), out _);
            }
        }

        public JsonElement Definition { get; private init; }


        private readonly string? _valueString;

        [Required]
        public string? ValueString
        {
            get => _valueString;
            init
            {
                _valueString = value;
                Value = ParseJson(value, nameof(ValueString), out _);
            }
        }

        public JsonElement Value { get; private init; }


        private static JsonElement ParseJson(string? str, string propertyName, out ValidationResult? error)
        {
            try
            {
                error = null;
                return JsonDocument.Parse(str ?? "").RootElement;
            }
            catch (Exception e)
            {
                error = new ValidationResult($"Invalid string containing json object in {propertyName}. Error: {e}", new[] { propertyName });
                return default;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            ParseJson(DefinitionString, nameof(DefinitionString), out var definitionError);
            if (definitionError != null)
                yield return definitionError;
            ParseJson(ValueString, nameof(ValueString), out var valueError);
            if (valueError != null)
                yield return valueError;
        }
    }
}