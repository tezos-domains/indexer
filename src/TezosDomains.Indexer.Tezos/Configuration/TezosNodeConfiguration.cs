﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Indexer.Tezos.Configuration
{
    public sealed class TezosNodeConfiguration : IConfigurationInstance, IValidatableObject
    {
        [Required]
        public Uri ConnectionString { get; init; } = null!;

        public TimeSpan? Timeout { get; init; }

        [Required]
        public IReadOnlyList<TimeSpan> RetryDelays { get; init; } = null!;

        object IConfigurationInstance.GetDiagnosticInfo() => this;

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            if (Timeout != null && Timeout <= TimeSpan.Zero)
                yield return new ValidationResult("Timeout must be greater than zero.");
        }

        public void ApplyTo(HttpClient client, string baseRelativePath = "")
        {
            client.BaseAddress = UrlCombine(ConnectionString.AbsoluteUri, baseRelativePath);
            client.Timeout = Timeout ?? client.Timeout;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private Uri UrlCombine(string baseUrl, params string[] segments)
        {
            return new(
                string.Join(
                    "/",
                    new[] { baseUrl.TrimEnd('/') }
                        .Concat(segments.Select(s => s.Trim('/')))
                )
            );
        }
    }
}