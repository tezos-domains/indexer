﻿using System.ComponentModel.DataAnnotations;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Indexer.Tezos.Configuration
{
    public sealed class AffiliateConfiguration : IConfigurationInstance
    {
        [Required]
        public string AffiliateContract { get; set; } = null!;

        //[Required]
        //public decimal AffiliateFee { get; set; }

        public object GetDiagnosticInfo() => this;

        public string ReplaceSourceAddressWhenAffiliateContract(string sourceAddress, string? rootSourceAddress)
            => sourceAddress == AffiliateContract && !string.IsNullOrEmpty(rootSourceAddress) ? rootSourceAddress : sourceAddress;
    }
}