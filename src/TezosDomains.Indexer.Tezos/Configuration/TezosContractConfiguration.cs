﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TezosDomains.Common;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Indexer.Tezos.Configuration
{
    public sealed class TezosContractConfiguration : IValidatableObject, IConfigurationInstance
    {
        [Range(1, int.MaxValue)]
        public int StartFromLevel { get; init; }

        [Required]
        public string ChainId { get; set; } = null!;

        /// <summary>
        /// Indexer will only index blocks which have been confirmed {NumberOfBlockConfirmations} times
        /// </summary>
        [Range(0, int.MaxValue, ErrorMessage = "Value should be greater than or equal to 0")]
        public int NumberOfBlockConfirmations { get; set; }

        [Required]
        public IReadOnlyDictionary<string, IReadOnlyList<string>> Subscriptions { get; init; } = null!;

        [Required]
        public IReadOnlyList<ContractStorageItemConfiguration> Storage { get; init; } = null!;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
            => Subscriptions.SelectMany(pair => ValidateContractSubscriptions(pair.Key, pair.Value, propertyPath: $"{nameof(Subscriptions)}[{pair.Key}]"))
                .Concat(Storage.SelectMany((s, idx) => s.GetValidationErrors($"{nameof(Storage)}[{idx}]")));

        private static IEnumerable<ValidationResult> ValidateContractSubscriptions(
            string contractAddress,
            IEnumerable<string> subscriptionClasses,
            string propertyPath
        )
        {
            if (!subscriptionClasses.Any())
                yield return new ValidationResult($"Subscription classes for {contractAddress} cannot be empty", new[] { propertyPath });
        }

        object IConfigurationInstance.GetDiagnosticInfo() => this;
    }
}