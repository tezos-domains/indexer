﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TezosDomains.Common.Configuration;

namespace TezosDomains.Indexer.Tezos.Configuration
{
    public sealed class MarketplaceConfiguration : IConfigurationInstance
    {
        [Required]
        public IReadOnlyList<string> AllowedNftContracts { get; set; } = null!;

        [Required]
        public string MarketplaceContract { get; set; } = null!;

        [Required]
        public decimal MarketplaceFee { get; set; }

        public object GetDiagnosticInfo() => this;
    }
}