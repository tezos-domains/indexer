﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;

namespace TezosDomains.Indexer.Tezos.Health
{
    public sealed class TezosNodeHealthCheck : IHealthCheck
    {
        private readonly ITezosClient _tezosClient;

        public TezosNodeHealthCheck(ITezosClient tezosClient)
        {
            _tezosClient = tezosClient;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellation = default)
        {
            try
            {
                var version = await _tezosClient.GetNodeVersionAsync(cancellation);
                return HealthCheckResult.Healthy($"Tezos Node connection is working. Tezos Node version: {version.Version}");
            }
            catch (Exception e)
            {
                return HealthCheckResult.Unhealthy("Tezos Node connection unhealthy", e);
            }
        }
    }
}