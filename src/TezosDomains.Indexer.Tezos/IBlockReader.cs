﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos
{
    public interface IBlockReader
    {
        Task<TezosBlock> NextBlockAsync(CancellationToken cancellation);
        void Reset(int? currentBlock, string? chainId);
    }
}