﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Common.Metrics;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos.Metrics
{
    internal sealed class TezosClientMetricsDecorator : MetricsDecorator<ITezosClient>, ITezosClient
    {
        public TezosClientMetricsDecorator(ITezosClient decorated) : base(decorated, TezosMetrics.HttpRequestsCounter, TezosMetrics.HttpRequestsDuration)
        {
        }

        public Task<TezosBlock> GetBlockAsync(int level, CancellationToken cancellation)
        {
            TezosMetrics.BlockLevel.Set(level);
            return UpdateCounters(client => client.GetBlockAsync(level, cancellation));
        }

        public Task<TezosBlockHeader> GetHeadAsync(CancellationToken cancellation)
        {
            return UpdateCounters(client => client.GetHeadAsync(cancellation));
        }

        public Task<ContractScript> GetContractScriptAsync(string contract, CancellationToken cancellation)
        {
            return UpdateCounters(client => client.GetContractScriptAsync(contract, cancellation));
        }

        public Task<NodeVersion> GetNodeVersionAsync(CancellationToken cancellation)
        {
            return UpdateCounters(client => client.GetNodeVersionAsync(cancellation));
        }
    }
}