﻿using Prometheus;
using TezosDomains.Common.Metrics;

namespace TezosDomains.Indexer.Tezos.Metrics
{
    public static class TezosMetrics
    {
        public static readonly Counter HttpRequestsCounter = MetricsCollectors.CreateRequestCounter(
            "td_tezosclient_http_requests_counter",
            "Request counter to Tezos Node."
        );

        public static readonly Histogram HttpRequestsDuration = MetricsCollectors.CreateRequestDurationHistogram(
            "td_tezosclient_http_requests_duration_seconds",
            "Tezos Node requests duration with deserialization.",
            buckets: Histogram.ExponentialBuckets(0.001, 2.0, 16)
        );

        public static readonly Gauge BlockLevel = Prometheus.Metrics.CreateGauge(
            "td_tezosclient_latest_block_level",
            "Last block level requested."
        );
    }
}