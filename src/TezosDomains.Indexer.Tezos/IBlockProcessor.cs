﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos
{
    public interface IBlockProcessor
    {
        Task InitAsync(CancellationToken cancellation);
        IReadOnlyList<IBlockUpdate> Process(TezosBlock tezosBlock);
    }
}