﻿using Netezos.Encoding;
using NokitaKaze.Base58Check;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Encoding
{
    public static class TezosEncoding
    {
        private static string ToHexString(Span<byte> inArray) => Convert.ToHexString(inArray).ToLowerInvariant();
        private static byte[] FromHexString(string hexString) => Convert.FromHexString(hexString);


        public static class PackedData
        {
            public static JsonElement Unpack(string data) => JsonDocument.Parse(Netezos.Encoding.Micheline.Unpack(FromHexString(data)).ToJson()).RootElement;
        }

        public static class String
        {
            public static string ConvertFromHex(string hexEncodedName) => System.Text.Encoding.UTF8.GetString(FromHexString(hexEncodedName));
            public static string ConvertToHex(string name) => ToHexString(System.Text.Encoding.UTF8.GetBytes(name));
        }

        public static class Address
        {
            public static readonly IReadOnlyList<(string Base58, string Hex, string HexPadding, byte[] Bytes)> Prefixes = new[]
            {
                ("tz1", "0000", "", new byte[] { 6, 161, 159 }),
                ("tz2", "0001", "", new byte[] { 6, 161, 161 }),
                ("tz3", "0002", "", new byte[] { 6, 161, 164 }),
                ("KT1", "01", "00", new byte[] { 2, 90, 121 }),
                ("txr1", "02", "00", new byte[] { 1, 128, 120, 31 }), // Address prefix for originated tx rollup
                ("tz4", "0003", "", new byte[] { 6, 161, 166 }), //bls12_381
                ("sr1", "03", "00", new byte[] { 6, 124, 117 }), //smart rollup
            };

            private const int HexLength = 44;
            private const int SignificantByteLength = 20; // (HexLength - prefix.Hex.Length - prefix.HexPadding.Length) / 2

            public const string Description =
                "Base58Check-encoded string of length 36 prefixed with tz1 (ed25519), tz2 (secp256k1), tz3 (p256), KT1 or txr1 (rollup).";

            public static string ConvertFromBase58(string base58EncodedAddress)
            {
                if (base58EncodedAddress is null)
                    throw new ArgumentNullException(nameof(base58EncodedAddress));

                try
                {
                    var addressWithEntrypointSplit = base58EncodedAddress.Split("%", 2);
                    var address = addressWithEntrypointSplit[0];
                    var prefix = Prefixes.FirstOrDefault(p => address.StartsWith(p.Base58));
                    if (prefix.Base58 == null)
                        throw new NotSupportedException($"Prefix isn't supported. Supported ones: {Prefixes.Select(p => p.Base58).Join()}");

                    var bytes = Base58CheckEncoding.Decode(address);
                    var hexValue = ToHexString(bytes.AsSpan().Slice(prefix.Bytes.Length, SignificantByteLength));
                    var hexAddress = prefix.Hex + hexValue + prefix.HexPadding;

                    var entrypointNameOrEmpty = String.ConvertToHex(addressWithEntrypointSplit.ElementAtOrDefault(1) ?? string.Empty);
                    return $"{hexAddress}{entrypointNameOrEmpty}";
                }
                catch (Exception ex)
                {
                    throw new ArgumentException($"Invalid address '{base58EncodedAddress}'. It must be {Description}", nameof(base58EncodedAddress), ex);
                }
            }

            public static string ConvertFromHex(string hexEncodedAddress)
            {
                if (hexEncodedAddress is null)
                    throw new ArgumentNullException(nameof(hexEncodedAddress));

                try
                {
                    if (hexEncodedAddress.Length < HexLength)
                        throw new Exception($"Address must have {HexLength} characters.");

                    var addressPart = hexEncodedAddress[..HexLength];
                    var prefix = Prefixes.FirstOrDefault(p => addressPart.StartsWith(p.Hex));
                    if (prefix.Base58 == null)
                        throw new NotSupportedException($"Prefix isn't supported. Supported ones: {Prefixes.Select(p => p.Hex).Join()}");

                    var hexValue = addressPart[prefix.Hex.Length..^prefix.HexPadding.Length];
                    var bytes = prefix.Bytes.Concat(FromHexString(hexValue)).ToArray();

                    var base58Address = Base58CheckEncoding.Encode(bytes);

                    return hexEncodedAddress[HexLength..] switch
                    {
                        { Length: 0 } => base58Address,
                        //address with entrypoint, i.e. 016aa235d26f4e91fe3c3c89aad914834b9d612314007761616161 <=> KT1JJbWfW8CHUY95hG9iq2CEMma1RiKhMHDR%waaaa
                        { } entrypoint => $"{base58Address}%{String.ConvertFromHex(entrypoint)}"
                    };
                }
                catch (Exception ex)
                {
                    throw new ArgumentException($"Invalid hex-encoded address '{hexEncodedAddress}'.", nameof(hexEncodedAddress), ex);
                }
            }
        }
    }
}