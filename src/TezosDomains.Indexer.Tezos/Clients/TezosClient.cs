﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Serializers;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Clients
{
    internal sealed class TezosClient : ITezosClient
    {
        private readonly HttpClient _client;
        private readonly ILogger _logger;

        public TezosClient(HttpClient client, TezosNodeConfiguration configuration, ILogger<TezosClient> logger)
        {
            _logger = logger;
            _client = client;

            configuration.ApplyTo(_client);
        }

        public async Task<ContractScript> GetContractScriptAsync(string contract, CancellationToken cancellation)
        {
            _logger.LogDebug("Getting contract {contract}", contract);
            var result = await GetResponse<JsonElement>($"chains/main/blocks/head/context/contracts/{contract}", cancellation);
            return ContractScript.Create(contract, result);
        }

        private async Task<T> GetResponse<T>(string requestUri, CancellationToken cancellation)
            where T : notnull
        {
            var response = await _client.GetAsync(requestUri, cancellation);

            var statusCodeInt = (int) response.StatusCode;
            if (statusCodeInt < 200 || statusCodeInt >= 300)
            {
                var contentStr = await TryReadAsStringAsync(response.Content);
                throw new Exception($"Response code {statusCodeInt} {response.StatusCode} from {requestUri} doesn't indicate success. Details: {contentStr}");
            }

            await using var stream = await response.Content.ReadAsStreamAsync(cancellation);
            var result = await TezosNodeDeserializer.DeserializeAsync<T>(stream, cancellation);
            return result.GuardNotNull($"Null returned from: {requestUri}");
        }

        public Task<NodeVersion> GetNodeVersionAsync(CancellationToken cancellation)
        {
            _logger.LogDebug("Getting version...");
            return GetResponse<NodeVersion>("version", cancellation);
        }

        public Task<TezosBlock> GetBlockAsync(int level, CancellationToken cancellation)
        {
            _logger.LogDebug($"Getting block {level}");
            return GetResponse<TezosBlock>($"chains/main/blocks/{level}", cancellation);
        }

        public Task<TezosBlockHeader> GetHeadAsync(CancellationToken cancellation)
        {
            _logger.LogDebug("Getting the current head");
            return GetResponse<TezosBlockHeader>("chains/main/blocks/head/header", cancellation);
        }

        private static async Task<string> TryReadAsStringAsync(HttpContent content)
        {
            try
            {
                return await content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}