﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Serializers;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Clients
{
    public class BlockMonitor : IDisposable, IBlockMonitor
    {
        private readonly HttpClient _client;
        private readonly ILogger _logger;
        private volatile StreamReader? _streamReader;
        private volatile HttpResponseMessage? _httpResponse;

        public BlockMonitor(HttpClient client, TezosNodeConfiguration configuration, ILogger<BlockMonitor> logger)
        {
            _client = client;
            _logger = logger;

            configuration.ApplyTo(_client, baseRelativePath: "monitor/heads/main");
        }

        private async Task<StreamReader> GetMonitorStreamReader(CancellationToken cancellation)
        {
            if (_streamReader != null)
            {
                return _streamReader;
            }

            _logger.LogDebug($"Opening monitor stream at {_client.BaseAddress}");
            try
            {
                _httpResponse = await _client.SendAsync(new HttpRequestMessage(), HttpCompletionOption.ResponseHeadersRead, cancellation);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Cannot connect to Tezos Node at {_client.BaseAddress}");
                throw;
            }

            var stream = await _httpResponse.Content.ReadAsStreamAsync(cancellation);
            _streamReader = new StreamReader(stream);
            return _streamReader;
        }

        private static async Task<string> ReadLineAsync(TextReader reader, CancellationToken cancellation)
        {
            var lineBuffer = new StringBuilder();
            var readBuffer = new char[1];

            while (await reader.ReadAsync(readBuffer, cancellation) > 0)
            {
                if (readBuffer[0] != '\r' && readBuffer[0] != '\n')
                    lineBuffer.Append(readBuffer[0]);
                else if (lineBuffer.Length > 0)
                    return lineBuffer.ToString();
            }

            throw new ApplicationException("HTTP stream ended.");
        }

        public async Task<TezosBlockHeader> WaitForNextAsync(CancellationToken cancellation)
        {
            var reader = await GetMonitorStreamReader(cancellation);
            var line = await ReadLineAsync(reader, cancellation);
            _logger.LogTrace(line);

            var header = TezosNodeDeserializer.Deserialize<TezosBlockHeader>(line).GuardNotNull();
            cancellation.ThrowIfCancellationRequested();
            return header;
        }

        public void Dispose()
        {
            _httpResponse?.Dispose();
            _httpResponse = null;
            _streamReader = null;
        }

        public bool IsOpen => _streamReader != null;
    }
}