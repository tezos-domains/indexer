﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos.Clients
{
    public interface ITezosClient
    {
        Task<TezosBlock> GetBlockAsync(int level, CancellationToken cancellation);
        Task<TezosBlockHeader> GetHeadAsync(CancellationToken cancellation);
        Task<ContractScript> GetContractScriptAsync(string contract, CancellationToken cancellation);
        Task<NodeVersion> GetNodeVersionAsync(CancellationToken cancellation);
    }
}