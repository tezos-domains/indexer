﻿namespace TezosDomains.Indexer.Tezos
{
    /// <summary>A marker so that only certain objects can be added as block updates.</summary>
    public interface IBlockUpdate
    {
    }
}