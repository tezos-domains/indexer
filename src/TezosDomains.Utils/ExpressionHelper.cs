﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TezosDomains.Utils
{
    public static class ExpressionHelper
    {
        public static string GetFieldName<TDocument, TField>(this Expression<Func<TDocument, TField>> fieldExpression)
        {
            var memberExpression =
                fieldExpression.Body as MemberExpression
                ?? (fieldExpression.Body as UnaryExpression)?.Operand as MemberExpression // Value is upcasted
                ?? throw new InvalidOperationException($"Expression must be simple property retrieval but it's {fieldExpression}");

            return GetFieldNames(memberExpression).Join(".");

            static IEnumerable<string> GetFieldNames(MemberExpression memberExpr)
            {
                if (memberExpr.Expression is MemberExpression me)
                    foreach (var t in GetFieldNames(me))
                        yield return t;

                //array accessor a=>a[0].b  ==> a.0.b
                if (memberExpr.Expression is MethodCallExpression mce
                    && mce.Method.Name == "get_Item"
                    && mce.Arguments.Count == 1
                    && mce.Arguments[0] is ConstantExpression idx
                    && mce.Object is MemberExpression ome)
                {
                    foreach (var fieldName in GetFieldNames(ome))
                        yield return fieldName;

                    yield return (idx.Value?.ToString().WhiteSpaceToNull()).GuardNotNull("Null or empty string can't be used as an indexer key.");
                }

                yield return memberExpr.Member.Name;
            }
        }

        public static string ExtractMethodName<TDelegate>(this Expression<TDelegate> expression)
            => expression.Body is MethodCallExpression mce
                ? mce.Method.Name
                : throw new ArgumentException($"Expression is not a {nameof(MethodCallExpression)}", nameof(expression));
    }
}