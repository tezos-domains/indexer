﻿using System;

namespace TezosDomains.Utils
{
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class GraphQLOrderAsDeclaredAttribute : Attribute
    {
    }
}