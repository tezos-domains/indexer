﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace TezosDomains.Utils
{
    public static class ServiceProviderExtensions
    {
        public static TService Create<TService>(this IServiceProvider serviceProvider, params object[] parameters)
            where TService : notnull
            => ActivatorUtilities.CreateInstance<TService>(serviceProvider, parameters: parameters);
    }
}