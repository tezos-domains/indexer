using System;
using System.Collections.Generic;
using System.Linq;

namespace TezosDomains.Utils
{
    public static class EnumerableExtensions
    {
        public static int RequiredIndexOf<T>(this IEnumerable<T> items, Predicate<T> matchItem)
            => items.IndexOf(matchItem)
               ?? throw new IndexOutOfRangeException($"Item wasn't found in a sequence of {typeof(T)} items.");

        public static int? IndexOf<T>(this IEnumerable<T> items, Predicate<T> matchItem)
            => items.Select(((item, index) => (item, index: (int?) index)))
                .FirstOrDefault(x => matchItem(x.item))
                .index;

        public static IReadOnlyList<T> NullToEmpty<T>(this IReadOnlyList<T>? enumerable)
            => enumerable ?? Array.Empty<T>();

        public static IEnumerable<T> NullToEmpty<T>(this IEnumerable<T>? enumerable)
            => enumerable ?? Array.Empty<T>();

        /// <summary>In addition to LINQ method, checks that the result type actually inherits the source type hence this type filtering is meaningful.</summary>
        public static IEnumerable<TResult> OfType<TSource, TResult>(this IEnumerable<TSource> enumerable)
            where TResult : TSource
            => enumerable.OfType<TResult>();

        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> items)
#pragma warning disable 8619
            => items.Where(i => i != null);
#pragma warning restore 8619

        public static void Deconstruct<T>(this IEnumerable<T> enumerable, out T first, out List<T> rest)
        {
            rest = enumerable.ToList();
            first = rest.First();
            rest.RemoveAt(0);
        }

        public static string Join<T>(this IEnumerable<T> enumerable, string separator = ", ")
            => string.Join(separator, enumerable);

        public static T SingleWithItemDetails<T>(this IEnumerable<T> enumerable)
        {
            var items = enumerable.AsReadOnlyCollection();
            return items.Count == 1
                ? items.First()
                : throw new Exception($"Sequence of {typeof(T)} must contain a single item but there are: {items.Join()}.");
        }

        public static IReadOnlyCollection<T> AsReadOnlyCollection<T>(this IEnumerable<T> enumerable)
            => enumerable as IReadOnlyCollection<T> ?? enumerable.ToList();

        public static List<TTarget> ConvertAll<TSource, TTarget>(this IEnumerable<TSource> enumerable, Func<TSource, TTarget> converter)
        {
            var result = new List<TTarget>((enumerable as IReadOnlyCollection<TSource>)?.Count ?? 0);
            foreach (var item in enumerable)
                result.Add(converter(item));

            return result;
        }
    }
}