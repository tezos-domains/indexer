﻿using System;
using System.Runtime.CompilerServices;

namespace TezosDomains.Utils
{
    public static class NullableExtensions
    {
        public static T GuardNotNull<T>(this T? value, string? message = null, [CallerArgumentExpression("value")] string? valueExpression = null)
            => value ?? throw CreateException(message, valueExpression);

        public static T GuardNotNull<T>(this T? value, string? message = null, [CallerArgumentExpression("value")] string? valueExpression = null)
            where T : struct
            => value ?? throw CreateException(message, valueExpression);

        private static Exception CreateException(string? message, string? valueExpression)
        {
            var fullMessage = $"{message ?? "The value cannot be null."} Code expression: {valueExpression}";
            return new ArgumentNullException(fullMessage, innerException: null);
        }
    }
}