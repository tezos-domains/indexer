﻿using System;
using System.ComponentModel;
using System.Linq;

namespace TezosDomains.Utils
{
    public class DescriptionFormatAttribute : DescriptionAttribute
    {
        public DescriptionFormatAttribute(string format, params object[] args)
            : base(string.Format(format, args.Select(a => (object)new Arg(a)).ToArray()))
        {
        }

        private record Arg(object Value) : IFormattable
        {
            public string ToString(string? format, IFormatProvider? formatProvider)
            {
                var str = Value.ToString() ?? "";
                var formattedStr = format switch
                {
                    null => str,
                    "camel" => str.ToCamelCase(),
                    "upper" => str.ToUpperInvariant(),
                    _ => throw new Exception($"Unknown format '{format}'."),
                };
                return $"`{formattedStr}`";
            }
        }
    }
}