﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Common.Metrics;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Metrics
{
    public sealed class QueryServiceMetricsDecorator<TDocument> : MetricsDecorator<IQueryService<TDocument>>, IQueryService<TDocument>
        where TDocument : IHasDbCollection
    {
        public QueryServiceMetricsDecorator(IQueryService<TDocument> decorated) : base(
            decorated,
            MongoDbMetrics.QueryServiceRequestsCounter,
            MongoDbMetrics.QueryServiceRequestsDuration
        )
        {
        }

        public Task<TDocument?> FindSingleAsync(FilterDefinition<TDocument> filter, CancellationToken cancellation)
            => UpdateCounters(s => s.FindSingleAsync(filter, cancellation));

        public Task<List<TDocument>> FindAllAsync(DocumentQuery<TDocument> query, CancellationToken cancellation)
            => UpdateCounters(s => s.FindAllAsync(query, cancellation));

        public Task<long> CountAsync(FilterDefinition<TDocument> filter, CancellationToken cancellation)
            => UpdateCounters(s => s.CountAsync(filter, cancellation));
    }
}