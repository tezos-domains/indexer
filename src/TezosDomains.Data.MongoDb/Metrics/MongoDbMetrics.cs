﻿using Prometheus;
using TezosDomains.Common.Metrics;

namespace TezosDomains.Data.MongoDb.Metrics
{
    public class MongoDbMetrics
    {
        public static readonly Counter QueryServiceRequestsCounter = MetricsCollectors.CreateRequestCounter(
            "td_mongodb_queryservice_requests_counter",
            "Request counter of a mongodb query service."
        );

        public static readonly Histogram QueryServiceRequestsDuration = MetricsCollectors.CreateRequestDurationHistogram(
            "td_mongodb_queryservice_requests_duration_seconds",
            "A mongodb query service requests duration."
        );
    }
}