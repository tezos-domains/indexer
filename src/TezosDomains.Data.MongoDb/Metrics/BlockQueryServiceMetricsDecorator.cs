﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Common.Metrics;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Data.MongoDb.Metrics
{
    public class BlockQueryServiceMetricsDecorator : MetricsDecorator<IBlockQueryService>, IBlockQueryService
    {
        public BlockQueryServiceMetricsDecorator(IBlockQueryService decorated) : base(
            decorated,
            MongoDbMetrics.QueryServiceRequestsCounter,
            MongoDbMetrics.QueryServiceRequestsDuration
        )
        {
        }


        public Task<int?> GetStartLevelOrNullAsync(CancellationToken cancellation)
            => UpdateCounters(s => s.GetStartLevelOrNullAsync(cancellation));


        public Task<Block?> FindAsync(string hash, CancellationToken cancellation)
            => UpdateCounters(s => s.FindAsync(hash, cancellation));


        public Task<Block?> FindAsync(int level, CancellationToken cancellation)
            => UpdateCounters(s => s.FindAsync(level, cancellation));


        public Task<Block?> GetLatestOrNullAsync(CancellationToken cancellation)
            => UpdateCounters(s => s.GetLatestOrNullAsync(cancellation));
    }
}