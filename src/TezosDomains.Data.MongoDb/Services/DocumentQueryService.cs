﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Services
{
    public class DocumentQueryService<TDocument> : IQueryService<TDocument>
        where TDocument : class, IHasDbCollection
    {
        protected IMongoCollection<TDocument> DbCollection { get; }

        public DocumentQueryService(MongoDbContext dbContext)
            => DbCollection = dbContext.Get<TDocument>();

        public virtual Task<TDocument?> FindSingleAsync(FilterDefinition<TDocument> filter, CancellationToken cancellation)
            => DbCollection
                .Find(filter)
                .SingleOrDefaultAsync(cancellation)
                .AsNullableResult();

        public virtual Task<List<TDocument>> FindAllAsync(DocumentQuery<TDocument> query, CancellationToken cancellation)
        {
            var dbFilter = BuildFilter(query);
            var dbSort = query.Order.ToDbSort();
            return DbCollection
                .Find(dbFilter)
                .Sort(dbSort)
                .Limit(query.TakeCount)
                .ToListAsync(cancellation);
        }

        public virtual Task<long> CountAsync(FilterDefinition<TDocument> filter, CancellationToken cancellation)
            => DbCollection
                .Find(filter)
                .CountDocumentsAsync(cancellation);

        protected FilterDefinition<TDocument> BuildFilter(DocumentQuery<TDocument> query)
        {
            var pagingFilter = query.AfterIndex?.ToDbFilter(query.Order);
            return pagingFilter != null
                ? Builders<TDocument>.Filter.And(query.Filter, pagingFilter)
                : query.Filter;
        }
    }
}