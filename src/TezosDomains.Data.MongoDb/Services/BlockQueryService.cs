﻿using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Data.MongoDb.Services
{
    public class BlockQueryService : IBlockQueryService
    {
        private readonly MongoDbContext _dbContext;
        private Task<int?>? _blockStartLevelTask;

        public BlockQueryService(MongoDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Task<int?> GetStartLevelOrNullAsync(CancellationToken cancellation)
            => _blockStartLevelTask ??= GetFirstAsync(
                Builders<Block>.Sort.Ascending(b => b.Level),
                Builders<Block>.Projection.Expression(b => (int?)b.Level),
                cancellation
            );

        public Task<Block?> GetLatestOrNullAsync(CancellationToken cancellation)
            => GetFirstAsync(Builders<Block>.Sort.Descending(b => b.Level), Builders<Block>.Projection.Expression(b => (Block?)b), cancellation);


        private Task<TResult> GetFirstAsync<TResult>(
            SortDefinition<Block> sort,
            ProjectionDefinition<Block, TResult> projection,
            CancellationToken cancellation
        )
            => _dbContext.Get<Block>()
                .Find(Builders<Block>.Filter.Empty)
                .Sort(sort)
                .Project(projection)
                .FirstOrDefaultAsync(cancellation);

        public Task<Block?> FindAsync(string hash, CancellationToken cancellation)
            => FindAsync(b => b.Hash, hash, cancellation);

        public Task<Block?> FindAsync(int level, CancellationToken cancellation)
            => FindAsync(b => b.Level, level, cancellation);

        private Task<Block?> FindAsync<TField>(Expression<Func<Block, TField>> field, TField uniqueValue, CancellationToken cancellation)
            => _dbContext.Get<Block>()
                .Find(Builders<Block>.Filter.Eq(field, uniqueValue))
                .SingleOrDefaultAsync(cancellation)
                .AsNullableResult();
    }
}