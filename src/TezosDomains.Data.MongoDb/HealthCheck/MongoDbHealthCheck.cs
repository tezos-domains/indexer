﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.MongoDb.Configurations;

namespace TezosDomains.Data.MongoDb.HealthCheck
{
    public sealed class MongoDbHealthCheck : IHealthCheck
    {
        private readonly ILogger<MongoDbHealthCheck> _logger;

        private readonly MongoClient _mongoClient;
        private readonly string _specifiedDatabase;

        public MongoDbHealthCheck(MongoClient mongoClient, MongoDbConfiguration configuration, ILogger<MongoDbHealthCheck> logger)
        {
            _mongoClient = mongoClient;
            _logger = logger;
            _specifiedDatabase = configuration.DatabaseName;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellation)
        {
            try
            {
                using var cursor = await _mongoClient
                    .GetDatabase(_specifiedDatabase)
                    .ListCollectionNamesAsync(cancellationToken: cancellation);
                await cursor.FirstOrDefaultAsync(cancellation);

                return HealthCheckResult.Healthy();
            }
            catch (Exception ex)
            {
                _logger.LogWarning("MongoDb health check failed", ex);
                return new HealthCheckResult(context.Registration.FailureStatus, exception: ex);
            }
        }
    }
}