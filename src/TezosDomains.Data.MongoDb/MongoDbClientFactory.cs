﻿using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Events;
using System;
using TezosDomains.Data.MongoDb.Configurations;

namespace TezosDomains.Data.MongoDb
{
    internal sealed class MongoDbClientFactory
    {
        private readonly MongoDbConfiguration _configuration;
        private readonly ILogger<MongoDbClientFactory> _logger;

        public MongoDbClientFactory(MongoDbConfiguration configuration, ILogger<MongoDbClientFactory> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public MongoClient Create()
        {
            if (string.IsNullOrWhiteSpace(_configuration.DatabaseName))
                throw new ArgumentException("Mongo database cannot be null or empty", nameof(_configuration.DatabaseName));

            var settings = MongoClientSettings.FromConnectionString(_configuration.ConnectionString);

            if (_configuration.Timeout != default)
                settings.ConnectTimeout = settings.ServerSelectionTimeout = settings.WaitQueueTimeout = _configuration.Timeout;
            if (_configuration.MaxConnectionPoolSize != null)
                settings.MaxConnectionPoolSize = _configuration.MaxConnectionPoolSize.Value;

            settings.ClusterConfigurator = cluster =>
            {
                if (!_logger.IsEnabled(LogLevel.Trace))
                    return;

                cluster.Subscribe<CommandStartedEvent>(
                    startedEvent => _logger.LogTrace(
                        "MongoDb command '{eCommandName}' started with request: {eCommand}",
                        startedEvent.CommandName,
                        startedEvent.Command.ToJson()
                    )
                );
                cluster.Subscribe<CommandSucceededEvent>(
                    succeededEvent => _logger.LogTrace(
                        "MongoDb command '{eCommandName}' finished after {eDuration} with response: {eReply}",
                        succeededEvent.CommandName,
                        succeededEvent.Duration,
                        succeededEvent.Reply.ToJson()
                    )
                );
            };

            _logger.LogInformation(
                "Creating MongoClient with servers: '{servers}', user: '{user}' and database: '{database}' ",
                _configuration.Servers,
                _configuration.Username,
                _configuration.DatabaseName
            );
            return new MongoClient(settings);
        }
    }
}