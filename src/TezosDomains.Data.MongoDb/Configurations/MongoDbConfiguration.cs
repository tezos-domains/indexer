﻿using MongoDB.Driver;
using System;
using System.ComponentModel.DataAnnotations;
using TezosDomains.Common.Configuration;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Configurations
{
    public sealed record MongoDbConfiguration : IConfigurationInstance
    {
        public TimeSpan DistributedLockLifetime { get; init; }
        public TimeSpan Timeout { get; init; }

        [Range(0, int.MaxValue)]
        public int? MaxConnectionPoolSize { get; init; }

        private readonly string _connectionString = null!;

        [Required]
        public string ConnectionString
        {
            get => _connectionString;
            init
            {
                try
                {
                    _connectionString = value;

                    var mongoUrl = MongoUrl.Create(value);
                    Servers = mongoUrl.Servers.Join(",");
                    Username = mongoUrl.Username;
                    DatabaseName ??= mongoUrl.DatabaseName;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Invalid {nameof(ConnectionString)} with value: {value}", ex);
                }
            }
        }

        public string Servers { get; private init; } = null!;
        public string Username { get; private init; } = null!;


        private readonly string _databaseName = null!;

        public string DatabaseName
        {
            get => _databaseName;
            init => _databaseName = value.WhiteSpaceToNull().GuardNotNull($"{nameof(DatabaseName)} can't be empty.");
        }

        // Excludes ConnectionString b/c it contains password.
        object IConfigurationInstance.GetDiagnosticInfo() => new { DistributedLockLifetime, Timeout, Servers, Username, DatabaseName };
    }
}