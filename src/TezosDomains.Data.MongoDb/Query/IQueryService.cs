﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query
{
    public interface IQueryService<TDocument>
        where TDocument : IHasDbCollection
    {
        Task<TDocument?> FindSingleAsync(FilterDefinition<TDocument> filter, CancellationToken cancellation);
        Task<List<TDocument>> FindAllAsync(DocumentQuery<TDocument> query, CancellationToken cancellation);
        Task<long> CountAsync(FilterDefinition<TDocument> filter, CancellationToken cancellation);
    }
}