﻿using MongoDB.Driver;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Auctions
{
    [DocumentFilterDescription(typeof(Auction))]
    public sealed record AuctionsFilter : DocumentWithHistoryFilter<Auction, AuctionsFilter, HistoryFilterContext>
    {
        [DocumentFilterPropertyDescription(typeof(Auction), nameof(Auction.DomainName))]
        public StringFilter? DomainName { get; set; }

        [DocumentFilterPropertyDescription(typeof(Auction), "State")]
        public AuctionStateFilter? State { get; set; }

        [DescriptionFormat("Filters {0}-s by bidder's address of the {1:camel}.", nameof(Auction), nameof(Auction.HighestBid))]
        public AddressFilter? HighestBidder { get; set; }

        [DescriptionFormat("Filters {0}-s by {1:camel} addresses of {2:camel}.", nameof(Auction), nameof(Bid.Bidder), nameof(Auction.Bids))]
        public AddressArrayFilter? Bidders { get; set; }

        [DocumentFilterPropertyDescription(typeof(Auction), nameof(Auction.EndsAtUtc))]
        public NullableDateTimeFilter? EndsAtUtc { get; set; }

        [DocumentFilterPropertyDescription(typeof(Auction), nameof(Auction.BidCount))]
        public IntFilter? BidCount { get; set; }

        public override FilterDefinition<Auction>? ToDbFilter(FilterDefinitionBuilder<Auction> builder, HistoryFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                DomainName?.ToLowerInvariant().ToDbFilter(builder, d => d.DomainName),
                HighestBidder?.ToDbFilter(builder, a => a.HighestBid.Bidder),
                Bidders?.ToDbFilter(builder, a => a.Bids, b => b.Bidder),
                State?.ToDbFilter(builder, context),
                EndsAtUtc?.ToDbFilter(builder, d => d.EndsAtUtc),
                BidCount?.ToDbFilter(builder, d => d.BidCount)
            );
    }
}