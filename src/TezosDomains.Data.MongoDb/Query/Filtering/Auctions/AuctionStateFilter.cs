﻿using MongoDB.Driver;
using System;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Auctions
{
    [DescriptionFormat("Filters {0}-s by their {1}.", nameof(Auction), "state")]
    public sealed record AuctionStateFilter : EnumFilter<AuctionState>
    {
        public FilterDefinition<Auction>? ToDbFilter(FilterDefinitionBuilder<Auction> builder, HistoryFilterContext context)
            => builder.AndNullable(
                In?.Count > 0 ? builder.Or(In.Select(s => Build(s, context.Timestamp))) : null,
                NotIn?.Count > 0 ? builder.Or(NotIn.Select(s => builder.Not(Build(s, context.Timestamp)))) : null
            );

        private static FilterDefinition<Auction> Build(AuctionState state, DateTime atUtc)
            => state switch
            {
                AuctionState.InProgress => AuctionFilters.InProgress(atUtc),
                AuctionState.CanBeSettled => AuctionFilters.CanBeSettled(atUtc),
                AuctionState.SettlementExpired => AuctionFilters.SettlementExpired(atUtc),
                AuctionState.Settled => AuctionFilters.Settled(),
                _ => throw state.CreateInvalidError(),
            };
    }
}