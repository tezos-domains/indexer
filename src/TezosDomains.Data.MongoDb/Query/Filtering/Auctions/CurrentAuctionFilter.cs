﻿using MongoDB.Driver;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Auctions
{
    public sealed record CurrentAuctionFilter(
        string DomainName
    ) : DocumentWithHistoryFilter<Auction, CurrentAuctionFilter, HistoryFilterContext>
    {
        public override FilterDefinition<Auction>? ToDbFilter(FilterDefinitionBuilder<Auction> builder, HistoryFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                AuctionFilters.Current(context.Timestamp, DomainName)
            );
    }
}