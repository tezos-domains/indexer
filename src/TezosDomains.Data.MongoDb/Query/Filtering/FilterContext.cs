﻿using System;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering;

public record EmptyFilterContext(bool IsTopLevelFilter = true);

public record HistoryFilterContext(
    DateTime Timestamp,
    bool ExcludeHistory,
    bool IsTopLevelFilter = true
) : EmptyFilterContext(IsTopLevelFilter);

public sealed record RecordFilterContext(
    DateTime Timestamp,
    bool ExcludeHistory,
    RecordValidity Validity,
    bool IsTopLevelFilter = true
) : HistoryFilterContext(Timestamp, ExcludeHistory, IsTopLevelFilter);

public record OffersFilterContext(
    DateTime Timestamp,
    bool ExcludeHistory,
    TimeSpan DomainIsExpiringThreshold,
    bool IsTopLevelFilter = true
) : HistoryFilterContext(Timestamp, ExcludeHistory, IsTopLevelFilter);