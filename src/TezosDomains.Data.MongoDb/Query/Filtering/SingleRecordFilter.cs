﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering
{
    public sealed record SingleRecordFilter<TRecord> : RecordFilter<TRecord, SingleRecordFilter<TRecord>>
        where TRecord : class, IRecord
    {
        public SingleRecordFilter(string key, RecordValidity validity)
        {
            Key = key;
            Validity = validity;
        }

        public string Key { get; set; }

        public override FilterDefinition<TRecord>? ToDbFilter(FilterDefinitionBuilder<TRecord> builder, RecordFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                builder.ByKeys(new[] { Key })
            );
    }
}