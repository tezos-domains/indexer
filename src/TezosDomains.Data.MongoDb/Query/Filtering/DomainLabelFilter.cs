﻿using MongoDB.Driver;
using System.ComponentModel;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Domains;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering;

[DocumentFilterPropertyDescriptionAttribute(typeof(Domain), nameof(Domain.Label))]
public sealed record DomainLabelFilter : StringFilter
{
    [Description("Specifies the domain label composition (letters, numbers, dash) to match.")]
    public LabelCompositionFilter? Composition { get; set; }

    public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
        FilterDefinitionBuilder<TDocument> builder,
        FieldDefinition<TDocument, string?> field
    )
        => builder.AndNullable(
            base.ToDbFilter(builder, field),
            Composition?.ToDbFilter(builder, field)
        );
}