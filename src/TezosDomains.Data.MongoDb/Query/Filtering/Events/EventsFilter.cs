﻿using MongoDB.Driver;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Events
{
    [DocumentFilterDescription(typeof(Event))]
    public sealed record EventsFilter : NestedFilter<Event, EventsFilter, EmptyFilterContext>
    {
        [DescriptionFormat(
            "Filters {0}-s by related address ({1:camel}, {2:camel}, {3:camel}, {4:camel}, {5:camel} or {6:camel}).",
            nameof(Event),
            nameof(DomainBuyEvent.SourceAddress),
            nameof(AuctionBidEvent.PreviousBidderAddress),
            nameof(AuctionEndEvent.Participants),
            nameof(DomainTransferEvent.NewOwner),
            "SellerAddress",
            "BuyerAddress"
        )]
        public AddressFilter? Address { get; set; }

        [DocumentFilterPropertyDescription(typeof(Event), nameof(DomainBuyEvent.DomainName))]
        public StringFilter? DomainName { get; set; }

        [DocumentFilterPropertyDescription(typeof(Event), "Type")]
        public EventTypeFilter? Type { get; set; }

        [DescriptionFormat("Filters {0}-s by block in which they were created.", nameof(Event))]
        public AssociatedBlockFilter? Block { get; set; }

        [DescriptionFormat("Filters {0}-s by their {1:camel} (bought, sold, auctioned domain).", nameof(Event), nameof(OfferPlacedEvent.Price))]
        public MutezFilter? Price { get; set; }

        public override FilterDefinition<Event>? ToDbFilter(FilterDefinitionBuilder<Event> builder, EmptyFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                AddressToDbFilter(builder),
                DomainName?.ToDbFilter(builder, nameof(DomainBuyEvent.DomainName)),
                Type?.ToDbFilter(builder),
                Block?.ToDbFilter(builder),
                PriceToDbFilter(builder)
            );

        private FilterDefinition<Event>? PriceToDbFilter(FilterDefinitionBuilder<Event> builder)
        {
            if (Price == null)
                return null;

            return builder.Or(
                //same for DomainBuyEvent, DomainRenewEvent, OfferPlacedEvent, OfferUpdatedEvent, OfferExecutedEvent
                Price?.ToDbFilter(builder, nameof(OfferPlacedEvent.Price)),
                Price?.ToDbFilter(builder, nameof(AuctionSettleEvent.WinningBid))
            );
        }

        private FilterDefinition<Event>? AddressToDbFilter(FilterDefinitionBuilder<Event> builder)
        {
            if (Address == null)
                return null;

            return builder.Or(
                Address.ToDbFilter(builder, nameof(DomainBuyEvent.SourceAddress)),
                Address.ToDbFilter(builder, nameof(AuctionBidEvent.PreviousBidderAddress)),
                Address.ToDbFilter(builder, nameof(AuctionEndEvent.Participants)),
                Address.ToDbFilter(builder, nameof(DomainTransferEvent.NewOwner)),
                Address.ToDbFilter(builder, nameof(OfferExecutedEvent.InitiatorAddress))
            );
        }
    }
}