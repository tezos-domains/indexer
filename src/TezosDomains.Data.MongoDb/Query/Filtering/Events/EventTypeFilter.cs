﻿using MongoDB.Driver;
using System.Linq;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Events
{
    [DescriptionFormat("Filters {0}-s by their {1}.", nameof(Event), "type")]
    public sealed record EventTypeFilter : EnumFilter<EventType>
    {
        private static readonly StringFieldDefinition<Event, string> TypeField = new("_t");

        public FilterDefinition<Event>? ToDbFilter(FilterDefinitionBuilder<Event> builder)
            // Call ToString() b/c List<Enum> is not serialized as string https://github.com/mongodb/mongo-csharp-driver/pull/305#issuecomment-731475503
            => builder.AndNullable(
                In?.Count > 0 ? builder.In(TypeField, In.Select(i => i.ToString())) : null,
                NotIn?.Count > 0 ? builder.Nin(TypeField, NotIn.Select(i => i.ToString())) : null
            );
    }
}