﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering
{
    [DocumentFilterDescription(typeof(ReverseRecord))]
    public sealed record ReverseRecordsFilter : RecordFilter<ReverseRecord, ReverseRecordsFilter>
    {
        [DocumentFilterPropertyDescription(typeof(ReverseRecord), nameof(ReverseRecord.Address))]
        public AddressFilter? Address { get; set; }

        [DocumentFilterPropertyDescription(typeof(ReverseRecord), nameof(ReverseRecord.Name))]
        public NullableStringFilter? Name { get; set; }

        [DocumentFilterPropertyDescription(typeof(ReverseRecord), nameof(ReverseRecord.Owner))]
        public AddressFilter? Owner { get; set; }

        public override FilterDefinition<ReverseRecord>? ToDbFilter(FilterDefinitionBuilder<ReverseRecord> builder, RecordFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                Address?.ToDbFilter(builder, r => r.Address),
                Name?.ToLowerInvariant().ToDbFilter(builder, r => r.Name),
                Owner?.ToDbFilter(builder, r => r.Owner)
            );
    }
}