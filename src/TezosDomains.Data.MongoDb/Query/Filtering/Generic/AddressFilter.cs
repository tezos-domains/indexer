﻿using MongoDB.Driver;
using System.ComponentModel;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    [PropertyFilterDescription($"{nameof(Address)}!")]
    public record AddressFilter : ReferenceTypeFilter<Address, string>
    {
        [DescriptionFormat("Specifies a prefix of the {0} to match.", nameof(Address))]
        public AddressPrefix? StartsWith { get; set; }

        protected override string ConvertToDb(Address address) => address.Value;

        public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, string?> field
        )
            => builder.AndNullable(
                base.ToDbFilter(builder, field),
                FilterByRegex(field, StartsWith?.ToInvariantString(), startsWith => $"^{startsWith}")
            );
    }

    [PropertyFilterDescription(nameof(Address))]
    public record NullableAddressFilter : AddressFilter
    {
        [Description(IsNullDescription)]
        public bool? IsNull
        {
            get => IsNullInternal;
            set => IsNullInternal = value;
        }
    }
}