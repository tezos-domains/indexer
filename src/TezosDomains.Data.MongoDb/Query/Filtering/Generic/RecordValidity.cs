﻿using MongoDB.Driver;
using System;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    [GraphQLOrderAsDeclared]
    [DescriptionFormat("Validity of the record based on its validity {0}.", nameof(DateTime))]
    public enum RecordValidity
    {
        [DescriptionFormat("Validity {0} is in the future or null (domain does not expire at all).", nameof(DateTime))]
        Valid,

        [DescriptionFormat("Validity {0} is in the past.", nameof(DateTime))]
        Expired,

        [DescriptionFormat("All records regardless of their validity {0}.", nameof(DateTime))]
        All,
    }

    public static class RecordValidityHelper
    {
        public static FilterDefinition<TRecord>? ToDbFilter<TRecord>(this RecordValidity validity, FilterDefinitionBuilder<TRecord> builder, DateTime atUtc)
            where TRecord : IRecord
            => validity switch
            {
                RecordValidity.Valid => builder.Gte(r => r.ExpiresAtUtc, atUtc),
                RecordValidity.Expired => builder.Lt(r => r.ExpiresAtUtc, atUtc),
                RecordValidity.All => null,
                _ => throw validity.CreateInvalidError(),
            };
    }

    public sealed class RecordValidityDescriptionAttribute : DescriptionFormatAttribute
    {
        public RecordValidityDescriptionAttribute()
            : base("Filters validity of the entity based on its validity {0}. Default is {1:upper}.", nameof(DateTime), default(RecordValidity))
        {
        }
    }
}