﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    [Description("Filter conditions for filtering `id` property.")]
    public record IdEqualityFilter : ReferenceTypeFilter<string, string>
    {
        protected override string ConvertToDb(string value)
            //remove NodeGraphDto.Id prefix (GraphType name)
            //if ':' not found, it returns again the whole string
            => value.Substring(value.IndexOf(":", StringComparison.Ordinal) + 1);
    }

    [Description("Filter conditions for filtering block `hash` property.")]
    public record StringEqualityFilter : ReferenceTypeFilter<string, string>
    {
        protected override string ConvertToDb(string value) => value;
    }

    [PropertyFilterDescription($"{nameof(String)}!")]
    public record StringFilter : StringEqualityFilter
    {
        [Description("Specifies the prefix of the value to match.")]
        public string? StartsWith { get; set; }

        [Description("Specifies the suffix of the value to match.")]
        public string? EndsWith { get; set; }

        [Description("Specifies the similar string to the value to match.")]
        public string? Like { get; set; }

        [Description("Specifies the value length to match.")]
        public StringLengthFilter? Length { get; set; }

        public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, string?> field
        )
            => builder.AndNullable(
                base.ToDbFilter(builder, field),
                FilterByRegex(field, StartsWith, startsWith => $"^{startsWith}"),
                FilterByRegex(field, EndsWith, endsWith => $"{endsWith}$"),
                FilterByRegex(field, Like, like => like),
                FilterByRegex(field, Length?.GreaterThanOrEqualTo?.ToInvariantString(), gteLength => $"^.{{{gteLength},}}$"),
                FilterByRegex(field, Length?.LessThanOrEqualTo?.ToInvariantString(), lteLength => $"^.{{1,{lteLength}}}$"),
                FilterByRegex(field, Length?.EqualTo?.ToInvariantString(), eqLength => $"^.{{{eqLength}}}$")
            );

        private static readonly IReadOnlyList<PropertyInfo> Properties = typeof(StringFilter).GetProperties();

        public StringFilter ToLowerInvariant() // Lowers inherited properties too
        {
            var clone = new StringFilter();
            foreach (var property in Properties)
            {
                var value = property.GetValue(this);
                property.SetValue(clone, value is string str ? str.ToLowerInvariant() : value);
            }

            return clone;
        }

        [DescriptionFormat("Filter conditions for filtering length of associated property of type {0}.", nameof(String))]
        public sealed record StringLengthFilter
        {
            [Description("Specifies a shorter string than.")]
            public int? LessThanOrEqualTo { get; set; }

            [Description("Specifies a longer string than.")]
            public int? GreaterThanOrEqualTo { get; set; }

            [Description("Specifies a string length to match.")]
            public int? EqualTo { get; set; }
        }
    }

    [PropertyFilterDescription(nameof(String))]
    public record NullableStringFilter : StringFilter
    {
        [Description(IsNullDescription)]
        public bool? IsNull
        {
            get => IsNullInternal;
            set => IsNullInternal = value;
        }
    }
}