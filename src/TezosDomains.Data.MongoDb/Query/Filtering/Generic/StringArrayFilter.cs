﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract record ArrayFilter<TField>
    {
        [Description("Specifies a value that should be included in the array of the associated property.")]
        public TField? Include { get; set; }

        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, IEnumerable<string?>>> field
        )
            => Include != null ? builder.AnyEq(field, Include.ToString()) : null;

        public FilterDefinition<TDocument>? ToDbFilter<TDocument, TArrayElement>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, IEnumerable<TArrayElement>>> array,
            Expression<Func<TArrayElement, string?>> field
        )
            => Include != null ? builder.ElemMatch(array, Builders<TArrayElement>.Filter.Eq(field, Include.ToString())) : null;
    }

    [PropertyFilterDescription($"[{nameof(String)}!]")]
    public sealed record StringArrayFilter : ArrayFilter<string>
    {
        public StringArrayFilter ToLowerInvariant()
            => new() { Include = Include?.ToLowerInvariant() };
    }

    [PropertyFilterDescription($"[{nameof(Address)}!]")]
    public sealed record AddressArrayFilter : ArrayFilter<Address>
    {
    }
}