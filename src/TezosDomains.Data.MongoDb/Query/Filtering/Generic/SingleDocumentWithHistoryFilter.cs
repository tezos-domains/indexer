﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public sealed record SingleDocumentWithHistoryFilter<TDocument>(
        string Key
    ) : DocumentWithHistoryFilter<TDocument, SingleDocumentWithHistoryFilter<TDocument>, HistoryFilterContext>
        where TDocument : IHasHistory
    {
        public override FilterDefinition<TDocument>? ToDbFilter(FilterDefinitionBuilder<TDocument> builder, HistoryFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                builder.ByKeys(new[] { Key })
            );
    }
}