﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract record DocumentWithHistoryFilter<TDocument, TFilter, TContext> : NestedFilter<TDocument, TFilter, TContext>
        where TDocument : IHasHistory
        where TFilter : DocumentWithHistoryFilter<TDocument, TFilter, TContext>
        where TContext : HistoryFilterContext
    {
        public override FilterDefinition<TDocument>? ToDbFilter(FilterDefinitionBuilder<TDocument> builder, TContext context)
        {
            return builder.AndNullable(
                base.ToDbFilter(builder, context),
                GetHistoryFilter()
            );

            FilterDefinition<TDocument>? GetHistoryFilter()
            {
                //only add history condition to top-level query
                if (!context.IsTopLevelFilter)
                    return null;

                if (context.ExcludeHistory)
                    return builder.Empty.ExcludeHistory();

                return builder.And(
                    builder.Lte(d => d.Block.Timestamp, context.Timestamp),
                    builder.Gt(d => d.ValidUntilTimestamp, context.Timestamp)
                );
            }
        }
    }
}