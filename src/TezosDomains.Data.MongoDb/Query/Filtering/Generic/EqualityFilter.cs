using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract record EqualityFilter<TField, TDbField>
    {
        protected const string InDescription = "Specifies a list of exact values to match.";
        protected const string NotInDescription = "Specifies a list of exact values NOT to match.";

        protected const string IsNullDescription =
            "Matches `null` value if `true` is specified."
            + " Matches not `null` values if `false` is specified."
            + " Matches everything if nothing is specified.";

        protected virtual bool? IsNullInternal { get; set; }

        protected virtual TDbField? NullValue => default;

        [Description("Specifies the exact value to match.")]
        public TField? EqualTo { get; set; }

        [Description("Specifies the exact value NOT to match.")]
        public TField? NotEqualTo { get; set; }

        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(FilterDefinitionBuilder<TDocument> builder, Expression<Func<TDocument, TDbField?>> field)
            => ToDbFilter(builder, new ExpressionFieldDefinition<TDocument, TDbField?>(field));

        public virtual FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, TDbField?> field
        )
        {
            var (inList, notInList) = GetFilterLists();
            return builder.AndNullable(
                IsNullInternal switch
                {
                    null => null,
                    true => builder.Eq(field, NullValue),
                    false => builder.Ne(field, NullValue),
                },
                Filter(field, builder.Eq, EqualTo),
                Filter(field, builder.Ne, NotEqualTo),
                inList?.Count > 0 ? builder.In(field, inList.Select(v => (TDbField?)ConvertToDb(v))) : null,
                notInList?.Count > 0 ? builder.Nin(field, notInList.Select(v => (TDbField?)ConvertToDb(v))) : null
            );
        }

        protected abstract TDbField ConvertToDb(TField value);
        protected abstract (List<TField>? In, List<TField>? NotIn) GetFilterLists();


        protected FilterDefinition<TDocument>? Filter<TDocument>(
            FieldDefinition<TDocument, TDbField?> field,
            Func<FieldDefinition<TDocument, TDbField?>, TDbField?, FilterDefinition<TDocument>> @operator,
            TField? filterValue
        )
            => filterValue != null ? @operator(field, ConvertToDb(filterValue)) : null;

        protected static FilterDefinition<TDocument>? FilterByRegex<TDocument>(
            FieldDefinition<TDocument, string?> field,
            string? filterValue,
            Func<string, string> buildRegexFromEscapedValue
        )
        {
            if (string.IsNullOrEmpty(filterValue))
                return null;

            var regex = buildRegexFromEscapedValue(Regex.Escape(filterValue));
            return Builders<TDocument>.Filter.Regex(field, regex);
        }
    }

    public abstract record ReferenceTypeFilter<TField, TDbField> : EqualityFilter<TField, TDbField>
        where TField : class
    {
        [Description(InDescription)]
        public List<TField>? In { get; set; }

        [Description(NotInDescription)]
        public List<TField>? NotIn { get; set; }

        protected override (List<TField>? In, List<TField>? NotIn) GetFilterLists() => (In, NotIn);
    }

    public abstract record ValueTypeFilter<TField, TDbField> : EqualityFilter<TField?, TDbField>
        where TField : struct
    {
        [Description(InDescription)]
        public List<TField>? In { get; set; }

        [Description(NotInDescription)]
        public List<TField>? NotIn { get; set; }

        protected override (List<TField?>? In, List<TField?>? NotIn) GetFilterLists() => (In?.ConvertAll(v => (TField?)v), NotIn?.ConvertAll(v => (TField?)v));
    }
}