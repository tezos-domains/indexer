﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    [DescriptionFormat("Filter conditions for filtering items by associated {0} (of last change).", nameof(Block))]
    public sealed record AssociatedBlockFilter
    {
        private const string PropertyDescription = "Filters items by {0:camel} of the associated {1}.";

        [DescriptionFormat(PropertyDescription, nameof(Block.Hash), nameof(Block))]
        public StringEqualityFilter? Hash { get; set; }

        [DescriptionFormat(PropertyDescription, nameof(Block.Level), nameof(Block))]
        public IntFilter? Level { get; set; }

        [DescriptionFormat(PropertyDescription, nameof(Block.Timestamp), nameof(Block))]
        public DateTimeFilter? Timestamp { get; set; }

        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(FilterDefinitionBuilder<TDocument> builder)
            where TDocument : IHasBlock
            => builder.AndNullable(
                Hash?.ToDbFilter(builder, d => d.Block.Hash),
                Level?.ToDbFilter(builder, d => d.Block.Level),
                Timestamp?.ToDbFilter(builder, d => d.Block.Timestamp)
            );
    }
}