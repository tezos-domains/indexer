﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract record RecordFilter<TRecord, TFilter> : DocumentWithHistoryFilter<TRecord, TFilter, RecordFilterContext>
        where TRecord : class, IRecord
        where TFilter : RecordFilter<TRecord, TFilter>
    {
        [RecordValidityDescription]
        public RecordValidity? Validity { get; set; }

        [DescriptionFormat("Filters items by their {0:camel}.", nameof(Domain.ExpiresAtUtc))]
        public NullableDateTimeFilter? ExpiresAtUtc { get; set; }

        public override FilterDefinition<TRecord>? ToDbFilter(FilterDefinitionBuilder<TRecord> builder, RecordFilterContext context)
        {
            return builder.AndNullable(
                base.ToDbFilter(builder, context),
                GetValidityDbFilter(),
                ExpiresAtUtc?.ToDbFilter(builder, d => d.ExpiresAtUtc)
            );

            FilterDefinition<TRecord>? GetValidityDbFilter()
            {
                var validity = Validity ?? default;
                return context.IsTopLevelFilter || context.Validity != validity ? validity.ToDbFilter(builder, context.Timestamp) : null;
            }
        }
    }
}