﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract record NestedFilter<TDocument, TFilter, TContext> : IDocumentFilter<TDocument, TContext>
        where TDocument : IHasDbCollection
        where TFilter : class, IDocumentFilter<TDocument, TContext>
        where TContext : EmptyFilterContext
    {
        private const string PropertyDescription =
            "Additional filters of the same type are joined using the {0} operator. It's joined using AND with other filter properties.";

        [DescriptionFormat(PropertyDescription, "OR")]
        public List<TFilter>? Or { get; set; }

        [DescriptionFormat(PropertyDescription, "AND")]
        public List<TFilter>? And { get; set; }

        public virtual FilterDefinition<TDocument>? ToDbFilter(FilterDefinitionBuilder<TDocument> builder, TContext context)
            => builder.AndNullable(
                Filter(Or, builder.Or, context),
                Filter(And, builder.And, context)
            );

        private static FilterDefinition<TDocument>? Filter(
            List<TFilter>? children,
            Func<IEnumerable<FilterDefinition<TDocument>>, FilterDefinition<TDocument>> join,
            TContext context
        )
            => children?.Count > 0 ? join(children.ConvertAll(f => f.ToDbFilter(context with { IsTopLevelFilter = false }))) : null;
    }
}