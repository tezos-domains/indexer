﻿using MongoDB.Driver;
using System;
using System.ComponentModel;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract record ComparableFilter<TField> : ValueTypeFilter<TField, TField?>
        where TField : struct
    {
        [Description("Matches values less than the specified value.")]
        public TField? LessThan { get; set; }

        [Description("Matches values less than or equal to the specified value.")]
        public TField? LessThanOrEqualTo { get; set; }

        [Description("Matches values greater than the specified value.")]
        public TField? GreaterThan { get; set; }

        [Description("Matches values greater than or equal to the specified value.")]
        public TField? GreaterThanOrEqualTo { get; set; }

        public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, TField?> field
        )
            => builder.AndNullable(
                base.ToDbFilter(builder, field),
                Filter(field, builder.Lt, LessThan),
                Filter(field, builder.Lte, LessThanOrEqualTo),
                Filter(field, builder.Gt, GreaterThan),
                Filter(field, builder.Gte, GreaterThanOrEqualTo)
            );

        protected override TField? ConvertToDb(TField? value) => value;
    }

    [PropertyFilterDescription($"{nameof(DateTime)}!")]
    public record DateTimeFilter : ComparableFilter<DateTime>
    {
    }

    [PropertyFilterDescription(nameof(DateTime))]
    public record NullableDateTimeFilter : DateTimeFilter
    {
        [Description(IsNullDescription)]
        public bool? IsNull
        {
            get => IsNullInternal;
            set => IsNullInternal = value;
        }

        protected override DateTime? NullValue => DateTime.MaxValue;

        public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, DateTime?> field
        )
            => builder.AndNullable(
                base.ToDbFilter(builder, field),
                Filter(field, builder.Lt, LessThan),
                Filter(field, builder.Lte, LessThanOrEqualTo),
                //Historical usage when Nullable DateTime wasn't represented as DateTime.Max
                //limit the upper bound with DateTime.Max so we distinguish DateTime.Max == null and the DateTime range
                GreaterThan.HasValue ? builder.And(Filter(field, builder.Gt, GreaterThan), builder.Lt(field, DateTime.MaxValue)) : null,
                GreaterThanOrEqualTo.HasValue ? builder.And(Filter(field, builder.Gte, GreaterThanOrEqualTo), builder.Lt(field, DateTime.MaxValue)) : null
            );
    }

    [PropertyFilterDescription("Int!")]
    public record IntFilter : ComparableFilter<int>
    {
    }

    [PropertyFilterDescription("Mutez!")]
    public record MutezFilter : ComparableFilter<decimal>
    {
    }
}