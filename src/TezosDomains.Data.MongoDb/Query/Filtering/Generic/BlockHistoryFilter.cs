﻿using System;
using System.ComponentModel;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    [BlockHistoryDescription]
    public sealed record BlockHistoryFilter
    {
        [Description("Specifies a point in time using block `level`.")]
        public int? Level { get; set; }

        [Description("Specifies a point in time using an arbitrary timestamp matched against the block's `timestamp`.")]
        public DateTime? Timestamp { get; set; }
    }

    public sealed class BlockHistoryDescriptionAttribute : DescriptionAttribute
    {
        public BlockHistoryDescriptionAttribute() : base("Filters entities by block history.")
        {
        }
    }
}