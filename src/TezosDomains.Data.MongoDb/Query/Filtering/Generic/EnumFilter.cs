﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract record EnumFilter<TEnum>
        where TEnum : Enum
    {
        [Description("Specifies the exact values to be matched.")]
        public List<TEnum>? In { get; set; }

        [Description("Specifies the exact values to be excluded.")]
        public List<TEnum>? NotIn { get; set; }
    }
}