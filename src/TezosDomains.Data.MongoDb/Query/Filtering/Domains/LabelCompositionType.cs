﻿namespace TezosDomains.Data.MongoDb.Query.Filtering.Domains;

public enum LabelCompositionType
{
    Letters,
    Numbers,
    Dash
}