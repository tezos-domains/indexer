﻿using MongoDB.Driver;
using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Domains;

[DescriptionFormat("Filters {0}-s by characters within their {1}.", nameof(Domain), nameof(Domain.Label))]
public sealed record LabelCompositionFilter : EnumFilter<LabelCompositionType>
{
    public FilterDefinition<TDocument>? ToDbFilter<TDocument>(FilterDefinitionBuilder<TDocument> builder, FieldDefinition<TDocument, string?> field)
        => builder.AndNullable(
            In?.Count > 0 ? builder.Regex(field, GetPattern(In)) : null,
            NotIn?.Count > 0 ? builder.Regex(field, GetPattern(NotIn, isInverseMatch: true)) : null
        );


    public string GetPattern(IReadOnlyList<LabelCompositionType> compositions, bool isInverseMatch = false)
    {
        string pattern = "^[";

        if (isInverseMatch)
        {
            pattern += "^"; // Add the negation symbol to the pattern
        }

        foreach (var composition in compositions)
        {
            pattern += composition switch
            {
                LabelCompositionType.Letters => "a-zA-Z",
                LabelCompositionType.Numbers => "0-9",
                LabelCompositionType.Dash => "-",
                _ => throw composition.CreateInvalidError()
            };
        }

        pattern += "]+$";

        return pattern;
    }
}