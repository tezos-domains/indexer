﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Marketplace;

/// <summary>
/// Find SingleOrDefault BuyOffer in following states:
/// Valid - domain valid, offer valid
/// Other states except Removed,Executed - buyer is available to withdraw
/// </summary>
/// <param name="DomainName"></param>
public sealed record CurrentBuyOfferFilter(
    string DomainName,
    Address BuyerAddress
) : DocumentWithHistoryFilter<Offer, CurrentBuyOfferFilter, HistoryFilterContext>
{
    public override FilterDefinition<Offer>? ToDbFilter(FilterDefinitionBuilder<Offer> builder, HistoryFilterContext context)
        => builder.AndNullable(
            base.ToDbFilter(builder, context),
            OfferFilters.ByPresence(OfferBlockchainPresence.Present),
            builder.Eq(o => o.Domain!.Name, DomainName),
            builder.Eq(o => o.InitiatorAddress, (string)BuyerAddress),
            builder.Eq(o => o.Type, OfferType.BuyOffer)
        );
}