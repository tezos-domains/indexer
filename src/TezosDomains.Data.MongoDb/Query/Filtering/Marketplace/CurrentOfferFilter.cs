﻿using MongoDB.Driver;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Marketplace
{
    public sealed record CurrentOfferFilter(
        string DomainName
    ) : DocumentWithHistoryFilter<Offer, CurrentOfferFilter, HistoryFilterContext>
    {
        public override FilterDefinition<Offer>? ToDbFilter(FilterDefinitionBuilder<Offer> builder, HistoryFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                OfferFilters.Valid(context.Timestamp, domainNames: new[] { DomainName }),
                builder.Eq(o => o.Type, OfferType.SellOffer)
            );
    }
}