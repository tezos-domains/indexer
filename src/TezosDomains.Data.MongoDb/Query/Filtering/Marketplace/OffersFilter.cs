﻿using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Marketplace
{
    [DocumentFilterDescription(typeof(Offer))]
    public sealed record OffersFilter : OffersFilterBase<OffersFilter>
    {
        protected override Expression<Func<Offer, string?>> SellerAddressExpression => o => o.InitiatorAddress;
        protected override Expression<Func<Offer, string?>> BuyerAddressExpression => o => o.AcceptorAddress;
        protected override OfferType OfferType => OfferType.SellOffer;
    }

    [DocumentFilterDescription(typeof(Offer))]
    public sealed record BuyOffersFilter : OffersFilterBase<BuyOffersFilter>
    {
        protected override Expression<Func<Offer, string?>> SellerAddressExpression => o => o.AcceptorAddress;
        protected override Expression<Func<Offer, string?>> BuyerAddressExpression => o => o.InitiatorAddress;
        protected override OfferType OfferType => OfferType.BuyOffer;

        [DocumentFilterPropertyDescription(typeof(Offer), nameof(Id))]
        public IdEqualityFilter? Id { get; set; }

        public override FilterDefinition<Offer>? ToDbFilter(FilterDefinitionBuilder<Offer> builder, OffersFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                Id?.ToDbFilter(builder, o => o.OfferId)
            );
    }

    public abstract record OffersFilterBase<TOffersFilter> : DocumentWithHistoryFilter<Offer, TOffersFilter, OffersFilterContext>
        where TOffersFilter : DocumentWithHistoryFilter<Offer, TOffersFilter, OffersFilterContext>
    {
        protected abstract Expression<Func<Offer, string?>> SellerAddressExpression { get; }
        protected abstract Expression<Func<Offer, string?>> BuyerAddressExpression { get; }
        protected abstract OfferType OfferType { get; }


        [DocumentFilterPropertyDescription(typeof(Offer), nameof(SellerAddress))]
        public AddressFilter? SellerAddress { get; set; }

        [DocumentFilterPropertyDescription(typeof(Offer), nameof(BuyerAddress))]
        public AddressFilter? BuyerAddress { get; set; }

        [DescriptionFormat("Filters {0}-s by {1:camel} of offered {2:camel}.", nameof(Offer), nameof(Offer.Domain.Name), nameof(Offer.Domain))]
        public StringFilter? DomainName { get; set; }

        [DocumentFilterPropertyDescription(typeof(Offer), "State")]
        public OfferStateFilter? State { get; set; }

        [DocumentFilterPropertyDescription(typeof(Offer), nameof(Offer.CreatedAtUtc))]
        public DateTimeFilter? CreatedAtUtc { get; set; }

        [DocumentFilterPropertyDescription(typeof(Offer), nameof(Offer.ExpiresAtUtc))]
        public NullableDateTimeFilter? EndsAtUtc { get; set; }

        [DocumentFilterPropertyDescription(typeof(Offer), nameof(Offer.TokenId))]
        public IntFilter? TokenId { get; set; }

        [DocumentFilterPropertyDescription(typeof(Offer), nameof(Offer.Price))]
        public MutezFilter? Price { get; set; }

        public override FilterDefinition<Offer>? ToDbFilter(FilterDefinitionBuilder<Offer> builder, OffersFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                builder.Eq(o => o.Type, OfferType),
                SellerAddress?.ToDbFilter(builder, SellerAddressExpression),
                BuyerAddress?.ToDbFilter(builder, BuyerAddressExpression),
                DomainName?.ToLowerInvariant().ToDbFilter(builder, d => d.Domain!.Name),
                State?.ToDbFilter(builder, context),
                CreatedAtUtc?.ToDbFilter(builder, d => d.CreatedAtUtc),
                EndsAtUtc?.ToDbFilter(builder, d => d.ExpiresAtUtc),
                TokenId?.ToDbFilter(builder, d => d.TokenId),
                Price?.ToDbFilter(builder, d => d.Price)
            );
    }
}