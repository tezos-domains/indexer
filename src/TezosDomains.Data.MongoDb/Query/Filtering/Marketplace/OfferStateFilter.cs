﻿using MongoDB.Driver;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Filters;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Marketplace
{
    [DescriptionFormat("Filters {0}-s by their {1}.", nameof(Offer), "state")]
    public sealed record OfferStateFilter : EnumFilter<OfferState>
    {
        public FilterDefinition<Offer>? ToDbFilter(FilterDefinitionBuilder<Offer> builder, OffersFilterContext context)
            => builder.AndNullable(
                In?.Count > 0 ? builder.Or(In.Select(s => Build(s, context))) : null,
                NotIn?.Count > 0 ? builder.Or(NotIn.Select(s => builder.Not(Build(s, context)))) : null
            );

        private static FilterDefinition<Offer> Build(OfferState state, OffersFilterContext context)
            => state switch
            {
                OfferState.Active => OfferFilters.Active(context.DomainIsExpiringThreshold, context.Timestamp),
                OfferState.Executed => OfferFilters.ByPresence(OfferBlockchainPresence.Executed),
                OfferState.Removed => OfferFilters.ByPresence(OfferBlockchainPresence.Removed),

                OfferState.DomainIsExpiringSoon => OfferFilters.DomainExpiringSoon(context.DomainIsExpiringThreshold, context.Timestamp),
                OfferState.DomainExpired => OfferFilters.DomainExpired(context.Timestamp),
                OfferState.OfferExpired => OfferFilters.OfferExpired(context.Timestamp),

                OfferState.DomainDoesNotExist => OfferFilters.Validity(OfferValidity.DomainDoesNotExist),
                OfferState.DomainOperatorsContractMissing => OfferFilters.Validity(OfferValidity.DomainOperatorsContractMissing),
                OfferState.OfferSellerDomainOwnerMismatch => OfferFilters.Validity(OfferValidity.OfferSellerDomainOwnerMismatch),

                _ => throw state.CreateInvalidError(),
            };
    }
}