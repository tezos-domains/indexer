﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering
{
    [DocumentFilterDescription(typeof(Domain))]
    public sealed record DomainsFilter : RecordFilter<Domain, DomainsFilter>
    {
        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.Name))]
        public StringFilter? Name { get; set; }

        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.Label))]
        public DomainLabelFilter? Label { get; set; }

        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.Address))]
        public NullableAddressFilter? Address { get; set; }

        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.Owner))]
        public AddressFilter? Owner { get; set; }

        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.ParentOwner))]
        public AddressFilter? ParentOwner { get; set; }

        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.Level))]
        public IntFilter? Level { get; set; }

        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.Ancestors))]
        public StringArrayFilter? Ancestors { get; set; }

        [DocumentFilterPropertyDescription(typeof(Domain), nameof(Domain.Operators))]
        public StringArrayFilter? Operators { get; set; }

        public override FilterDefinition<Domain>? ToDbFilter(FilterDefinitionBuilder<Domain> builder, RecordFilterContext context)
            => builder.AndNullable(
                base.ToDbFilter(builder, context),
                Name?.ToLowerInvariant().ToDbFilter(builder, d => d.Name),
                Label?.ToDbFilter(builder, d => d.Label),
                Address?.ToDbFilter(builder, d => d.Address),
                Owner?.ToDbFilter(builder, d => d.Owner),
                ParentOwner?.ToDbFilter(builder, d => d.ParentOwner),
                Level?.ToDbFilter(builder, d => d.Level),
                Ancestors?.ToLowerInvariant().ToDbFilter(builder, d => d.Ancestors),
                Operators?.ToDbFilter(builder, d => d.Operators)
            );
    }
}