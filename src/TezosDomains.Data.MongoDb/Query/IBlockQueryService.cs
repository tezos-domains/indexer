﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query
{
    public interface IBlockQueryService
    {
        Task<int?> GetStartLevelOrNullAsync(CancellationToken cancellation);
        Task<Block?> FindAsync(string hash, CancellationToken cancellation);
        Task<Block?> FindAsync(int level, CancellationToken cancellation);
        Task<Block?> GetLatestOrNullAsync(CancellationToken cancellation);
    }
}