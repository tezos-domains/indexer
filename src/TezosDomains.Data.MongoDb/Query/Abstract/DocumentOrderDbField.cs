﻿using System;
using System.Linq.Expressions;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed record DocumentOrderDbField<TDocument>
    {
        public Expression<Func<TDocument, object?>> Expression { get; }
        public Func<TDocument, object?> Accessor { get; }
        public bool IsNullable { get; }
        public bool IsUnique { get; }
        public OrderDirection? DirectionOverride { get; init; }

        private DocumentOrderDbField(Expression<Func<TDocument, object?>> expression, bool isNullable, bool isUnique, OrderDirection? directionOverride)
        {
            Expression = expression;
            Accessor = expression.Compile();
            IsNullable = isNullable;
            IsUnique = isUnique;
            DirectionOverride = directionOverride;
        }

        public static DocumentOrderDbField<TDocument> Create(
            Expression<Func<TDocument, object>> expression,
            OrderDirection? direction = null,
            bool isUnique = false
        )
            => new(expression!, isNullable: false, isUnique, direction);

        public static DocumentOrderDbField<TDocument> CreateNullable(
            Expression<Func<TDocument, object?>> expression,
            OrderDirection? direction = null
        )
            => new(expression, isNullable: true, isUnique: false, direction);
    }
}