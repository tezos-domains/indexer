﻿using System;
using System.Collections.Generic;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed record DocumentOrderField<TDocument>
    {
        public string Name { get; }
        public string Description { get; }
        public IReadOnlyList<DocumentOrderDbField<TDocument>> DbFields { get; init; }

        public DocumentOrderField(string name, string description, IReadOnlyList<DocumentOrderDbField<TDocument>> dbFields)
        {
            Name = name;
            Description = description;
            DbFields = dbFields;

            if (dbFields.Count == 0)
                throw new ArgumentException($"At least one DB field must be defined. Field: {name}.");

            if (!dbFields[^1].IsUnique)
                throw new ArgumentException($"Last DB field {dbFields[^1]} must be unique. Field: {name}.");

            for (var i = 0; i < dbFields.Count - 1; i++)
                if (dbFields[i].IsUnique)
                    throw new ArgumentException($"Only the last DB field can be unique but also {i + 1}-th {dbFields[i]} is unique. Field: {name}.");
        }
    }
}