﻿using MongoDB.Driver;
using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public abstract record DocumentOrder<TDocument>(
        [property: Description("The field to order by.")]
        DocumentOrderField<TDocument> Field
    )
        where TDocument : IHasDbCollection
    {
        [DescriptionFormat("The ordering direction. Default is {0:upper}.", default(OrderDirection))]
        public OrderDirection? Direction { get; init; }

        public SortDefinition<TDocument> ToDbSort()
            => Builders<TDocument>.Sort.Combine(
                Field.DbFields.Select(
                    (f, idx) =>
                        //the first field order is always taken from GraphQL input (DocumentOrder.Direction)
                        //the following fields might have the direction overriden
                        BuildSort(f.Expression, idx == 0 ? (Direction ?? default) : f.DirectionOverride ?? (Direction ?? default))
                )
            );

        private static SortDefinition<TDocument> BuildSort(Expression<Func<TDocument, object?>> fieldExpression, OrderDirection direction)
            => direction switch
            {
                OrderDirection.Asc => Builders<TDocument>.Sort.Ascending(fieldExpression),
                OrderDirection.Desc => Builders<TDocument>.Sort.Descending(fieldExpression),
                _ => throw direction.CreateInvalidError(),
            };
    }
}