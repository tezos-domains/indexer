﻿using MongoDB.Driver;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public interface IDocumentFilter<TDocument, in TContext>
        where TDocument : IHasDbCollection
    {
        FilterDefinition<TDocument>? ToDbFilter(FilterDefinitionBuilder<TDocument> builder, TContext context);
    }

    public static class DocumentFilterExtensions
    {
        public static FilterDefinition<TDocument> ToDbFilter<TDocument, TContext>(
            this IDocumentFilter<TDocument, TContext> filter,
            TContext context
        )
            where TDocument : IHasDbCollection
        {
            var builder = Builders<TDocument>.Filter;
            return filter.ToDbFilter(builder, context) ?? builder.Empty;
        }
    }
}