﻿using MongoDB.Driver;
using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed record PagingIndex<TDocument>(
        string OrderFieldName,
        IReadOnlyList<object?> FieldValues
    )
        where TDocument : IHasDbCollection
    {
        private static FilterDefinitionBuilder<TDocument> Builder => Builders<TDocument>.Filter;

        public FilterDefinition<TDocument> ToDbFilter(DocumentOrder<TDocument> order)
            => ToDbFilter(order.Field.DbFields, FieldValues, order.Direction ?? default, isFirstOrderField: true);

        private static FilterDefinition<TDocument> ToDbFilter(
            IReadOnlyList<DocumentOrderDbField<TDocument>> dbFields,
            IReadOnlyList<object?> fieldValues,
            OrderDirection direction,
            bool isFirstOrderField
        )
        {
            var (dbField, remainingDbFields) = dbFields;
            var (fieldValue, remainingValues) = fieldValues;

            // the first field order is always taken from GraphQL input (DocumentOrder.Direction)
            //the following fields might have the direction overriden
            if (!isFirstOrderField && dbField.DirectionOverride.HasValue)
                direction = dbField.DirectionOverride.Value;

            var nextValuesFilter = direction switch
            {
                OrderDirection.Asc => Builder.Gt(dbField.Expression, fieldValue),
                OrderDirection.Desc => Builder.Lt(dbField.Expression, fieldValue),
                _ => throw direction.CreateInvalidError(),
            };

            // Simple case -> just get next by unique field.
            if (dbField.IsUnique)
                return nextValuesFilter;

            // First, Second - sorted ASC by First then Second:
            // a, 1
            // a, 3
            // b, 2
            // after (a, 1) => (First == a && Second > 1) || First > a
            var sameValueAndNextFieldsFilter = Builder.And(
                Builder.Eq(dbField.Expression, fieldValue),
                ToDbFilter(remainingDbFields, remainingValues, direction, isFirstOrderField: false)
            );

            if (!dbField.IsNullable)
                return Builder.Or(
                    sameValueAndNextFieldsFilter,
                    nextValuesFilter
                );

            // Handle nulls :-(
            return (direction, value: fieldValue) switch
            {
                // First, Second - sorted ASC by First then Second:
                // null, 1
                // null, 5
                // a, 3
                // a, 4
                // b, 2

                // after (null, 1) => (First == null && Second > 1) || First != null
                (OrderDirection.Asc, null) => Builder.Or(
                    sameValueAndNextFieldsFilter,
                    Builder.Ne(dbField.Expression, null)
                ),

                // after (a, 3) => (First == a && Second > 3) || First > a
                (OrderDirection.Asc, _) => Builder.Or(
                    sameValueAndNextFieldsFilter,
                    nextValuesFilter
                ),

                // First, Second - sorted DESC by First then Second:
                // b, 4
                // b, 2
                // a, 3
                // null, 5
                // null, 1

                // after (5, null) => First == null && Second < 5
                (OrderDirection.Desc, null) => sameValueAndNextFieldsFilter,

                // after (4, b) => (First == b && Second < 4) || First < b || First == null
                (OrderDirection.Desc, _) => Builder.Or(
                    sameValueAndNextFieldsFilter,
                    nextValuesFilter,
                    Builder.Eq(dbField.Expression, null)
                ),

                _ => throw direction.CreateInvalidError(),
            };
        }
    }
}