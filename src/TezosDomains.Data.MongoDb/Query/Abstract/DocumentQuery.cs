﻿using MongoDB.Driver;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed record DocumentQuery<TDocument>(
        FilterDefinition<TDocument> Filter,
        DocumentOrder<TDocument> Order,
        PagingIndex<TDocument>? AfterIndex,
        int TakeCount
    )
        where TDocument : IHasDbCollection;
}