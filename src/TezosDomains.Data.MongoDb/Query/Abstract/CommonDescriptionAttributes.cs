﻿using System;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed class DocumentFilterDescriptionAttribute : DescriptionFormatAttribute
    {
        public DocumentFilterDescriptionAttribute(Type documentType)
            : base("Complex object with filter conditions for filtering {0}-s.", documentType.Name)
        {
        }
    }

    internal sealed class DocumentFilterPropertyDescriptionAttribute : DescriptionFormatAttribute
    {
        public DocumentFilterPropertyDescriptionAttribute(Type documentType, string propertyName)
            : base("Filters {0}-s by their {1:camel}.", documentType.Name, propertyName)
        {
        }
    }

    internal sealed class PropertyFilterDescriptionAttribute : DescriptionFormatAttribute
    {
        public PropertyFilterDescriptionAttribute(string propertyTypeName)
            : base("Filter conditions for filtering associated property of the type {0}.", propertyTypeName)
        {
        }
    }

    public sealed class DocumentOrderDescriptionAttribute : DescriptionFormatAttribute
    {
        public DocumentOrderDescriptionAttribute(Type documentType)
            : base("Ordering options for {0}-s.", documentType.Name)
        {
        }
    }
}