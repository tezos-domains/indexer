﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    [DocumentOrderDescription(typeof(Domain))]
    public sealed record DomainOrder(DocumentOrderField<Domain> Field) : DocumentOrder<Domain>(Field)
    {
        private static readonly DocumentOrderDbField<Domain> UniqueDbField
            = DocumentOrderDbField<Domain>.Create(d => d.NameForOrdering, isUnique: true, direction: OrderDirection.Asc);

        public static readonly IReadOnlyList<DocumentOrderField<Domain>> Fields = new[]
        {
            new DocumentOrderField<Domain>(
                name: "DOMAIN",
                description: "Order domains by name and level.",
                dbFields: new[] { UniqueDbField }
            ),
            new DocumentOrderField<Domain>(
                name: "NAME",
                description: "Order domains by name.",
                dbFields: new[] { DocumentOrderDbField<Domain>.Create(d => d.Name, isUnique: true) }
            ),
            new DocumentOrderField<Domain>(
                name: "ADDRESS",
                description: "Order domains by address.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Domain>.CreateNullable(d => d.Address),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Domain>(
                name: "OWNER",
                description: "Order domains by owner.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Domain>.Create(d => d.Owner),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Domain>(
                name: "EXPIRES_AT",
                description: "Order domains by validity.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Domain>.Create(d => d.ExpiresAtUtc),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Domain>(
                name: "LEVEL",
                description: "Order domains by level.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Domain>.Create(d => d.Level),
                    UniqueDbField,
                }
            ),
        };

        public static readonly DomainOrder Default = new(Fields[0]);
    }
}