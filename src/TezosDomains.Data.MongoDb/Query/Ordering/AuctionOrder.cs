﻿using System;
using System.Collections.Generic;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    [DocumentOrderDescription(typeof(Auction))]
    public sealed record AuctionOrder(DocumentOrderField<Auction> Field) : DocumentOrder<Auction>(Field)
    {
        private static readonly DocumentOrderDbField<Auction> UniqueDbField
            = DocumentOrderDbField<Auction>.Create(a => a.AuctionId, isUnique: true);

        public static readonly IReadOnlyList<DocumentOrderField<Auction>> Fields = new[]
        {
            new DocumentOrderField<Auction>(
                name: "ID",
                description: "Order auctions by id.",
                dbFields: new[] { UniqueDbField }
            ),
            new DocumentOrderField<Auction>(
                name: "DOMAIN_NAME",
                description: "Order auctions by the domain name.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Auction>.Create(a => a.DomainName),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Auction>(
                name: "ENDS_AT",
                description: $"Order auctions by the auction end `{nameof(DateTime)}`.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Auction>.Create(a => a.EndsAtUtc),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Auction>(
                name: "HIGHEST_BID_AMOUNT",
                description: "Order auctions by the current highest bid amount and then by timestamp.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Auction>.Create(a => a.HighestBid.Amount),
                    DocumentOrderDbField<Auction>.Create(a => a.HighestBid.Block.Timestamp),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Auction>(
                name: "HIGHEST_BID_TIMESTAMP",
                description: "Order auctions by the current highest bid timestamp.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Auction>.Create(a => a.HighestBid.Block.Timestamp),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Auction>(
                name: "BID_COUNT",
                description: "Order auctions by the total number of bids and then by the last bid timestamp.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Auction>.Create(a => a.BidCount),
                    DocumentOrderDbField<Auction>.Create(a => a.HighestBid.Block.Timestamp),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Auction>(
                name: "BID_AMOUNT_SUM",
                description: "Order auctions by the sum of bids' amounts and then by the last bid timestamp.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Auction>.Create(a => a.BidAmountSum),
                    DocumentOrderDbField<Auction>.Create(a => a.HighestBid.Block.Timestamp),
                    UniqueDbField,
                }
            )
        };

        public static readonly AuctionOrder Default = new(Fields[2]) { Direction = OrderDirection.Desc };
    }
}