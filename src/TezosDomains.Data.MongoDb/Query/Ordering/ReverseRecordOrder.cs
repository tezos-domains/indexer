﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    [DocumentOrderDescription(typeof(ReverseRecord))]
    public sealed record ReverseRecordOrder(DocumentOrderField<ReverseRecord> Field) : DocumentOrder<ReverseRecord>(Field)
    {
        private static readonly DocumentOrderDbField<ReverseRecord> UniqueDbField
            = DocumentOrderDbField<ReverseRecord>.Create(r => r.Address, isUnique: true);

        public static readonly IReadOnlyList<DocumentOrderField<ReverseRecord>> Fields = new[]
        {
            new DocumentOrderField<ReverseRecord>(
                name: "NAME",
                description: "Order reverse records by the domain name.",
                dbFields: new[]
                {
                    DocumentOrderDbField<ReverseRecord>.CreateNullable(r => r.Name),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<ReverseRecord>(
                name: "ADDRESS",
                description: "Order reverse records by address.",
                dbFields: new[] { UniqueDbField }
            ),
            new DocumentOrderField<ReverseRecord>(
                name: "OWNER",
                description: "Order reverse records by owner.",
                dbFields: new[]
                {
                    DocumentOrderDbField<ReverseRecord>.Create(r => r.Owner),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<ReverseRecord>(
                name: "EXPIRES_AT",
                description: "Order reverse records by validity.",
                dbFields: new[]
                {
                    DocumentOrderDbField<ReverseRecord>.Create(r => r.ExpiresAtUtc),
                    UniqueDbField,
                }
            ),
        };

        public static readonly ReverseRecordOrder Default = new(Fields[0]);
    }
}