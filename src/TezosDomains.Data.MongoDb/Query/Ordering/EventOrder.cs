﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    [DocumentOrderDescription(typeof(Event))]
    public sealed record EventOrder(DocumentOrderField<Event> Field) : DocumentOrder<Event>(Field)
    {
        public static readonly IReadOnlyList<DocumentOrderField<Event>> Fields = new[]
        {
            new DocumentOrderField<Event>(
                name: "TIMESTAMP",
                description: "Order events by the timestamp of the associated block.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Event>.Create(e => e.Block.Timestamp),
                    DocumentOrderDbField<Event>.Create(e => e.Id, isUnique: true),
                }
            )
        };

        public static readonly EventOrder Default = new(Fields[0]) { Direction = OrderDirection.Desc };
    }
}