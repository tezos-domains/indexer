﻿using System;
using System.Collections.Generic;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    [DocumentOrderDescription(typeof(Offer))]
    public sealed record OfferOrder(DocumentOrderField<Offer> Field) : DocumentOrder<Offer>(Field)
    {
        private static readonly DocumentOrderDbField<Offer> UniqueDbField
            = DocumentOrderDbField<Offer>.Create(a => a.OfferId, isUnique: true);

        public static readonly IReadOnlyList<DocumentOrderField<Offer>> Fields = new[]
        {
            new DocumentOrderField<Offer>(
                name: "ID",
                description: "Order offers by id.",
                dbFields: new[] { UniqueDbField }
            ),
            new DocumentOrderField<Offer>(
                name: "DOMAIN_NAME",
                description: "Order offers by the domain name.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Offer>.CreateNullable(a => a.Domain!.Name),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Offer>(
                name: "CREATED_AT",
                description: $"Order offers by offer creation `{nameof(DateTime)}`.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Offer>.Create(a => a.CreatedAtUtc),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Offer>(
                name: "ENDS_AT",
                description: $"Order offers by offer expiration `{nameof(DateTime)}`.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Offer>.Create(a => a.ExpiresAtUtc),
                    UniqueDbField,
                }
            ),
            new DocumentOrderField<Offer>(
                name: "TOKEN_ID",
                description: "Order offers by token id.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Offer>.Create(a => a.TokenId, isUnique: true)
                }
            ),
            new DocumentOrderField<Offer>(
                name: "PRICE",
                description: "Order offers by price.",
                dbFields: new[]
                {
                    DocumentOrderDbField<Offer>.Create(a => a.Price),
                    UniqueDbField,
                }
            )
        };

        public static readonly OfferOrder Default = new(Fields[2]) { Direction = OrderDirection.Desc };
    }
}