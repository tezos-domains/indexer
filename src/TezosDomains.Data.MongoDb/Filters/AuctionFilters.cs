﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using TezosDomains.Data.Models.Auctions;

namespace TezosDomains.Data.MongoDb.Filters
{
    public static class AuctionFilters
    {
        private static FilterDefinitionBuilder<Auction> Builder => Builders<Auction>.Filter;

        public static FilterDefinition<Auction> CanBeSettled(DateTime atUtc, IEnumerable<string>? domainNames = null)
            => Builder.And(
                Builder.Eq(a => a.IsSettled, false),
                Builder.Gt(a => a.OwnedUntilUtc, atUtc),
                Builder.Lte(a => a.EndsAtUtc, atUtc),
                ByDomainNames(domainNames)
            );

        public static FilterDefinition<Auction> InProgress(DateTime atUtc, IEnumerable<string>? domainNames = null)
            => Builder.And(
                Builder.Gt(a => a.EndsAtUtc, atUtc),
                Builder.Lte(a => a.FirstBidAtUtc, atUtc),
                ByDomainNames(domainNames)
            );

        public static FilterDefinition<Auction> Active(
            DateTime atUtc,
            IEnumerable<string> inProgressDomainNames,
            IEnumerable<string> canBeSettledDomainNames
        )
            => Builder.Or(
                InProgress(atUtc, inProgressDomainNames),
                CanBeSettled(atUtc, canBeSettledDomainNames)
            );

        public static FilterDefinition<Auction> EndedBetween(DateTime exclusiveFromUtc, DateTime inclusiveToUtc)
            => Builder.And(
                Builder.Gt(a => a.EndsAtUtc, exclusiveFromUtc),
                Builder.Lte(a => a.EndsAtUtc, inclusiveToUtc)
            );

        public static FilterDefinition<Auction> Settled(bool isSettled = true)
            => Builder.Eq(a => a.IsSettled, isSettled);

        public static FilterDefinition<Auction> SettlementExpired(DateTime atUtc)
            => Builder.And(
                Builder.Lte(a => a.OwnedUntilUtc, atUtc),
                Settled(isSettled: false)
            );

        public static FilterDefinition<Auction> Current(DateTime atUtc, string domainName)
            => Builder.And(
                Builder.Eq(a => a.DomainName, domainName),
                Builder.Gt(a => a.OwnedUntilUtc, atUtc),
                Builder.Lte(a => a.FirstBidAtUtc, atUtc),
                Builder.Eq(a => a.IsSettled, false)
            );

        private static FilterDefinition<Auction> ByDomainNames(IEnumerable<string>? domainNames)
            => domainNames != null ? Builder.In(a => a.DomainName, domainNames) : Builder.Empty;
    }
}