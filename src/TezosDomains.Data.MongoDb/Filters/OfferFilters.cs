﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using TezosDomains.Data.Models.Marketplace;

namespace TezosDomains.Data.MongoDb.Filters
{
    public static class OfferFilters
    {
        private static FilterDefinitionBuilder<Offer> Builder => Builders<Offer>.Filter;

        public static FilterDefinition<Offer> Valid(
            DateTime atUtc,
            IEnumerable<int>? tokenIds = null,
            IEnumerable<string>? domainNames = null,
            TimeSpan? domainExpirationOffset = null
        )
        {
            var domainExpirationUtc = atUtc.Add(domainExpirationOffset ?? TimeSpan.Zero);
            return Builder.And(
                Builder.Gt(o => o.Domain!.ExpiresAtUtc, domainExpirationUtc),
                Builder.Gt(o => o.ExpiresAtUtc, atUtc),
                Builder.Eq(o => o.Presence, OfferBlockchainPresence.Present),
                Builder.Eq(o => o.Validity, OfferValidity.Valid),
                tokenIds != null ? Builder.In(o => o.TokenId, tokenIds) : Builder.Empty,
                domainNames != null ? Builder.In(o => o.Domain!.Name, domainNames) : Builder.Empty
            );
        }

        //Active offers are valid offers with domain expiration later than atUtc + domainIsExpiringThreshold
        public static FilterDefinition<Offer> Active(TimeSpan domainIsExpiringThreshold, DateTime atUtc)
            => Valid(atUtc, domainExpirationOffset: domainIsExpiringThreshold);

        public static FilterDefinition<Offer> DomainExpired(DateTime atUtc)
            => Builder.And(
                Builder.Lte(o => o.Domain!.ExpiresAtUtc, atUtc),
                Builder.Eq(o => o.Presence, OfferBlockchainPresence.Present)
            );

        public static FilterDefinition<Offer> OfferExpired(DateTime atUtc)
            => Builder.And(
                Builder.Lte(o => o.ExpiresAtUtc, atUtc),
                Builder.Eq(o => o.Presence, OfferBlockchainPresence.Present)
            );

        public static FilterDefinition<Offer> ByTokenId(IEnumerable<int> tokenIds)
            => Builder.In(o => o.TokenId, tokenIds);

        public static FilterDefinition<Offer> ByPresence(OfferBlockchainPresence presence, IEnumerable<int>? tokenIds = null)
            => Builder.And(
                tokenIds is not null ? Builder.In(o => o.TokenId, tokenIds) : Builder.Empty,
                Builder.Eq(o => o.Presence, presence)
            );

        public static FilterDefinition<Offer> Validity(OfferValidity validity)
            => Builder.And(
                Builder.Eq(o => o.Presence, OfferBlockchainPresence.Present),
                Builder.Eq(o => o.Validity, validity)
            );

        public static FilterDefinition<Offer> DomainExpiringSoon(TimeSpan domainIsExpiringThreshold, DateTime atUtc)
            => Builder.And(
                Builder.Lte(o => o.Domain!.ExpiresAtUtc, atUtc.Add(domainIsExpiringThreshold)),
                Builder.Gt(o => o.Domain!.ExpiresAtUtc, atUtc),
                Builder.Eq(o => o.Presence, OfferBlockchainPresence.Present)
            );
    }
}