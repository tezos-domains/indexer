﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Filters
{
    public static class DomainFilters
    {
        private static FilterDefinitionBuilder<Domain> Builder => Builders<Domain>.Filter;

        public static FilterDefinition<Domain> ExistingAtLatestBlock(IEnumerable<string> domainNames)
            => Builder.In(d => d.Name, domainNames);

        public static FilterDefinition<Domain> GetAllWithTokenId(IReadOnlyList<int>? tokenIds = null, IReadOnlyList<string>? validityKeys = null)
        {
            var validityKeysList = validityKeys.NullToEmpty();
            IReadOnlyList<int?> tokenIdsList = tokenIds.NullToEmpty().Cast<int?>().ToArray();
            return Builder.And(
                (tokenIdsList.Count, validityKeysList.Count) switch
                {
                    (> 0, > 0) => Builder.Or(
                        Builder.In(d => d.TokenId, tokenIdsList),
                        Builder.In(d => d.ValidityKey, validityKeysList)
                    ),
                    (0, _) => Builder.In(d => d.ValidityKey, validityKeysList),
                    (_, 0) => Builder.In(d => d.TokenId, tokenIdsList),
                    _ => Builder.Empty
                },
                Builder.Ne(d => d.TokenId, null)
            );
        }
    }
}