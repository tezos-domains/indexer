﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.MongoDb.Filters
{
    public static class CommonFilters
    {
        public static FilterDefinition<TDocument> CreatedAtBlock<TDocument>(this FilterDefinitionBuilder<TDocument> builder, int level)
            where TDocument : IHasBlock
            => builder.Eq(e => e.Block.Level, level);

        public static FilterDefinition<TDocument> ExcludeHistory<TDocument>(this FilterDefinition<TDocument> filter)
            where TDocument : IHasHistory
            => Builders<TDocument>.Filter.And(filter, Builders<TDocument>.Filter.Eq(d => d.ValidUntilTimestamp, DateTime.MaxValue));

        public static FilterDefinition<TDocument> ByKeys<TDocument>(this FilterDefinitionBuilder<TDocument> builder, IEnumerable<string> keys)
            where TDocument : IHasDbCollection
            => builder.In(DocumentHelper<TDocument>.KeyExpression, keys);
    }
}