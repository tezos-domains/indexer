﻿using Microsoft.Extensions.DependencyInjection;
using TezosDomains.Common.Configuration;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.HealthCheck;
using TezosDomains.Data.MongoDb.Initialization;
using TezosDomains.Data.MongoDb.Metrics;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Services;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb
{
    public static class ServiceCollectionExtensions
    {
        public static void AddMongoDb(this IServiceCollection services)
        {
            services.AddTezosDomainsConfigurationWithDiagnostics<MongoDbConfiguration>("MongoDb");

            services
                .AddHealthChecks()
                .AddCheck<MongoDbHealthCheck>("MongoDb");

            services
                .AddSingleton<MongoDbInitializer>()
                .AddSingleton<MongoDbContext>()
                .AddTransient<MongoDbHealthCheck>()
                .AddSingleton<IndexerDistributedLockService>()
                .AddTransientWithMetrics<IBlockQueryService, BlockQueryService, BlockQueryServiceMetricsDecorator>()
                .AddTransientWithMetrics<IQueryService<Domain>, DocumentQueryService<Domain>, QueryServiceMetricsDecorator<Domain>>()
                .AddTransientWithMetrics<IQueryService<ReverseRecord>, DocumentQueryService<ReverseRecord>, QueryServiceMetricsDecorator<ReverseRecord>>()
                .AddTransientWithMetrics<IQueryService<BidderBalances>, DocumentQueryService<BidderBalances>, QueryServiceMetricsDecorator<BidderBalances>>()
                .AddTransientWithMetrics<IQueryService<Auction>, DocumentQueryService<Auction>, QueryServiceMetricsDecorator<Auction>>()
                .AddTransientWithMetrics<IQueryService<Event>, DocumentQueryService<Event>, QueryServiceMetricsDecorator<Event>>()
                .AddTransientWithMetrics<IQueryService<Offer>, DocumentQueryService<Offer>, QueryServiceMetricsDecorator<Offer>>()
                .AddSingleton<MongoDbClientFactory>()
                .AddSingleton(sp => sp.GetRequiredService<MongoDbClientFactory>().Create());

            MongoDbSetup.InitializeMappingStructures();
        }

        private static IServiceCollection AddTransientWithMetrics<TService, TImplementation, TMetricsDecorator>(this IServiceCollection services)
            where TImplementation : class, TService
            where TMetricsDecorator : TService
            where TService : class
        {
            services.AddTransient<TImplementation>();
            services.AddTransient<TService>(p => p.Create<TMetricsDecorator>(p.GetRequiredService<TImplementation>()));

            return services;
        }
    }
}