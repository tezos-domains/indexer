﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.DistributedLock;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.MongoDb.Initialization
{
    internal static class MongoDbSetup
    {
        private static bool _initialized;
        private static readonly object Lock = new();

        internal static void InitializeMappingStructures()
        {
            lock (Lock)
            {
                if (_initialized)
                    return;

                try
                {
                    InitializeMappingStructuresInternal();
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed initializing Mongo Db mapping.", ex);
                }

                _initialized = true;
            }
        }

        private static void InitializeMappingStructuresInternal()
        {
            var pack = new ConventionPack
            {
                new ImmutableRecordConstructorConvention(),
                new ReadOnlyPropertyConvention(),
                new EnumRepresentationConvention(BsonType.String),
            };
            ConventionRegistry.Register("Conventions", pack, _ => true);

            BsonSerializer.RegisterSerializer(typeof(decimal), new DecimalSerializer(BsonType.Decimal128));
            BsonSerializer.RegisterSerializer(typeof(decimal?), new NullableSerializer<decimal>(new DecimalSerializer(BsonType.Decimal128)));

            RegisterClassMapWithKey<Block>();
            RegisterClassMap<HasHistory>(m => m.MapIdProperty(h => h.VersionId));

            RegisterClassMap<Auction>();
            RegisterClassMap<Domain>();
            RegisterClassMap<ReverseRecord>();
            RegisterClassMap<BidderBalances>();
            RegisterClassMap<Bid>();
            RegisterClassMap<Offer>();

            RegisterClassMapWithKey<Lock>();
            RegisterClassMap<Settings>(m => m.SetIgnoreExtraElements(true));

            RegisterEventClassMaps();
        }

        private static void RegisterClassMapWithKey<TDocument>()
            where TDocument : IHasDbCollection
            => RegisterClassMap<TDocument>(m => m.MapIdProperty(DocumentHelper<TDocument>.KeyExpression));

        private static void RegisterClassMap<TClass>(Action<BsonClassMap<TClass>>? initialize = null)
            => BsonClassMap.RegisterClassMap<TClass>(
                classMap =>
                {
                    classMap.AutoMap();
                    initialize?.Invoke(classMap);
                }
            );

        /// <summary>
        /// All event types must be registered up front.
        /// Because API uses an interface IEvent to read from events collection, it needs the registered maps prior deserialization.
        /// </summary>
        private static void RegisterEventClassMaps()
        {
            var eventTypes = typeof(Event).Assembly.GetTypes().Where(t => typeof(Event).IsAssignableFrom(t) && t.IsClass && !t.IsAbstract);
            foreach (var eventType in eventTypes)
                BsonClassMap.LookupClassMap(eventType); //Lookup will register classMap if not found
        }
    }
}