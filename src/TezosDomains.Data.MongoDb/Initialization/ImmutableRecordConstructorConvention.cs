﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Initialization
{
    /// <summary>
    /// Maps document constructor so that Mongo uses it for deserialization which enforces Mongo to validate that no property is missing.
    /// Otherwise Mongo would use <see cref="FormatterServices.GetUninitializedObject" /> and call <c>init</c> property accessors.
    /// </summary>
    internal sealed class ImmutableRecordConstructorConvention : ConventionBase, IClassMapConvention
    {
        public void Apply(BsonClassMap classMap)
        {
            try
            {
                if (classMap.ClassType.IsAbstract || classMap.ClassType == typeof(object))
                    return;

                var constructor = classMap.ClassType.GetConstructors().SingleWithItemDetails();
                if (!constructor.IsPublic || constructor.IsAbstract)
                    throw new Exception("Constructor must be public and final.");

                // Properties must be in same order as constructor parameters.
                var propertiesByName = GetProperties(classMap.ClassType)
                    .ToDictionary(p => p.Name, StringComparer.OrdinalIgnoreCase);
                var properties = constructor.GetParameters()
                    .Select(p => propertiesByName[p.Name.GuardNotNull()]);

                classMap.MapConstructor(constructor).SetArguments(properties);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed mapping {classMap.ClassType}.", ex);
            }
        }

        /// <summary>
        /// Retrieves properties in a way that their DeclaringType is the topmost one regardless of overrides/abstract.
        /// Otherwise Mongo wouldn't recognize their mapping on parent class.
        /// </summary>
        private static IEnumerable<PropertyInfo> GetProperties(Type type)
        {
            var baseProperties = type.BaseType != null ? GetProperties(type.BaseType).AsReadOnlyCollection() : Array.Empty<PropertyInfo>();
            var basePropertyNames = baseProperties.Select(p => p.Name).ToHashSet();

            return type.GetProperties()
                .Where(p => !basePropertyNames.Contains(p.Name))
                .Concat(baseProperties);
        }
    }
}