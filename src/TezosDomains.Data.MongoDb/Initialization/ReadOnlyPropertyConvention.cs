﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using System;
using System.Linq;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Initialization
{
    internal sealed class ReadOnlyPropertyConvention : ConventionBase, IClassMapConvention
    {
        public void Apply(BsonClassMap classMap)
        {
            try
            {
                var basePropertyNames = (classMap.ClassType.BaseType?.GetProperties())
                    .NullToEmpty()
                    .Select(p => p.Name)
                    .ToHashSet();

                foreach (var property in classMap.ClassType.GetProperties())
                {
                    if (property.SetMethod != null || basePropertyNames.Contains(property.Name))
                        continue;

                    classMap.MapMember(property);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed mapping {classMap.ClassType}.", ex);
            }
        }
    }
}