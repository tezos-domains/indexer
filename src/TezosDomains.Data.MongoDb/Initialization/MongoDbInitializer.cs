﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Initialization
{
    public sealed class MongoDbInitializer
    {
        private readonly ILogger _logger;
        private readonly MongoDbContext _dbContext;
        private volatile bool _initialized;
        private volatile Settings? _settings;

        public MongoDbInitializer(ILogger<MongoDbInitializer> logger, MongoDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
            _initialized = false;
        }

        public async Task InitializeAsync(string tezosChainId, CancellationToken cancellation)
        {
            if (_initialized)
                return;

            _initialized = true;

            await CreateCollectionsIfNotExistingAsync(cancellation);
            await InitializeSettingsAsync(tezosChainId, cancellation);
            await CreateIndexesIfMissingAsync(cancellation);
        }

        public Settings GetSettings()
        {
            if (!_initialized)
                throw new InvalidOperationException($"{nameof(MongoDbInitializer)} is not initialized yet.");

            return _settings.GuardNotNull();
        }

        private async Task InitializeSettingsAsync(string tezosChainId, CancellationToken cancellation)
        {
            var settings = await _dbContext.Get<Settings>()
                .Find(FilterDefinition<Settings>.Empty)
                .SingleOrDefaultAsync(cancellation);
            if (settings != null && settings.TezosChainId != tezosChainId)
            {
                throw new InvalidOperationException(
                    $"Database settings for tezosChainId: {settings.TezosChainId} is not the same as in configuration {tezosChainId}"
                );
            }

            if (settings == null)
            {
                _logger.LogInformation($"Inserting settings with chainId {tezosChainId}.");
                settings = new Settings(tezosChainId);
                await _dbContext.Get<Settings>().InsertOneAsync(settings, cancellationToken: cancellation);
            }

            _settings = settings;
        }

        private static IEnumerable<IndexKeysDefinition<ReverseRecord>> CreateReverseRecordsIndexKeys(IndexKeysDefinitionBuilder<ReverseRecord> builder)
            => new[] { builder.Ascending(r => r.Address), builder.Ascending(r => r.ValidityKey) };

        private static IEnumerable<IndexKeysDefinition<Block>> CreateBlocksIndexKeys(IndexKeysDefinitionBuilder<Block> builder)
            => new[] { builder.Descending(b => b.Level) };


        private static IEnumerable<IndexKeysDefinition<Domain>> CreateDomainsIndexKeys(IndexKeysDefinitionBuilder<Domain> builder)
            => new[]
            {
                builder.Ascending(d => d.ValidUntilTimestamp).Ascending(d => d.Name),
                builder.Ascending(d => d.TokenId).Ascending(d => d.ValidUntilTimestamp),
                builder.Ascending(d => d.ParentName).Ascending(d => d.ValidUntilTimestamp),
                builder.Ascending(d => d.ValidityKey).Ascending(d => d.ValidUntilTimestamp).Ascending(d => d.TokenId),
                builder.Ascending(d => d.Level).Ascending(d => d.ValidUntilTimestamp).Descending(d => d.ExpiresAtUtc).Ascending(d => d.NameForOrdering),
                builder.Ascending(d => d.Level)
                    .Ascending(d => d.ValidUntilTimestamp)
                    .Ascending(d => d.Label)
                    .Descending(d => d.ExpiresAtUtc)
                    .Ascending(d => d.NameForOrdering),
                builder.Descending(d => d.ValidUntilTimestamp).Descending(d => d.ExpiresAtUtc),
                builder.Descending(d => d.ValidUntilTimestamp).Ascending(d => d.NameForOrdering).Descending(d => d.ExpiresAtUtc),
                builder.Descending(d => d.ValidUntilTimestamp).Ascending(d => d.Ancestors).Ascending(d => d.NameForOrdering),
                builder.Descending(d => d.ValidUntilTimestamp).Ascending(d => d.Address).Descending(d => d.ExpiresAtUtc),
                builder.Descending(d => d.ValidUntilTimestamp).Ascending(d => d.Owner).Descending(d => d.ExpiresAtUtc),
                builder.Descending(d => d.ValidUntilTimestamp).Ascending(d => d.Operators).Descending(d => d.ExpiresAtUtc)
            };

        private static IEnumerable<IndexKeysDefinition<Event>> CreateEventsIndexKeys(IndexKeysDefinitionBuilder<Event> builder)
            => new[]
            {
                builder.Ascending("_t").Descending(e => e.Block.Timestamp).Ascending(e => e.Id),
                builder.Ascending(nameof(DomainBuyEvent.DomainName)).Descending(e => e.Block.Timestamp).Descending("_id").Ascending("_t"),
                builder.Ascending(nameof(AuctionBidEvent.PreviousBidderAddress)).Ascending("_t").Ascending(e => e.Block.Timestamp).Ascending("_id"),
                builder.Ascending(e => e.SourceAddress).Ascending("_t").Ascending(e => e.Block.Timestamp).Ascending("_id"),
                builder.Ascending(nameof(OfferExecutedEvent.InitiatorAddress)).Ascending("_t").Ascending(e => e.Block.Timestamp).Ascending("_id"),
                builder.Ascending(nameof(DomainTransferEvent.NewOwner)).Ascending("_t").Ascending(e => e.Block.Timestamp).Ascending("_id"),
                builder.Ascending(nameof(AuctionEndEvent.Participants)).Ascending("_t").Ascending(e => e.Block.Timestamp).Ascending("_id"),
            };

        private static IEnumerable<IndexKeysDefinition<Auction>> CreateAuctionsIndexKeys(IndexKeysDefinitionBuilder<Auction> builder)
            => new[]
            {
                builder
                    .Ascending(a => a.ValidUntilTimestamp)
                    .Ascending(a => a.IsSettled)
                    .Descending(a => a.EndsAtUtc)
                    .Descending(a => a.AuctionId)
                    .Ascending(a => a.OwnedUntilUtc)
            };

        private static IEnumerable<IndexKeysDefinition<Offer>> CreateOffersIndexKeys(IndexKeysDefinitionBuilder<Offer> builder)
            => new[]
            {
                builder
                    .Ascending(a => a.Presence)
                    .Ascending(a => a.Type)
                    .Ascending(a => a.Validity)
                    .Ascending(a => a.ValidUntilTimestamp)
                    .Ascending(a => a.CreatedAtUtc)
                    .Ascending(a => a.OfferId)
                    .Ascending(a => a.Domain!.ExpiresAtUtc)
                    .Ascending(a => a.ExpiresAtUtc),
                builder
                    .Ascending(a => a.Type)
                    .Ascending(a => a.InitiatorAddress)
                    .Ascending(a => a.Presence)
                    .Ascending(a => a.ValidUntilTimestamp)
                    .Descending(a => a.CreatedAtUtc)
                    .Descending(a => a.OfferId)
                    .Ascending(a => a.ExpiresAtUtc)
                    .Ascending(a => a.Domain!.ExpiresAtUtc),
                builder.Ascending(a => a.TokenId)
            };

        private static IEnumerable<IndexKeysDefinition<BidderBalances>> CreateBidderBalancesIndexKeys(IndexKeysDefinitionBuilder<BidderBalances> builder)
            => new[] { builder.Ascending(b => b.Address) };

        /// <summary>
        /// Index creation in mongodb is idempotent,
        /// therefore we can try to create the indexes as many time as we want
        /// </summary>
        private async Task CreateIndexesIfMissingAsync(CancellationToken cancellation)
        {
            try
            {
                _logger.LogInformation("Create or update database indexes STARTED.");

                var createdIndexesTasks = new[]
                {
                    CreateIndexesAsync(CreateDomainsIndexKeys, DomainOrder.Fields),
                    CreateIndexesAsync<Block>(CreateBlocksIndexKeys),
                    CreateIndexesAsync(CreateReverseRecordsIndexKeys, ReverseRecordOrder.Fields),
                    CreateIndexesAsync(CreateAuctionsIndexKeys, AuctionOrder.Fields),
                    CreateIndexesAsync(CreateEventsIndexKeys, EventOrder.Fields),
                    CreateIndexesAsync<BidderBalances>(CreateBidderBalancesIndexKeys),
                    CreateIndexesAsync(CreateOffersIndexKeys, OfferOrder.Fields),
                };
                var createdIndexes = await Task.WhenAll(createdIndexesTasks);

                _logger.LogInformation(
                    "Create or update database indexes FINISHED. Following indexes created (or were already existing): {createdIndexes}.",
                    createdIndexes.SelectMany(i => i).ToList()
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unable to create mongoDb indexes");
            }

            async Task<IEnumerable<string>> CreateIndexesAsync<TDocument>(
                Func<IndexKeysDefinitionBuilder<TDocument>, IEnumerable<IndexKeysDefinition<TDocument>>> createIndexKeys,
                IEnumerable<DocumentOrderField<TDocument>>? orderFields = null
            ) where TDocument : IHasDbCollection
            {
                var builder = Builders<TDocument>.IndexKeys;

                var explicitIndexes = createIndexKeys(builder)
                    .Select(k => new CreateIndexModel<TDocument>(k));

                var orderIndexes = orderFields.NullToEmpty()
                    .Where(f => f.DbFields.Count > 1) // Indexes for a field must be specified explicitly.
                    .Select(f => new { f.Name, Keys = builder.Combine(f.DbFields.Select(d => builder.Ascending(d.Expression))) })
                    .Select(x => new CreateIndexModel<TDocument>(x.Keys, new CreateIndexOptions { Name = $"Order:{x.Name}" }));

                var indexes = explicitIndexes.Concat(orderIndexes);
                var dbCollection = _dbContext.Get<TDocument>();
                return await dbCollection.Indexes.CreateManyAsync(indexes, cancellation);
            }
        }

        private async Task CreateCollectionsIfNotExistingAsync(CancellationToken cancellation)
        {
            _logger.LogInformation("Create collections STARTED.");

            var existingCollectionsCursor = await _dbContext.Database.ListCollectionNamesAsync(cancellationToken: cancellation);
            foreach (var name in MongoDbContext.CollectionNames.Values.Except(await existingCollectionsCursor.ToListAsync(cancellation)))
            {
                await _dbContext.Database.CreateCollectionAsync(name, cancellationToken: cancellation);
            }

            _logger.LogInformation("Create collections FINISHED.");
        }
    }
}