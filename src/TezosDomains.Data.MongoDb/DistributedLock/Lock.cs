﻿using System;
using TezosDomains.Data.Models;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.MongoDb.DistributedLock
{
    /// <summary>
    /// Document used for holding a distributed lock in mongo.
    /// </summary>
    public sealed record Lock(
        // The name of the resource being held.
        [property: DocumentKey] string Resource,
        // The timestamp for when the lock expires. This is used if the lock is not maintained or cleaned up by the owner (e.g. process was shut down).
        DateTime ExpireAtUtc,
        // The name of machine which acquired the lock.
        string MachineName,
        // The process id of the application which acquired the lock.
        int ProcessId
    ) : IHasDbCollection;
}