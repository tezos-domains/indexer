﻿using MongoDB.Driver;
using System.Linq;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Helpers
{
    public static class FilterDefinitionBuilderExtensions
    {
        public static FilterDefinition<TDocument>? AndNullable<TDocument>(
            this FilterDefinitionBuilder<TDocument> builder,
            params FilterDefinition<TDocument>?[] filters
        )
        {
            var nonNullFilters = filters.WhereNotNull().ToList();
            return nonNullFilters.Count switch
            {
                0 => null,
                1 => nonNullFilters[0],
                _ => builder.And(nonNullFilters),
            };
        }
    }
}