﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Data.MongoDb.Helpers
{
    public static class MongoCollectionExtensions
    {
        public static Task<Dictionary<TKey, TDocument>> ToDictionaryAsync<TDocument, TKey>(
            this IAsyncCursorSource<TDocument> cursorSource,
            Func<TDocument, TKey> keySelector,
            CancellationToken cancellation
        )
            where TKey : notnull
            => cursorSource.ToDictionaryAsync(keySelector, d => d, cancellation);

        public static async Task<Dictionary<TKey, TValue>> ToDictionaryAsync<TDocument, TKey, TValue>(
            this IAsyncCursorSource<TDocument> cursorSource,
            Func<TDocument, TKey> keySelector,
            Func<TDocument, TValue> valueSelector,
            CancellationToken cancellation
        )
            where TKey : notnull
        {
            using var cursor = await cursorSource.ToCursorAsync(cancellation).ConfigureAwait(false);
            var result = new Dictionary<TKey, TValue>();

            while (await cursor.MoveNextAsync(cancellation).ConfigureAwait(false))
                foreach (var document in cursor.Current)
                    result.Add(keySelector(document), valueSelector(document));

            return result;
        }
    }
}