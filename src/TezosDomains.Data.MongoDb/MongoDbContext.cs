﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.DistributedLock;

namespace TezosDomains.Data.MongoDb
{
    public sealed class MongoDbContext
    {
        public IMongoDatabase Database { get; }

        public MongoDbContext(MongoClient client, MongoDbConfiguration configuration)
            => Database = client.GetDatabase(configuration.DatabaseName);

        public static readonly IReadOnlyDictionary<Type, string> CollectionNames = new Dictionary<Type, string>
        {
            { GetDbType<Settings>(), "settings" },
            { GetDbType<Block>(), "blocks" },
            { GetDbType<Domain>(), "domains" },
            { GetDbType<ReverseRecord>(), "reverse_records" },
            { GetDbType<Auction>(), "auctions" },
            { GetDbType<Lock>(), "locks" },
            { GetDbType<Event>(), "events" },
            { GetDbType<BidderBalances>(), "bidder_balances" },
            { GetDbType<Offer>(), "offers" }
        };

        public IMongoCollection<TDocument> Get<TDocument>()
            where TDocument : IHasDbCollection
            => Database.GetCollection<TDocument>(CollectionNames[typeof(TDocument)]);

        private static Type GetDbType<TDocument>()
            where TDocument : IHasDbCollection
            => typeof(TDocument);
    }
}