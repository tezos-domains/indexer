# Monitoring

Indexer & API monitoring is done using two independent ways:

 - ## Health endpoints

Both services have a HTTP endpoint `/health` showing their health status. 

Health endpoint provides only limitted view about the service's health status. It can provide only one of three states `Healty`, `Degraded`, `Unhealthy` for each component. The overall healthy status is used for loadbalancers.

The health history of both services is accessible via `https://status.tezos.domains`.

 - ## Prometheus metrics

 The more detailed view on performace is done using prometheus metrics. Both services exposes a prometheus endpoint `/metrics`.

 These metrics are consumed by a prometheus service. This service is plugged in Grafana as a datasource.

 ### Grafana dashborads

There are two grafana dashboards:

 - TezosDomains

is an overall perfomarmance dashboard with variables `Environment`, `Interval`, `Client Instance`, `API Instance`.

 - TezosDomains mainnet alerts

 is a dashboard linked to Indexer & API mainnet deployment. It watches the metrics and automatically triggers alerts.

