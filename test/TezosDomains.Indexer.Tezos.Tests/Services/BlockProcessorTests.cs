﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;
using TezosDomains.Indexer.Tezos.Services;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Tezos.Tests.Services
{
    public class BlockProcessorTests
    {
        private static readonly JsonElement EmptyJsonDocument = JsonDocument.Parse("{}").RootElement;

        [Test]
        public async Task InitAsync_ShouldInitContractProvider()
        {
            //arrange
            var ct = TestCancellationToken.Get();
            var contractProvider = Substitute.For<IContractDefinitionProvider>();
            var target = new BlockProcessor(contractProvider, null!);

            //act
            await target.InitAsync(ct);

            //assert
            await contractProvider.Received(1).InitAsync(ct);
        }

        [Test]
        public async Task Process_ShouldNotify_ContractSubscriber_WhenSubscribedDestinationAddressInTransactionAsync()
        {
            //arrange

            #region Block setup

            var block = new TezosBlock()
            {
                OperationGroups = new[]
                {
                    new OperationGroup { Hash = "h1", Operations = new[] { new Operation() { Kind = "wrong_kind" } } },
                    new OperationGroup
                    {
                        Hash = "h2_tz1proxy_invalid_operation_status", Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz1proxy",
                                Amount = "0",
                                Metadata = new OperationMetadata() { OperationResult = new OperationResult() { Status = "wrong_status" } }
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h3_tz1proxy_valid_entrypoint", Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz1proxy",
                                Source = "h3",
                                Metadata =
                                    new OperationMetadata()
                                        { OperationResult = new OperationResult() { Status = Michelson.Const.OperationResultStatus.Applied } },
                                Parameters = new OperationParameters() { Entrypoint = "tz1proxy.entrypoint" },
                                Amount = "123",
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h4_tz2destination_valid_bigmap_update",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz2destination",
                                Source = "h4",
                                Parameters = new OperationParameters() { Entrypoint = "tz2destination.entrypoint" },
                                Amount = "123",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new OperationResult()
                                    {
                                        Status = Michelson.Const.OperationResultStatus.Applied,
                                        LazyStorageDiffs = new[]
                                        {
                                            new LazyStorageDiff()
                                            {
                                                Kind = Michelson.Const.LazyStorageDiffKind.BigMap,
                                                Id = "tz2destination.bigmap",
                                                BigMapDiff = new LazyBigMapDiff()
                                                {
                                                    Action = Michelson.Const.BigMapDiffAction.Update,
                                                    Updates = new[] { new LazyBigMapUpdate() },
                                                },
                                            },
                                            new LazyStorageDiff()
                                            {
                                                Kind = "sapling_state",
                                                Id = "whatever",
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                    new OperationGroup
                    {
                        Hash = "h5_tz2destination_wrong_bigmap_action",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz2destination",
                                Parameters = new OperationParameters() { Entrypoint = "tz2destination.entrypoint" },
                                Amount = "123",
                                Source = "h5",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new OperationResult()
                                    {
                                        Status = Michelson.Const.OperationResultStatus.Applied,
                                        LazyStorageDiffs = new[]
                                        {
                                            new LazyStorageDiff()
                                            {
                                                Kind = Michelson.Const.LazyStorageDiffKind.BigMap,
                                                Id = "tz2destination.bigmap",
                                                BigMapDiff = new LazyBigMapDiff() { Action = "wrong_action" },
                                            },
                                        },
                                    }
                                },
                            },
                        },
                    },
                    new OperationGroup
                    {
                        Hash = "h6_tz1proxy_valid_entrypoint_internalOperation",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Source = "h6",
                                Destination = "tz1noSubscription",
                                Amount = "0",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new OperationResult() { Status = Michelson.Const.OperationResultStatus.Applied },
                                    InternalOperations = new[]
                                    {
                                        new InternalOperation()
                                        {
                                            Kind = Michelson.Const.Operation.Kind.Transaction,
                                            Destination = "tz1proxy",
                                            Parameters = new OperationParameters() { Entrypoint = "tz1proxy.entrypoint" },
                                            Amount = "123",
                                            Source = "h6.internal",
                                            Result = new OperationResult() { Status = Michelson.Const.OperationResultStatus.Applied },
                                        },
                                    },
                                },
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h7_tz2destination_valid_bigmap_update_internalOperation",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Source = "h7",
                                Destination = "tz1noSubscription",
                                Amount = "0",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new OperationResult() { Status = Michelson.Const.OperationResultStatus.Applied },
                                    InternalOperations = new[]
                                    {
                                        new InternalOperation
                                        {
                                            Result = new OperationResult()
                                            {
                                                Status = Michelson.Const.OperationResultStatus.Applied,
                                                LazyStorageDiffs = new[]
                                                {
                                                    new LazyStorageDiff()
                                                    {
                                                        Kind = Michelson.Const.LazyStorageDiffKind.BigMap,
                                                        Id = "tz2destination.bigmap",
                                                        BigMapDiff = new LazyBigMapDiff()
                                                        {
                                                            Action = Michelson.Const.BigMapDiffAction.Update,
                                                            Updates = new[] { new LazyBigMapUpdate() },
                                                        },
                                                    },
                                                },
                                            },
                                            Kind = Michelson.Const.Operation.Kind.Transaction,
                                            Parameters = new OperationParameters() { Entrypoint = "tz2destination.entrypoint" },
                                            Amount = "123",
                                            Source = "h7.internal",
                                            Destination = "tz2destination",
                                        }
                                    }
                                },
                            },
                        },
                    },
                    new OperationGroup
                    {
                        Hash = "h8_no_subscription_on_tz3address_address",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz3address",
                                Source = "h8",
                                Amount = "0",
                                Metadata = new OperationMetadata()
                                    { OperationResult = new OperationResult() { Status = Michelson.Const.OperationResultStatus.Applied } }
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h10_tz2destination_outgoingmoneytransaction_internalOperation",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Source = "h10",
                                Destination = "tz1noSubscription",
                                Amount = "0",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new OperationResult() { Status = Michelson.Const.OperationResultStatus.Applied },
                                    InternalOperations = new[]
                                    {
                                        new InternalOperation
                                        {
                                            Result = new OperationResult { Status = Michelson.Const.OperationResultStatus.Applied },
                                            Kind = Michelson.Const.Operation.Kind.Transaction,
                                            Amount = "123",
                                            Source = "tz2destination",
                                            Destination = "h10.internal",
                                        }
                                    }
                                },
                            },
                        },
                    },
                    new OperationGroup
                    {
                        //an outgoing transaction without positive amount should not trigger the subscriber
                        Hash = "h11_tz2destination_outgoingtransaction_internalOperation",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Source = "h11",
                                Destination = "tz1noSubscription",
                                Amount = "0",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new OperationResult() { Status = Michelson.Const.OperationResultStatus.Applied },
                                    InternalOperations = new[]
                                    {
                                        new InternalOperation
                                        {
                                            Result = new OperationResult { Status = Michelson.Const.OperationResultStatus.Applied },
                                            Kind = Michelson.Const.Operation.Kind.Transaction,
                                            Amount = "0",
                                            Source = "tz2destination",
                                            Destination = "h11.internal",
                                        }
                                    }
                                },
                            },
                        },
                    },
                }
            };

            #endregion

            var clientMock = Substitute.For<ITezosClient>();
            var recordLayoutFactoryMock = Substitute.For<ILayoutFactory>();

            var csProxy1 = SetUpContractMocks("tz1proxy", clientMock);
            SetUpLayoutMocks(recordLayoutFactoryMock, csProxy1);
            var csDestination2 = SetUpContractMocks("tz2destination", clientMock);
            SetUpLayoutMocks(recordLayoutFactoryMock, csDestination2);

            var subscriptionConfigurations = new Dictionary<string, IReadOnlyList<string>>
            {
                { "tz1proxy", new[] { "EntrypointSubscriberClass" } },
                { "tz2destination", new[] { "BigMapSubscriberClass" } }
            };
            var contractProvider = new ContractDefinitionProvider(
                new TezosContractConfiguration { Subscriptions = subscriptionConfigurations, Storage = new List<ContractStorageItemConfiguration>() },
                clientMock,
                recordLayoutFactoryMock,
                Substitute.For<ILogger<ContractDefinitionProvider>>()
            );
            await contractProvider.InitAsync(CancellationToken.None);

            var entrypointContractSubscriberMock = Substitute.For<IContractSubscriber>();
            var bigMapContractSubscriberMock = Substitute.For<IContractSubscriber>();
            var subscriberProvider = Substitute.For<ISubscriberProvider>();
            subscriberProvider.Subscribers.Returns(
                new Dictionary<string, IReadOnlyList<IContractSubscriber>>
                {
                    { "tz1proxy", new[] { entrypointContractSubscriberMock } },
                    { "tz2destination", new[] { bigMapContractSubscriberMock } },
                }
            );
            var target = new BlockProcessor(contractProvider, subscriberProvider);

            //act
            target.Process(block);

            //assert
            entrypointContractSubscriberMock.Received(1)
                .OnEntrypoint(
                    Arg.Is<OnEntrypointArguments>(
                        a => a.Block == block
                             && a.OperationGroupHash == "h3_tz1proxy_valid_entrypoint"
                             && a.EntrypointName == "tz1proxy.entrypoint"
                             && a.SourceAddress == "h3"
                             && a.Amount == 123
                    )
                );
            entrypointContractSubscriberMock.Received(1)
                .OnEntrypoint(
                    Arg.Is<OnEntrypointArguments>(
                        a => a.Block == block
                             && a.OperationGroupHash == "h6_tz1proxy_valid_entrypoint_internalOperation"
                             && a.EntrypointName == "tz1proxy.entrypoint"
                             && a.SourceAddress == "h6.internal"
                             && a.Amount == 123
                    )
                );
            //only previous two call were done, no other call onEntrypoint nor any call for OnBigMap
            entrypointContractSubscriberMock.ReceivedCalls().Should().HaveCount(2);

            bigMapContractSubscriberMock.Received(1)
                .OnBigMapDiff(
                    Arg.Is<OnBigMapDiffArguments>(
                        a => a.Block == block
                             && a.OperationGroupHash == "h4_tz2destination_valid_bigmap_update"
                             && a.BigMapName == "bigmap"
                    )
                );
            bigMapContractSubscriberMock.Received(1)
                .OnBigMapDiff(
                    Arg.Is<OnBigMapDiffArguments>(
                        a => a.Block == block
                             && a.OperationGroupHash == "h7_tz2destination_valid_bigmap_update_internalOperation"
                             && a.BigMapName == "bigmap"
                    )
                );
            bigMapContractSubscriberMock.Received(1)
                .OnOutgoingMoneyTransaction(
                    Arg.Is<OnOutgoingTransactionArguments>(
                        a => a.Block == block
                             && a.OperationGroupHash == "h10_tz2destination_outgoingmoneytransaction_internalOperation"
                             && a.DestinationAddress == "h10.internal"
                             && a.Amount == 123
                    )
                );
            //three onEntrypointCalls(h4,h5,h7), two onBigMapCalls, one OnOutgoingMoneyTransaction
            bigMapContractSubscriberMock.ReceivedCalls().Should().HaveCount(6);
        }

        private static ContractScript SetUpContractMocks(string address, ITezosClient clientMock)
        {
            //create contract scripts
            var contractScript = new ContractScript(address, new JsonElement(), EmptyJsonDocument);

            //setup tezos node client mock
            clientMock.GetContractScriptAsync(contractScript.Address, Arg.Any<CancellationToken>())
                .Returns(contractScript);

            return contractScript;
        }

        private static void SetUpLayoutMocks(ILayoutFactory layoutFactoryMock, ContractScript contractScriptProxy)
        {
            layoutFactoryMock.CreateEntrypointLayouts(contractScriptProxy.Address, contractScriptProxy)
                .Returns(
                    new Dictionary<string, RecordLayout>
                        { { contractScriptProxy.Address + ".entrypoint", new RecordLayout(EmptyJsonDocument) } }
                );
            layoutFactoryMock.CreateBigMapLayouts(contractScriptProxy)
                .Returns(
                    new Dictionary<string, BigMapLayout>
                    {
                        {
                            contractScriptProxy.Address + ".bigmap",
                            new BigMapLayout("bigmap", EmptyJsonDocument, new RecordLayout(EmptyJsonDocument))
                        }
                    }
                );
        }
    }
}