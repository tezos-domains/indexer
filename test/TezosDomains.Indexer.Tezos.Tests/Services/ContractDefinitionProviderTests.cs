﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.ParsedData;
using TezosDomains.Indexer.Tezos.Services;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Tezos.Tests.Services
{
    public class ContractDefinitionProviderTests
    {
        private static readonly JsonElement EmptyJsonDocument = JsonDocument.Parse("{}").RootElement;

        [Test]
        public async Task ShouldDownloadContractDefinitionAndSubscribeAndMergeItWithLocalStorageDefinition()
        {
            //arrange
            var subscriptionConfig = new Dictionary<string, IReadOnlyList<string>>
            {
                ["contractAddress"] = new[] { "SubscriberClassName" }
            };
            var config = new TezosContractConfiguration
            {
                Subscriptions = subscriptionConfig, Storage = new[]
                {
                    new ContractStorageItemConfiguration()
                    {
                        Contracts = new[] { "contractAddress" },
                        Name = "storageProperty",
                        DefinitionString = "{}",
                        ValueString = "{}"
                    }
                },
                ChainId = "bla",
                StartFromLevel = 1
            };

            var tezosClient = Substitute.For<ITezosClient>();
            var layoutFactory = Substitute.For<ILayoutFactory>();
            var logger = Substitute.For<ILogger<ContractDefinitionProvider>>();
            var target = new ContractDefinitionProvider(config, tezosClient, layoutFactory, logger);

            //create contract scripts
            var contractScript = new ContractScript("contractAddress", new JsonElement(), EmptyJsonDocument);

            //setup tezos node client mock
            var ct = TestCancellationToken.Get();
            tezosClient.GetContractScriptAsync(contractScript.Address, ct).Returns(contractScript);

            var bigMapLayouts = new Dictionary<string, BigMapLayout>();
            var entrypointLayouts = new Dictionary<string, RecordLayout>();

            layoutFactory.CreateBigMapLayouts(contractScript).Returns(bigMapLayouts);
            layoutFactory.CreateEntrypointLayouts("contractAddress", contractScript).Returns(entrypointLayouts);

            //act
            await target.InitAsync(ct);

            //assert
            var contract = target.SubscribedContracts.Single();
            contract.Key.Should().Be("contractAddress");
            contract.Value.BigMapLayouts.Should().BeSameAs(bigMapLayouts);
            contract.Value.EntrypointLayouts.Should().BeSameAs(entrypointLayouts);
            contract.Value.Address.Should().Be("contractAddress");

            contract.Value.StorageProperties.Should()
                .BeEquivalentTo(
                    new Dictionary<string, IMichelsonValue>()
                        { { "storageProperty", new MichelsonValue(config.Storage[0].Definition, config.Storage[0].Value) } }
                );

            await tezosClient.Received(1).GetContractScriptAsync("contractAddress", ct);
        }
    }
}