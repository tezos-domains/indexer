﻿using NUnit.Framework;
using TezosDomains.Indexer.Tezos.Serializers;

namespace TezosDomains.Indexer.Tezos.Tests.Serializer
{
    [TestFixture]
    public class JsonSnakeCaseNamingPolicyTest
    {
        [TestCase("Prop", "prop")]
        [TestCase("FancyPants", "fancy_pants")]
        [TestCase("", "")]
        public void ConvertFromPascalCase(string pascalCase, string snakeCase)
        {
            Assert.That(JsonSnakeCaseNamingPolicy.Instance.ConvertName(pascalCase), Is.EqualTo(snakeCase));
        }
    }
}