﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos.Tests
{
    public class MichelsonValueTest
    {
        [Test]
        public void GetValueAsNullableInt_ShouldReturnNull_WhenNone()
        {
            var definition = JsonDocument.Parse($"{{\"prim\":\"{Michelson.Const.Prim.Option}\"}}").RootElement;
            var value = JsonDocument.Parse($"{{\"prim\":\"{Michelson.Const.Prim.None}\"}}").RootElement;
            new MichelsonValue(definition, value).GetValueAsNullableInt().Should().BeNull();
        }

        [Test]
        public void GetValueAsDateTime_ShouldReturnDateTime_WhenPrimString()
        {
            var definition = JsonDocument.Parse($"{{\"prim\":\"{Michelson.Const.Prim.Timestamp}\"}}").RootElement;
            var value = JsonDocument.Parse("{\"string\":\"2022-01-31T23:00:00.000Z\"}").RootElement;
            new MichelsonValue(definition, value).GetValueAsDateTime().Should().Be(new DateTime(2022, 01, 31, 23, 0, 0, DateTimeKind.Utc));
        }
    }
}