﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Serializers;

namespace TezosDomains.Indexer.Tezos.Tests.Layout.Factory
{
    [TestFixture]
    public class LayoutFactoryTests
    {
        private LayoutFactory _instance = null!;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _instance = new LayoutFactory();
        }

        private ContractScript DeserializeContractFromFileOrGigaNode(string folder, string contract)
        {
            var path = JsonHelper.GetFilePath("Layout", "Factory", folder, $"{contract}.json");
            if (!File.Exists(path))
            {
                var contractString = new HttpClient()
                    .GetStringAsync("https://delphinet-tezos.giganode.io/chains/main/blocks/head/context/contracts/" + contract)
                    .Result;
                File.WriteAllText(path, contractString);
                return DeserializeContractFromFileOrGigaNode(folder, contract);
            }

            var contractJson = TezosNodeDeserializer.Deserialize<JsonElement>(JsonHelper.ReadJsonFile(path));
            return ContractScript.Create(contract, contractJson);
        }

        [TestCase("Delphinet", "KT1CR6vXJ1qeY4ALDQfUaLFi3FcJJZ8WDygo")]
        public void CreateBigMapLayouts_ShouldContainUsedBigMaps(string network, string nameRegistryContractAddress)
        {
            var contractScript = DeserializeContractFromFileOrGigaNode(network, nameRegistryContractAddress);

            var layouts = _instance.CreateBigMapLayouts(contractScript);

            layouts.Values.Select(v => v.Name).Should().Contain(new List<string> { "records", "reverse_records", "expiry_map" });
        }

        [TestCase("Delphinet", "KT1SvUM7iyTEa4TLjnonD5BiiyaLxs6j6wsn", "buy")]
        [TestCase("Delphinet", "KT1EyNFbCTLNEy7gY3XvcDgC7BZE3xs7SayR", "renew")]
        [TestCase("Delphinet", "KT1FpZPYp5Ne198YyZJkLeu6v94CaVoZHThd", "bid")]
        [TestCase("Delphinet", "KT1C4BtpmqGQt1Y917BPKaETbtUqxUZebirF", "withdraw")]
        [TestCase("Delphinet", "KT1URVbKdq6aWrofHYj7SCBn9U1ygHCgzddX", "settle")]
        [TestCase("Delphinet", "KT1TTiRLpRiQZ9CmXVF9r2YEmX9MbmDRqzPP", "commit")]
        [TestCase("Delphinet", "KT1CwTQbFm2QHVGggSAHVeR7nJ6h4scH843N", "buy")]
        [TestCase("Delphinet", "KT1JZEmkAJQaYrNSKubcYT9DLSMmz6zLcP3C", "renew")]
        [TestCase("Delphinet", "KT1X6NEHEFyp4GESYFZWuQJuA7tv1WSeqcKc", "bid")]
        [TestCase("Delphinet", "KT1TUMj5HFmRitAAt7SAMcePLsDNBLCAsre7", "withdraw")]
        [TestCase("Delphinet", "KT1DNriEHLPtkQ4TQVaDjj7GUYbMLWcKpiNq", "settle")]
        [TestCase("Delphinet", "KT1BVYUTAqUfXbg9GntHPxJcMpf1fHrdGQCT", "commit")]
        [TestCase("Delphinet", "KT1ScdYz5vqPqGPTMqufwY1tvdPP7E5HAayU", "buy")]
        [TestCase("Delphinet", "KT1LV1AJetmLXtvuraZTWzJa9TKpaabCW8dQ", "renew")]
        [TestCase("Delphinet", "KT1X2kg7XGfok2m6L8bMBsA27YNvN9xpS1DY", "bid")]
        [TestCase("Delphinet", "KT1MgLpKtuJpLFC3gvLSksSqNzQXrYoTAPcj", "withdraw")]
        [TestCase("Delphinet", "KT1CsihxV8fDgoFx98mpEgTazKKSp2C5pXmr", "settle")]
        [TestCase("Delphinet", "KT1VgDrc2VS7JHtqdR3K6F63azQdyzqD5i4R", "commit")]
        [TestCase("Delphinet", "KT1UKyfVorer7j8Wk5oafGNPYZPv65EW1KFM", "buy")]
        [TestCase("Delphinet", "KT1QC7yGJF3RyuYGtQMfzbn5jyDmADT8kTHM", "renew")]
        [TestCase("Delphinet", "KT1H7jHcCbt5rft1q4CehVpqi81gDBbp7E9N", "bid")]
        [TestCase("Delphinet", "KT1F5md8HLRiCyTc5ddCTh9UjNGSKLMZZ36s", "withdraw")]
        [TestCase("Delphinet", "KT1R4rsjci49fjryQrpLM4zuqHKzWN5txSft", "settle")]
        public void CreateEntrypointLayouts_ShouldContainEntrypoint(string network, string proxyContractAddress, string entrypointName)
        {
            var contractScript = DeserializeContractFromFileOrGigaNode(network, proxyContractAddress);

            var layouts = _instance.CreateEntrypointLayouts(proxyContractAddress, contractScript);

            layouts.Keys.Should().Contain(entrypointName);
        }
    }
}