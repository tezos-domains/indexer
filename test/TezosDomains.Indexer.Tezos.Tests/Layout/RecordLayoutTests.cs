﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Text.Json;

namespace TezosDomains.Indexer.Tezos.Tests.Layout
{
    [TestFixture]
    public class LayoutTests
    {
        [Test]
        public void Apply_ShouldExtractValuesUsingTheirDefinition()
        {
            #region DefinitionElement

            var definition = @"
                              {
                                ""prim"": ""pair"",
                                ""args"": [
                                  {
                                    ""prim"": ""pair"",
                                    ""args"": [
                                      {
                                        ""prim"": ""pair"",
                                        ""args"": [
                                          {
                                            ""prim"": ""option"",
                                            ""args"": [{ ""prim"": ""address"" }],
                                            ""annots"": [""%address""]
                                          },
                                          {
                                            ""prim"": ""map"",
                                            ""args"": [{ ""prim"": ""string"" }, { ""prim"": ""bytes"" }],
                                            ""annots"": [""%data""]
                                          }
                                        ]
                                      },
                                      {
                                        ""prim"": ""pair"",
                                        ""args"": [
                                          {
                                            ""prim"": ""option"",
                                            ""args"": [{ ""prim"": ""bytes"" }],
                                            ""annots"": [""%expiry_key""]
                                          },
                                          {
                                            ""prim"": ""map"",
                                            ""args"": [{ ""prim"": ""string"" }, { ""prim"": ""bytes"" }],
                                            ""annots"": [""%internal_data""]
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    ""prim"": ""pair"",
                                    ""args"": [
                                      {
                                        ""prim"": ""pair"",
                                        ""args"": [
                                          { ""prim"": ""nat"", ""annots"": [""%level""] },
                                          { ""prim"": ""address"", ""annots"": [""%owner""] }
                                        ]
                                      },
                                      {
                                        ""prim"": ""option"",
                                        ""args"": [{ ""prim"": ""nat"" }],
                                        ""annots"": [""%validator""]
                                      }
                                    ]
                                  }
                                ]
                              }";

            #endregion

            #region ValueElement

            var value = @"
                        {
                          ""prim"": ""Pair"",
                          ""args"": [
                            {
                              ""prim"": ""Pair"",
                              ""args"": [
                                {
                                  ""prim"": ""Pair"",
                                  ""args"": [
                                    {
                                      ""prim"": ""Some"",
                                      ""args"": [ { ""bytes"": ""0000e81b5d37fc4c2635d9b1c6d657eb570b86d80096"" } ]
                                    },
                                    [
                                      {
                                        ""prim"": ""Elt"",
                                        ""args"": [
                                          { ""string"": ""td:ttl"" },
                                          { ""bytes"": ""343230"" }
                                        ]
                                      }
                                    ]
                                  ]
                                },
                                {
                                  ""prim"": ""Pair"",
                                  ""args"": [
                                    {
                                      ""prim"": ""Some"",
                                      ""args"": [ { ""bytes"": ""6f6b2e74657374"" } ]
                                    },
                                    []
                                  ]
                                }
                              ]
                            },
                            {
                              ""prim"": ""Pair"",
                              ""args"": [
                                {
                                  ""prim"": ""Pair"",
                                  ""args"": [
                                    { ""int"": ""2"" },
                                    { ""bytes"": ""0000e81b5d37fc4c2635d9b1c6d657eb570b86d80096"" }
                                  ]
                                },
                                {
                                  ""prim"": ""Some"",
                                  ""args"": [ { ""int"": ""1"" } ]
                                }
                              ]
                            }
                          ]
                        }";

            #endregion

            var layout = new Layouts.RecordLayout(JsonDocument.Parse(definition).RootElement);

            var parsedValues = layout.Apply(JsonDocument.Parse(value).RootElement);

            parsedValues["address"].GetValueAsNullableAddress().Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            parsedValues["owner"].GetValueAsAddress().Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            parsedValues["level"].GetValueAsInt().Should().Be(2);
            parsedValues["expiry_key"].GetValueAsNullableString().Should().Be("ok.test");
            parsedValues["data"].GetValueAsMap().Should().BeEquivalentTo(new Dictionary<string, string> { { "td:ttl", "343230" } });
            parsedValues["internal_data"].GetValueAsMap().Should().BeEmpty();
        }

        [Test]
        public void Apply_ShouldAcceptEntrypointValueAsJsonArray_WhenDefinitionHasMultipleParameters()
        {
            #region DefinitionElement

            var definition = @"
            {
                ""prim"": ""pair"",
                ""args"": [
                    {
                        ""prim"": ""bytes"",
                        ""annots"": [
                            ""%name""
                        ]
                    },
                    {
                        ""prim"": ""option"",
                        ""args"": [
                            {
                                ""prim"": ""address""
                            }
                        ],
                        ""annots"": [
                            ""%address""
                        ]
                    },
                    {
                        ""prim"": ""address"",
                        ""annots"": [
                            ""%owner""
                        ]
                    },
                    {
                        ""prim"": ""map"",
                        ""args"": [
                            {
                                ""prim"": ""string""
                            },
                            {
                                ""prim"": ""bytes""
                            }
                        ],
                        ""annots"": [
                            ""%data""
                        ]
                    }
                ],
                ""annots"": [
                    ""%update_record""
                ]
            }";

            #endregion

            #region ValueElement

            var value = @"[
            {
            ""bytes"": ""6d6572626f636f702e74657a""
            },
            {
                ""prim"": ""Some"",
                ""args"": [
                {
                    ""string"": ""tz1SebmhV9P6pKfx7otPpdECdqY2JPZYB5gM""
                }
                ]
            },
            {
                ""string"": ""tz1SebmhV9P6pKfx7otPpdECdqY2JPZYB5gM""
            },
            [
            ]
        ]";

            #endregion

            var layout = new Layouts.RecordLayout(JsonDocument.Parse(definition).RootElement);

            var parsedValues = layout.Apply(JsonDocument.Parse(value).RootElement, enumerateValueWhenArray: true);
            parsedValues["name"].GetValueAsString().Should().Be("merbocop.tez");
            parsedValues["address"].GetValueAsNullableAddress().Should().Be("tz1SebmhV9P6pKfx7otPpdECdqY2JPZYB5gM");
            parsedValues["owner"].GetValueAsAddress().Should().Be("tz1SebmhV9P6pKfx7otPpdECdqY2JPZYB5gM");
            parsedValues["data"].GetValueAsMap().Should().BeEmpty();
        }

        [Test]
        public void Apply_ShouldAcceptEntrypointValueAsJsonArray_WhenDefinitionHasSingleParameter()
        {
            #region DefinitionElement

            var definition = @"{
                                ""prim"": ""lambda"",
                                ""args"": [
                                ],
                                ""annots"": [
                                    ""%admin_update""
                                ]
                            }";

            #endregion

            #region ValueElement

            var value = @"[
                                {
                                    ""prim"": ""DUP""
                                },
                                {
                                    ""prim"": ""CDR""
                                },
                                {
                                    ""prim"": ""SWAP""
                                },
                                {
                                    ""prim"": ""DUP""
                                }
                            ]";

            #endregion

            var layout = new Layouts.RecordLayout(JsonDocument.Parse(definition).RootElement);

            var parsedValues = layout.Apply(JsonDocument.Parse(value).RootElement, enumerateValueWhenArray: true);
            parsedValues.Keys.Should().ContainSingle("admin_update");
        }
    }
}