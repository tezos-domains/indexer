﻿using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using System;
using TezosDomains.Indexer.Subscribers;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public class AuctionSubscriberTests : SubscriberTestsBase<AuctionSubscriber>
    {
        [Test]
        public void OnBigMapDiff_WithBigMapValue_ShouldCreateAuctionBidUpdate()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            var testTime = DateTime.UtcNow;
            Properties.Setup("last_bidder", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("last_bid", v => v.GetValueAsMutezDecimal(), 10);
            Properties.Setup("ownership_period", v => v.GetValueAsInt(), 365);
            Properties.Setup("ends_at", v => v.GetValueAsDateTime(), testTime);
            KeyPropertyMock.GetValueAsString().Returns("used-shoe");

            //act
            var updates = Target_OnBigMapDiff(layoutName: "auctions");

            //assert
            var expectedUpdate = new AuctionBidUpdate(
                OperationGroupHash: TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                Bidder: TestAddress1,
                Amount: 10,
                EndsAtUtc: testTime,
                OwnedUntilUtc: testTime.AddDays(365)
            );
            updates.Should().AllBeEquivalentTo(expectedUpdate);
        }

        [Test]
        public void OnBigMapDiff_WithoutBigMapValue_ShouldCreateAuctionSettleUpdate()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            KeyPropertyMock.GetValueAsString().Returns("used-shoe");

            //act
            var updates = Target_OnBigMapDiff(layoutName: "auctions");

            //assert
            var expectedUpdate = new AuctionSettledUpdate(
                OperationGroupHash: TestOperationGroupHash,
                DomainName: "used-shoe.tez"
            );
            updates.Should().AllBeEquivalentTo(expectedUpdate);
        }

        protected override AuctionSubscriber CreateSubscriber() => new();
    }
}