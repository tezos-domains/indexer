﻿using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TezosDomains.Indexer.Subscribers;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public class NameRegistrySubscriberTests : SubscriberTestsBase<NameRegistrySubscriber>
    {
        [Test]
        public void OnBigMapDiff_Records_ShouldCreateUpdateAndPossibleTransfer()
        {
            //arrange
            KeyPropertyMock.GetValueAsString().Returns("first.used-shoe.tez");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("address", v => v.GetValueAsNullableAddress(), TestAddress2);
            Properties.Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string> { { "a", "b" } });
            Properties.Setup("expiry_key", v => v.GetValueAsNullableString(), "used-shoe.tez");
            Properties.Setup("tzip12_token_id", v => v.GetValueAsNullableInt(), 1);
            Properties.Setup(
                "internal_data",
                v => v.GetAndUnpackValueFromMapAsAddressArray("operators"),
                new[] { "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm" }
            );

            //act
            var updates = Target_OnBigMapDiff(layoutName: "records");

            //assert
            var expectedUpdate = new DomainUpdate(
                OperationGroupHash: TestOperationGroupHash,
                Name: "first.used-shoe.tez",
                Address: TestAddress2,
                Owner: TestAddress1,
                ValidityKey: "used-shoe.tez",
                Data: new Dictionary<string, string> { { "a", "b" } },
                TokenId: 1,
                Operators: new[] { "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm" }
            );
            updates.Should().AllBeEquivalentTo(expectedUpdate);
        }

        [Test]
        public void OnBigMapDiff_ReverseRecords_ShouldCreateUpdate()
        {
            //arrange
            KeyPropertyMock.GetValueAsAddress().Returns(TestAddress1);
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress2);
            Properties.Setup("name", v => v.GetValueAsNullableString(), "used-shoe.tez");

            //act
            var updates = Target_OnBigMapDiff(layoutName: "reverse_records");

            //assert
            var expectedUpdate = new ReverseRecordUpdate(
                OperationGroupHash: TestOperationGroupHash,
                Address: TestAddress1,
                Owner: TestAddress2,
                Name: "used-shoe.tez"
            );
            updates.Should().AllBeEquivalentTo(expectedUpdate);
        }

        [Test]
        public void OnBigMapDiff_ExpiryMap_ShouldCreateUpdate([Values] bool valuePresent)
        {
            //arrange
            KeyPropertyMock.GetValueAsString().Returns("used-shoe.tez");
            var now = DateTime.UtcNow;
            if (valuePresent)
                Properties.Setup("__value__", v => v.GetValueAsDateTime(), now);

            //act
            var updates = Target_OnBigMapDiff(layoutName: "expiry_map");

            //assert
            var expectedUpdate = new ValidityUpdate(
                OperationGroupHash: TestOperationGroupHash,
                ValidityKey: "used-shoe.tez",
                ExpiresAtUtc: valuePresent ? now : DateTime.MaxValue
            );
            updates.Should().AllBeEquivalentTo(expectedUpdate);
        }

        protected override NameRegistrySubscriber CreateSubscriber() => new();
    }
}