﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Test.Subscribers.Events;

public class OfferExecutedEntrypointSubscriberTests : SubscriberTestsBase<OfferExecutedEntrypointSubscriber<SellOfferExtractionStrategy>>
{
    [Test]
    public void OnEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        var priceWithoutFee = TestRandom.GetDecimal();
        var fee = TestRandom.GetDecimal();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);
        Properties.Setup("seller", v => v.GetValueAsAddress(), TestAddress1);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "execute_offer");

        //assert
        var expectedEvent = new OfferExecutedEvent(
            Id: "ignored",
            Block: TestBlockSlim,
            SourceAddress: TestSourceAddress,
            OperationGroupHash: TestOperationGroupHash,
            TokenId: tokenId,
            InitiatorAddress: TestAddress1,
            PriceWithoutFee: priceWithoutFee,
            Fee: fee
        );
        VerifyEvent(updates, expectedEvent);
    }

    protected override OfferExecutedEntrypointSubscriber<SellOfferExtractionStrategy> CreateSubscriber() =>
        new(new SellOfferExtractionStrategy(new MarketplaceConfiguration()));
}