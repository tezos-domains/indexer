﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class AuctionSettleEntrypointSubscriberTests : SubscriberTestsBase<AuctionSettleEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("address", v => v.GetValueAsNullableAddress(), TestAddress2);
            Properties.Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string> { { "a", "b" } });

            //act
            var updates = Target_OnEntrypoint(entrypointName: "settle");

            //assert
            var expectedEvent = new AuctionSettleEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                DomainOwnerAddress: TestAddress1,
                DomainForwardRecordAddress: TestAddress2,
                Data: new Dictionary<string, string> { { "a", "b" } },
                RegistrationDurationInDays: 0,
                WinningBid: 0
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override AuctionSettleEntrypointSubscriber CreateSubscriber() => new();

    }
}