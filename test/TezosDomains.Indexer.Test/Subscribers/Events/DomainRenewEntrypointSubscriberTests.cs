﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;
using TezosDomains.Indexer.Tezos.Configuration;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainRenewEntrypointSubscriberTests : SubscriberTestsBase<DomainRenewEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("duration", v => v.GetValueAsInt(), 365);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "renew");

            //assert
            var expectedEvent = new DomainRenewEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                Price: TestAmount,
                DurationInDays: 365
            );
            VerifyEvent(updates, expectedEvent);
        }


        [Test]
        public void OnEntrypoint_ShouldSubstituteSourceAddressWhenAffiliateContract()
        {
            //arrange
            TestRootSourceAddress = TestSourceAddress;
            TestSourceAddress = "KT1PKFkGAfh8bqKQs1rZzXGz6W1X2SSc3977";
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("duration", v => v.GetValueAsInt(), 365);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "renew");

            //assert
            var expectedEvent = new DomainRenewEvent(
                Id: "ignored",
                TestBlockSlim,
                TestRootSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                Price: TestAmount,
                DurationInDays: 365
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override DomainRenewEntrypointSubscriber CreateSubscriber() =>
            new(new AffiliateConfiguration() { AffiliateContract = "KT1PKFkGAfh8bqKQs1rZzXGz6W1X2SSc3977" });
    }
}