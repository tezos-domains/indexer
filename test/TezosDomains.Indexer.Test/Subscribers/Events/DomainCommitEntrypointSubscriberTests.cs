﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainCommitEntrypointSubscriberTests : SubscriberTestsBase<DomainCommitEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Properties.Setup("commit", v => v.GetValueAsHexBytesString(), "commit-hash");

            //act
            var updates = Target_OnEntrypoint(entrypointName: "commit");

            //assert
            var expectedEvent = new DomainCommitEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                CommitmentHash: "commit-hash"
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override DomainCommitEntrypointSubscriber CreateSubscriber() => new();

    }
}