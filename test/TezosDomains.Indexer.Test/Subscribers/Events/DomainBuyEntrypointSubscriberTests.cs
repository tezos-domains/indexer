﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;
using TezosDomains.Indexer.Tezos.Configuration;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainBuyEntrypointSubscriberTests : SubscriberTestsBase<DomainBuyEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("address", v => v.GetValueAsNullableAddress(), TestAddress2);
            Properties.Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string> { { "a", "b" } });
            Properties.Setup("duration", v => v.GetValueAsInt(), 365);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "buy");

            //assert
            var expectedEvent = new DomainBuyEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                Price: TestAmount,
                DurationInDays: 365,
                DomainOwnerAddress: TestAddress1,
                DomainForwardRecordAddress: TestAddress2,
                Data: new Dictionary<string, string> { { "a", "b" } }
            );
            VerifyEvent(updates, expectedEvent);
        }

        [Test]
        public void OnEntrypoint_ShouldSubstituteSourceAddressWhenAffiliateContract()
        {
            //arrange
            TestRootSourceAddress = TestSourceAddress;
            TestSourceAddress = "KT1PKFkGAfh8bqKQs1rZzXGz6W1X2SSc3977";
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("address", v => v.GetValueAsNullableAddress(), TestAddress2);
            Properties.Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string> { { "a", "b" } });
            Properties.Setup("duration", v => v.GetValueAsInt(), 365);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "buy");

            //assert
            var expectedEvent = new DomainBuyEvent(
                Id: "ignored",
                TestBlockSlim,
                TestRootSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                Price: TestAmount,
                DurationInDays: 365,
                DomainOwnerAddress: TestAddress1,
                DomainForwardRecordAddress: TestAddress2,
                Data: new Dictionary<string, string> { { "a", "b" } }
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override DomainBuyEntrypointSubscriber CreateSubscriber() =>
            new(new AffiliateConfiguration() { AffiliateContract = "KT1PKFkGAfh8bqKQs1rZzXGz6W1X2SSc3977" });
    }
}