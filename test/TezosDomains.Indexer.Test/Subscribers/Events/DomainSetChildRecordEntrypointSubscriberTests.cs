﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainSetChildRecordEntrypointSubscriberTests : SubscriberTestsBase<DomainSetChildRecordEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("parent", v => v.GetValueAsString(), "tez");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("address", v => v.GetValueAsNullableAddress(), TestAddress2);
            Properties.Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string> { { "a", "b" } });

            //act
            var updates = Target_OnEntrypoint(entrypointName: "set_child_record");

            //assert
            var expectedEvent = new DomainSetChildRecordEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                DomainOwnerAddress: TestAddress1,
                DomainForwardRecordAddress: TestAddress2,
                Data: new Dictionary<string, string> { { "a", "b" } },
                IsNewRecord: true
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override DomainSetChildRecordEntrypointSubscriber CreateSubscriber() => new();

    }
}