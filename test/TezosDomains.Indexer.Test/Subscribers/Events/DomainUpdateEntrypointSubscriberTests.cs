﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainUpdateEntrypointSubscriberTests : SubscriberTestsBase<DomainUpdateEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Properties.Setup("name", v => v.GetValueAsString(), "used-shoe.tez");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("address", v => v.GetValueAsNullableAddress(), TestAddress2);
            Properties.Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string> { { "a", "b" } });

            //act
            var updates = Target_OnEntrypoint(entrypointName: "update_record");

            //assert
            var expectedEvent = new DomainUpdateEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                DomainOwnerAddress: TestAddress1,
                DomainForwardRecordAddress: TestAddress2,
                Data: new Dictionary<string, string> { { "a", "b" } }
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override DomainUpdateEntrypointSubscriber CreateSubscriber() => new();

    }
}