﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Test.Subscribers.Events;

public class BuyOfferExecutedEntrypointSubscriberTests : SubscriberTestsBase<OfferExecutedEntrypointSubscriber<BuyOfferExtractionStrategy>>
{
    [Test]
    public void OnEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        var priceWithoutFee = TestRandom.GetDecimal();
        var fee = TestRandom.GetDecimal();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);
        Properties.Setup("buyer", v => v.GetValueAsAddress(), TestAddress1);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "execute_offer");

        //assert
        var expectedEvent = new BuyOfferExecutedEvent(
            Id: "ignored",
            Block: TestBlockSlim,
            SourceAddress: TestSourceAddress,
            OperationGroupHash: TestOperationGroupHash,
            TokenId: tokenId,
            InitiatorAddress: TestAddress1,
            PriceWithoutFee: priceWithoutFee,
            Fee: fee
        );
        VerifyEvent(updates, expectedEvent);
    }

    protected override OfferExecutedEntrypointSubscriber<BuyOfferExtractionStrategy> CreateSubscriber() => new(new BuyOfferExtractionStrategy());
}