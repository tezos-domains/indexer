﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Test.Subscribers.Events;

public class BuyOfferPlacedEntrypointSubscriberTests : SubscriberTestsBase<OfferPlacedEntrypointSubscriber<BuyOfferExtractionStrategy>>
{
    [Test]
    public void OnEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        var priceWithoutFee = TestRandom.GetDecimal();
        var fee = TestRandom.GetDecimal();
        var expiresAtOrNull = TestDateTime.GetFutureOrMax();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);
        Properties.Setup("price", v => v.GetValueAsMutezDecimal(), priceWithoutFee);
        Properties.Setup("expiration", v => v.GetValueAsNullableDateTime(), expiresAtOrNull);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "place_offer");

        //assert
        var expectedEvent = new BuyOfferPlacedEvent(
            Id: "ignored",
            Block: TestBlockSlim,
            SourceAddress: TestSourceAddress,
            OperationGroupHash: TestOperationGroupHash,
            TokenId: tokenId,
            PriceWithoutFee: priceWithoutFee,
            Fee: fee,
            ExpiresAtUtc: expiresAtOrNull
        );
        VerifyEvent(updates, expectedEvent);
    }

    protected override OfferPlacedEntrypointSubscriber<BuyOfferExtractionStrategy> CreateSubscriber() =>
        new(new BuyOfferExtractionStrategy());
}