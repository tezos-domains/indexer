﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Test.Subscribers.Events;

public class OfferRemovedEntrypointSubscriberTests : SubscriberTestsBase<OfferRemovedEntrypointSubscriber<SellOfferExtractionStrategy>>
{
    [Test]
    public void OnEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "remove_offer");

        //assert
        var expectedEvent = new OfferRemovedEvent(
            Id: "ignored",
            Block: TestBlockSlim,
            SourceAddress: TestSourceAddress,
            OperationGroupHash: TestOperationGroupHash,
            TokenId: tokenId
        );
        VerifyEvent(updates, expectedEvent);
    }

    protected override OfferRemovedEntrypointSubscriber<SellOfferExtractionStrategy> CreateSubscriber() =>
        new(new SellOfferExtractionStrategy(new MarketplaceConfiguration()));
}