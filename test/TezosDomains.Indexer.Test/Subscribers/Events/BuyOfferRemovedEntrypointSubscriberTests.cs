﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Events.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Test.Subscribers.Events;

public class BuyOfferRemovedEntrypointSubscriberTests : SubscriberTestsBase<OfferRemovedEntrypointSubscriber<BuyOfferExtractionStrategy>>
{
    [Test]
    public void OnEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "remove_offer");

        //assert
        var expectedEvent = new BuyOfferRemovedEvent(
            Id: "ignored",
            Block: TestBlockSlim,
            SourceAddress: TestSourceAddress,
            OperationGroupHash: TestOperationGroupHash,
            TokenId: tokenId
        );
        VerifyEvent(updates, expectedEvent);
    }

    protected override OfferRemovedEntrypointSubscriber<BuyOfferExtractionStrategy> CreateSubscriber() =>
        new(new BuyOfferExtractionStrategy());
}