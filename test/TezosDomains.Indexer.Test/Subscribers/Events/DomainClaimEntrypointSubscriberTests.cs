﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainClaimEntrypointSubscriberTests : SubscriberTestsBase<DomainClaimEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Properties.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "claim");

            //assert
            var expectedEvent = new DomainClaimEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                DomainOwnerAddress: TestAddress1,
                DomainForwardRecordAddress: null
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override DomainClaimEntrypointSubscriber CreateSubscriber() => new();

    }
}