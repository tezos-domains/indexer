﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class ReverseRecordUpdateEntrypointSubscriberTests : SubscriberTestsBase<ReverseRecordUpdateEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Properties.Setup("name", v => v.GetValueAsNullableString(), "used-shoe.tez");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);
            Properties.Setup("address", v => v.GetValueAsAddress(), TestAddress2);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "update_reverse_record");

            //assert
            var expectedEvent = new ReverseRecordUpdateEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                Name: "used-shoe.tez",
                ReverseRecordOwnerAddress: TestAddress1,
                ReverseRecordAddress: TestAddress2
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override ReverseRecordUpdateEntrypointSubscriber CreateSubscriber() => new();

    }
}