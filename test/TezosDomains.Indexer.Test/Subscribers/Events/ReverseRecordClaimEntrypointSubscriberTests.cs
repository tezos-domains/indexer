﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class ReverseRecordClaimEntrypointSubscriberTests : SubscriberTestsBase<ReverseRecordClaimEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Properties.Setup("name", v => v.GetValueAsNullableString(), "used-shoe.tez");
            Properties.Setup("owner", v => v.GetValueAsAddress(), TestAddress1);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "claim_reverse_record");

            //assert
            var expectedEvent = new ReverseRecordClaimEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                Name: "used-shoe.tez",
                ReverseRecordOwnerAddress: TestAddress1
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override ReverseRecordClaimEntrypointSubscriber CreateSubscriber() => new();

    }
}