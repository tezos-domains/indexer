﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class AuctionBidEntrypointSubscriberTests : SubscriberTestsBase<AuctionBidEntrypointSubscriber>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("label", v => v.GetValueAsString(), "used-shoe");
            Properties.Setup("bid", v => v.GetValueAsMutezDecimal(), 123);

            //act
            var updates = Target_OnEntrypoint(entrypointName: "bid");

            //assert
            var expectedEvent = new AuctionBidEvent(
                Id: "ignored",
                TestBlockSlim,
                TestSourceAddress,
                TestOperationGroupHash,
                DomainName: "used-shoe.tez",
                BidAmount: 123,
                TransactionAmount: TestAmount,
                PreviousBidderAddress: null,
                PreviousBidAmount: null
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override AuctionBidEntrypointSubscriber CreateSubscriber() => new();
    }
}