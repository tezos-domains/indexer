﻿using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;
using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public abstract class SubscriberTestsBase<TSubscriber>
        where TSubscriber : class, IContractSubscriber
    {
        protected TSubscriber Target { get; private set; } = null!;
        protected TezosBlock TestTezosBlock { get; private set; } = null!;
        protected BlockSlim TestBlockSlim { get; private set; } = null!;
        protected string TestSourceAddress { get; set; } = null!;
        protected string TestRootSourceAddress { get; set; } = null!;
        protected string TestOperationGroupHash { get; private set; } = null!;
        protected int TestAmount { get; private set; }
        protected IMichelsonValue KeyPropertyMock { get; private set; } = null!;
        protected Dictionary<string, IMichelsonValue> Properties { get; private set; } = null!;
        protected Dictionary<string, IMichelsonValue> Storage { get; private set; } = null!;
        protected string TestAddress1 { get; private set; } = null!;
        protected string TestAddress2 { get; private set; } = null!;
        protected string TestContractAddress1 { get; private set; } = null!;

        protected abstract TSubscriber CreateSubscriber();

        [SetUp]
        public void SetUp()
        {
            Target = CreateSubscriber();

            // Generate test data.
            TestTezosBlock = new TezosBlock
            {
                Hash = TestRandom.GetString(),
                Header = new TezosBlockHeader
                {
                    Level = TestRandom.GetInt(),
                    Timestamp = DateTime.UtcNow
                }
            };
            TestBlockSlim = BlockMapper.MapSlim(TestTezosBlock);
            TestSourceAddress = $"src-addr-{TestRandom.GetString()}";
            TestOperationGroupHash = $"ogh-{TestRandom.GetString()}";
            TestAmount = TestRandom.GetInt();
            KeyPropertyMock = Substitute.For<IMichelsonValue>();
            Properties = new();
            Storage = new();
            TestAddress1 = $"addr1-{TestRandom.GetString()}";
            TestAddress2 = $"addr2-{TestRandom.GetString()}";
            TestContractAddress1 = $"contract-addr1-{TestRandom.GetString()}";
        }

        protected IEnumerable<IBlockUpdate> Target_OnEntrypoint(string entrypointName)
            => Target.OnEntrypoint(
                new OnEntrypointArguments(
                    TestTezosBlock,
                    TestOperationGroupHash,
                    entrypointName,
                    TestSourceAddress,
                    TestAmount,
                    Properties,
                    Storage,
                    TestRootSourceAddress
                )
            );

        [Test]
        public void OnEntrypoint_ShouldDoNothing_IfUnknownEntrypoint()
        {
            //act
            var updates = Target_OnEntrypoint("unknown-entrypoint");

            //assert
            updates.Should().BeEmpty();
        }

        protected IEnumerable<IBlockUpdate> Target_OnBigMapDiff(string layoutName)
            => Target.OnBigMapDiff(
                new OnBigMapDiffArguments(TestTezosBlock, TestOperationGroupHash, layoutName, KeyPropertyMock, Properties, Storage)
            );

        [Test]
        public void OnBigMapDiff_ShouldDoNothing_IfUnknownLayout()
        {
            //act
            var updates = Target_OnBigMapDiff("unknown-layout");

            //assert
            updates.Should().BeEmpty();
        }

        protected IEnumerable<IBlockUpdate> Target_OnOutgoingTransaction(string sourceAddress, string destinationAddress)
            => Target.OnOutgoingMoneyTransaction(
                new OnOutgoingTransactionArguments(TestTezosBlock, TestOperationGroupHash, sourceAddress, destinationAddress, TestAmount, Storage)
            );

        protected void VerifyEvent<TEvent>(IEnumerable<IBlockUpdate> actualUpdates, TEvent expectedEvent)
            where TEvent : Event
        {
            var updates = actualUpdates.ToArray();
            updates.Should().BeEquivalentTo(new[] { new EventUpdate(expectedEvent) }, o => o.Excluding(a => a.Event.Id));
            updates.Cast<EventUpdate>().Select(e => e.Event.GetType().Name).Should().AllBe(expectedEvent.GetType().Name);
        }
    }

    public static class PropertiesBuilderExtensions
    {
        public static void Setup<T>(this Dictionary<string, IMichelsonValue> properties, string key, Func<IMichelsonValue, T> valueFunc, T value)
        {
            var michelson = Substitute.For<IMichelsonValue>();
            valueFunc(michelson).Returns(value);
            properties[key] = michelson;
        }
    }
}