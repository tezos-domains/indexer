﻿using FluentAssertions;
using NUnit.Framework;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Subscribers.Marketplace;
using TezosDomains.Indexer.Updates.Marketplace;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.Test.Subscribers;

public class BuyOfferEntrypointSubscriberTests : SubscriberTestsBase<OfferEntrypointSubscriber<BuyOfferExtractionStrategy>>
{
    [Test]
    public void OnPlaceOfferEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        var amount = TestRandom.GetDecimal();
        var fee = TestRandom.GetDecimal();
        var expiresAtOrNull = TestDateTime.GetFutureOrMax();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "claim_reverse_record");

        //assert
        var expectedUpdate = new SetOfferUpdate(
            Type: OfferType.BuyOffer,
            OperationGroupHash: TestOperationGroupHash,
            TokenContract: TestContractAddress1,
            TokenId: tokenId,
            InitiatorAddress: TestSourceAddress,
            PriceWithoutFee: amount,
            Fee: fee,
            ExpiresAtUtc: expiresAtOrNull
        );

        updates.Should().AllBeEquivalentTo(expectedUpdate);
    }


    [Test]
    public void OnExecuteOfferEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        var amount = TestRandom.GetDecimal();
        var fee = TestRandom.GetDecimal();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);
        Properties.Setup("buyer", v => v.GetValueAsAddress(), TestAddress1);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "claim_reverse_record");

        //assert
        var expectedUpdate = new ExecuteOfferUpdate(
            Type: OfferType.BuyOffer,
            OperationGroupHash: TestOperationGroupHash,
            TokenContract: TestContractAddress1,
            TokenId: tokenId,
            InitiatorAddress: TestAddress1,
            PriceWithoutFee: amount,
            Fee: fee,
            AcceptorAddress: TestSourceAddress
        );

        updates.Should().AllBeEquivalentTo(expectedUpdate);
    }

    [Test]
    public void OnRemoveOfferEntrypoint_ShouldParseDataSuccessfully()
    {
        //arrange
        var tokenId = TestRandom.GetInt();
        Properties.Setup("token_contract", v => v.GetValueAsAddress(), TestContractAddress1);
        Properties.Setup("token_id", v => v.GetValueAsInt(), tokenId);

        //act
        var updates = Target_OnEntrypoint(entrypointName: "claim_reverse_record");

        //assert
        var expectedUpdate = new RemoveOfferUpdate(
            Type: OfferType.BuyOffer,
            OperationGroupHash: TestOperationGroupHash,
            TokenContract: TestContractAddress1,
            TokenId: tokenId,
            InitiatorAddress: TestSourceAddress
        );

        updates.Should().AllBeEquivalentTo(expectedUpdate);
    }

    protected override OfferEntrypointSubscriber<BuyOfferExtractionStrategy> CreateSubscriber() => new(new BuyOfferExtractionStrategy());
}