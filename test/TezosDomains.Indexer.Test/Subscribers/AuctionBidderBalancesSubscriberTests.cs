﻿using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Subscribers;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public class AuctionBidderBalancesSubscriberTests : SubscriberTestsBase<AuctionBidderBalancesSubscriber>
    {
        [Test]
        public void OnBigMapDiff_WithBigMapValue_ShouldCreateUpdate([Values] bool withBalance)
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            KeyPropertyMock.GetValueAsAddress().Returns(TestAddress1);

            if (withBalance)
                Properties.Setup("__value__", v => v.GetValueAsMutezDecimal(), 10);

            //act
            var updates = Target_OnBigMapDiff("bidder_balances");

            //assert
            var expectedUpdate = new BidderBalancesUpdate(
                OperationGroupHash: TestOperationGroupHash,
                Address: TestAddress1,
                TldName: "tez",
                Balance: withBalance ? 10 : null
            );
            updates.Should().AllBeEquivalentTo(expectedUpdate);
        }

        [Test]
        public void WithdrawEntrypoint_ShouldCreateAuctionWithdrawEvent()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");
            Properties.Setup("withdraw", v => v.GetValueAsAddress(), TestAddress1);

            //act
            var updates = Target_OnEntrypoint("withdraw");

            //assert
            var expectedEvent = new AuctionWithdrawEvent(
                Id: "ignored",
                TestBlockSlim,
                SourceAddress: TestSourceAddress,
                DestinationAddress: TestAddress1,
                TestOperationGroupHash,
                TldName: "tez",
                WithdrawnAmount: TestAmount
            );
            VerifyEvent(updates, expectedEvent);
        }

        [Test]
        public void OutgoingTransaction_ShouldCreateOutgoingMoneyTransactionEvent()
        {
            //arrange
            Storage.Setup("tld", v => v.GetValueAsString(), "tez");

            //act
            var updates = Target_OnOutgoingTransaction(sourceAddress: TestAddress1, destinationAddress: TestAddress2);

            //assert
            var expectedEvent = new OutgoingMoneyTransactionEvent(
                Id: "ignored",
                TestBlockSlim,
                SourceAddress: TestAddress1,
                DestinationAddress: TestAddress2,
                TestOperationGroupHash,
                WithdrawnAmount: TestAmount
            );
            VerifyEvent(updates, expectedEvent);
        }

        protected override AuctionBidderBalancesSubscriber CreateSubscriber() => new();

    }
}