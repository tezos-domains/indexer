﻿using FluentAssertions;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Utilities;

namespace TezosDomains.Data.Tests.Utilities
{
    public class DocumentHelperTests
    {
        [Test]
        public void GetKey_Extension_ShouldGetValueCorrectly()
        {
            var balances = new BidderBalances(default!, default!, Address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", default!, default, default);

            balances.GetKey().Should().Be("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n");
        }

        [Test]
        public void GetKey_ShouldBeInitializedCorrectly()
        {
            var record = new ReverseRecord(
                default!,
                default!,
                Address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                default!,
                default!,
                default!,
                default!,
                default,
                default
            );

            DocumentHelper<ReverseRecord>.GetKey(record).Should().Be("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n");
        }

        [Test]
        public void KeyExpression_ShouldBeInitializedCorrectly()
        {
            var balances = new BidderBalances(default!, default!, Address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", default!, default, default);

            DocumentHelper<BidderBalances>.KeyExpression.Compile().Invoke(balances).Should().Be("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n");
        }

        [Test]
        public void KeyPropertyName_ShouldBeInitializedCorrectly()
        {
            DocumentHelper<Domain>.KeyPropertyName.Should().Be(nameof(Domain.Name));
        }
    }
}