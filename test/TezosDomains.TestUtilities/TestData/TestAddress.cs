﻿using System.Collections.Generic;

namespace TezosDomains.TestUtilities.TestData
{
    public static class TestAddress
    {
        private static readonly IReadOnlyList<string> Addresses = new[]
        {
            "tz1Vd1rXpV8hTHbFXCXN3c3qzCsgcU5BZw1e",
            "KT1H7jHcCbt5rft1q4CehVpqi81gDBbp7E9N",
            "tz1VxS7ff4YnZRs8b4mMP4WaMVpoQjuo1rjf",
            "KT1URVbKdq6aWrofHYj7SCBn9U1ygHCgzddX",
            "tz1RpdqrmqYY6JUn5a3RVCdFCi1UeBMaMNwN",
            "KT1SvUM7iyTEa4TLjnonD5BiiyaLxs6j6wsn",
            "KT1CwTQbFm2QHVGggSAHVeR7nJ6h4scH843N",
            "KT1LV1AJetmLXtvuraZTWzJa9TKpaabCW8dQ",
            "KT1UKyfVorer7j8Wk5oafGNPYZPv65EW1KFM",
            "KT1DNriEHLPtkQ4TQVaDjj7GUYbMLWcKpiNq",
            "KT1TUMj5HFmRitAAt7SAMcePLsDNBLCAsre7",
            "tz3dsRx27KTtsVcSJHtXj2vNBosczF9G5iww",
            "tz1TEZtYnuLiZLdA6c7JysAUJcHMrogu4Cpr",
            "KT1F5md8HLRiCyTc5ddCTh9UjNGSKLMZZ36s",
            "tz1cSoWttzYi9taqyHfcKS86b5M31SoaTQdg",
            "tz1e7uhpwmiKp8Yd2KwFwmVhoT31L478KUe3",
            "KT1JZEmkAJQaYrNSKubcYT9DLSMmz6zLcP3C",
            "KT1TTiRLpRiQZ9CmXVF9r2YEmX9MbmDRqzPP",
            "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
            "KT1FpZPYp5Ne198YyZJkLeu6v94CaVoZHThd",
            "tz1NRTQeqcuwybgrZfJavBY3of83u8uLpFBj",
            "tz5E6hfHW2ZreLcTTZx7UsHipAdXzkQbte87",
            "KT1MgLpKtuJpLFC3gvLSksSqNzQXrYoTAPcj",
            "KT1CR6vXJ1qeY4ALDQfUaLFi3FcJJZ8WDygo",
            "KT1VgDrc2VS7JHtqdR3K6F63azQdyzqD5i4R",
            "KT1QC7yGJF3RyuYGtQMfzbn5jyDmADT8kTHM",
            "KT1CsihxV8fDgoFx98mpEgTazKKSp2C5pXmr",
            "tz1RRayQohB8XRXN7kMoHbf2NFwNiH3oMRQQ",
            "tz1PgkWZdr5Vcbb3CbhubheYuEGpxJtKDMhT",
            "KT1R4rsjci49fjryQrpLM4zuqHKzWN5txSft",
            "KT1C4BtpmqGQt1Y917BPKaETbtUqxUZebirF",
            "tz1aWXP237BLwNHJcCD4b3DutCevhqq2T1Z9",
            "tz3WXYtyDUNL91qfiCJtVUX746QpNv5i5ve5",
            "tz1gKdWttzYi9taqyHfcKS86b5M31SoaTOdf",
            "KT1X6NEHEFyp4GESYFZWuQJuA7tv1WSeqcKc",
            "KT1X2kg7XGfok2m6L8bMBsA27YNvN9xpS1DY",
            "KT1ScdYz5vqPqGPTMqufwY1tvdPP7E5HAayU",
            "tz1R1hfHW2ZreLcTTZx7UsHipAdXzkQbte35",
            "KT1EyNFbCTLNEy7gY3XvcDgC7BZE3xs7SayR",
            "tz1XXayQohB8XRXN7kMoHbf2NFwNiH3oMRQQ",
            "KT1BVYUTAqUfXbg9GntHPxJcMpf1fHrdGQCT",
            "tz1T8UYSbVuRm6CdhjvwCfXsKXb4yL9ai9Q3",
        };

        private static int _counter = 0;

        public static string GetRandom()
            => Addresses[_counter++ % Addresses.Count];
    }
}