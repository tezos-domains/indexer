﻿using System;

namespace TezosDomains.TestUtilities.TestData
{
    public static class TestDateTime
    {
        public static DateTime Today => DateTime.UtcNow.Date;

        public static DateTime GetFuture()
            => Today.AddDays(TestRandom.GetInt(exclusiveMax: 100_000));

        public static DateTime GetFutureOrMax()
            => TestRandom.GetBool() ? GetFuture() : DateTime.MaxValue;
    }
}