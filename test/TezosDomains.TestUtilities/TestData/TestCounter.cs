﻿using System.Threading;

namespace TezosDomains.TestUtilities.TestData
{
    public static class TestCounter
    {
        private static volatile int Value;

        public static int GetNext()
            => Interlocked.Add(ref Value, 1);
    }
}