﻿using System;

namespace TezosDomains.TestUtilities.TestData
{
    public static class TestRandom
    {
        private static readonly Random Generator = new();

        public static int GetInt(int exclusiveMax = int.MaxValue)
        {
            lock (Generator)
                return Generator.Next(exclusiveMax);
        }

        public static decimal GetDecimal()
            => GetInt() + GetInt(exclusiveMax: 1_000) / 1_000m;

        public static string GetString()
            => Guid.NewGuid().ToString("N");

        public static bool GetBool()
            => GetInt(exclusiveMax: 2) == 1;
    }
}