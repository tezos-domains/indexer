﻿namespace TezosDomains.TestUtilities.TestData
{
    public static class TestDomainName
    {
        public static string GetRandom()
        {
            var tldLength = 2 + TestRandom.GetInt(exclusiveMax: 3);
            var tld = GetRandomLabel()[..tldLength];
            return $"{GetRandomLabel()}.{tld}";
        }

        public static string GetRandomLabel() => TestRandom.GetString();
    }
}