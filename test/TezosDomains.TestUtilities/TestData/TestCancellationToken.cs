﻿using System.Threading;

namespace TezosDomains.TestUtilities.TestData
{
    public static class TestCancellationToken
    {
        /// <summary>Create a fake using CTS in order to be unique each time.</summary>
        public static CancellationToken Get()
            => new CancellationTokenSource().Token;
    }
}