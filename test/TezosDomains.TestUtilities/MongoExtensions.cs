﻿using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;

namespace TezosDomains.TestUtilities
{
    public static class MongoExtensions
    {
        public static void Insert<TDocument>(this MongoDbContext dbContext, params TDocument[] documents)
            where TDocument : IHasDbCollection
            => dbContext.Get<TDocument>().InsertMany(documents);

        public static TDocument WithValidUntil<TDocument>(this TDocument document, Block block)
            where TDocument : HasHistory<TDocument>
            => document with
            {
                ValidUntilBlockLevel = block.Level,
                ValidUntilTimestamp = block.Timestamp,
            };
    }
}