﻿using Microsoft.Extensions.Logging;
using System;

namespace TezosDomains.TestUtilities
{
    public sealed class TestLogger<T> : ILogger<T>
    {
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            if (logLevel >= LogLevel.Trace)
                Console.WriteLine($"{DateTime.Now}: {logLevel} {formatter(state, exception)} {exception}");
        }

        public bool IsEnabled(LogLevel logLevel) => true;

        public IDisposable BeginScope<TState>(TState state) => new EmptyDisposable();

        private sealed class EmptyDisposable : IDisposable
        {
            public void Dispose()
            {
            }
        }
    }
}