﻿using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.TestUtilities;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.IntegrationTests.Tests
{
    [Timeout(180_000)]
    [TestFixture("Mumbainet", 55154, 55155)]
    public sealed class SmokeTests
    {
        private readonly string _networkName;
        private readonly int _startBlockLevel;
        private readonly int _targetBlockLevel;
        private readonly string _databaseName = $"t_{Guid.NewGuid()}";
        private int SleepWhenRequestingBlockLevel => _targetBlockLevel + 2;

        public SmokeTests(string networkName, int startBlockLevel, int targetBlockLevel)
        {
            _networkName = networkName;
            _startBlockLevel = startBlockLevel;
            _targetBlockLevel = targetBlockLevel;
        }

        private IHost _host = null!;
        private MongoDbContext _dbContext = null!;

        [OneTimeSetUp]
        public async Task OneTimeSetup()
        {
            var args = new[]
            {
                "--Environment=Development",
                $"--TezosDomains:TezosContract:StartFromLevel={_startBlockLevel}",
                $"--TEZOS_NETWORK={_networkName}",
            };
            _host = Program.CreateHostBuilder(Substitute.For<Serilog.ILogger>(), args)
                .ConfigureServices(
                    services =>
                    {
                        services.RemoveAll(typeof(MongoDbConfiguration));
                        services.AddSingleton(
                            sp =>
                            {
                                var configuration = sp.GetRequiredService<IConfiguration>();
                                var mongoDbConfiguration = configuration.GetSection("TezosDomains:MongoDb").Get<MongoDbConfiguration>();
                                return mongoDbConfiguration with { DatabaseName = _databaseName };
                            }
                        );

                        services.RemoveAll(typeof(TezosClient));
                        services.AddHttpClient<TezosClient>()
                            .AddHttpMessageHandler(() => new SleepWhenRequestingBlockTezosClientHandler(SleepWhenRequestingBlockLevel))
                            .AddHttpMessageHandler(() => new CachedHttpClientHandler(_networkName));

                        services.RemoveAll(typeof(ILogger<>));
                        services.AddSingleton(typeof(ILogger<>), typeof(TestLogger<>));
                    }
                )
                .Build();

            _dbContext = _host.Services.GetRequiredService<MongoDbContext>();
            await _host.StartAsync();
        }

        [OneTimeTearDown]
        public async Task OneTimeTearDown()
        {
            await _host.StopAsync();
            await DropDatabaseAsync();
            _host.Dispose();
        }

        private async Task DropDatabaseAsync()
        {
            var config = _host.Services.GetRequiredService<MongoDbConfiguration>();
            var client = _host.Services.GetRequiredService<MongoClient>();
            await client.DropDatabaseAsync(config.DatabaseName);
        }

        [Test, Order(1)]
        public async Task ShouldIndexTestDomainAfterDeployment()
        {
            //act
            await WaitUntilBlockExistsAsync(_targetBlockLevel);
            await _host.StopAsync();

            //assert
            var domain = _dbContext.SingleByKey<Domain>("ok.test");
            var record = _dbContext.SingleByKey<ReverseRecord>("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            var events = _dbContext.ListAll<Event>();

            domain.Name.Should().Be("ok.test");
            domain.Address.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            domain.Owner.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            domain.Data.Should().BeEquivalentTo(new Dictionary<string, string> { { "td:ttl", "343230" } });

            domain.TokenId.Should().BeNull();
            domain.Label.Should().Be("ok");
            domain.Ancestors.Should().BeEquivalentTo(new[] { "test" });
            domain.ParentName.Should().Be("test");
            domain.ExpiresAtUtc.Should().BeAfter(new DateTime(2100, 1, 1));
            domain.ValidityKey.Should().Be("ok.test");
            domain.Level.Should().Be(2);
            domain.NameForOrdering.Should().Be("ok.test");

            record.Address.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            record.Name.Should().Be("ok.test");
            record.Owner.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            record.ExpiresAtUtc.Should().BeAfter(new DateTime(2100, 1, 1));
            record.ValidityKey.Should().Be("ok.test");

            var grantEvent = FindSingle<DomainGrantEvent>(events, e => e.DomainName == "ok.test");
            grantEvent.SourceAddress.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            grantEvent.DurationInDays.Should().BeGreaterThan(28_000);
            grantEvent.DomainOwnerAddress.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            grantEvent.DomainForwardRecordAddress.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            grantEvent.Data.Should().BeEquivalentTo(new Dictionary<string, string> { { "td:ttl", "343230" } });

            var domainEvent = FindSingle<DomainSetChildRecordEvent>(events, e => e.DomainName == "ok.test");
            domainEvent.SourceAddress.Should().Be("tz1QGcAWeCTih1t7YGiUKmPdVMvRQpXv9d8A");
            domainEvent.DomainOwnerAddress.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            domainEvent.DomainForwardRecordAddress.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            domainEvent.Data.Should().BeEquivalentTo(new Dictionary<string, string> { { "td:ttl", "343230" } });
            domainEvent.IsNewRecord.Should().Be(true);

            var recordEvent = FindSingle<ReverseRecordClaimEvent>(events, e => e.Name == "ok.test");
            recordEvent.SourceAddress.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            recordEvent.ReverseRecordOwnerAddress.Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
        }

        [Test, Order(2)]
        public async Task ShouldRollbackBlocksOnReorganization()
        {
            //arrange
            // Delete the latest so that we can fetch it and detect reorg.
            await _dbContext.Get<Block>().DeleteManyAsync(Builders<Block>.Filter.Gte(b => b.Level, SleepWhenRequestingBlockLevel - 1));

            // Prepare block that we'll verify to be restored.
            var expectedBlock = await FindLatestBlockAsync() ?? throw new Exception("Previous test failed - didn't index anything.");

            // Cripple all indexed blocks.
            var deleted = await _dbContext.Get<Block>().DeleteManyAsync(FilterDefinition<Block>.Empty);
            var newBlocks = Enumerable.Range(0, (int)deleted.DeletedCount.GuardNotNull())
                .Select(offset => new Block(expectedBlock.Level - offset, $"bullshit-hash-{offset}", DateTime.UtcNow, "predecessor"));
            await _dbContext.Get<Block>().InsertManyAsync(newBlocks);

            // Verify setup.
            var actualBlock = await FindBlockAsync(expectedBlock.Level);
            (actualBlock?.Hash).Should().NotBe(expectedBlock.Hash);
            (actualBlock?.Predecessor).Should().NotBe(expectedBlock.Predecessor);

            //act
            _host.Dispose();
            await OneTimeSetup();

            //assert
            //All block should be deleted and re-indexed again.
            await WaitUntil(
                async () =>
                {
                    actualBlock = await FindBlockAsync(expectedBlock.Level);
                    Console.WriteLine($"Waiting for block {expectedBlock} to be restored and there is {actualBlock}.");
                    return actualBlock?.Hash == expectedBlock.Hash
                           && actualBlock?.Predecessor == expectedBlock.Predecessor;
                }
            );
        }

        private static TEvent FindSingle<TEvent>(IEnumerable<Event> events, Expression<Func<TEvent, bool>> predicate)
            => events.OfType<TEvent>().Should().ContainSingle(predicate).Subject;

        private Task WaitUntilBlockExistsAsync(int blockLevelToWait)
        {
            Console.WriteLine($"Waiting for block level {blockLevelToWait} to exist.");
            return WaitUntil(
                async () =>
                {
                    var latestBlock = await FindLatestBlockAsync();
                    var exists = latestBlock?.Level >= blockLevelToWait;

                    Console.WriteLine($"Waiting for block level {blockLevelToWait} to exist. The latest block is {latestBlock?.Level}.");
                    return exists;
                }
            );
        }

        private static async Task WaitUntil(Func<Task<bool>> predicate)
        {
            while (!await predicate())
                await Task.Delay(TimeSpan.FromSeconds(5));
        }

        private async Task<Block?> FindBlockAsync(int level)
            => await _dbContext.Get<Block>()
                .Find(Builders<Block>.Filter.Eq(b => b.Level, level))
                .SingleOrDefaultAsync();

        private async Task<Block?> FindLatestBlockAsync()
            => await _dbContext.Get<Block>()
                .AsQueryable()
                .OrderByDescending(b => b.Level)
                .FirstOrDefaultAsync();
    }
}