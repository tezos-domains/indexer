﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class AuctionTests : UpdateTestBase
    {
        private string _domainName = null!;

        [SetUp]
        public void Setup()
            => _domainName = TestDomainName.GetRandom();

        [Test]
        public async Task ShouldCreateAuction_WhenAddingFirstBid()
        {
            //arrange
            var block = TestBlock.Get();
            var createUpdate = TestAuctionUpdate.GetBid(_domainName);

            //act
            await StoreUpdatesAsync(new[] { createUpdate }, block);

            //assert
            var expectedBid = new Bid(
                Block: block,
                OperationGroupHash: createUpdate.OperationGroupHash,
                Bidder: createUpdate.Bidder,
                Amount: createUpdate.Amount
            );
            var expectedAuction = new Auction(
                Block: block,
                OperationGroupHash: createUpdate.OperationGroupHash,
                AuctionId: $"{_domainName}:{block.Level}",
                DomainName: _domainName,
                Bids: new[] { expectedBid },
                EndsAtUtc: createUpdate.EndsAtUtc,
                OwnedUntilUtc: createUpdate.OwnedUntilUtc,
                IsSettled: false,
                FirstBidAtUtc: block.Timestamp,
                BidCount: 1,
                HighestBid: expectedBid,
                CountOfUniqueBidders: 1,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyAuctionsInDb(new[] { expectedAuction });
        }

        [Test]
        public async Task ShouldModifyAuction_WhenAddingAnotherBid()
        {
            //arrange
            var block1 = TestBlock.Get();
            var block2 = TestBlock.Get();
            var firstBid = TestAuctionUpdate.GetBid(_domainName);
            await StoreUpdatesAsync(new[] { firstBid }, block2);
            var initialAuction = DbContext.SingleBy<Auction>(a => a.DomainName == _domainName);
            var historyAuction = initialAuction with // Ensures that processor filters for the latest block.
            {
                Block = block1,
                ValidUntilBlockLevel = block2.Level,
                ValidUntilTimestamp = block2.Timestamp,
            };
            DbContext.Insert(historyAuction);

            //act
            var updateBlock = TestBlock.Get();
            var lastBid = TestAuctionUpdate.GetBid(_domainName);
            var modifyUpdate = new[] { TestAuctionUpdate.GetBid(_domainName, bidder: firstBid.Bidder), lastBid };
            await StoreUpdatesAsync(modifyUpdate, updateBlock);

            //assert
            var expectedBids = modifyUpdate.Select(
                    u => new Bid(
                        Block: updateBlock,
                        OperationGroupHash: u.OperationGroupHash,
                        Bidder: u.Bidder,
                        Amount: u.Amount
                    )
                )
                .ToArray();

            var expectedAuction = new Auction(
                Block: updateBlock,
                OperationGroupHash: lastBid.OperationGroupHash,
                AuctionId: initialAuction.AuctionId,
                DomainName: _domainName,
                Bids: expectedBids.Prepend(initialAuction.Bids.Single()).ToArray(),
                EndsAtUtc: lastBid.EndsAtUtc,
                OwnedUntilUtc: lastBid.OwnedUntilUtc,
                IsSettled: false,
                FirstBidAtUtc: initialAuction.FirstBidAtUtc,
                BidCount: 3,
                HighestBid: expectedBids.Last(),
                CountOfUniqueBidders: 2,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyAuctionsInDb(new[] { expectedAuction, initialAuction.WithValidUntil(updateBlock), historyAuction });
        }

        private void VerifyAuctionsInDb(IEnumerable<Auction> expectedAuctions)
        {
            var actualAuctions = DbContext.ListAll<Auction>(a => a.DomainName == _domainName);
            actualAuctions.Should().BeEquivalentTo(expectedAuctions);
        }
    }
}