﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class BidderBalancesTests : UpdateTestBase
    {
        private string _address = null!;

        [SetUp]
        public void Setup()
            => _address = TestAddress.GetRandom();

        [Test]
        public async Task ShouldCreateBidder_IfFirstBalance()
        {
            //act
            var block = TestBlock.Get();
            var update = TestBidderBalancesUpdate.Get(address: _address);
            await StoreUpdatesAsync(new[] { update }, block);

            //assert
            var expectedBidder = new BidderBalances(
                Block: block,
                OperationGroupHash: update.OperationGroupHash,
                Address: _address,
                Balances: new Dictionary<string, decimal>
                {
                    { update.TldName, update.Balance!.Value }
                },
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyBiddersInDb(new[] { expectedBidder });
        }

        [Test]
        public async Task ShouldAddBalanceOnTld_IfBalanceOnNewTld()
        {
            var createUpdate = TestBidderBalancesUpdate.Get(address: _address);
            var modifyUpdate = TestBidderBalancesUpdate.Get(address: _address);

            await RunModifyTestAsync(
                createUpdates: new[] { createUpdate },
                modifyUpdate: modifyUpdate,
                expectedBalances: new Dictionary<string, decimal>
                {
                    { createUpdate.TldName, createUpdate.Balance!.Value },
                    { modifyUpdate.TldName, modifyUpdate.Balance!.Value },
                }
            );
        }

        [Test]
        public async Task ShouldSetBalanceOnTld_IfBalanceOnExistingTld()
        {
            var createUpdate = TestBidderBalancesUpdate.Get(address: _address);
            var modifyUpdate = TestBidderBalancesUpdate.Get(address: _address, tldName: createUpdate.TldName);

            await RunModifyTestAsync(
                createUpdates: new[] { createUpdate },
                modifyUpdate: modifyUpdate,
                expectedBalances: new Dictionary<string, decimal>
                {
                    { createUpdate.TldName, modifyUpdate.Balance!.Value },
                }
            );
        }

        [Test]
        public async Task ShouldRemoveBalanceOnTld_IfBalanceIsNull()
        {
            var createUpdate = TestBidderBalancesUpdate.Get(address: _address);

            await RunModifyTestAsync(
                createUpdates: new[] { createUpdate },
                modifyUpdate: TestBidderBalancesUpdate.Get(address: _address, tldName: createUpdate.TldName, balance: null),
                expectedBalances: new Dictionary<string, decimal>()
            );
        }

        private async Task RunModifyTestAsync(
            IReadOnlyList<BidderBalancesUpdate> createUpdates,
            BidderBalancesUpdate modifyUpdate,
            Dictionary<string, decimal> expectedBalances
        )
        {
            //arrange
            await StoreUpdatesAsync(createUpdates);
            var initialBidder = DbContext.SingleByKey<BidderBalances>(_address);

            //act
            var modifyBlock = TestBlock.Get();
            await StoreUpdatesAsync(new[] { modifyUpdate }, modifyBlock);

            //assert
            var expectedBidder = new BidderBalances(
                Block: modifyBlock,
                OperationGroupHash: modifyUpdate.OperationGroupHash,
                Address: _address,
                Balances: expectedBalances,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyBiddersInDb(new[] { expectedBidder, initialBidder.WithValidUntil(modifyBlock) });
        }

        private void VerifyBiddersInDb(IEnumerable<BidderBalances> expectedBidders)
        {
            var actualBidders = DbContext.ListAllByKey<BidderBalances>(_address);
            actualBidders.Should().BeEquivalentTo(expectedBidders);
        }
    }
}