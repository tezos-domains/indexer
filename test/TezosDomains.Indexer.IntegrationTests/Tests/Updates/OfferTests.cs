﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates.Marketplace;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class OfferTests : UpdateTestBase
    {
        private int _tokenId;

        [SetUp]
        public void Setup()
        {
            _tokenId = TestRandom.GetInt();
        }

        [TestCase(true, null, null, true, false, OfferValidity.Valid, OfferState.Active)]
        [TestCase(true, 8, 1, true, false, OfferValidity.Valid, OfferState.Active)]
        [TestCase(true, 3, null, true, false, OfferValidity.Valid, OfferState.DomainIsExpiringSoon)]
        [TestCase(true, -1, null, true, false, OfferValidity.Valid, OfferState.DomainExpired)]
        [TestCase(true, null, -1, true, false, OfferValidity.Valid, OfferState.OfferExpired)]
        [TestCase(true, null, null, false, false, OfferValidity.OfferSellerDomainOwnerMismatch, OfferState.OfferSellerDomainOwnerMismatch)]
        [TestCase(true, null, null, true, true, OfferValidity.DomainOperatorsContractMissing, OfferState.DomainOperatorsContractMissing)]
        [TestCase(false, null, null, false, false, OfferValidity.DomainDoesNotExist, OfferState.DomainDoesNotExist)]
        public async Task ShouldCreateOffer(
            bool insertDomain,
            int? domainExpirationOffset,
            int? offerExpirationOffset,
            bool isOwnerAndSellerSame,
            bool domainOperatorContractMissing,
            OfferValidity expectedValidity,
            OfferState expectedState
        )
        {
            //arrange
            var utcToday = DateTime.UtcNow.Date;
            Domain? domain = null;
            if (insertDomain)
            {
                var expiresAtUtc = domainExpirationOffset is null ? (DateTime?)null : utcToday.AddDays(domainExpirationOffset.Value);
                var operators = domainOperatorContractMissing ? null : new[] { TestOfferUpdate.MARKETPLACE_CONTRACT };
                domain = TestDomain.Get(expiresAtUtc: expiresAtUtc, tokenId: _tokenId, operators: operators);
                DbContext.Insert(domain);
            }

            var block = TestBlock.Get();
            var update = TestOfferUpdate.GetSetOffer(
                OfferType.SellOffer,
                tokenId: _tokenId,
                initiatorAddress: isOwnerAndSellerSame ? domain.GuardNotNull().Owner : TestAddress.GetRandom(),
                expiresAtUtc: offerExpirationOffset is not null ? utcToday.AddDays(offerExpirationOffset.Value) : null
            );

            //act
            await StoreUpdatesAsync(new[] { update }, block);

            //assert
            var domainData = domain is null ? null : DomainData.Create(domain);
            var expectedOffer = new Offer(
                Block: block,
                OperationGroupHash: update.OperationGroupHash,
                OfferId: Offer.GenerateId(update.Type, update.InitiatorAddress, _tokenId, block.Level),
                InitiatorAddress: update.InitiatorAddress,
                TokenContract: update.TokenContract,
                TokenId: _tokenId,
                Type: OfferType.SellOffer,
                CreatedAtUtc: block.Timestamp,
                ExpiresAtUtc: update.ExpiresAtUtc,
                PriceWithoutFee: update.PriceWithoutFee,
                Fee: update.Fee,
                Presence: OfferBlockchainPresence.Present,
                Domain: domainData,
                Validity: expectedValidity,
                AcceptorAddress: null,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            var actual = VerifyAuctionsInDb(expectedOffer);
            actual.GetState(DateTime.UtcNow.Date, domainIsExpiringThreshold: TimeSpan.FromDays(7)).Should().Be(expectedState);
        }

        [Test]
        public async Task ShouldCreateMultipleOffersForSameTokenId()
        {
            //arrange
            var domain = TestDomain.Get(tokenId: _tokenId, operators: new[] { TestOfferUpdate.MARKETPLACE_CONTRACT });
            DbContext.Insert(domain);

            var block = TestBlock.Get();
            var validOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId, initiatorAddress: domain.Owner);
            var invalidOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId);

            //act
            await StoreUpdatesAsync(new[] { validOfferUpdate, invalidOfferUpdate }, block);

            //assert
            var expectedValidOffer = new Offer(
                Block: block,
                OperationGroupHash: validOfferUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(validOfferUpdate.Type, validOfferUpdate.InitiatorAddress, _tokenId, block.Level),
                InitiatorAddress: validOfferUpdate.InitiatorAddress,
                TokenContract: validOfferUpdate.TokenContract,
                TokenId: _tokenId,
                Type: OfferType.SellOffer,
                CreatedAtUtc: block.Timestamp,
                ExpiresAtUtc: validOfferUpdate.ExpiresAtUtc,
                PriceWithoutFee: validOfferUpdate.PriceWithoutFee,
                Fee: validOfferUpdate.Fee,
                Presence: OfferBlockchainPresence.Present,
                Domain: DomainData.Create(domain),
                Validity: OfferValidity.Valid,
                AcceptorAddress: null,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            var validActual = VerifyAuctionsInDb(expectedValidOffer);
            validActual.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.Active);

            var expectedInvalidOffer = expectedValidOffer with
            {
                OfferId = Offer.GenerateId(OfferType.SellOffer, invalidOfferUpdate.InitiatorAddress, _tokenId, block.Level),
                OperationGroupHash = invalidOfferUpdate.OperationGroupHash,
                InitiatorAddress = invalidOfferUpdate.InitiatorAddress,
                PriceWithoutFee = invalidOfferUpdate.PriceWithoutFee,
                Fee = invalidOfferUpdate.Fee,
                Validity = OfferValidity.OfferSellerDomainOwnerMismatch
            };
            var invalidActual = VerifyAuctionsInDb(expectedInvalidOffer);
            invalidActual.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.OfferSellerDomainOwnerMismatch);
        }

        [TestCase(true, false)]
        [TestCase(false, true)]
        public async Task ShouldExecuteOffer(bool insertOffer, bool shouldThrowException)
        {
            //arrange
            Domain domain = TestDomain.Get(tokenId: _tokenId, operators: new[] { TestOfferUpdate.MARKETPLACE_CONTRACT });
            DbContext.Insert(domain);

            var prepareBlock = TestBlock.Get();
            SetOfferUpdate? prepareUpdate = null;
            if (insertOffer)
            {
                prepareUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, _tokenId, initiatorAddress: domain.Owner);
                await StoreUpdatesAsync(new[] { prepareUpdate }, prepareBlock);
            }

            var block = TestBlock.Get();
            var executeUpdate = TestOfferUpdate.GetExecuteOffer(OfferType.SellOffer, _tokenId, sellerAddress: prepareUpdate?.InitiatorAddress);

            //act
            if (shouldThrowException)
            {
                Assert.ThrowsAsync<ArgumentNullException>(async () => await StoreUpdatesAsync(new[] { executeUpdate }, block));
            }
            else
            {
                //transfer domainUpdate should not update the executedOffer data
                var domainTransferredUpdate = TestDomainUpdate.Get(tokenId: _tokenId, name: domain.Name, owner: TestAddress.GetRandom());
                await StoreUpdatesAsync(new IBlockUpdate[] { executeUpdate, domainTransferredUpdate }, block);

                //assert
                var domainData = DomainData.Create(domain) with
                {
                    Data = domainTransferredUpdate.Data, Operators = domainTransferredUpdate.Operators, Owner = domainTransferredUpdate.Owner
                };
                var expectedOffer = new Offer(
                    Block: block,
                    OperationGroupHash: domainTransferredUpdate.OperationGroupHash,
                    OfferId: Offer.GenerateId(executeUpdate.Type, executeUpdate.InitiatorAddress, _tokenId, prepareBlock.Level),
                    InitiatorAddress: executeUpdate.InitiatorAddress,
                    TokenContract: executeUpdate.TokenContract,
                    TokenId: _tokenId,
                    Type: OfferType.SellOffer,
                    CreatedAtUtc: prepareBlock.Timestamp,
                    ExpiresAtUtc: prepareUpdate.GuardNotNull().ExpiresAtUtc,
                    PriceWithoutFee: prepareUpdate.GuardNotNull().PriceWithoutFee,
                    Fee: prepareUpdate.GuardNotNull().Fee,
                    Presence: OfferBlockchainPresence.Executed,
                    Domain: domainData,
                    AcceptorAddress: executeUpdate.AcceptorAddress,
                    Validity: OfferValidity.OfferSellerDomainOwnerMismatch,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );
                var actual = VerifyAuctionsInDb(expectedOffer);
                actual.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.Executed);
            }
        }

        [TestCase(true, false)]
        [TestCase(false, true)]
        public async Task ShouldRemoveOffer(bool insertOffer, bool shouldThrowException)
        {
            //arrange
            var prepareBlock = TestBlock.Get();
            SetOfferUpdate? prepareUpdate = null;
            if (insertOffer)
            {
                prepareUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, _tokenId);
                await StoreUpdatesAsync(new[] { prepareUpdate }, prepareBlock);
            }

            var block = TestBlock.Get();
            var removeUpdate = TestOfferUpdate.GetRemoveOffer(OfferType.SellOffer, _tokenId, initiatorAddress: prepareUpdate?.InitiatorAddress);

            //act
            if (shouldThrowException)
            {
                Assert.ThrowsAsync<ArgumentNullException>(async () => await StoreUpdatesAsync(new[] { removeUpdate }, block));
            }
            else
            {
                await StoreUpdatesAsync(new[] { removeUpdate }, block);

                //assert
                var expectedOffer = new Offer(
                    Block: block,
                    OperationGroupHash: removeUpdate.OperationGroupHash,
                    OfferId: Offer.GenerateId(removeUpdate.Type, removeUpdate.InitiatorAddress, _tokenId, prepareBlock.Level),
                    InitiatorAddress: removeUpdate.InitiatorAddress,
                    TokenContract: removeUpdate.TokenContract,
                    TokenId: _tokenId,
                    Type: OfferType.SellOffer,
                    CreatedAtUtc: prepareBlock.Timestamp,
                    ExpiresAtUtc: prepareUpdate.GuardNotNull().ExpiresAtUtc,
                    PriceWithoutFee: prepareUpdate.GuardNotNull().PriceWithoutFee,
                    Fee: prepareUpdate.GuardNotNull().Fee,
                    Presence: OfferBlockchainPresence.Removed,
                    Domain: null,
                    AcceptorAddress: null,
                    Validity: OfferValidity.DomainDoesNotExist,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );
                var actual = VerifyAuctionsInDb(expectedOffer);
                actual.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.Removed);
            }
        }

        [Test]
        public async Task DomainUpdate_ShouldUpdateOfferDomainData()
        {
            //arrange
            var domain1 = TestDomain.Get(tokenId: _tokenId, owner: TestAddress.GetRandom(), operators: new[] { TestOfferUpdate.MARKETPLACE_CONTRACT });
            DbContext.Insert(domain1);

            var prepareBlock = TestBlock.Get();
            var setOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId, initiatorAddress: domain1.Owner);
            await StoreUpdatesAsync(new IBlockUpdate[] { setOfferUpdate }, prepareBlock);


            //act
            var newDomainOwner = TestAddress.GetRandom();
            var actBlock = TestBlock.Get();
            var executeOfferUpdate = TestOfferUpdate.GetExecuteOffer(OfferType.SellOffer, _tokenId, sellerAddress: domain1.Owner, buyerAddress: newDomainOwner);
            var domainUpdate = TestDomainUpdate.Get(domain1.Name, tokenId: _tokenId, owner: newDomainOwner);
            await StoreUpdatesAsync(
                new IBlockUpdate[]
                {
                    executeOfferUpdate,
                    domainUpdate
                },
                actBlock
            );


            //assert
            var expectedValidOffer = new Offer(
                Block: actBlock,
                OperationGroupHash: domainUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(OfferType.SellOffer, setOfferUpdate.InitiatorAddress, setOfferUpdate.TokenId, prepareBlock.Level),
                InitiatorAddress: setOfferUpdate.InitiatorAddress,
                TokenContract: setOfferUpdate.TokenContract,
                TokenId: setOfferUpdate.TokenId,
                Type: OfferType.SellOffer,
                CreatedAtUtc: prepareBlock.Timestamp,
                ExpiresAtUtc: setOfferUpdate.ExpiresAtUtc,
                PriceWithoutFee: setOfferUpdate.PriceWithoutFee,
                Fee: setOfferUpdate.Fee,
                Presence: OfferBlockchainPresence.Executed,
                Domain: DomainData.Create(domain1) with { Owner = newDomainOwner, Data = domainUpdate.Data, Operators = domainUpdate.Operators },
                AcceptorAddress: executeOfferUpdate.AcceptorAddress,
                Validity: OfferValidity.OfferSellerDomainOwnerMismatch,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyAuctionsInDb(expectedValidOffer);
        }

        [Test]
        public async Task ValidityUpdate_ShouldUpdateOfferDomainData([Values] bool onlyValidityUpdate)
        {
            //arrange
            var domainName = "validity.test";
            var domain1 = TestDomain.Get(
                name: domainName,
                tokenId: _tokenId,
                owner: TestAddress.GetRandom(),
                operators: new[] { TestOfferUpdate.MARKETPLACE_CONTRACT },
                validityKey: domainName
            );
            DbContext.Insert(domain1);

            var prepareBlock = TestBlock.Get();
            var offerDomain1 = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId, initiatorAddress: domain1.Owner);
            await StoreUpdatesAsync(new IBlockUpdate[] { offerDomain1 }, prepareBlock);


            //act
            var newDomainOwner = TestAddress.GetRandom();
            var actBlock = TestBlock.Get();
            var executeOfferUpdate = TestOfferUpdate.GetExecuteOffer(OfferType.SellOffer, _tokenId, sellerAddress: domain1.Owner, buyerAddress: newDomainOwner);
            var validityUpdate = TestValidityUpdate.Get(domainName);
            await StoreUpdatesAsync(
                onlyValidityUpdate ? new IBlockUpdate[] { validityUpdate } : new IBlockUpdate[] { executeOfferUpdate, validityUpdate },
                actBlock
            );


            //assert
            var expectedOffer = new Offer(
                Block: actBlock,
                OperationGroupHash: validityUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(OfferType.SellOffer, offerDomain1.InitiatorAddress, offerDomain1.TokenId, prepareBlock.Level),
                InitiatorAddress: offerDomain1.InitiatorAddress,
                TokenContract: offerDomain1.TokenContract,
                TokenId: offerDomain1.TokenId,
                Type: OfferType.SellOffer,
                CreatedAtUtc: prepareBlock.Timestamp,
                ExpiresAtUtc: offerDomain1.ExpiresAtUtc,
                PriceWithoutFee: offerDomain1.PriceWithoutFee,
                Fee: offerDomain1.Fee,
                Presence: onlyValidityUpdate ? OfferBlockchainPresence.Present : OfferBlockchainPresence.Executed,
                Domain: DomainData.Create(domain1),
                AcceptorAddress: onlyValidityUpdate ? null : executeOfferUpdate.AcceptorAddress,
                Validity: OfferValidity.Valid,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyAuctionsInDb(expectedOffer);
        }


        private Offer VerifyAuctionsInDb(Offer expectedAuction)
        {
            var actualOffer = DbContext.SingleBy<Offer>(
                a => a.TokenId == _tokenId && a.InitiatorAddress == expectedAuction.InitiatorAddress && a.ValidUntilBlockLevel == int.MaxValue
            );
            actualOffer.Should().BeEquivalentTo(expectedAuction);
            return actualOffer;
        }
    }
}