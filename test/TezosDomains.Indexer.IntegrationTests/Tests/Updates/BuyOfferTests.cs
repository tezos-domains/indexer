﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates.Marketplace;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class BuyOfferTests : UpdateTestBase
    {
        private int _tokenId;

        [SetUp]
        public void Setup()
        {
            _tokenId = TestRandom.GetInt();
        }

        [TestCase(true, null, null, OfferValidity.Valid, OfferState.Active)]
        [TestCase(true, 8, 1, OfferValidity.Valid, OfferState.Active)]
        [TestCase(true, 3, null, OfferValidity.Valid, OfferState.DomainIsExpiringSoon)]
        [TestCase(true, -1, null, OfferValidity.Valid, OfferState.DomainExpired)]
        [TestCase(true, null, -1, OfferValidity.Valid, OfferState.OfferExpired)]
        [TestCase(false, null, null, OfferValidity.DomainDoesNotExist, OfferState.DomainDoesNotExist)]
        public async Task ShouldCreateBuyOffer(
            bool insertDomain,
            int? domainExpirationOffset,
            int? offerExpirationOffset,
            OfferValidity expectedValidity,
            OfferState expectedState
        )
        {
            //arrange
            var utcToday = DateTime.UtcNow.Date;
            Domain? domain = null;
            if (insertDomain)
            {
                var expiresAtUtc = domainExpirationOffset is null ? (DateTime?)null : utcToday.AddDays(domainExpirationOffset.Value);
                domain = TestDomain.Get(expiresAtUtc: expiresAtUtc, tokenId: _tokenId);
                DbContext.Insert(domain);
            }

            var block = TestBlock.Get();
            var update = TestOfferUpdate.GetSetOffer(
                OfferType.BuyOffer,
                tokenId: _tokenId,
                expiresAtUtc: offerExpirationOffset is not null ? utcToday.AddDays(offerExpirationOffset.Value) : null
            );

            //act
            await StoreUpdatesAsync(new[] { update }, block);

            //assert
            var domainData = domain is null ? null : DomainData.Create(domain);
            var expectedOffer = new Offer(
                Block: block,
                OperationGroupHash: update.OperationGroupHash,
                OfferId: Offer.GenerateId(update.Type, update.InitiatorAddress, _tokenId, block.Level),
                InitiatorAddress: update.InitiatorAddress,
                AcceptorAddress: domain?.Owner,
                TokenContract: update.TokenContract,
                TokenId: _tokenId,
                Type: OfferType.BuyOffer,
                CreatedAtUtc: block.Timestamp,
                ExpiresAtUtc: update.ExpiresAtUtc,
                PriceWithoutFee: update.PriceWithoutFee,
                Fee: update.Fee,
                Presence: OfferBlockchainPresence.Present,
                Domain: domainData,
                Validity: expectedValidity,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            var actual = VerifyOfferInDb(expectedOffer);
            actual.GetState(DateTime.UtcNow.Date, domainIsExpiringThreshold: TimeSpan.FromDays(7)).Should().Be(expectedState);
        }

        [Test]
        public async Task ShouldCreateMultipleSellAndBuyOffersForSameTokenId()
        {
            //arrange
            var domain = TestDomain.Get(tokenId: _tokenId, operators: new[] { TestOfferUpdate.MARKETPLACE_CONTRACT });
            DbContext.Insert(domain);

            var block = TestBlock.Get();
            var validSellOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId, initiatorAddress: domain.Owner);
            var invalidSellOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId);
            var validBuyOfferUpdate1 = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, tokenId: _tokenId);
            var validBuyOfferUpdate2 = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, tokenId: _tokenId);

            //act
            await StoreUpdatesAsync(new[] { validSellOfferUpdate, invalidSellOfferUpdate, validBuyOfferUpdate1, validBuyOfferUpdate2 }, block);

            //assert
            var expectedValidSellOffer = new Offer(
                Block: block,
                OperationGroupHash: validSellOfferUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(validSellOfferUpdate.Type, validSellOfferUpdate.InitiatorAddress, _tokenId, block.Level),
                InitiatorAddress: validSellOfferUpdate.InitiatorAddress,
                TokenContract: validSellOfferUpdate.TokenContract,
                TokenId: _tokenId,
                Type: OfferType.SellOffer,
                CreatedAtUtc: block.Timestamp,
                ExpiresAtUtc: validSellOfferUpdate.ExpiresAtUtc,
                PriceWithoutFee: validSellOfferUpdate.PriceWithoutFee,
                Fee: validSellOfferUpdate.Fee,
                Presence: OfferBlockchainPresence.Present,
                Domain: DomainData.Create(domain),
                Validity: OfferValidity.Valid,
                AcceptorAddress: null,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyOfferInDb(expectedValidSellOffer);
            expectedValidSellOffer.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default)
                .Should()
                .Be(OfferState.Active);

            Verify(invalidSellOfferUpdate, OfferState.OfferSellerDomainOwnerMismatch, OfferValidity.OfferSellerDomainOwnerMismatch);
            Verify(validBuyOfferUpdate1, OfferState.Active, OfferValidity.Valid);
            Verify(validBuyOfferUpdate2, OfferState.Active, OfferValidity.Valid);

            void Verify(SetOfferUpdate update, OfferState state, OfferValidity validity)
            {
                var offer = expectedValidSellOffer with
                {
                    OfferId = Offer.GenerateId(update.Type, update.InitiatorAddress, _tokenId, block.Level),
                    OperationGroupHash = update.OperationGroupHash,
                    InitiatorAddress = update.InitiatorAddress,
                    AcceptorAddress = update.Type == OfferType.BuyOffer ? domain.Owner : null,
                    PriceWithoutFee = update.PriceWithoutFee,
                    Fee = update.Fee,
                    Validity = validity,
                    Type = update.Type,
                };
                VerifyOfferInDb(offer);
                offer.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default)
                    .Should()
                    .Be(state);
            }
        }

        /// <summary>
        /// Tests multiple (validSell,invalidSell,buy1,buy2) offers for same domain (tokenId) when buyOffer is executed
        /// validSell => invalid (OfferSellerDomainOwnerMismatch) (no longer domain belongs to seller)
        /// invalidSell => stays invalid (OfferSellerDomainOwnerMismatch) (domain.Owner is updated)
        /// buy1 => executed (OfferSellerDomainOwnerMismatch because domain.Owner updated)
        /// buy2 => valid (domain.Owner updated)
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task ShouldExecuteOneOfferAndInvalidateOthersAndUpdateDomainData()
        {
            //arrange
            var buyerAddress = TestAddress.GetRandom();
            var sellerAddress = TestAddress.GetRandom();
            var domain = TestDomain.Get(tokenId: _tokenId, owner: sellerAddress);
            DbContext.Insert(domain);

            var prepareBlock = TestBlock.Get();
            var validSellOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId, initiatorAddress: sellerAddress);
            var invalidSellOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId: _tokenId);
            var validBuyOfferUpdate1 = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, tokenId: _tokenId);
            var validBuyOfferUpdate2 = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, tokenId: _tokenId, initiatorAddress: buyerAddress);
            await StoreUpdatesAsync(new[] { validSellOfferUpdate, invalidSellOfferUpdate, validBuyOfferUpdate1, validBuyOfferUpdate2 }, prepareBlock);

            //act
            var actBlock = TestBlock.Get();
            var executeOfferUpdate = TestOfferUpdate.GetExecuteOffer(OfferType.BuyOffer, _tokenId, sellerAddress: sellerAddress, buyerAddress: buyerAddress);
            var domainUpdate = TestDomainUpdate.Get(domain.Name, tokenId: _tokenId, owner: buyerAddress);
            await StoreUpdatesAsync(new IBlockUpdate[] { executeOfferUpdate, domainUpdate }, actBlock);


            //assert
            var expectedExecutedBuyOffer = new Offer(
                Block: actBlock,
                OperationGroupHash: domainUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(OfferType.BuyOffer, buyerAddress, _tokenId, prepareBlock.Level),
                InitiatorAddress: buyerAddress,
                AcceptorAddress: sellerAddress,
                TokenContract: validBuyOfferUpdate2.TokenContract,
                TokenId: _tokenId,
                Type: OfferType.BuyOffer,
                CreatedAtUtc: prepareBlock.Timestamp,
                ExpiresAtUtc: validBuyOfferUpdate2.ExpiresAtUtc,
                PriceWithoutFee: validBuyOfferUpdate2.PriceWithoutFee,
                Fee: validBuyOfferUpdate2.Fee,
                Presence: OfferBlockchainPresence.Executed,
                Domain: DomainData.Create(domain) with { Owner = buyerAddress, Data = domainUpdate.Data },
                Validity: OfferValidity.OfferSellerDomainOwnerMismatch,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyOfferInDb(expectedExecutedBuyOffer);
            expectedExecutedBuyOffer.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default)
                .Should()
                .Be(OfferState.Executed);

            Verify(
                invalidSellOfferUpdate,
                OfferBlockchainPresence.Present,
                OfferState.OfferSellerDomainOwnerMismatch,
                OfferValidity.OfferSellerDomainOwnerMismatch
            );
            Verify(validBuyOfferUpdate1, OfferBlockchainPresence.Present, OfferState.Active, OfferValidity.Valid);
            Verify(
                validSellOfferUpdate,
                OfferBlockchainPresence.Present,
                OfferState.OfferSellerDomainOwnerMismatch,
                OfferValidity.OfferSellerDomainOwnerMismatch
            );

            void Verify(SetOfferUpdate update, OfferBlockchainPresence presence, OfferState state, OfferValidity validity)
            {
                var offer = expectedExecutedBuyOffer with
                {
                    OfferId = Offer.GenerateId(update.Type, update.InitiatorAddress, _tokenId, prepareBlock.Level),
                    InitiatorAddress = update.InitiatorAddress,
                    AcceptorAddress = update.Type == OfferType.BuyOffer ? buyerAddress : null, //Acceptor == DomainOwner == Buyer when offer executed
                    PriceWithoutFee = update.PriceWithoutFee,
                    Fee = update.Fee,
                    Validity = validity,
                    Type = update.Type,
                    Presence = presence
                };
                VerifyOfferInDb(offer);
                offer.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default)
                    .Should()
                    .Be(state);
            }
        }

        [TestCase(true, false)]
        [TestCase(false, true)]
        public async Task ShouldExecuteOffer(bool insertOffer, bool shouldThrowException)
        {
            //arrange
            Domain domain = TestDomain.Get(tokenId: _tokenId);
            DbContext.Insert(domain);

            var prepareBlock = TestBlock.Get();
            SetOfferUpdate? prepareUpdate = null;
            if (insertOffer)
            {
                prepareUpdate = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, _tokenId);
                await StoreUpdatesAsync(new[] { prepareUpdate }, prepareBlock);
            }

            var block = TestBlock.Get();
            var executeUpdate = TestOfferUpdate.GetExecuteOffer(
                OfferType.BuyOffer,
                _tokenId,
                sellerAddress: domain.Owner,
                buyerAddress: prepareUpdate?.InitiatorAddress
            );

            //act
            if (shouldThrowException)
            {
                Assert.ThrowsAsync<ArgumentNullException>(async () => await StoreUpdatesAsync(new[] { executeUpdate }, block));
            }
            else
            {
                //transfer domainUpdate should not update the executedOffer data
                var domainTransferredUpdate = TestDomainUpdate.Get(tokenId: _tokenId, name: domain.Name, owner: prepareUpdate?.InitiatorAddress);
                await StoreUpdatesAsync(new IBlockUpdate[] { executeUpdate, domainTransferredUpdate }, block);

                //assert
                var domainData = DomainData.Create(domain) with
                {
                    Data = domainTransferredUpdate.Data,
                    Operators = domainTransferredUpdate.Operators,
                    Owner = domainTransferredUpdate.Owner
                };
                var expectedOffer = new Offer(
                    Block: block,
                    OperationGroupHash: domainTransferredUpdate.OperationGroupHash,
                    OfferId: Offer.GenerateId(executeUpdate.Type, executeUpdate.InitiatorAddress, _tokenId, prepareBlock.Level),
                    InitiatorAddress: executeUpdate.InitiatorAddress,
                    TokenContract: executeUpdate.TokenContract,
                    TokenId: _tokenId,
                    Type: OfferType.BuyOffer,
                    CreatedAtUtc: prepareBlock.Timestamp,
                    ExpiresAtUtc: prepareUpdate.GuardNotNull().ExpiresAtUtc,
                    PriceWithoutFee: prepareUpdate.GuardNotNull().PriceWithoutFee,
                    Fee: prepareUpdate.GuardNotNull().Fee,
                    Presence: OfferBlockchainPresence.Executed,
                    Domain: domainData,
                    AcceptorAddress: executeUpdate.AcceptorAddress,
                    Validity: OfferValidity.OfferSellerDomainOwnerMismatch,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );
                var actual = VerifyOfferInDb(expectedOffer);
                actual.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.Executed);
            }
        }

        [TestCase(true, false)]
        [TestCase(false, true)]
        public async Task ShouldRemoveOffer(bool insertOffer, bool shouldThrowException)
        {
            //arrange
            var prepareBlock = TestBlock.Get();
            SetOfferUpdate? prepareUpdate = null;
            if (insertOffer)
            {
                prepareUpdate = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, _tokenId);
                await StoreUpdatesAsync(new[] { prepareUpdate }, prepareBlock);
            }

            var block = TestBlock.Get();
            var removeUpdate = TestOfferUpdate.GetRemoveOffer(type: OfferType.BuyOffer, tokenId: _tokenId, initiatorAddress: prepareUpdate?.InitiatorAddress);

            //act
            if (shouldThrowException)
            {
                Assert.ThrowsAsync<ArgumentNullException>(async () => await StoreUpdatesAsync(new[] { removeUpdate }, block));
            }
            else
            {
                await StoreUpdatesAsync(new[] { removeUpdate }, block);

                //assert
                var expectedOffer = new Offer(
                    Block: block,
                    OperationGroupHash: removeUpdate.OperationGroupHash,
                    OfferId: Offer.GenerateId(removeUpdate.Type, removeUpdate.InitiatorAddress, _tokenId, prepareBlock.Level),
                    InitiatorAddress: removeUpdate.InitiatorAddress,
                    TokenContract: removeUpdate.TokenContract,
                    TokenId: _tokenId,
                    Type: OfferType.BuyOffer,
                    CreatedAtUtc: prepareBlock.Timestamp,
                    ExpiresAtUtc: prepareUpdate.GuardNotNull().ExpiresAtUtc,
                    PriceWithoutFee: prepareUpdate.GuardNotNull().PriceWithoutFee,
                    Fee: prepareUpdate.GuardNotNull().Fee,
                    Presence: OfferBlockchainPresence.Removed,
                    Domain: null,
                    AcceptorAddress: null,
                    Validity: OfferValidity.DomainDoesNotExist,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                );
                var actual = VerifyOfferInDb(expectedOffer);
                actual.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.Removed);
            }
        }

        [Test]
        public async Task ShouldRemoveAndSetOfferWithinOneBlock()
        {
            //arrange
            var prepareBlock = TestBlock.Get();
            var prepareUpdate = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, _tokenId);
            await StoreUpdatesAsync(new[] { prepareUpdate }, prepareBlock);

            var block = TestBlock.Get();
            var removeUpdate = TestOfferUpdate.GetRemoveOffer(OfferType.BuyOffer, _tokenId, prepareUpdate.InitiatorAddress);
            var updatedOfferUpdate = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, _tokenId, initiatorAddress: prepareUpdate.InitiatorAddress);
            //act

            await StoreUpdatesAsync(new IBlockUpdate[] { removeUpdate, updatedOfferUpdate }, block);

            //assert
            var expectedRemovedOffer = new Offer(
                Block: block,
                OperationGroupHash: removeUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(removeUpdate.Type, removeUpdate.InitiatorAddress, _tokenId, prepareBlock.Level),
                InitiatorAddress: removeUpdate.InitiatorAddress,
                TokenContract: removeUpdate.TokenContract,
                TokenId: _tokenId,
                Type: OfferType.BuyOffer,
                CreatedAtUtc: prepareBlock.Timestamp,
                ExpiresAtUtc: prepareUpdate.GuardNotNull().ExpiresAtUtc,
                PriceWithoutFee: prepareUpdate.GuardNotNull().PriceWithoutFee,
                Fee: prepareUpdate.GuardNotNull().Fee,
                Presence: OfferBlockchainPresence.Removed,
                Domain: null,
                AcceptorAddress: null,
                Validity: OfferValidity.DomainDoesNotExist,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            var actualRemoved = VerifyOfferInDb(expectedRemovedOffer);
            actualRemoved.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.Removed);

            var expectedUpdatedOffer = new Offer(
                Block: block,
                OperationGroupHash: updatedOfferUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(updatedOfferUpdate.Type, updatedOfferUpdate.InitiatorAddress, _tokenId, block.Level),
                InitiatorAddress: updatedOfferUpdate.InitiatorAddress,
                TokenContract: updatedOfferUpdate.TokenContract,
                TokenId: _tokenId,
                Type: OfferType.BuyOffer,
                CreatedAtUtc: block.Timestamp,
                ExpiresAtUtc: updatedOfferUpdate.GuardNotNull().ExpiresAtUtc,
                PriceWithoutFee: updatedOfferUpdate.GuardNotNull().PriceWithoutFee,
                Fee: updatedOfferUpdate.GuardNotNull().Fee,
                Presence: OfferBlockchainPresence.Present,
                Domain: null,
                AcceptorAddress: null,
                Validity: OfferValidity.DomainDoesNotExist,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            var actualUpdated = VerifyOfferInDb(expectedUpdatedOffer);
            actualUpdated.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default).Should().Be(OfferState.DomainDoesNotExist);
        }

        [Test]
        public async Task ValidityUpdate_ShouldUpdateOfferDomainData([Values] bool onlyValidityUpdate)
        {
            //arrange
            var buyer = TestAddress.GetRandom();
            var domainName = "validity.test";
            var domain1 = TestDomain.Get(
                name: domainName,
                tokenId: _tokenId,
                owner: TestAddress.GetRandom(),
                validityKey: domainName
            );
            DbContext.Insert(domain1);

            var prepareBlock = TestBlock.Get();
            var offerDomain1 = TestOfferUpdate.GetSetOffer(OfferType.BuyOffer, tokenId: _tokenId, initiatorAddress: buyer);
            await StoreUpdatesAsync(new IBlockUpdate[] { offerDomain1 }, prepareBlock);


            //act
            var actBlock = TestBlock.Get();
            var executeOfferUpdate = TestOfferUpdate.GetExecuteOffer(OfferType.BuyOffer, _tokenId, sellerAddress: domain1.Owner, buyerAddress: buyer);
            var validityUpdate = TestValidityUpdate.Get(domainName, expiresAtUtc: DateTime.UtcNow.Date.Add(TimeSpan.FromDays(2)));
            await StoreUpdatesAsync(
                onlyValidityUpdate ? new IBlockUpdate[] { validityUpdate } : new IBlockUpdate[] { executeOfferUpdate, validityUpdate },
                actBlock
            );


            //assert
            var expectedOffer = new Offer(
                Block: actBlock,
                OperationGroupHash: validityUpdate.OperationGroupHash,
                OfferId: Offer.GenerateId(OfferType.BuyOffer, offerDomain1.InitiatorAddress, offerDomain1.TokenId, prepareBlock.Level),
                InitiatorAddress: offerDomain1.InitiatorAddress,
                TokenContract: offerDomain1.TokenContract,
                TokenId: offerDomain1.TokenId,
                Type: OfferType.BuyOffer,
                CreatedAtUtc: prepareBlock.Timestamp,
                ExpiresAtUtc: offerDomain1.ExpiresAtUtc,
                PriceWithoutFee: offerDomain1.PriceWithoutFee,
                Fee: offerDomain1.Fee,
                Presence: onlyValidityUpdate ? OfferBlockchainPresence.Present : OfferBlockchainPresence.Executed,
                Domain: DomainData.Create(domain1) with { ExpiresAtUtc = validityUpdate.ExpiresAtUtc },
                AcceptorAddress: domain1.Owner,
                Validity: OfferValidity.Valid,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyOfferInDb(expectedOffer);
            expectedOffer.GetState(DateTime.UtcNow, domainIsExpiringThreshold: default)
                .Should()
                .Be(onlyValidityUpdate ? OfferState.Active : OfferState.Executed);
            expectedOffer.GetState(DateTime.UtcNow, domainIsExpiringThreshold: TimeSpan.FromDays(7))
                .Should()
                .Be(onlyValidityUpdate ? OfferState.DomainIsExpiringSoon : OfferState.Executed);
        }


        private Offer VerifyOfferInDb(Offer expectedAuction)
        {
            var actualOffer = DbContext.SingleBy<Offer>(a => a.OfferId == expectedAuction.OfferId && a.ValidUntilBlockLevel == int.MaxValue);
            actualOffer.Should().BeEquivalentTo(expectedAuction);
            return actualOffer;
        }
    }
}