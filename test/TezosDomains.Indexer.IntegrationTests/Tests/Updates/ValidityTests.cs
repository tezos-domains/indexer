﻿using FluentAssertions;
using NUnit.Framework;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class ValidityTests : UpdateTestBase
    {
        [Test]
        public async Task ShouldUpdateValidity()
        {
            //arrange
            var expiresBefore = TestDateTime.GetFutureOrMax();
            const string validityKey = "foo.bar";

            var domainToUpdate = TestDomain.Get(validityKey: validityKey, expiresAtUtc: expiresBefore);
            var unrelatedDomain = TestDomain.Get(validityKey: TestDomainName.GetRandom(), expiresAtUtc: TestDateTime.GetFutureOrMax());
            var noValidityDomain = TestDomain.Get(expiresAtUtc: TestDateTime.GetFutureOrMax());
            DbContext.Insert(domainToUpdate, unrelatedDomain, noValidityDomain);

            var recordToUpdate = TestReverseRecord.Get(validityKey: validityKey, expiresAtUtc: expiresBefore);
            var unrelatedRecord = TestReverseRecord.Get(validityKey: TestDomainName.GetRandom(), expiresAtUtc: TestDateTime.GetFutureOrMax());
            var noValidityRecord = TestReverseRecord.Get(expiresAtUtc: TestDateTime.GetFutureOrMax());
            DbContext.Insert(recordToUpdate, unrelatedRecord, noValidityRecord);

            //act
            var expiresAfter = TestDateTime.GetFutureOrMax();
            var updateBlock = TestBlock.Get();
            var update = TestValidityUpdate.Get(validityKey, expiresAfter);
            await StoreUpdatesAsync(new[] { update }, updateBlock);

            //assert
            var domains = DbContext.ListAll<Domain>();
            domains.Should()
                .BeEquivalentTo(
                    new[]
                    {
                        domainToUpdate with
                        {
                            ExpiresAtUtc = expiresAfter,
                            Block = updateBlock,
                            OperationGroupHash = update.OperationGroupHash,
                        },
                        domainToUpdate.WithValidUntil(updateBlock),
                        unrelatedDomain,
                        noValidityDomain
                    }
                );

            var records = DbContext.ListAll<ReverseRecord>();
            records.Should()
                .BeEquivalentTo(
                    new[]
                    {
                        recordToUpdate with
                        {
                            ExpiresAtUtc = expiresAfter,
                            Block = updateBlock,
                            OperationGroupHash = update.OperationGroupHash,
                        },
                        recordToUpdate.WithValidUntil(updateBlock),
                        unrelatedRecord,
                        noValidityRecord
                    }
                );
        }
    }
}