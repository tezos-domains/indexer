﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Data;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.Indexer.Tezos;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class DomainTests : UpdateTestBase
    {
        private string _label = null!;
        private string _name = null!;
        private const string ParentName = "foo.bar";

        [SetUp]
        public void Setup()
        {
            _label = TestDomainName.GetRandomLabel();
            _name = $"{_label}.{ParentName}";
        }


        [Test]
        public async Task ShouldCreateDomain_IfDoesNotExistYet([Values] ParentUpdatePosition parentUpdatePosition)
        {
            //prepare
            var priorBlock = TestBlock.Get();
            var parentUpdate = TestDomainUpdate.Get(ParentName, owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n");

            var updateBlock = TestBlock.Get();
            var update = TestDomainUpdate.Get(_name);
            var blockUpdates = new[] { update };

            var laterBlock = TestBlock.Get();
            var expectedDomains = new List<Domain>();
            string expectedOperationGroupHash = update.OperationGroupHash;
            //act
            switch (parentUpdatePosition)
            {
                case ParentUpdatePosition.BlockBeforeDomainUpdate:
                    await StoreUpdatesAsync(new[] { parentUpdate }, priorBlock);

                    await StoreUpdatesAsync(blockUpdates, updateBlock);
                    break;

                case ParentUpdatePosition.SameBlockPriorDomainUpdate:
                    blockUpdates = blockUpdates.Prepend(parentUpdate).ToArray();

                    await StoreUpdatesAsync(blockUpdates, updateBlock);
                    break;

                case ParentUpdatePosition.SameBlockAfterDomainUpdate:
                    blockUpdates = blockUpdates.Append(parentUpdate).ToArray();

                    await StoreUpdatesAsync(blockUpdates, updateBlock);
                    break;

                case ParentUpdatePosition.BlockAfterDomainUpdate:
                    //insert parent data
                    await StoreUpdatesAsync(new[] { TestDomainUpdate.Get(ParentName, owner: "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9") }, priorBlock);
                    //insert child data
                    await StoreUpdatesAsync(blockUpdates, updateBlock);
                    //update parent data
                    await StoreUpdatesAsync(new[] { parentUpdate }, laterBlock);
                    expectedDomains.Add(
                        new Domain(
                            Block: updateBlock,
                            OperationGroupHash: update.OperationGroupHash,
                            Name: _name,
                            Address: update.Address,
                            Owner: update.Owner,
                            Data: update.Data,
                            TokenId: update.TokenId,
                            Label: _label,
                            Ancestors: new[] { "foo.bar", "bar" },
                            ParentName: "foo.bar",
                            ParentOwner: "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9",
                            ExpiresAtUtc: DateTime.MaxValue,
                            ValidityKey: null,
                            Level: 3,
                            NameForOrdering: $"foo.bar.{_label}",
                            Operators: Array.Empty<string>(),
                            LastAuction: null,
                            ValidUntilBlockLevel: laterBlock.Level,
                            ValidUntilTimestamp: laterBlock.Timestamp
                        )
                    );
                    updateBlock = laterBlock;
                    expectedOperationGroupHash = parentUpdate.OperationGroupHash;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(parentUpdatePosition), parentUpdatePosition, null);
            }

            //assert
            var expectedDomain = new Domain(
                Block: updateBlock,
                OperationGroupHash: expectedOperationGroupHash,
                Name: _name,
                Address: update.Address,
                Owner: update.Owner,
                Data: update.Data,
                TokenId: update.TokenId,
                Label: _label,
                Ancestors: new[] { "foo.bar", "bar" },
                ParentName: "foo.bar",
                ParentOwner: parentUpdate.Owner,
                ExpiresAtUtc: DateTime.MaxValue,
                ValidityKey: null,
                Level: 3,
                NameForOrdering: $"foo.bar.{_label}",
                Operators: Array.Empty<string>(),
                LastAuction: null,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            expectedDomains.Add(expectedDomain);
            VerifyDomainsInDb(expectedDomains);
        }

        public enum ParentUpdatePosition
        {
            BlockBeforeDomainUpdate,
            SameBlockPriorDomainUpdate,
            SameBlockAfterDomainUpdate,
            BlockAfterDomainUpdate,
        }

        [Test]
        public async Task ShouldModifyDomain_IfAlreadyExists()
        {
            //arrange
            await StoreUpdatesAsync(new[] { TestDomainUpdate.Get(_name) });
            var initialDomain = DbContext.SingleByKey<Domain>(_name);

            //act
            var updateBlock = TestBlock.Get();
            var update = TestDomainUpdate.Get(
                initialDomain.Name,
                operators: new[] { "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n" }
            );
            await StoreUpdatesAsync(new[] { update }, updateBlock);

            //assert
            var expectedDomain = new Domain(
                Block: updateBlock,
                OperationGroupHash: update.OperationGroupHash,
                Name: initialDomain.Name,
                Address: update.Address,
                Owner: update.Owner,
                Data: update.Data,
                TokenId: update.TokenId,
                Label: initialDomain.Label,
                Ancestors: initialDomain.Ancestors,
                ParentName: initialDomain.ParentName,
                ParentOwner: initialDomain.ParentOwner,
                ExpiresAtUtc: DateTime.MaxValue,
                ValidityKey: null,
                Level: initialDomain.Level,
                NameForOrdering: initialDomain.NameForOrdering,
                Operators: update.Operators,
                LastAuction: null,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyDomainsInDb(new[] { expectedDomain, initialDomain.WithValidUntil(updateBlock) });
        }

        [Test]
        public async Task ShouldModifyDomain_OnAuctionBid()
        {
            //arrange
            await StoreUpdatesAsync(new[] { TestDomainUpdate.Get(_name) });
            var initialDomain = DbContext.SingleByKey<Domain>(_name);

            //act
            var updateBlock = TestBlock.Get();
            var update = TestAuctionUpdate.GetBid(initialDomain.Name);

            await StoreUpdatesAsync(new[] { update }, updateBlock);

            //assert
            var bid = new Bid(updateBlock, update.OperationGroupHash, update.Bidder, update.Amount);
            var expectedDomain = new Domain(
                Block: updateBlock,
                OperationGroupHash: update.OperationGroupHash,
                Name: initialDomain.Name,
                Address: initialDomain.Address,
                Owner: initialDomain.Owner,
                Data: initialDomain.Data,
                TokenId: initialDomain.TokenId,
                Label: initialDomain.Label,
                Ancestors: initialDomain.Ancestors,
                ParentName: initialDomain.ParentName,
                ParentOwner: initialDomain.ParentOwner,
                ExpiresAtUtc: DateTime.MaxValue,
                ValidityKey: null,
                Level: initialDomain.Level,
                NameForOrdering: initialDomain.NameForOrdering,
                Operators: initialDomain.Operators,
                LastAuction: new Auction(
                    Auction.GenerateId(initialDomain.Name, updateBlock.Level),
                    updateBlock,
                    update.OperationGroupHash,
                    initialDomain.Name,
                    new[] { bid },
                    update.EndsAtUtc,
                    update.OwnedUntilUtc,
                    IsSettled: false,
                    FirstBidAtUtc: updateBlock.Timestamp,
                    BidCount: 1,
                    HighestBid: bid,
                    CountOfUniqueBidders: 1,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                ),
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyDomainsInDb(new[] { expectedDomain, initialDomain.WithValidUntil(updateBlock) });
        }

        [Test]
        public async Task ShouldModifyDomain_OnAuctionSettle()
        {
            //arrange
            var block = TestBlock.Get();
            var bid = TestAuctionUpdate.GetBid(domainName: _name, auctionEndInUtc: block.Timestamp);
            await StoreUpdatesAsync(new[] { bid }, block);

            //act
            var updateBlock = TestBlock.Get();
            var settle = TestAuctionUpdate.GetSettle(bid.DomainName);
            var domain = TestDomainUpdate.Get(bid.DomainName, operationGroupHash: settle.OperationGroupHash);
            await StoreUpdatesAsync(new IBlockUpdate[] { settle, domain }, updateBlock);

            //assert
            var (label, parentName, ancestors, level, nameForOrdering) = DomainNameUtils.Parse(domain.Name);
            var expectedBid = new Bid(block, bid.OperationGroupHash, bid.Bidder, bid.Amount);
            var expectedDomain = new Domain(
                Block: updateBlock,
                OperationGroupHash: settle.OperationGroupHash,
                Name: domain.Name,
                Address: domain.Address,
                Owner: domain.Owner,
                Data: domain.Data,
                TokenId: domain.TokenId,
                Label: label,
                Ancestors: ancestors,
                ParentName: parentName,
                ParentOwner: null,
                ExpiresAtUtc: DateTime.MaxValue,
                ValidityKey: null,
                Level: level,
                NameForOrdering: nameForOrdering,
                Operators: new List<string>(),
                LastAuction: new Auction(
                    AuctionId: Auction.GenerateId(domain.Name, block.Level),
                    Block: updateBlock,
                    OperationGroupHash: settle.OperationGroupHash,
                    DomainName: domain.Name,
                    Bids: new[] { expectedBid },
                    EndsAtUtc: bid.EndsAtUtc,
                    OwnedUntilUtc: bid.OwnedUntilUtc,
                    IsSettled: true,
                    FirstBidAtUtc: block.Timestamp,
                    BidCount: 1,
                    HighestBid: expectedBid,
                    CountOfUniqueBidders: 1,
                    ValidUntilBlockLevel: int.MaxValue,
                    ValidUntilTimestamp: DateTime.MaxValue
                ),
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyDomainsInDb(new[] { expectedDomain });
        }

        [Test]
        public async Task ShouldResolveExpirationUsingValidityKey([Values] bool hasLimitedExpiration)
        {
            var validityDomain = TestDomain.Get(
                name: $"{TestDomainName.GetRandomLabel()}.wtf",
                expiresAtUtc: hasLimitedExpiration ? TestDateTime.GetFuture() : null
            );
            DbContext.Insert(validityDomain);

            //act
            await StoreUpdatesAsync(new[] { TestDomainUpdate.Get(_name, validityKey: validityDomain.Name) });

            //assert
            var domain = DbContext.SingleByKey<Domain>(_name);
            domain.ValidityKey.Should().Be(validityDomain.Name);
            domain.ExpiresAtUtc.Should().Be(validityDomain.ExpiresAtUtc);
        }

        private void VerifyDomainsInDb(IEnumerable<Domain> expectedDomains)
        {
            var actualDomains = DbContext.ListAllByKey<Domain>(_name);
            actualDomains.Should().BeEquivalentTo(expectedDomains);
        }
    }
}