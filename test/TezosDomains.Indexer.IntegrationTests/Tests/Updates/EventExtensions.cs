﻿using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public static class EventExtensions
    {
        public static IBlockUpdate AsUpdate(this Event e) => new EventUpdate(e);
    }
}