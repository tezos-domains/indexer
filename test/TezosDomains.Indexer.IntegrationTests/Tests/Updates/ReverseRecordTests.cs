﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class ReverseRecordTests : UpdateTestBase
    {
        private string _address = null!;

        [SetUp]
        public void Setup()
            => _address = TestAddress.GetRandom();

        [Test]
        public async Task ShouldCreateReverseRecord_IfDoesNotExist()
        {
            //act
            var block = TestBlock.Get();
            var update = TestReverseRecordUpdate.Get(_address);
            await StoreUpdatesAsync(new[] { update }, block);

            //assert
            var expectedRecord = new ReverseRecord(
                Block: block,
                OperationGroupHash: update.OperationGroupHash,
                Address: _address,
                Name: null,
                Owner: update.Owner,
                ExpiresAtUtc: DateTime.MaxValue,
                ValidityKey: null,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyRecordsInDb(new[] { expectedRecord });
        }

        [Test]
        public async Task ShouldModifyReverseRecord_IfAlreadyExists()
        {
            //arrange
            await StoreUpdatesAsync(new[] { TestReverseRecordUpdate.Get(_address) });
            var initialRecord = DbContext.SingleByKey<ReverseRecord>(_address);

            //act
            var updateBlock = TestBlock.Get();
            var update = TestReverseRecordUpdate.Get(_address);
            await StoreUpdatesAsync(new[] { update }, updateBlock);

            //assert
            var expectedRecord = new ReverseRecord(
                Block: updateBlock,
                OperationGroupHash: update.OperationGroupHash,
                Address: _address,
                Name: update.Name,
                Owner: update.Owner,
                ExpiresAtUtc: DateTime.MaxValue,
                ValidityKey: null,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
            VerifyRecordsInDb(new[] { expectedRecord, initialRecord.WithValidUntil(updateBlock) });
        }

        [Test]
        public async Task ShouldResolveExpirationUsingName([Values] bool hasLimitedExpiration)
        {
            var validityDomain = TestDomain.Get(
                name: $"{TestDomainName.GetRandomLabel()}.wtf",
                expiresAtUtc: hasLimitedExpiration ? TestDateTime.GetFuture() : null,
                validityKey: $"{TestDomainName.GetRandomLabel()}.omg"
            );
            DbContext.Insert(validityDomain);

            //act
            await StoreUpdatesAsync(new[] { TestReverseRecordUpdate.Get(_address, name: validityDomain.Name) });

            //assert
            var record = DbContext.SingleByKey<ReverseRecord>(_address);
            record.ValidityKey.Should().Be(validityDomain.ValidityKey);
            record.ExpiresAtUtc.Should().Be(validityDomain.ExpiresAtUtc);
        }

        private void VerifyRecordsInDb(IEnumerable<ReverseRecord> expectedRecords)
        {
            var actualRecords = DbContext.ListAllByKey<ReverseRecord>(_address);
            actualRecords.Should().BeEquivalentTo(expectedRecords);
        }
    }
}