﻿using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class EventTests : UpdateTestBase
    {
        //Avoid different results when leap year (365->366)
        public static int DurationOneYearInDays => (DateTime.UtcNow.Date.AddYears(1) - DateTime.UtcNow.Date).Days;

        [Test]
        public async Task StoreAllEventTypes()
        {
            //arrange
            var bidUpdate = TestAuctionUpdate.GetBid(
                domainName: "auction-settle.ddd",
                bidder: "bidderAddr",
                amount: 1,
                auctionEndInUtc: DateTime.UtcNow.AddDays(value: -1),
                ownedUntilUtc: DateTime.UtcNow.AddYears(value: 1)
            );
            await StoreUpdatesAsync(updates: new[] { bidUpdate });

            var block = TestBlock.Get();

            #region Event setup

            var auctionSettleEvent = new AuctionSettleEvent(
                Id: Event.GenerateId(),
                Block: block,
                SourceAddress: "address6",
                OperationGroupHash: "ogh5",
                DomainName: "auction-settle.ddd",
                DomainOwnerAddress: "address7-own",
                DomainForwardRecordAddress: "address8-fwd",
                Data: new Dictionary<string, string> { { "k1", "v1" } },
                RegistrationDurationInDays: 123,
                WinningBid: 100
            );

            var events = new Event[]
            {
                new AuctionBidEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address1",
                    OperationGroupHash: "ogh1",
                    DomainName: "auction-bid.aaa",
                    BidAmount: 1,
                    TransactionAmount: 1,
                    PreviousBidderAddress: null,
                    PreviousBidAmount: null
                ),
                new AuctionEndEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address4",
                    DomainName: "address3",
                    WinningBid: 2,
                    Participants: new[] { "address4" }
                ),
                auctionSettleEvent,
                new DomainBuyEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address9",
                    OperationGroupHash: "ogh6",
                    DomainName: "domain-buy.eee",
                    Price: 4,
                    DurationInDays: 365,
                    DomainOwnerAddress: "address10-own",
                    DomainForwardRecordAddress: "address11-fwd",
                    Data: new Dictionary<string, string> { { "k2", "v2" } }
                ),
                new DomainRenewEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address10",
                    OperationGroupHash: "ogh7",
                    DomainName: "domain-renew.fff",
                    Price: 5,
                    DurationInDays: 365 * 2
                ),
                new DomainCommitEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address11",
                    OperationGroupHash: "ogh8",
                    CommitmentHash: Guid.NewGuid().ToString(format: "N")
                ),
                new DomainUpdateEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address12",
                    OperationGroupHash: "ogh9",
                    DomainName: "domain-update.ggg",
                    DomainOwnerAddress: "address12-own",
                    DomainForwardRecordAddress: "address12-fwd",
                    Data: new Dictionary<string, string> { { "k3", "v3" } }
                ),
                new DomainSetChildRecordEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address13",
                    OperationGroupHash: "ogh10",
                    DomainName: "domain-set-child-record.hhh",
                    DomainOwnerAddress: "address13-own",
                    DomainForwardRecordAddress: "address13-fwd",
                    Data: new Dictionary<string, string> { { "k4", "v4" } },
                    IsNewRecord: true
                ),
                new ReverseRecordClaimEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address14",
                    OperationGroupHash: "ogh11",
                    Name: null,
                    ReverseRecordOwnerAddress: "address14-own"
                ),
                new ReverseRecordUpdateEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address15",
                    OperationGroupHash: "ogh12",
                    Name: "reverse-record.iii",
                    ReverseRecordOwnerAddress: "address15-own",
                    ReverseRecordAddress: "address15-addr"
                ),
                new DomainGrantEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address16",
                    OperationGroupHash: "ogh3",
                    DomainName: "domain-claim.dev",
                    DomainOwnerAddress: "address16-own",
                    DurationInDays: 365,
                    DomainForwardRecordAddress: "address11-fwd",
                    Data: new Dictionary<string, string> { { "k5", "v5" } }
                ),
                new DomainClaimEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address17",
                    OperationGroupHash: "ogh14",
                    DomainName: "domain-claim.dev",
                    DomainOwnerAddress: "address17-own",
                    DomainForwardRecordAddress: null
                )
            };

            var auctionWithdrawEvent = new AuctionWithdrawEvent(
                Id: Event.GenerateId(),
                Block: block,
                SourceAddress: "address2",
                DestinationAddress: "address20",
                OperationGroupHash: "ogh2",
                TldName: "bbb",
                WithdrawnAmount: 0
            );
            var auctionWithdrawEventUpdates = new[]
            {
                auctionWithdrawEvent.AsUpdate(), new BidderBalancesUpdate(OperationGroupHash: "ogh2", Address: "address2", TldName: "bbb", Balance: null),
                new OutgoingMoneyTransactionEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "address2",
                    DestinationAddress: "address20",
                    OperationGroupHash: "ogh2",
                    WithdrawnAmount: 123
                ).AsUpdate()
            };

            #endregion

            //act
            await StoreUpdatesAsync(updates: AsUpdates(events).Concat(auctionWithdrawEventUpdates).ToList(), block);

            //assert
            var index = events.RequiredIndexOf(e => e == auctionSettleEvent);
            events[index] = auctionSettleEvent with { RegistrationDurationInDays = DurationOneYearInDays + 1, WinningBid = 1 };
            events = events.Append(auctionWithdrawEvent with { WithdrawnAmount = 123 }).ToArray();
            VerifyEventsInDb(events);
            VerifyEventsInDb(DbContext.ListAll<Event>());
        }

        [Test]
        public async Task UpdateAuctionBidEvent_When_BidsComeInSeparateBlocks()
        {
            //arrange
            var block1 = TestBlock.Get();
            var block2 = TestBlock.Get();
            var bidEvent1 = TestEvent.GetAuctionBid(block2);
            var blockUpdates = new[]
            {
                bidEvent1.AsUpdate(),
                TestAuctionUpdate.GetBid(
                    domainName: bidEvent1.DomainName,
                    bidder: bidEvent1.SourceAddress,
                    amount: bidEvent1.BidAmount
                )
            };
            await StoreUpdatesAsync(blockUpdates, block2);
            AddAuctionVersion(bidEvent1.DomainName, historyBlock: block1);

            //act
            var block3 = TestBlock.Get();
            var bidEvent2 = TestEvent.GetAuctionBid(block3, domainName: bidEvent1.DomainName);
            await StoreUpdatesAsync(AsUpdates(bidEvent2), block3);

            //assert
            VerifyEventsInDb(new[] { bidEvent1, bidEvent2.WithPreviousBid(bidEvent1.SourceAddress, bidEvent1.BidAmount) });
        }

        [Test]
        public async Task UpdateAuctionBidEvent_When_BidsComeInSameBlock()
        {
            //arrange
            var block1 = TestBlock.Get();
            var secondBid = new AuctionBidEvent(
                Id: Event.GenerateId(),
                Block: block1,
                SourceAddress: "address2",
                OperationGroupHash: "ogh1",
                DomainName: "auction-bid.aaa",
                BidAmount: 2,
                TransactionAmount: 2,
                PreviousBidderAddress: null,
                PreviousBidAmount: null
            );
            var firstBid = new AuctionBidEvent(
                Id: Event.GenerateId(),
                Block: block1,
                SourceAddress: "address1",
                OperationGroupHash: "ogh2",
                DomainName: "auction-bid.aaa",
                BidAmount: 1,
                TransactionAmount: 1,
                PreviousBidderAddress: null,
                PreviousBidAmount: null
            );

            //act
            await StoreUpdatesAsync(new[] { secondBid.AsUpdate(), firstBid.AsUpdate() }, block1);

            //assert
            VerifyEventsInDb(new[] { firstBid, secondBid.WithPreviousBid(firstBid.SourceAddress, firstBid.BidAmount) });
        }

        [Test]
        public async Task GenerateAuctionEndEventAndStore()
        {
            //arrange
            var block1 = TestBlock.Get();
            var block2 = TestBlock.Get();
            var bid = TestAuctionUpdate.GetBid(auctionEndInUtc: block2.Timestamp.AddSeconds(1));
            await StoreUpdatesAsync(new[] { bid }, block2, previousBlock: block1);
            AddAuctionVersion(bid.DomainName, historyBlock: block1);

            //act
            var block3 = TestBlock.Get();
            await StoreUpdatesAsync(Array.Empty<IBlockUpdate>(), block3, previousBlock: block2);

            //assert
            var expected = new AuctionEndEvent(
                Id: "ignored",
                Block: block3,
                SourceAddress: bid.Bidder,
                DomainName: bid.DomainName,
                WinningBid: bid.Amount,
                Participants: new[] { bid.Bidder }
            );
            VerifyEventsInDb(new[] { expected }, excludingId: true);
        }

        [Test]
        public async Task GeneratePossibleDomainEventsAndStore()
        {
            //arrange
            var block1 = TestBlock.Get();
            var domainUpdate = TestDomainUpdate.Get(name: "test.domain");
            var buyEvent = new DomainBuyEvent(
                Id: Event.GenerateId(),
                Block: block1,
                SourceAddress: domainUpdate.Owner,
                OperationGroupHash: domainUpdate.OperationGroupHash,
                DomainName: domainUpdate.Name,
                Price: 4,
                DurationInDays: 365,
                DomainOwnerAddress: domainUpdate.Owner,
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>()
            );
            await StoreUpdatesAsync(new[] { domainUpdate, buyEvent.AsUpdate() }, block1);

            var block2 = TestBlock.Get();
            //DomainUpdate - Only Owner changed => generate TransferEvent and DISCARD this DomainUpdateEvent
            var onlyOwnerUpdate = domainUpdate with { Owner = "onlyOwnerUpdate-own" };
            var onlyOwnerDomainUpdateEvent = new DomainUpdateEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: domainUpdate.Owner,
                OperationGroupHash: onlyOwnerUpdate.OperationGroupHash,
                DomainName: onlyOwnerUpdate.Name,
                DomainOwnerAddress: onlyOwnerUpdate.Owner,
                DomainForwardRecordAddress: onlyOwnerUpdate.Address,
                Data: onlyOwnerUpdate.Data
            );
            //DomainSetChildRecordEvent(DomainUpdate) - Owner and Address changed => generate TransferEvent and keep this DomainSetChildRecordEvent
            var ownerAndAddressUpdate = onlyOwnerUpdate with { Owner = "ownerAndAddressUpdate-own", Address = "ownerAndAddressUpdate" };
            var ownerAndAddressDomainUpdateEvent = new DomainSetChildRecordEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: onlyOwnerUpdate.Owner,
                OperationGroupHash: ownerAndAddressUpdate.OperationGroupHash,
                DomainName: ownerAndAddressUpdate.Name,
                DomainOwnerAddress: ownerAndAddressUpdate.Owner,
                DomainForwardRecordAddress: ownerAndAddressUpdate.Address,
                Data: ownerAndAddressUpdate.Data,
                IsNewRecord: false
            );
            //DomainUpdate - Only operators changed => generate DomainUpdateOperatorsEvent
            var onlyOperatorsUpdate = ownerAndAddressUpdate with { Operators = new[] { "KT1TV6QNrRfe9GZycNJNrcDhPa7zC3sT3ajM" } };
            //DomainUpdate - Nothing changed => keep this DomainUpdateEvent (no TransferEvent is generated)
            var noChangeUpdate = onlyOperatorsUpdate with { };
            var noChangeDomainUpdateEvent = new DomainUpdateEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: noChangeUpdate.Owner,
                OperationGroupHash: noChangeUpdate.OperationGroupHash,
                DomainName: noChangeUpdate.Name,
                DomainOwnerAddress: noChangeUpdate.Owner,
                DomainForwardRecordAddress: noChangeUpdate.Address,
                Data: noChangeUpdate.Data
            );
            //DomainClaimed - Skip DomainSetChildRecordEvent
            var domainClaimEvent = new DomainClaimEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: "claimed-src",
                OperationGroupHash: ownerAndAddressUpdate.OperationGroupHash,
                DomainName: "claimed.dev",
                DomainOwnerAddress: "claimed-own",
                DomainForwardRecordAddress: null
            );
            var domainClaimedSetChildRecordEvent = new DomainSetChildRecordEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: "KT1...",
                OperationGroupHash: domainClaimEvent.OperationGroupHash,
                DomainName: domainClaimEvent.DomainName,
                DomainOwnerAddress: domainClaimEvent.DomainOwnerAddress,
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>(),
                IsNewRecord: false
            );
            var domainClaimedUpdateEvent = new DomainUpdateEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: "claimed-src",
                OperationGroupHash: domainClaimEvent.OperationGroupHash,
                DomainName: domainClaimEvent.DomainName,
                DomainOwnerAddress: domainClaimEvent.DomainOwnerAddress,
                DomainForwardRecordAddress: "claimed-fwd",
                Data: new Dictionary<string, string>()
            );

            //act
            await StoreUpdatesAsync(
                new[]
                {
                    onlyOwnerDomainUpdateEvent.AsUpdate(), onlyOwnerUpdate,
                    ownerAndAddressDomainUpdateEvent.AsUpdate(), ownerAndAddressUpdate,
                    onlyOperatorsUpdate,
                    noChangeDomainUpdateEvent.AsUpdate(), noChangeUpdate,
                    domainClaimEvent.AsUpdate(),
                    domainClaimedSetChildRecordEvent.AsUpdate(),
                    domainClaimedUpdateEvent.AsUpdate()
                },
                block2,
                block1
            );

            //assert
            var events = new Event[]
            {
                buyEvent,
                new DomainTransferEvent(
                    Id: Event.GenerateId(),
                    Block: block2,
                    SourceAddress: domainUpdate.Owner,
                    OperationGroupHash: onlyOwnerUpdate.OperationGroupHash,
                    DomainName: onlyOwnerUpdate.Name,
                    NewOwner: onlyOwnerUpdate.Owner
                ),
                ownerAndAddressDomainUpdateEvent,
                new DomainTransferEvent(
                    Id: Event.GenerateId(),
                    Block: block2,
                    SourceAddress: onlyOwnerUpdate.Owner,
                    OperationGroupHash: ownerAndAddressUpdate.OperationGroupHash,
                    DomainName: ownerAndAddressUpdate.Name,
                    NewOwner: ownerAndAddressUpdate.Owner
                ),
                new DomainUpdateOperatorsEvent(
                    Id: Event.GenerateId(),
                    Block: block2,
                    SourceAddress: onlyOperatorsUpdate.Owner,
                    OperationGroupHash: onlyOperatorsUpdate.OperationGroupHash,
                    DomainName: onlyOperatorsUpdate.Name,
                    Operators: new[] { "KT1TV6QNrRfe9GZycNJNrcDhPa7zC3sT3ajM" }
                ),
                noChangeDomainUpdateEvent,
                (domainClaimEvent with { DomainForwardRecordAddress = "claimed-fwd" })
            };

            VerifyEventsInDb(events, excludingId: true);
            VerifyEventsInDb(DbContext.ListAll<Event>());
        }

        [Test]
        public async Task ProcessAuctionSettleEventAndStore()
        {
            // arrange
            var block1 = TestBlock.Get();
            var block2 = TestBlock.Get();
            var bid = TestAuctionUpdate.GetBid(
                amount: 10,
                auctionEndInUtc: block2.Timestamp,
                ownedUntilUtc: block2.Timestamp.AddYears(1)
            );
            await StoreUpdatesAsync(new[] { bid }, block2);
            AddAuctionVersion(bid.DomainName, historyBlock: block1);

            //act
            var block3 = TestBlock.Get();
            var auctionSettleEvent = new AuctionSettleEvent(
                Id: Event.GenerateId(),
                Block: block3,
                SourceAddress: bid.Bidder,
                OperationGroupHash: bid.OperationGroupHash,
                DomainName: bid.DomainName,
                DomainOwnerAddress: bid.Bidder,
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>(),
                RegistrationDurationInDays: 123,
                WinningBid: 100
            );
            await StoreUpdatesAsync(AsUpdates(auctionSettleEvent), block3, previousBlock: block2);

            //assert
            VerifyEventsInDb(new[] { auctionSettleEvent with { RegistrationDurationInDays = DurationOneYearInDays, WinningBid = 10 } });
        }

        private void AddAuctionVersion(string domainName, Block historyBlock)
        {
            var auction = DbContext.Get<Auction>().Find(a => a.DomainName == domainName).Single();
            DbContext.Insert(
                auction with
                {
                    Block = historyBlock,
                    ValidUntilBlockLevel = auction.Block.Level,
                    ValidUntilTimestamp = auction.Block.Timestamp,
                }
            );
        }

        [Test]
        public async Task ProcessAuctionWithdrawEventAndStore()
        {
            //arrange + act
            var block = TestBlock.Get();

            var auctionWithdrawEvent = new AuctionWithdrawEvent(
                Id: Event.GenerateId(),
                Block: block,
                SourceAddress: "address1",
                DestinationAddress: "address2",
                OperationGroupHash: "ogh1",
                TldName: "a2",
                WithdrawnAmount: 0
            );
            var updates = new[]
            {
                auctionWithdrawEvent.AsUpdate(),
                new BidderBalancesUpdate("ogh1", "address1", "a2", 0),
                new OutgoingMoneyTransactionEvent(
                    Id: Event.GenerateId(),
                    Block: block,
                    SourceAddress: "tld-contract-addr",
                    DestinationAddress: "address2",
                    OperationGroupHash: "ogh1",
                    WithdrawnAmount: 100
                ).AsUpdate()
            };
            await StoreUpdatesAsync(updates, block);

            //assert
            var expectedEvent = auctionWithdrawEvent with { WithdrawnAmount = 100 };
            VerifyEventsInDb(new[] { expectedEvent });
        }

        [Test]
        public async Task ProcessDomainSetChildRecordEventAndStore()
        {
            //arrange
            var block1 = TestBlock.Get();
            await StoreUpdatesAsync(new[] { TestDomainUpdate.Get("existing.sub.domain", "owner") }, block1);

            //act
            var block2 = TestBlock.Get();
            var newSubdomainChange = new DomainSetChildRecordEvent(
                Id: Event.GenerateId(),
                Block: block1,
                SourceAddress: "source1",
                OperationGroupHash: "ogh1",
                DomainName: "new.sub.domain",
                DomainOwnerAddress: "owner",
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>(),
                IsNewRecord: true
            );
            var existingSubdomainChange = new DomainSetChildRecordEvent(
                Id: Event.GenerateId(),
                Block: block1,
                SourceAddress: "source1",
                OperationGroupHash: "ogh2",
                DomainName: "existing.sub.domain",
                DomainOwnerAddress: "owner",
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>(),
                IsNewRecord: true
            );
            var blockUpdates2 = new[]
            {
                newSubdomainChange.AsUpdate(), TestDomainUpdate.Get(newSubdomainChange.DomainName, newSubdomainChange.DomainOwnerAddress),
                existingSubdomainChange.AsUpdate(), TestDomainUpdate.Get(existingSubdomainChange.DomainName, existingSubdomainChange.DomainOwnerAddress)
            };
            await StoreUpdatesAsync(blockUpdates2, block2, previousBlock: block1);

            //assert
            VerifyEventsInDb(new[] { newSubdomainChange, existingSubdomainChange with { IsNewRecord = false } });
        }

        [Test]
        public async Task GenerateAndStoreDomainGrantEvent_WhenNotBoughtNorSettledNorClaimed()
        {
            //arrange
            var block1 = TestBlock.Get();
            var wonUpdate = TestAuctionUpdate.GetBid(
                domainName: "won-in-auction.domain",
                bidder: "address1",
                amount: 12,
                auctionEndInUtc: block1.Timestamp,
                ownedUntilUtc: block1.Timestamp.AddYears(1)
            );
            await StoreUpdatesAsync(new[] { wonUpdate }, block1);

            //act
            var block2 = TestBlock.Get();
            var buyEvent = new DomainBuyEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: "address1",
                OperationGroupHash: "ogh",
                DomainName: "bought.domain",
                Price: 1,
                DurationInDays: DurationOneYearInDays,
                DomainOwnerAddress: "owner",
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>()
            );
            var settleEvent = new AuctionSettleEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: "address1",
                OperationGroupHash: "ogh",
                DomainName: wonUpdate.DomainName,
                DomainOwnerAddress: "owner",
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>(),
                RegistrationDurationInDays: DurationOneYearInDays,
                WinningBid: wonUpdate.Amount
            );
            var claimEvent = new DomainClaimEvent(
                Id: Event.GenerateId(),
                Block: block2,
                SourceAddress: "address1",
                OperationGroupHash: "ogh",
                DomainName: wonUpdate.DomainName,
                DomainOwnerAddress: "owner",
                DomainForwardRecordAddress: null
            );
            var grantUpdate = TestDomainUpdate.Get(
                name: "granted.domain",
                owner: "owner1",
                address: "address1",
                validityKey: "granted.domain"
            );
            var grantForeverUpdate = TestDomainUpdate.Get(
                name: "granted-forever.domain",
                owner: "owner1",
                address: "address1",
                validityKey: "granted-forever.domain"
            );
            var blockUpdates2 = new[]
            {
                buyEvent.AsUpdate(),
                settleEvent.AsUpdate(),
                claimEvent.AsUpdate(),
                TestValidityUpdate.Get("granted.domain", block2.Timestamp.AddYears(1)),
                grantUpdate,
                grantForeverUpdate,
                TestDomainUpdate.Get(name: "bought.domain", owner: "owner2"),
                TestDomainUpdate.Get(name: wonUpdate.DomainName, owner: "owner3"),
                TestDomainUpdate.Get(name: "subdomain.granted.domain", owner: "owner1")
            };

            await StoreUpdatesAsync(blockUpdates2, block2);

            //assert
            var expectedGrantEvent = new DomainGrantEvent(
                Id: "ignored",
                Block: block2,
                SourceAddress: "owner1",
                OperationGroupHash: grantUpdate.OperationGroupHash,
                DomainName: grantUpdate.Name,
                DurationInDays: DurationOneYearInDays,
                DomainOwnerAddress: "owner1",
                DomainForwardRecordAddress: "address1",
                Data: grantUpdate.Data
            );
            var expectedGrantForeverEvent = new DomainGrantEvent(
                Id: "ignored",
                Block: block2,
                SourceAddress: "owner1",
                OperationGroupHash: grantForeverUpdate.OperationGroupHash,
                DomainName: grantForeverUpdate.Name,
                DurationInDays: null,
                DomainOwnerAddress: "owner1",
                DomainForwardRecordAddress: "address1",
                Data: grantForeverUpdate.Data
            );
            VerifyEventsInDb(new Event[] { buyEvent, settleEvent, claimEvent, expectedGrantEvent, expectedGrantForeverEvent }, excludingId: true);
        }

        [Test]
        public async Task UpdateOfferEvent()
        {
            //arrange
            var block1 = TestBlock.Get();
            var tokenId = 1;
            var notExistingTokenId = 2;
            var buyEvent = new DomainBuyEvent(
                Id: Event.GenerateId(),
                Block: block1,
                SourceAddress: "address1",
                OperationGroupHash: "ogh",
                DomainName: "test.domain",
                Price: 1,
                DurationInDays: 365,
                DomainOwnerAddress: "owner",
                DomainForwardRecordAddress: null,
                Data: new Dictionary<string, string>()
            );
            var existingOffer = TestOfferUpdate.GetSetOffer(OfferType.SellOffer, tokenId, initiatorAddress: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm");
            var existingBuyOffer = TestOfferUpdate.GetSetOffer(
                OfferType.BuyOffer,
                tokenId,
                initiatorAddress: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm"
            );
            var domainUpdate = TestDomainUpdate.Get(name: "test.domain", tokenId: tokenId);
            await StoreUpdatesAsync(new[] { buyEvent.AsUpdate(), domainUpdate, existingOffer, existingBuyOffer }, block1);

            //act
            var block2 = TestBlock.Get();
            var offerPlaced = TestEvent.GetOfferPlaced(block2, tokenId);
            var updatedOfferPlaced = TestEvent.GetOfferPlaced(block2, tokenId, sourceAddress: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm");
            var offerExecuted = TestEvent.GetOfferExecuted(block2, tokenId);
            var offerRemoved = TestEvent.GetOfferRemoved(block2, tokenId);
            var buyOfferPlaced = TestEvent.GetBuyOfferPlaced(block2, tokenId);
            var buyOfferExecuted = TestEvent.GetBuyOfferExecuted(block2, tokenId);
            var buyOfferRemoved = TestEvent.GetBuyOfferRemoved(block2, tokenId);
            var skipOfferWithoutExistingDomainPlaced = TestEvent.GetOfferPlaced(block2, notExistingTokenId);
            var blockUpdates = new[]
            {
                offerPlaced.AsUpdate(), updatedOfferPlaced.AsUpdate(), offerExecuted.AsUpdate(), offerRemoved.AsUpdate(),
                buyOfferPlaced.AsUpdate(), buyOfferExecuted.AsUpdate(), buyOfferRemoved.AsUpdate(), skipOfferWithoutExistingDomainPlaced.AsUpdate()
            };
            await StoreUpdatesAsync(blockUpdates, block2);

            //assert
            VerifyEventsInDb(
                new Event[]
                {
                    buyEvent,
                    offerPlaced with { DomainName = "test.domain" },
                    offerExecuted with { DomainName = "test.domain" },
                    offerRemoved with { DomainName = "test.domain" },
                    new OfferUpdatedEvent(
                        updatedOfferPlaced.Id,
                        updatedOfferPlaced.Block,
                        updatedOfferPlaced.SourceAddress,
                        updatedOfferPlaced.OperationGroupHash,
                        updatedOfferPlaced.TokenId,
                        updatedOfferPlaced.PriceWithoutFee,
                        updatedOfferPlaced.Fee,
                        updatedOfferPlaced.ExpiresAtUtc
                    ) { DomainName = "test.domain" },
                    buyOfferPlaced with { DomainName = "test.domain", DomainOwner = domainUpdate.Owner },
                    buyOfferExecuted with
                    {
                        DomainName = "test.domain",
                        //PriceWithoutFee,Fee fields are not updated because BuyOffer was placed in same block
                        PriceWithoutFee = 0,
                        Fee = 0
                    },
                    buyOfferRemoved with { DomainName = "test.domain" },
                    //skipOfferWithoutExistingDomainPlaced is skipped/not indexed
                }
            );
        }

        private void VerifyEventsInDb(IEnumerable<Event> expectedEvents, bool excludingId = false)
        {
            var dbEvents = DbContext.ListAll<Event>();
            dbEvents.Should().BeEquivalentTo(expectedEvents, o => (excludingId ? o.Excluding(e => e.Id) : o).RespectingRuntimeTypes());
        }

        private static IReadOnlyList<IBlockUpdate> AsUpdates(params Event[] events)
            => events.Select(e => e.AsUpdate()).ToList();
    }
}