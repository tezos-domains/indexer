﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public class BlockTests : UpdateTestBase
    {
        [Test]
        public async Task StoreBlockInDb()
        {
            //act
            var block = TestBlock.Get();
            await StoreUpdatesAsync(Array.Empty<IBlockUpdate>(), block);

            //assert
            var blocks = DbContext.ListAllByKey<Block>(block.Hash);
            blocks.Single().Should().BeEquivalentTo(block);
        }

        [Test]
        public async Task StoreBlockInDb_ShouldFail_WhenStoredBlockIsNotASuccessor()
        {
            //arrange
            var block = TestBlock.Get();
            await StoreUpdatesAsync(Array.Empty<IBlockUpdate>(), block);

            //act+assert
            //store same block twice
            Assert.ThrowsAsync<InvalidOperationException>(async () => await StoreUpdatesAsync(Array.Empty<IBlockUpdate>(), block));
            //store with lower level

            var block2 = new Block(block.Level - 1, "invalid_block", DateTime.UtcNow.Date, "bla");
            var updates2 = Array.Empty<IBlockUpdate>();
            Assert.ThrowsAsync<InvalidOperationException>(async () => await StoreUpdatesAsync(updates2, block2));
        }
    }
}