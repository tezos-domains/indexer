﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.Initialization;
using TezosDomains.Indexer.IntegrationTests.TestData;
using TezosDomains.Indexer.Services;
using TezosDomains.Indexer.Tezos;
using TezosDomains.TestUtilities;

namespace TezosDomains.Indexer.IntegrationTests.Tests.Updates
{
    public abstract class UpdateTestBase
    {
        private MongoDbConfiguration _config = null!;
        public MongoDbContext DbContext { get; private set; } = null!;
        public DefaultUpdateService DefaultUpdateService { get; private set; } = null!;
        public MongoClient Client { get; private set; } = null!;

        [SetUp]
        public async Task SetUpBase()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("testsettings.json")
                .AddUserSecrets<BlockTests>(optional: true)
                .AddEnvironmentVariables()
                .AddInMemoryCollection(new Dictionary<string, string> { { "TezosDomains:MongoDb:DatabaseName", $"t_{Guid.NewGuid()}" } })
                .Build();

            var hostEnvironment = Substitute.For<IHostEnvironment>();
            var startup = new Startup(configuration, hostEnvironment);
            var services = new ServiceCollection();
            startup.ConfigureServices(services);
            services.AddSingleton(typeof(ILogger<>), typeof(TestLogger<>));
            services.AddSingleton<IConfiguration>(configuration);
            services.AddSingleton(hostEnvironment);
            var serviceProvider = services.BuildServiceProvider();

            _config = serviceProvider.GetRequiredService<MongoDbConfiguration>();

            Client = serviceProvider.GetRequiredService<MongoClient>();
            DbContext = serviceProvider.GetRequiredService<MongoDbContext>();
            DefaultUpdateService = serviceProvider.GetRequiredService<DefaultUpdateService>();

            var dbInitializer = serviceProvider.GetRequiredService<MongoDbInitializer>();
            await dbInitializer.InitializeAsync("chainId", CancellationToken.None);
        }

        [TearDown]
        public async Task TearDownBase()
            => await Client.DropDatabaseAsync(_config.DatabaseName);

        protected async Task StoreUpdatesAsync(IReadOnlyList<IBlockUpdate> updates, Block? block = null, Block? previousBlock = null)
        {
            try
            {
                var args = new StoreUpdatesArguments(updates, block ?? TestBlock.Get(), previousBlock);
                await DefaultUpdateService.StoreUpdatesAsync(args, CancellationToken.None);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}