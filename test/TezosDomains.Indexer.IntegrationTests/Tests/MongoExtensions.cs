﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Filters;

namespace TezosDomains.Indexer.IntegrationTests.Tests
{
    public static class MongoExtensions
    {
        public static TDocument SingleByKey<TDocument>(this MongoDbContext dbContext, string key)
            where TDocument : IHasHistory
        {
            var builder = Builders<TDocument>.Filter;
            var filter = builder
                .ByKeys(new[] { key })
                .ExcludeHistory();
            return dbContext.Get<TDocument>().Find(filter).Single();
        }

        public static List<TDocument> ListAllByKey<TDocument>(this MongoDbContext dbContext, params string[] keys)
            where TDocument : IHasDbCollection
        {
            var filter = Builders<TDocument>.Filter.ByKeys(keys);
            return dbContext.Get<TDocument>().Find(filter).ToList();
        }

        public static TDocument SingleBy<TDocument>(this MongoDbContext dbContext, Expression<Func<TDocument, bool>> filter)
            where TDocument : IHasDbCollection
            => dbContext.Get<TDocument>().Find(filter).Single();

        public static List<TDocument> ListAll<TDocument>(this MongoDbContext dbContext, Expression<Func<TDocument, bool>>? filter = null)
            where TDocument : IHasDbCollection
            => dbContext.Get<TDocument>().Find(filter ?? (d => true)).ToList();
    }
}