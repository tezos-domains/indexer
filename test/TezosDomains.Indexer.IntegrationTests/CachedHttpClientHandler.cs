﻿using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Indexer.IntegrationTests
{
    public sealed class CachedHttpClientHandler : DelegatingHandler
    {
        private readonly string _networkName;

        public CachedHttpClientHandler(string networkName)
            => _networkName = networkName;

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellation)
        {
            if (request.Method != HttpMethod.Get || request.Headers.Accept.All(a => a.MediaType != MediaTypeNames.Application.Json))
                return await base.SendAsync(request, cancellation);

            var cacheFilePath = Path.GetFullPath($"../../../HttpCache/{GetRequestId(request)}.gzip");
            if (!File.Exists(cacheFilePath))
            {
                var response = await base.SendAsync(request, cancellation);
                if (!response.IsSuccessStatusCode || response.Content.Headers.ContentType?.MediaType != MediaTypeNames.Application.Json)
                    return response;

                var contentBytes = await response.Content.ReadAsByteArrayAsync(cancellation);
                await File.WriteAllBytesAsync(cacheFilePath, Compress(contentBytes), cancellation);
            }

            var cachedBytes = await File.ReadAllBytesAsync(cacheFilePath, cancellation);
            var cachedResponse = new HttpResponseMessage(HttpStatusCode.OK);
            cachedResponse.Content = new ByteArrayContent(Decompress(cachedBytes));
            cachedResponse.Content.Headers.ContentType = MediaTypeHeaderValue.Parse(MediaTypeNames.Application.Json);

            return cachedResponse;
        }

        private string GetRequestId(HttpRequestMessage request)
            => _networkName + Regex.Replace(request.RequestUri?.AbsolutePath ?? "", "\\W", "_");

        private static byte[] Compress(byte[] bytes)
        {
            var result = new MemoryStream(bytes.Length);
            using (var gzip = new GZipStream(result, CompressionLevel.Optimal))
                gzip.Write(bytes, 0, bytes.Length);

            return result.ToArray();
        }

        private static byte[] Decompress(byte[] bytes)
        {
            var result = new MemoryStream(bytes.Length);
            using (var gzip = new GZipStream(new MemoryStream(bytes, false), CompressionMode.Decompress))
                gzip.CopyTo(result);

            return result.ToArray();
        }
    }
}