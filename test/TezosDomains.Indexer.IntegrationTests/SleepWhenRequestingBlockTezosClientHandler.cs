﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Indexer.IntegrationTests
{
    public sealed class SleepWhenRequestingBlockTezosClientHandler : DelegatingHandler
    {
        private static readonly Task<HttpResponseMessage> InfiniteTask = new TaskCompletionSource<HttpResponseMessage>().Task;
        private readonly int _blockLevel;

        public SleepWhenRequestingBlockTezosClientHandler(int blockLevel)
            => _blockLevel = blockLevel;

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellation)
            => request.RequestUri?.AbsolutePath.Contains($"/chains/main/blocks/{_blockLevel}") == true
                ? InfiniteTask
                : base.SendAsync(request, cancellation);
    }
}