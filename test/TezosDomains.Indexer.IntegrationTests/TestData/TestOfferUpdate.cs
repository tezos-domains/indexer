﻿using System;
using TezosDomains.Common;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.Indexer.Updates.Marketplace;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestOfferUpdate
    {
        public const string TOKEN_CONTRACT = "KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p";
        public const string MARKETPLACE_CONTRACT = "KT1UaPcfphum7ShyndWuC5uKSGACw3HUpjs5";

        public static SetOfferUpdate GetSetOffer(
            OfferType type,
            int? tokenId = null,
            string? initiatorAddress = null,
            DateTime? expiresAtUtc = null,
            string? operationGroupHash = null
        )
        {
            var price = TestRandom.GetDecimal();
            return new(
                type,
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                TokenContract: TOKEN_CONTRACT,
                TokenId: tokenId ?? TestRandom.GetInt(),
                InitiatorAddress: initiatorAddress ?? TestAddress.GetRandom(),
                PriceWithoutFee: price * 0.975m,
                Fee: price * 0.025m,
                ExpiresAtUtc: expiresAtUtc.ConvertNullToMax()
            );
        }

        public static ExecuteOfferUpdate GetExecuteOffer(
            OfferType type,
            int? tokenId = null,
            string? sellerAddress = null,
            string? buyerAddress = null,
            string? operationGroupHash = null
        )
        {
            var price = TestRandom.GetDecimal();
            return new(
                type,
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                TokenContract: TOKEN_CONTRACT,
                TokenId: tokenId ?? TestRandom.GetInt(),
                InitiatorAddress: (type == OfferType.SellOffer ? sellerAddress : buyerAddress) ?? TestAddress.GetRandom(),
                AcceptorAddress: (type == OfferType.SellOffer ? buyerAddress : sellerAddress) ?? TestAddress.GetRandom(),
                PriceWithoutFee: price * 0.975m,
                Fee: price * 0.025m
            );
        }

        public static RemoveOfferUpdate GetRemoveOffer(
            OfferType type,
            int? tokenId = null,
            string? initiatorAddress = null,
            string? operationGroupHash = null
        )
            => new(
                type,
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                TokenContract: TOKEN_CONTRACT,
                TokenId: tokenId ?? TestRandom.GetInt(),
                InitiatorAddress: initiatorAddress ?? TestAddress.GetRandom()
            );
    }
}