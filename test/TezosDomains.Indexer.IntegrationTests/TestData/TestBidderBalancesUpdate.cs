﻿using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestBidderBalancesUpdate
    {
        public static BidderBalancesUpdate Get(
            string? address = null,
            string? tldName = null,
            decimal? balance = -1,
            string? operationGroupHash = null
        )
            => new BidderBalancesUpdate(
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                Address: address ?? TestAddress.GetRandom(),
                TldName: tldName ?? TestDomainName.GetRandomLabel(),
                Balance: balance != -1 ? balance : TestRandom.GetDecimal()
            );
    }
}