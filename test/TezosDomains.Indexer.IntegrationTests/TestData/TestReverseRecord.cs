﻿using System;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestReverseRecord
    {
        public static ReverseRecord Get(
            string? address = null,
            string? name = null,
            string? validityKey = null,
            DateTime? expiresAtUtc = null
        )
        {
            return new ReverseRecord(
                Block: TestBlock.Get(),
                OperationGroupHash: TestRandom.GetString(),
                Address: address ?? TestAddress.GetRandom(),
                Name: name,
                Owner: TestAddress.GetRandom(),
                ExpiresAtUtc: expiresAtUtc.ConvertNullToMax(),
                ValidityKey: validityKey,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
        }
    }
}