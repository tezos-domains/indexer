﻿using System;
using System.Collections.Generic;
using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestDomainUpdate
    {
        public static DomainUpdate Get(
            string name,
            string? owner = null,
            string? address = null,
            string? validityKey = null,
            IReadOnlyDictionary<string, string>? data = null,
            string? operationGroupHash = null,
            string[]? operators = null,
            int? tokenId = null
        )
            => new DomainUpdate(
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                Name: name,
                Address: address,
                Owner: owner ?? TestAddress.GetRandom(),
                ValidityKey: validityKey,
                Data: data ?? new Dictionary<string, string> { { "test", TestRandom.GetString() } },
                TokenId: tokenId ?? TestRandom.GetInt(),
                Operators: operators ?? Array.Empty<string>()
            );
    }
}