﻿using System;
using System.Collections.Generic;
using TezosDomains.Common;
using TezosDomains.Data;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.TestUtilities.TestData;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestDomain
    {
        public static Domain Get(
            string? name = null,
            string? owner = null,
            DateTime? expiresAtUtc = null,
            string? validityKey = null,
            int? tokenId = null,
            string[]? operators = null,
            Auction? lastAuction = null
        )
        {
            name ??= TestDomainName.GetRandom();
            var (label, parent, ancestors, level, nameForOrdering) = DomainNameUtils.Parse(name);
            return new Domain(
                Block: TestBlock.Get(),
                OperationGroupHash: TestRandom.GetString(),
                Name: name,
                Address: null,
                Owner: owner ?? TestAddress.GetRandom(),
                Data: new Dictionary<string, string>(),
                TokenId: tokenId ?? (level == 2 ? TestRandom.GetInt() : null),
                Label: label,
                Ancestors: ancestors,
                ParentName: parent,
                ParentOwner: TestAddress.GetRandom(),
                ExpiresAtUtc: expiresAtUtc.ConvertNullToMax(),
                ValidityKey: validityKey,
                Level: level,
                NameForOrdering: nameForOrdering,
                Operators: operators.NullToEmpty(),
                LastAuction: lastAuction,
                ValidUntilBlockLevel: int.MaxValue,
                ValidUntilTimestamp: DateTime.MaxValue
            );
        }
    }
}