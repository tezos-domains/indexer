﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestEvent
    {
        public static AuctionBidEvent GetAuctionBid(
            BlockSlim? block = null,
            string? domainName = null
        )
            => new(
                Id: Event.GenerateId(),
                Block: block ?? TestBlock.Get(),
                SourceAddress: TestAddress.GetRandom(),
                OperationGroupHash: TestRandom.GetString(),
                DomainName: domainName ?? TestDomainName.GetRandom(),
                BidAmount: TestRandom.GetDecimal(),
                TransactionAmount: TestRandom.GetInt(),
                PreviousBidderAddress: null,
                PreviousBidAmount: null
            );

        public static OfferPlacedEvent GetOfferPlaced(Block block, int tokenId, string? sourceAddress = null)
            => new(
                Id: TestRandom.GetString(),
                Block: block,
                SourceAddress: sourceAddress ?? TestAddress.GetRandom(),
                OperationGroupHash: TestRandom.GetString(),
                TokenId: tokenId,
                PriceWithoutFee: TestRandom.GetDecimal(),
                Fee: TestRandom.GetDecimal(),
                ExpiresAtUtc: TestDateTime.GetFutureOrMax()
            );

        public static OfferExecutedEvent GetOfferExecuted(Block block, int tokenId)
            => new(
                Id: TestRandom.GetString(),
                Block: block,
                SourceAddress: TestAddress.GetRandom(),
                OperationGroupHash: TestRandom.GetString(),
                TokenId: tokenId,
                InitiatorAddress: TestAddress.GetRandom(),
                PriceWithoutFee: TestRandom.GetDecimal(),
                Fee: TestRandom.GetDecimal()
            );

        public static OfferRemovedEvent GetOfferRemoved(Block block, int tokenId)
            => new(
                Id: TestRandom.GetString(),
                Block: block,
                SourceAddress: TestAddress.GetRandom(),
                OperationGroupHash: TestRandom.GetString(),
                TokenId: tokenId
            );

        public static BuyOfferPlacedEvent GetBuyOfferPlaced(Block block, int tokenId, string? sourceAddress = null)
            => new(
                Id: TestRandom.GetString(),
                Block: block,
                SourceAddress: sourceAddress ?? TestAddress.GetRandom(),
                OperationGroupHash: TestRandom.GetString(),
                TokenId: tokenId,
                PriceWithoutFee: TestRandom.GetDecimal(),
                Fee: TestRandom.GetDecimal(),
                ExpiresAtUtc: TestDateTime.GetFutureOrMax()
            );

        public static BuyOfferExecutedEvent GetBuyOfferExecuted(Block block, int tokenId)
            => new(
                Id: TestRandom.GetString(),
                Block: block,
                SourceAddress: TestAddress.GetRandom(),
                OperationGroupHash: TestRandom.GetString(),
                TokenId: tokenId,
                InitiatorAddress: TestAddress.GetRandom(),
                PriceWithoutFee: TestRandom.GetDecimal(),
                Fee: TestRandom.GetDecimal()
            );

        public static BuyOfferRemovedEvent GetBuyOfferRemoved(Block block, int tokenId)
            => new(
                Id: TestRandom.GetString(),
                Block: block,
                SourceAddress: TestAddress.GetRandom(),
                OperationGroupHash: TestRandom.GetString(),
                TokenId: tokenId
            );
    }
}