﻿using System;
using TezosDomains.Common;
using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestValidityUpdate
    {
        public static ValidityUpdate Get(
            string validityKey,
            DateTime? expiresAtUtc = null,
            string? operationGroupHash = null
        )
            => new ValidityUpdate(
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                ValidityKey: validityKey,
                ExpiresAtUtc: expiresAtUtc.ConvertNullToMax()
            );
    }
}