﻿using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestReverseRecordUpdate
    {
        public static ReverseRecordUpdate Get(
            string? address = null,
            string? owner = null,
            string? name = null,
            string? operationGroupHash = null
        )
            => new ReverseRecordUpdate(
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                Address: address ?? TestAddress.GetRandom(),
                Owner: owner ?? TestAddress.GetRandom(),
                Name: name
            );
    }
}