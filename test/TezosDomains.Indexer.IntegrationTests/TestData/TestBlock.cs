﻿using TezosDomains.Data.Models;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestBlock
    {
        public static Block Get()
        {
            var level = TestCounter.GetNext();
            return new Block(
                Level: level,
                Hash: TestRandom.GetString(),
                Timestamp: TestDateTime.Today.AddSeconds(level),
                Predecessor: TestRandom.GetString()
            );
        }
    }
}