﻿using System;
using TezosDomains.Indexer.Updates;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Indexer.IntegrationTests.TestData
{
    public static class TestAuctionUpdate
    {
        public static AuctionBidUpdate GetBid(
            string? domainName = null,
            string? bidder = null,
            decimal? amount = null,
            DateTime? auctionEndInUtc = null,
            DateTime? ownedUntilUtc = null,
            string? operationGroupHash = null
        )
            => new(
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                DomainName: domainName ?? TestDomainName.GetRandom(),
                Bidder: bidder ?? TestAddress.GetRandom(),
                Amount: amount ?? TestRandom.GetDecimal(),
                EndsAtUtc: auctionEndInUtc ?? TestDateTime.GetFuture(),
                OwnedUntilUtc: ownedUntilUtc ?? TestDateTime.GetFuture()
            );

        public static AuctionSettledUpdate GetSettle(
            string? domainName = null,
            string? operationGroupHash = null
        )
            => new(
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                DomainName: domainName ?? TestDomainName.GetRandom()
            );
    }
}