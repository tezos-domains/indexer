﻿using FluentAssertions;
using NUnit.Framework;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Tests
{
    public class ExpressionHelperTests
    {
        [Test]
        public void GetFieldName_ShouldWorkForArrayAccessorWithProperty()
        {
            var fieldName = ExpressionHelper.GetFieldName<Auction, decimal>(a => a.Bids[0].Amount);
            fieldName.Should().Be("Bids.0.Amount");
        }
    }
}