﻿using FluentAssertions;
using MongoDB.Bson.Serialization;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Initialization;

namespace TezosDomains.Data.MongoDb.Tests
{
    public class MongoDbSetupTests
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
            => MongoDbSetup.InitializeMappingStructures();

        public static IEnumerable<Type> GetDocumentTypes()
            => typeof(IHasDbCollection).Assembly.GetTypes()
                .Where(t => typeof(IHasDbCollection).IsAssignableFrom(t) && t.IsClass && !t.IsAbstract && !t.IsGenericType);

        [TestCaseSource(nameof(GetDocumentTypes))]
        public void DocumentTypes_ShouldHaveMappingRegistered(Type documentType)
            => BsonClassMap.IsClassMapRegistered(documentType).Should().BeTrue();
    }
}