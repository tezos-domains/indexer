using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.TestUtilities;

namespace TezosDomains.Api.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public sealed class CommonTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            DbContext.Insert(
                TestDomain.Get("2.test.tez", address: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm"),
                TestDomain.Get("test.tez", expiresAtUtc: GetTime(2150, 2, 3)),
                TestDomain.Get("3.other.tez", address: null),
                TestDomain.Get("1.test.tez", address: null),
                TestDomain.Get("expired.test.tez", expiresAtUtc: GetTime(2001, 2, 3))
            );
        }

        [TestCase("equalTo", "name", @"""1.test.tez""", new[] { "1.test.tez" })]
        [TestCase("notEqualTo", "name", @"""1.test.tez""", new[] { "3.other.tez", "test.tez", "2.test.tez" })]
        [TestCase("startsWith", "name", @"""1.""", new[] { "1.test.tez" })]
        [TestCase("endsWith", "name", @"""ther.tez""", new[] { "3.other.tez" })]
        [TestCase("like", "name", @"""other""", new[] { "3.other.tez" })]
        [TestCase("isNull", "address", "true", new[] { "3.other.tez", "1.test.tez" })]
        [TestCase("isNull", "address", "false", new[] { "test.tez", "2.test.tez" })]
        [TestCase("in", "name", @"[""1.test.tez"", ""2.test.tez""]", new[] { "1.test.tez", "2.test.tez" })]
        [TestCase("notIn", "name", @"[""1.test.tez"", ""2.test.tez""]", new[] { "3.other.tez", "test.tez" })]
        public Task StringFilter_ShouldFilterCorrectly(string @operator, string field, string value, string[] expectedNames)
            => RunDomainsTest(@operator, field, value, expectedNames);

        [TestCase("startsWith", "address", "TZ3", new[] { "2.test.tez" })]
        public Task AddressFilter_ShouldFilterCorrectly(string @operator, string field, string value, string[] expectedNames)
            => RunDomainsTest(@operator, field, value, expectedNames);

        [TestCase("equalTo", "level", "2", new[] { "test.tez" })]
        [TestCase("notEqualTo", "level", "2", new[] { "3.other.tez", "1.test.tez", "2.test.tez" })]
        [TestCase("lessThan", "level", "3", new[] { "test.tez" })]
        [TestCase("lessThanOrEqualTo", "level", "2", new[] { "test.tez" })]
        [TestCase("greaterThanOrEqualTo", "level", "3", new[] { "3.other.tez", "1.test.tez", "2.test.tez" })]
        [TestCase("greaterThan", "level", "2", new[] { "3.other.tez", "1.test.tez", "2.test.tez" })]
        [TestCase("in", "level", "[2]", new[] { "test.tez" })]
        [TestCase("notIn", "level", "[2]", new[] { "3.other.tez", "1.test.tez", "2.test.tez" })]
        public Task IntFilter_ShouldFilterCorrectly(string @operator, string field, string value, string[] expectedNames)
            => RunDomainsTest(@operator, field, value, expectedNames);

        [TestCase("equalTo", @"""2001-02-03T00:00:00Z""", new[] { "expired.test.tez" })]
        [TestCase("notEqualTo", @"""2001-02-03T00:00:00Z""", new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" })]
        [TestCase("lessThan", @"""2150-02-03T00:00:00Z""", new[] { "expired.test.tez" })]
        [TestCase("lessThanOrEqualTo", @"""2001-02-03T00:00:00Z""", new[] { "expired.test.tez" })]
        [TestCase("greaterThanOrEqualTo", @"""2150-02-03T00:00:00Z""", new[] { "test.tez" })]
        [TestCase("greaterThan", @"""2001-02-03T00:00:00Z""", new[] { "test.tez" })]
        [TestCase("isNull", "true", new[] { "3.other.tez", "1.test.tez", "2.test.tez" })]
        [TestCase("isNull", "false", new[] { "test.tez", "expired.test.tez" })]
        [TestCase("in", @"[""2001-02-03T00:00:00Z""]", new[] { "expired.test.tez" })]
        [TestCase("notIn", @"[""2001-02-03T00:00:00Z""]", new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" })]
        public Task DateTimeFilter_ShouldFilterCorrectly(string @operator, string value, string[] expectedNames)
            => RunDomainsTest(@$"where: {{ validity: ALL, expiresAtUtc: {{ {@operator}: {value} }}}}", expectedNames);

        [Test]
        public async Task RelayConnection_ShouldSupportPaging()
        {
            const string orderArgs = "order: { field: NAME direction: DESC }";
            var allDomains = await GetDomainsAsync(orderArgs);

            var expectedNames = new[] { "test.tez", "3.other.tez", "2.test.tez", "1.test.tez" };
            (allDomains.Items?.Select(i => i?.Name)).Should().Equal(expectedNames);
            (allDomains.Edges?.Select(e => e.Node?.Name)).Should().Equal(expectedNames);
            allDomains.Edges!.ForEach(e => e.Cursor.Should().NotBeNullOrWhiteSpace());
            allDomains.Edges.Select(e => e.Cursor).Should().OnlyHaveUniqueItems();
            allDomains.TotalCount.Should().Be(4);
            allDomains.PageInfo.Should()
                .BeEquivalentTo(
                    new PageInfo
                    {
                        StartCursor = allDomains.Edges[0].Cursor,
                        EndCursor = allDomains.Edges[^1].Cursor,
                    }
                );

            await VerifyPaging($@"after: ""{allDomains.Edges[0].Cursor}"", first: 2");
            await VerifyPaging($@"before: ""{allDomains.Edges[^1].Cursor}"", last: 2");

            async Task VerifyPaging(string pagingArgs)
            {
                var domainsPage = await GetDomainsAsync($"{pagingArgs}, {orderArgs}");

                domainsPage.Items!.Select(d => d?.Name).Should().Equal("3.other.tez", "2.test.tez");
                domainsPage.PageInfo.Should()
                    .BeEquivalentTo(
                        new PageInfo
                        {
                            StartCursor = allDomains.Edges[1].Cursor,
                            EndCursor = allDomains.Edges[2].Cursor,
                            HasPreviousPage = true,
                            HasNextPage = true,
                        }
                    );
            }
        }

        [Test]
        public Task NestedFilters_ShouldSupportOrOperator()
            => RunDomainsTest(
                @"where: { or: [{ name: { startsWith: ""1"" }}, { name: { startsWith: ""2"" }}]}",
                expectedNames: new[] { "1.test.tez", "2.test.tez" }
            );

        [Test]
        public Task NestedFilters_ShouldSupportAndOperator()
            => RunDomainsTest(
                @"where: { and: [{ name: { endsWith: ""test.tez"" }}, { address: { isNull: true }}]}",
                expectedNames: new[] { "1.test.tez" }
            );

        public Task RunDomainsTest(string @operator, string field, string value, string[] expectedNames)
            => RunDomainsTest(@$"where: {{ {field}: {{ {@operator}: {value} }}}}", expectedNames);

        private async Task RunDomainsTest(string queryArgs, string[] expectedNames)
        {
            var domains = await GetDomainsAsync(queryArgs);

            (domains.Items?.Select(d => d?.Name)).Should().Equal(expectedNames);
        }

        private Task<Connection<DomainDto>> GetDomainsAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<DomainDto>>($"domains({queryArgs}) {Edges(DomainDto.FieldsQuery)}");
    }
}