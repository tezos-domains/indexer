﻿using FluentAssertions;
using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.SystemTextJson;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Utils;

namespace TezosDomains.Api.IntegrationTests
{
    public abstract class TestBase
    {
        private static readonly string AppName = Assembly.GetAssembly(typeof(TestBase))!.GetName().Name!;
        private static int _portCounter = 5000;

        private string _hostUrl = null!;
        private IHost _host = null!;
        protected MongoDbContext DbContext = null!;
        protected readonly DateTime Now = DateTime.UtcNow;

        [OneTimeSetUp]
        public async Task OneTimeSetupBase()
        {
            // start our host
            _hostUrl = "http://localhost:" + _portCounter++;
            _host = Program.CreateHostBuilder()
                .ConfigureAppConfiguration(
                    (ctx, builder) =>
                    {
                        ctx.HostingEnvironment.ApplicationName = AppName;

                        // force testsettings.json and custom user secrets to avoid loading any of the original settings for the project
                        builder.Sources.Clear();
                        builder
                            .AddJsonFile("testsettings.json")
                            .AddUserSecrets<Program>(optional: true)
                            .AddEnvironmentVariables()
                            .AddInMemoryCollection(
                                new Dictionary<string, string>
                                {
                                    { "Urls", _hostUrl },
                                    { "TezosDomains:MongoDb:DatabaseName", $"t_{Guid.NewGuid()}" }
                                }
                            );
                    }
                )
                .Build();
            await _host.StartAsync();

            DbContext = _host.Services.GetRequiredService<MongoDbContext>();
        }

        [OneTimeTearDown]
        public async Task OneTimeTeardown()
        {
            await DropDatabaseAsync();
            await _host.StopAsync();
            _host.Dispose();
        }

        private async Task DropDatabaseAsync()
        {
            var config = _host.Services.GetRequiredService<MongoDbConfiguration>();
            var client = _host.Services.GetRequiredService<MongoClient>();
            await client.DropDatabaseAsync(config.DatabaseName);
        }

        public sealed class ResponseDto<TData>
        {
            public TData Data { get; set; } = default!;
        }

        protected async Task<GraphQLResponse<TResponse>> SendRawToGraphQLAsync<TResponse>(string query)
        {
            try
            {
                Console.WriteLine($"Executing Graph query: {query}");
                using var client = new GraphQLHttpClient(
                    _hostUrl + "/graphql",
                    new SystemTextJsonSerializer { Options = { NumberHandling = JsonNumberHandling.AllowReadingFromString } }
                );
                var request = new GraphQLRequest { Query = query };

                return await client.SendQueryAsync<TResponse>(request);
            }
            catch (GraphQLHttpRequestException ex)
            {
                throw new Exception($"{ex.Content}{Environment.NewLine}{ex}".Trim());
            }
        }

        protected async Task<TData> SendToGraphQLAsync<TData>(string query, string? expectedError = null)
        {
            var response = await SendRawToGraphQLAsync<ResponseDto<TData>>($"query Test {{ data: {query} }}");

            var errors = response.Errors?.Select(e => e.Message).Join() ?? "";
            if (expectedError != null)
                errors.Should().Contain(expectedError);
            else
                errors.Should().BeEmpty();

            return response.Data.Data;
        }

        protected static DateTime GetTime(int year, int month, int day)
            => new DateTime(year, month, day, 0, 0, 0, 0, DateTimeKind.Utc);

        protected static TDocument Historize<TDocument>(TDocument document, BlockSlim block, BlockSlim validUntil)
            where TDocument : HasHistory<TDocument>
            => document with
            {
                Block = block,
                ValidUntilBlockLevel = validUntil.Level,
                ValidUntilTimestamp = validUntil.Timestamp,
            };

        public static string Edges(string fields)
            => $@"{{
                edges {{
                    node {fields}
                    cursor
                }}
                totalCount
                pageInfo {{
                    hasNextPage
                    hasPreviousPage
                    startCursor
                    endCursor
                }}
            }}";

        protected static string AtBlockFilter(int? level, int? daysOffset)
        {
            DateTime? timestamp = daysOffset != null ? DateTime.UtcNow.AddSeconds(1).AddDays(daysOffset.Value) : null;
            var levelStr = level?.ToString() ?? "null";

            return $"atBlock: {{ timestamp: {ToGraphQL(timestamp)}, level: {levelStr} }}";
        }

        protected static string ToGraphQL(DateTime? time)
            => time != null ? @$"""{time:O}""" : "null";
    }
}