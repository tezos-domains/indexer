﻿using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Events.Marketplace;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public sealed class EventQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            #region Insert events

            DbContext.Insert<Event>(
                new AuctionWithdrawEvent(
                    Id: "e-2",
                    Block: new BlockSlim(Level: 2, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(98)),
                    SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    DestinationAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash: "ogh2",
                    TldName: "bbb",
                    WithdrawnAmount: 1000
                ),
                new AuctionBidEvent( // Different position to verify ordering by timestamp.
                    Id: "e-1",
                    Block: new BlockSlim(Level: 1, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(99)),
                    SourceAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    OperationGroupHash: "ogh1",
                    DomainName: "auction-bid.aaa",
                    BidAmount: 1,
                    TransactionAmount: 1,
                    PreviousBidderAddress: null,
                    PreviousBidAmount: null
                ),
                new AuctionEndEvent(
                    Id: "e-3",
                    Block: new BlockSlim(Level: 3, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(97)),
                    SourceAddress: "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    DomainName: "ended.aaa",
                    WinningBid: 2,
                    Participants: new[] { "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU", "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL" }
                ),
                new AuctionSettleEvent(
                    Id: "e-4",
                    Block: new BlockSlim(Level: 4, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(96)),
                    SourceAddress: "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    OperationGroupHash: "ogh5",
                    DomainName: "auction-settle.ddd",
                    DomainOwnerAddress: "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    DomainForwardRecordAddress: "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    Data: DataItemDto.GetTestInput(testSeed: "d1"),
                    RegistrationDurationInDays: 365,
                    WinningBid: 100
                ),
                new DomainBuyEvent(
                    Id: "e-5",
                    Block: new BlockSlim(Level: 5, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(95)),
                    SourceAddress: "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash: "ogh5",
                    DomainName: "domain-buy.eee",
                    Price: 4,
                    DurationInDays: 365,
                    DomainOwnerAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    DomainForwardRecordAddress: null,
                    Data: DataItemDto.GetTestInput(testSeed: "d2")
                ),
                new DomainRenewEvent(
                    Id: "e-6",
                    Block: new BlockSlim(Level: 6, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(94)),
                    SourceAddress: "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9",
                    OperationGroupHash: "ogh6",
                    DomainName: "domain-renew.fff",
                    Price: 5,
                    DurationInDays: 365 * 2
                ),
                new DomainCommitEvent(
                    Id: "e-7",
                    Block: new BlockSlim(Level: 7, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(93)),
                    SourceAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    OperationGroupHash: "ogh8",
                    CommitmentHash: "cmt-haha"
                ),
                new DomainUpdateEvent(
                    Id: "e-8",
                    Block: new BlockSlim(Level: 8, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(92)),
                    SourceAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    OperationGroupHash: "ogh9",
                    DomainName: "domain-update.ggg",
                    DomainOwnerAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    DomainForwardRecordAddress: null,
                    Data: DataItemDto.GetTestInput(testSeed: "d3")
                ),
                new DomainSetChildRecordEvent(
                    Id: "e-9",
                    Block: new BlockSlim(Level: 9, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(91)),
                    SourceAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    OperationGroupHash: "ogh10",
                    DomainName: "domain-set-child-record.hhh",
                    DomainOwnerAddress: "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    DomainForwardRecordAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    Data: DataItemDto.GetTestInput(testSeed: "d4"),
                    IsNewRecord: true
                ),
                new ReverseRecordClaimEvent(
                    Id: "e-10",
                    Block: new BlockSlim(Level: 10, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(90)),
                    SourceAddress: "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    OperationGroupHash: "ogh11",
                    Name: null,
                    ReverseRecordOwnerAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7"
                ),
                new ReverseRecordUpdateEvent(
                    Id: "e-11",
                    Block: new BlockSlim(Level: 11, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(89)),
                    SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash: "ogh12",
                    Name: "reverse-record.iii",
                    ReverseRecordOwnerAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    ReverseRecordAddress: "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU"
                ),
                new AuctionBidEvent(
                    Id: "e-12",
                    Block: new BlockSlim(Level: 12, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(88)),
                    SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash: "ogh13",
                    DomainName: "auction-bid.aaa",
                    BidAmount: 2,
                    TransactionAmount: 2,
                    PreviousBidderAddress: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    PreviousBidAmount: 1
                ),
                new DomainGrantEvent(
                    Id: "e-13",
                    Block: new BlockSlim(Level: 13, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(87)),
                    SourceAddress: "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash: "ogh14",
                    DomainName: "domain-grant.eee",
                    DurationInDays: 365,
                    DomainOwnerAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    DomainForwardRecordAddress: null,
                    Data: DataItemDto.GetTestInput(testSeed: "d5")
                ),
                new DomainTransferEvent(
                    Id: "e-14",
                    Block: new BlockSlim(Level: 14, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(86)),
                    SourceAddress: "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash: "ogh15",
                    DomainName: "domain-transfer.ttt",
                    NewOwner: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL"
                ),
                new DomainUpdateOperatorsEvent(
                    Id: "e-15",
                    Block: new BlockSlim(Level: 15, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(85)),
                    SourceAddress: "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash: "ogh16",
                    DomainName: "domain-update-operators.ttt",
                    Operators: new[] { "KT1TV6QNrRfe9GZycNJNrcDhPa7zC3sT3ajM" }
                ),
                new OfferPlacedEvent(
                        Id: "e-16",
                        Block: new BlockSlim(Level: 16, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(84)),
                        SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                        OperationGroupHash: "ogh17",
                        TokenId: 123,
                        PriceWithoutFee: 10000m,
                        Fee: 250m,
                        ExpiresAtUtc: GetTime(2050, 1, 1)
                    )
                    { DomainName = "offer.yyy" },
                new OfferExecutedEvent(
                        Id: "e-17",
                        Block: new BlockSlim(Level: 17, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(83)),
                        SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                        OperationGroupHash: "ogh18",
                        TokenId: 123,
                        InitiatorAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                        PriceWithoutFee: 97m,
                        Fee: 3m
                    )
                    { DomainName = "offer.yyy" },
                new OfferRemovedEvent(
                        Id: "e-18",
                        Block: new BlockSlim(Level: 18, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(82)),
                        SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                        OperationGroupHash: "ogh19",
                        TokenId: 123
                    )
                    { DomainName = "offer.yyy" },
                new OfferUpdatedEvent(
                        Id: "e-19",
                        Block: new BlockSlim(Level: 19, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(81)),
                        SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                        OperationGroupHash: "ogh20",
                        TokenId: 124,
                        PriceWithoutFee: 1000m,
                        Fee: 25m,
                        ExpiresAtUtc: GetTime(2050, 1, 1)
                    )
                    { DomainName = "offer.zzz" },
                new DomainClaimEvent(
                    Id: "e-20",
                    Block: new BlockSlim(Level: 20, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(80)),
                    SourceAddress: "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash: "ogh21",
                    DomainName: "domain-claim.dev",
                    DomainOwnerAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    DomainForwardRecordAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7"
                ),
                new BuyOfferPlacedEvent(
                        Id: "e-21",
                        Block: new BlockSlim(Level: 21, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(79)),
                        SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                        OperationGroupHash: "ogh22",
                        TokenId: 123,
                        PriceWithoutFee: 10000m,
                        Fee: 250m,
                        ExpiresAtUtc: GetTime(2050, 1, 1)
                    )
                    { DomainName = "offer.yyy", DomainOwner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL" },
                new BuyOfferExecutedEvent(
                        Id: "e-22",
                        Block: new BlockSlim(Level: 22, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(78)),
                        SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                        OperationGroupHash: "ogh23",
                        TokenId: 123,
                        InitiatorAddress: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                        PriceWithoutFee: 97m,
                        Fee: 3m
                    )
                    { DomainName = "offer.yyy" },
                new BuyOfferRemovedEvent(
                        Id: "e-23",
                        Block: new BlockSlim(Level: 23, Hash: "h", Timestamp: TestDateTime.Today.AddMinutes(77)),
                        SourceAddress: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                        OperationGroupHash: "ogh24",
                        TokenId: 123
                    )
                    { DomainName = "offer.yyy" }
            );

            DbContext.Insert(
                TestDomain.Get("valid.domain", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", expiresAtUtc: TestDateTime.Today.AddYears(1)),
                TestDomain.Get("expired.domain", "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7", expiresAtUtc: TestDateTime.Today.AddDays(-1))
            );
            DbContext.Insert(
                TestReverseRecord.Get(
                    address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    name: "valid.domain",
                    expiresAtUtc: TestDateTime.Today.AddYears(1),
                    operationGroupHash: "ogh-rr-0"
                ), //valid rr with valid domain
                TestReverseRecord.Get(
                    address: "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    name: "expired.domain",
                    expiresAtUtc: TestDateTime.Today.AddDays(-1)
                ), //invalid reverse record
                TestReverseRecord.Get(
                    address: "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    operationGroupHash: "ogh-rr-2"
                ), //valid rr without domain
                TestReverseRecord.Get(
                    address: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    operationGroupHash: "ogh-rr-3"
                )
            );

            #endregion
        }

        [Test]
        public async Task ShouldLoadAllFieldsOrderedCorrectly()
        {
            #region Expected events

            var expectedReverseRecord0 = ReverseRecordDto.GetExpected(
                address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                name: "valid.domain",
                owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                expiresAtUtc: TestDateTime.Today.AddYears(1),
                hasOwnerReverseRecord: true,
                operationGroupHash: "ogh-rr-0"
            );
            var expectedReverseRecord2 = ReverseRecordDto.GetExpected(
                address: "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                hasOwnerReverseRecord: true,
                operationGroupHash: "ogh-rr-2"
            );
            var expectedReverseRecord3 = ReverseRecordDto.GetExpected(
                address: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                hasOwnerReverseRecord: true,
                operationGroupHash: "ogh-rr-3"
            );
            var expectedEvents = new[]
            {
                new EventDto
                {
                    Id = "AuctionBidEvent:e-1",
                    TypeName = "AuctionBidEvent",
                    Type = "AUCTION_BID_EVENT",
                    Block = BlockDto.GetExpected(level: 1, hash: "h", timestamp: TestDateTime.Today.AddMinutes(99)),
                    SourceAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    SourceAddressReverseRecord = expectedReverseRecord0,
                    OperationGroupHash = "ogh1",
                    DomainName = "auction-bid.aaa",
                    BidAmount = 1,
                    TransactionAmount = 1,
                },
                new EventDto
                {
                    Id = "AuctionWithdrawEvent:e-2",
                    TypeName = "AuctionWithdrawEvent",
                    Type = "AUCTION_WITHDRAW_EVENT",
                    Block = BlockDto.GetExpected(level: 2, hash: "h", timestamp: TestDateTime.Today.AddMinutes(98)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh2",
                    TldName = "bbb",
                    WithdrawnAmount = 1000,
                },
                new EventDto
                {
                    Id = "AuctionEndEvent:e-3",
                    TypeName = "AuctionEndEvent",
                    Type = "AUCTION_END_EVENT",
                    Block = BlockDto.GetExpected(level: 3, hash: "h", timestamp: TestDateTime.Today.AddMinutes(97)),
                    SourceAddress = "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    SourceAddressReverseRecord = expectedReverseRecord2,
                    DomainName = "ended.aaa",
                    WinningBid = 2,
                    Participants = new[]
                    {
                        new AuctionParticipantDto
                        {
                            Id = "AuctionParticipant:e-3:tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                            TypeName = "AuctionParticipant",
                            Address = "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU"
                        },
                        new AuctionParticipantDto
                        {
                            Id = "AuctionParticipant:e-3:tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                            TypeName = "AuctionParticipant",
                            Address = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL"
                        }
                    }
                },
                new EventDto
                {
                    Id = "AuctionSettleEvent:e-4",
                    TypeName = "AuctionSettleEvent",
                    Type = "AUCTION_SETTLE_EVENT",
                    Block = BlockDto.GetExpected(level: 4, hash: "h", timestamp: TestDateTime.Today.AddMinutes(96)),
                    SourceAddress = "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    OperationGroupHash = "ogh5",
                    DomainName = "auction-settle.ddd",
                    DomainOwnerAddress = "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    DomainForwardRecordAddress = "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    Data = DataItemDto.GetExpected(parentId: "AuctionSettleEvent:e-4", testSeed: "d1"),
                    RegistrationDurationInDays = 365,
                    WinningBid = 100,
                },
                new EventDto
                {
                    Id = "DomainBuyEvent:e-5",
                    TypeName = "DomainBuyEvent",
                    Type = "DOMAIN_BUY_EVENT",
                    Block = BlockDto.GetExpected(level: 5, hash: "h", timestamp: TestDateTime.Today.AddMinutes(95)),
                    SourceAddress = "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash = "ogh5",
                    DomainName = "domain-buy.eee",
                    Price = 4,
                    DurationInDays = 365,
                    DomainOwnerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    DomainForwardRecordAddress = null,
                    Data = DataItemDto.GetExpected(parentId: "DomainBuyEvent:e-5", testSeed: "d2"),
                },
                new EventDto
                {
                    Id = "DomainRenewEvent:e-6",
                    TypeName = "DomainRenewEvent",
                    Type = "DOMAIN_RENEW_EVENT",
                    Block = BlockDto.GetExpected(level: 6, hash: "h", timestamp: TestDateTime.Today.AddMinutes(94)),
                    SourceAddress = "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9",
                    OperationGroupHash = "ogh6",
                    DomainName = "domain-renew.fff",
                    Price = 5,
                    DurationInDays = 365 * 2
                },
                new EventDto
                {
                    Id = "DomainCommitEvent:e-7",
                    TypeName = "DomainCommitEvent",
                    Type = "DOMAIN_COMMIT_EVENT",
                    Block = BlockDto.GetExpected(level: 7, hash: "h", timestamp: TestDateTime.Today.AddMinutes(93)),
                    SourceAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    SourceAddressReverseRecord = expectedReverseRecord0,
                    OperationGroupHash = "ogh8",
                    CommitmentHash = "cmt-haha"
                },
                new EventDto
                {
                    Id = "DomainUpdateEvent:e-8",
                    TypeName = "DomainUpdateEvent",
                    Type = "DOMAIN_UPDATE_EVENT",
                    Block = BlockDto.GetExpected(level: 8, hash: "h", timestamp: TestDateTime.Today.AddMinutes(92)),
                    SourceAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    SourceAddressReverseRecord = expectedReverseRecord0,
                    OperationGroupHash = "ogh9",
                    DomainName = "domain-update.ggg",
                    DomainOwnerAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    Data = DataItemDto.GetExpected(parentId: "DomainUpdateEvent:e-8", testSeed: "d3"),
                },
                new EventDto
                {
                    Id = "DomainSetChildRecordEvent:e-9",
                    TypeName = "DomainSetChildRecordEvent",
                    Type = "DOMAIN_SET_CHILD_RECORD_EVENT",
                    Block = BlockDto.GetExpected(level: 9, hash: "h", timestamp: TestDateTime.Today.AddMinutes(91)),
                    SourceAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    SourceAddressReverseRecord = expectedReverseRecord0,
                    OperationGroupHash = "ogh10",
                    DomainName = "domain-set-child-record.hhh",
                    DomainOwnerAddress = "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    DomainForwardRecordAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    Data = DataItemDto.GetExpected(parentId: "DomainSetChildRecordEvent:e-9", testSeed: "d4"),
                    IsNewRecord = true
                },
                new EventDto
                {
                    Id = "ReverseRecordClaimEvent:e-10",
                    TypeName = "ReverseRecordClaimEvent",
                    Type = "REVERSE_RECORD_CLAIM_EVENT",
                    Block = BlockDto.GetExpected(level: 10, hash: "h", timestamp: TestDateTime.Today.AddMinutes(90)),
                    SourceAddress = "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    SourceAddressReverseRecord = expectedReverseRecord2,
                    OperationGroupHash = "ogh11",
                    ReverseRecordOwnerAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7"
                },
                new EventDto
                {
                    Id = "ReverseRecordUpdateEvent:e-11",
                    TypeName = "ReverseRecordUpdateEvent",
                    Type = "REVERSE_RECORD_UPDATE_EVENT",
                    Block = BlockDto.GetExpected(level: 11, hash: "h", timestamp: TestDateTime.Today.AddMinutes(89)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh12",
                    Name = "reverse-record.iii",
                    ReverseRecordOwnerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    ReverseRecordAddress = "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU"
                },
                new EventDto
                {
                    Id = "AuctionBidEvent:e-12",
                    TypeName = "AuctionBidEvent",
                    Type = "AUCTION_BID_EVENT",
                    Block = BlockDto.GetExpected(level: 12, hash: "h", timestamp: TestDateTime.Today.AddMinutes(88)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh13",
                    DomainName = "auction-bid.aaa",
                    BidAmount = 2,
                    TransactionAmount = 2,
                    PreviousBidderAddress = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    PreviousBidderAddressReverseRecord = expectedReverseRecord3,
                    PreviousBidAmount = 1,
                },
                new EventDto
                {
                    Id = "DomainGrantEvent:e-13",
                    TypeName = "DomainGrantEvent",
                    Type = "DOMAIN_GRANT_EVENT",
                    Block = BlockDto.GetExpected(level: 13, hash: "h", timestamp: TestDateTime.Today.AddMinutes(87)),
                    SourceAddress = "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash = "ogh14",
                    DomainName = "domain-grant.eee",
                    NullableDurationInDays = 365,
                    DomainOwnerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    DomainForwardRecordAddress = null,
                    Data = DataItemDto.GetExpected(parentId: "DomainGrantEvent:e-13", testSeed: "d5"),
                },
                new EventDto
                {
                    Id = "DomainTransferEvent:e-14",
                    TypeName = "DomainTransferEvent",
                    Type = "DOMAIN_TRANSFER_EVENT",
                    Block = BlockDto.GetExpected(level: 14, hash: "h", timestamp: TestDateTime.Today.AddMinutes(86)),
                    SourceAddress = "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash = "ogh15",
                    DomainName = "domain-transfer.ttt",
                    NewOwner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    NewOwnerReverseRecord = expectedReverseRecord3
                },
                new EventDto
                {
                    Id = "DomainUpdateOperatorsEvent:e-15",
                    TypeName = "DomainUpdateOperatorsEvent",
                    Type = "DOMAIN_UPDATE_OPERATORS_EVENT",
                    Block = BlockDto.GetExpected(level: 15, hash: "h", timestamp: TestDateTime.Today.AddMinutes(85)),
                    SourceAddress = "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash = "ogh16",
                    DomainName = "domain-update-operators.ttt",
                    Operators = new[] { "KT1TV6QNrRfe9GZycNJNrcDhPa7zC3sT3ajM" }
                },
                new EventDto
                {
                    Id = "OfferPlacedEvent:e-16",
                    TypeName = "OfferPlacedEvent",
                    Type = "OFFER_PLACED_EVENT",
                    Block = BlockDto.GetExpected(level: 16, hash: "h", timestamp: TestDateTime.Today.AddMinutes(84)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh17",
                    DomainName = "offer.yyy",
                    TokenId = 123,
                    Price = 10250m,
                    PriceWithoutFee = 10000m,
                    Fee = 250m,
                    ExpiresAtUtc = GetTime(2050, 1, 1)
                },
                new EventDto
                {
                    Id = "OfferExecutedEvent:e-17",
                    TypeName = "OfferExecutedEvent",
                    Type = "OFFER_EXECUTED_EVENT",
                    Block = BlockDto.GetExpected(level: 17, hash: "h", timestamp: TestDateTime.Today.AddMinutes(83)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh18",
                    DomainName = "offer.yyy",
                    TokenId = 123,
                    Price = 100m,
                    PriceWithoutFee = 97m,
                    Fee = 3m,
                    SellerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    SellerAddressReverseRecord = expectedReverseRecord0
                },
                new EventDto
                {
                    Id = "OfferRemovedEvent:e-18",
                    TypeName = "OfferRemovedEvent",
                    Type = "OFFER_REMOVED_EVENT",
                    Block = BlockDto.GetExpected(level: 18, hash: "h", timestamp: TestDateTime.Today.AddMinutes(82)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh19",
                    TokenId = 123,
                    DomainName = "offer.yyy",
                },
                new EventDto
                {
                    Id = "OfferUpdatedEvent:e-19",
                    TypeName = "OfferUpdatedEvent",
                    Type = "OFFER_UPDATED_EVENT",
                    Block = BlockDto.GetExpected(level: 19, hash: "h", timestamp: TestDateTime.Today.AddMinutes(81)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh20",
                    TokenId = 124,
                    Price = 1025m,
                    PriceWithoutFee = 1000m,
                    Fee = 25m,
                    ExpiresAtUtc = GetTime(2050, 1, 1),
                    DomainName = "offer.zzz",
                },
                new EventDto
                {
                    Id = "DomainClaimEvent:e-20",
                    TypeName = "DomainClaimEvent",
                    Type = "DOMAIN_CLAIM_EVENT",
                    Block = BlockDto.GetExpected(level: 20, hash: "h", timestamp: TestDateTime.Today.AddMinutes(80)),
                    SourceAddress = "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    OperationGroupHash = "ogh21",
                    DomainName = "domain-claim.dev",
                    DomainOwnerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    DomainForwardRecordAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7"
                },
                new EventDto
                {
                    Id = "BuyOfferPlacedEvent:e-21",
                    TypeName = "BuyOfferPlacedEvent",
                    Type = "BUY_OFFER_PLACED_EVENT",
                    Block = BlockDto.GetExpected(level: 21, hash: "h", timestamp: TestDateTime.Today.AddMinutes(79)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh22",
                    DomainName = "offer.yyy",
                    DomainOwner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    TokenId = 123,
                    Price = 10250m,
                    PriceWithoutFee = 10000m,
                    Fee = 250m,
                    ExpiresAtUtc = GetTime(2050, 1, 1)
                },
                new EventDto
                {
                    Id = "BuyOfferExecutedEvent:e-22",
                    TypeName = "BuyOfferExecutedEvent",
                    Type = "BUY_OFFER_EXECUTED_EVENT",
                    Block = BlockDto.GetExpected(level: 22, hash: "h", timestamp: TestDateTime.Today.AddMinutes(78)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh23",
                    DomainName = "offer.yyy",
                    TokenId = 123,
                    Price = 100m,
                    PriceWithoutFee = 97m,
                    Fee = 3m,
                    BuyerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    BuyerAddressReverseRecord = expectedReverseRecord0
                },
                new EventDto
                {
                    Id = "BuyOfferRemovedEvent:e-23",
                    TypeName = "BuyOfferRemovedEvent",
                    Type = "BUY_OFFER_REMOVED_EVENT",
                    Block = BlockDto.GetExpected(level: 23, hash: "h", timestamp: TestDateTime.Today.AddMinutes(77)),
                    SourceAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    OperationGroupHash = "ogh24",
                    TokenId = 123,
                    DomainName = "offer.yyy",
                }
            };

            #endregion


            var events = await GetAllAsync("first: 50"); // 20 b/c default is 10 but we have more events.

            events.Items.Should().BeEquivalentTo(expectedEvents, o => o.RespectingRuntimeTypes().WithStrictOrdering());
        }

        [TestCase("startsWith: TZ2")]
        [TestCase(@"in: [""tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL""]")]
        [TestCase(@"notIn: [""tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n""]")]
        public async Task FilterByAddress_ShouldReturnAuctionEndEventThroughParticipants(string addressFilter)
        {
            var data = await GetAllAsync($"where: {{ address: {{ {addressFilter} }}, type: {{ in: [AUCTION_END_EVENT] }} }}");

            data.Items?.Single()?.Id.Should().Be("AuctionEndEvent:e-3");
        }

        [TestCase("domainName", "auction-settle.ddd", new[] { "AUCTION_SETTLE_EVENT" })]
        [TestCase("price", "100", new[] { "AUCTION_SETTLE_EVENT", "OFFER_EXECUTED_EVENT", "BUY_OFFER_EXECUTED_EVENT" })]
        [TestCase(
            "address",
            "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
            new[] { "AUCTION_END_EVENT", "AUCTION_BID_EVENT", "DOMAIN_TRANSFER_EVENT" },
            Description = "Filter by SourceAddress, AuctionBidEvent.PreviousBidderAddress, AuctionEndEvent.Participants, DomainTransferEvent.NewOwner"
        )]
        public Task ShouldFilterByField(string field, string value, string[] expectedTypes)
            => RunTest($@"where: {{ {field}: {{ equalTo: ""{value}"" }} }}", expectedTypes);

        [TestCase("AUCTION_BID_EVENT, DOMAIN_BUY_EVENT", new[] { "AUCTION_BID_EVENT", "DOMAIN_BUY_EVENT", "AUCTION_BID_EVENT" })]
        [TestCase("AUCTION_END_EVENT", new[] { "AUCTION_END_EVENT" })]
        [TestCase("OFFER_EXECUTED_EVENT", new[] { "OFFER_EXECUTED_EVENT" })] //bug fix: previously OFFER_EXECUTED_EVENT returned also BUY_OFFER_EXECUTED_EVENT 
        public Task ShouldFilterByType(string types, string[] expectedTypes)
            => RunTest(@$"where: {{ type: {{ in: [{types}] }} }}", expectedTypes);


        [TestCase(null, 78, new[] { "BUY_OFFER_REMOVED_EVENT" })]
        [TestCase(94, 97, new[] { "AUCTION_SETTLE_EVENT", "DOMAIN_BUY_EVENT" })]
        [TestCase(96, null, new[] { "AUCTION_BID_EVENT", "AUCTION_WITHDRAW_EVENT", "AUCTION_END_EVENT" })]
        public async Task ShouldFilterByTimestamp(int? greaterMinutes, int? lessMinutes, string[] expectedTypes)
        {
            DateTime? greaterTime = greaterMinutes != null ? TestDateTime.Today.AddMinutes(greaterMinutes.Value) : null;
            DateTime? lessTime = lessMinutes != null ? TestDateTime.Today.AddMinutes(lessMinutes.Value) : null;

            await RunTest(@$"where: {{ block: {{ timestamp: {{ greaterThan: {ToGraphQL(greaterTime)}, lessThan: {ToGraphQL(lessTime)} }} }} }}", expectedTypes);
        }

        private async Task RunTest(string queryArgs, string[] expectedTypes)
        {
            var entities = await GetAllAsync(queryArgs);

            (entities.Items?.Select(p => p?.Type)).Should().Equal(expectedTypes);
        }

        private Task<Connection<EventDto>> GetAllAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<EventDto>>($"events({queryArgs}) {Edges(EventDto.FieldsQuery)}");
    }
}