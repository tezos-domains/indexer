using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.MongoDb.Query.Filtering.Domains;
using TezosDomains.TestUtilities;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public class DomainQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            DbContext.Insert(
                TestDomain.Get(
                    name: "2.test.tez",
                    address: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm"
                ),
                TestDomain.Get(
                    name: "3.other.tez",
                    address: null,
                    owner: "KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf",
                    operators: new[] { "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9" }
                ),
                TestDomain.Get(
                    name: "1.test.tez",
                    address: "KT1JJbWfW8CHUY95hG9iq2CEMma1RiKhMHDR%waaaa"
                ),
                TestDomain.Get(
                    name: "expired.test.tez",
                    expiresAtUtc: GetTime(2001, 2, 3)
                )
            );

            var block2 = TestBlock.Get(level: 2);
            var domainWithHistory = TestDomain.Get(
                block: block2,
                operationGroupHash: "ogh-test.tez",
                name: "test.tez",
                parentOwner: "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9",
                expiresAtUtc: GetTime(2150, 2, 3),
                data: DataItemDto.GetTestInput(testSeed: "dd"),
                tokenId: 123,
                operators: new[] { "KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n" },
                lastAuction: TestAuction.Get(
                    id: "test.tez:1",
                    operationGroupHash: "ogh-auction-test.tez",
                    endsAtUtc: GetTime(2150, 2, 1),
                    ownedUntilUtc: GetTime(2150, 2, 3),
                    isSettled: true,
                    bids: new TestBid(Bidder: "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9", Amount: 1, OperationGroupHash: "ogh-auction-test.tez")
                )
            );
            var block1 = TestBlock.Get(level: 1, daysOffset: -1);
            DbContext.Insert(
                domainWithHistory,
                Historize(
                    domainWithHistory with { Level = 7 },
                    block: block1,
                    validUntil: domainWithHistory.Block
                )
            );

            DbContext.Insert(
                TestReverseRecord.Get(address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", name: "test.tez", operationGroupHash: "ogh-rr"),
                TestReverseRecord.Get(address: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", name: "3.test.tez") // Doesn't correspond with both address and name
            );

            DbContext.Insert(TestBlock.Get(level: 0, daysOffset: -2), block1, block2, TestBlock.Get(level: 3, daysOffset: 1));
        }

        [Test]
        public async Task Domain_ShouldLoadAllFields()
        {
            var domain = await GetDomainAsync(@"name: ""test.tez""");

            VerifyTestDomain(domain!);
        }

        [Test]
        public async Task Domain_ShouldLoadNull_IfNotExist()
        {
            var domain = await GetDomainAsync(@"name: ""omg.tez""");

            domain.Should().BeNull();
        }

        [TestCase("test.tez", "VALID", true)]
        [TestCase("test.tez", "ALL", true)]
        [TestCase("test.tez", "EXPIRED", false)]
        [TestCase("1.test.tez", "VALID", true)] // Has ExpiresAtUtc == null -> doesn't expire
        [TestCase("expired.test.tez", "VALID", false)]
        [TestCase("expired.test.tez", "ALL", true)]
        [TestCase("expired.test.tez", "EXPIRED", true)]
        public async Task Domain_ShouldFilterByValidity(string name, string validity, bool expectedSuccess)
        {
            var domain = await GetDomainAsync($@"name: ""{name}"", validity: {validity}");

            (domain?.Name).Should().Be(expectedSuccess ? name : null);
        }

        public static readonly IEnumerable HistoryTestCases = new[]
        {
            new object?[] { 0, null, null },
            new object?[] { 1, null, 7 },
            new object?[] { 2, null, 2 },
            new object?[] { 3, null, 2 },
            new object?[] { null, -2, null },
            new object?[] { null, -1, 7 },
            new object?[] { null, 0, 2 },
            new object?[] { null, 1, 2 },
        };

        [TestCaseSource(nameof(HistoryTestCases))]
        public async Task Domain_ShouldFilterByHistory(int? blockLevel, int? blockDaysOffset, int? expectedLevel)
        {
            var domain = await GetDomainAsync(
                @"name: ""test.tez"","
                + AtBlockFilter(blockLevel, blockDaysOffset)
            );

            (domain?.Level).Should().Be(expectedLevel);
        }

        [TestCaseSource(nameof(HistoryTestCases))]
        public async Task Domains_ShouldFilterByHistory(int? blockLevel, int? blockDaysOffset, int? expectedLevel)
        {
            var domains = await GetDomainsAsync(
                @"where: { name: { equalTo: ""test.tez"" } },"
                + AtBlockFilter(blockLevel, blockDaysOffset)
            );

            (domains.Items?.SingleOrDefault()?.Level).Should().Be(expectedLevel);
        }

        [Test]
        public async Task Domains_ShouldCountHistoricalEntriesCorrectly()
        {
            var domains = await GetDomainsAsync(
                @"where: { address: { startsWith: TZ1 } },
                atBlock: { level: 2 }"
            );

            domains.TotalCount.Should().Be(1);
        }

        [Test]
        public async Task Domains_ShouldLoadAllFields()
        {
            var domains = await GetDomainsAsync(@"where: { name: { equalTo: ""test.tez"" }}");

            VerifyTestDomain(domains.Items?.Single());
        }

        private static void VerifyTestDomain(DomainDto? domain)
        {
            domain.Should().NotBeNull();
            var addressReverseRecord = ReverseRecordDto.GetExpected(
                operationGroupHash: "ogh-rr",
                address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                name: "test.tez",
                hasOwnerReverseRecord: true
            );
            var bidDto = new BidDto
            {
                Amount = 1M,
                Bidder = "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9",
                Id = "Bid:test.tez:1:0",
                OperationGroupHash = "ogh-auction-test.tez",
                TypeName = "Bid"
            };
            var expected = new DomainDto
            {
                Id = "Domain:test.tez",
                TypeName = "Domain",
                OperationGroupHash = "ogh-test.tez",
                Name = "test.tez",
                Label = "test",
                Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                Owner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                ParentName = "tez",
                ParentOwner = "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9",
                ExpiresAtUtc = GetTime(2150, 2, 3),
                Level = 2,
                Data = DataItemDto.GetExpected(parentId: "Domain:test.tez", testSeed: "dd"),
                TokenId = 123,
                AddressReverseRecord = addressReverseRecord,
                OwnerReverseRecord = null,
                ParentOwnerReverseRecord = null,
                Operators = new[]
                {
                    DomainOperatorDto.GetExpected("KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf"),
                    DomainOperatorDto.GetExpected("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", addressReverseRecord),
                },
                LastAuction = new AuctionDto()
                {
                    BidAmountSum = 1,
                    BidCount = 1,
                    Bids = new[] { bidDto },
                    DomainName = "test.tez",
                    EndsAtUtc = GetTime(2150, 2, 1),
                    HighestBid = bidDto,
                    Id = "Auction:test.tez:1",
                    OperationGroupHash = "ogh-auction-test.tez",
                    OwnedUntilUtc = GetTime(2150, 2, 3),
                    StartedAtLevel = 1,
                    State = "SETTLED",
                    TypeName = "Auction",
                    CountOfUniqueBidders = 1
                }
            };
            domain.Should().BeEquivalentTo(expected, c => c.Excluding(d => d.Subdomains).Excluding(d => d.ReverseRecord));
            (domain?.ExpiresAtUtc?.Kind).Should().Be(DateTimeKind.Utc);
            (domain?.Subdomains.Items?.Select(d => d?.Name)).Should().Equal("1.test.tez", "2.test.tez");
        }

        [Test]
        public Task Domains_ShouldOrderByReverseNameByDefault()
            => RunDomainsTest("where: {}", expectedNames: new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" });

        [TestCase("order: { field: NAME }", new[] { "1.test.tez", "2.test.tez", "3.other.tez", "test.tez" })]
        [TestCase("order: { field: NAME direction: DESC }", new[] { "test.tez", "3.other.tez", "2.test.tez", "1.test.tez" })]
        [TestCase("order: { field: EXPIRES_AT direction: DESC }", new[] { "3.other.tez", "1.test.tez", "2.test.tez", "test.tez" })]
        public Task Domains_ShouldOrderAccordingToOrderArgument(string queryArgs, string[] expectedNames)
            => RunDomainsTest(queryArgs, expectedNames);

        [TestCase("name", @"""1.TEST.tez""", new[] { "1.test.tez" })] // Filter should be case-insensitive
        [TestCase("label", @"""1""", new[] { "1.test.tez" })] // Filter should be case-insensitive
        [TestCase("address", @"""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""", new[] { "2.test.tez" })]
        [TestCase("owner", @"""KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf""", new[] { "3.other.tez" })]
        [TestCase("parentOwner", @"""tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9""", new[] { "test.tez" })]
        [TestCase("level", "2", new[] { "test.tez" })]
        public Task Domains_ShouldFilterByField(string field, string value, string[] expectedNames)
            => RunDomainsTest(@$"where: {{ {field}: {{ equalTo: {value} }}}}", expectedNames);

        [Test]
        public Task Domains_ShouldFilterByAncestorField() // Filter should be case-insensitive
            => RunDomainsTest(@"where: { ancestors: { include: ""other.TEZ"" }}", expectedNames: new[] { "3.other.tez" });

        [Test]
        public Task Domains_ShouldFilterByOperatorField()
            => RunDomainsTest(@"where: { operators: { include: ""tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9"" }}", expectedNames: new[] { "3.other.tez" });


        [TestCase("VALID", new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" })]
        [TestCase("EXPIRED", new[] { "expired.test.tez" })]
        [TestCase("ALL", new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez", "expired.test.tez" })]
        public Task Domains_ShouldFilterByValidityField(string validity, string[] expectedNames)
            => RunDomainsTest($"where: {{ validity: {validity} }}", expectedNames);


        [TestCase(new[] { LabelCompositionType.Letters }, new[] { "test.tez" })]
        [TestCase(new[] { LabelCompositionType.Numbers }, new[] { "3.other.tez", "1.test.tez", "2.test.tez", })]
        [TestCase(new[] { LabelCompositionType.Dash }, new string[0])]
        [TestCase(new[] { LabelCompositionType.Letters, LabelCompositionType.Numbers }, new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" })]
        public Task Domains_ShouldFilterByLabelComposition_Including(LabelCompositionType[] nameRestrictions, string[] expectedNames)
            => RunDomainsTest(
                $"where: {{ label: {{ composition: {{ in: [{string.Join(",", nameRestrictions.Select(nr => nr.ToString().ToUpperInvariant()))} ] }} }} }}",
                expectedNames
            );

        [TestCase(new[] { LabelCompositionType.Letters }, new[] { "3.other.tez", "1.test.tez", "2.test.tez" })]
        [TestCase(new[] { LabelCompositionType.Numbers }, new[] { "test.tez", })]
        [TestCase(new[] { LabelCompositionType.Dash }, new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" })]
        [TestCase(new[] { LabelCompositionType.Letters, LabelCompositionType.Numbers }, new string[0])]
        public Task Domains_ShouldFilterByLabelComposition_Excluding(LabelCompositionType[] nameRestrictions, string[] expectedNames)
            => RunDomainsTest(
                $"where: {{ label: {{ composition: {{ notIn: [{string.Join(",", nameRestrictions.Select(nr => nr.ToString().ToUpperInvariant()))} ] }} }} }}",
                expectedNames
            );

        private async Task RunDomainsTest(string queryArgs, string[] expectedNames)
        {
            var domains = await GetDomainsAsync(queryArgs);

            (domains.Items?.Select(d => d?.Name)).Should().Equal(expectedNames);
        }

        [TestCase("test.tez", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n")]
        [TestCase("1.test.tez", null)] // Domain has no address
        [TestCase("2.test.tez", null)] // Domain address corresponding to reverse record with different name
        public async Task ShouldLoadRelatedReverseRecord_IfNameAndAddressMatched(string name, string? expectedAddress)
        {
            var domain = await GetDomainAsync($@"name: ""{name}""");

            (domain!.ReverseRecord?.Address).Should().Be(expectedAddress);
            (domain.ReverseRecord?.Domain?.Name).Should().Be(expectedAddress != null ? name : null);
        }

        private Task<DomainDto?> GetDomainAsync(string queryArgs)
            => SendToGraphQLAsync<DomainDto?>($@"domain({queryArgs}) {DomainDto.FieldsQuery}");

        private Task<Connection<DomainDto>> GetDomainsAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<DomainDto>>($"domains({queryArgs}) {Edges(DomainDto.FieldsQuery)}");
    }
}