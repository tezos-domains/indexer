using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.TestUtilities;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public class AuctionQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            #region Insert Auctions

            const int currentBlockLevel = 3;
            DbContext.Insert(
                TestAuction.Get(
                    id: "default.auc:65431",
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    endsAtUtc: GetTime(2150, 2, 4),
                    ownedUntilUtc: GetTime(2200, 2, 3),
                    bids: new[]
                    {
                        new TestBid(Bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", Amount: 1m, OperationGroupHash: "ogh-bid-0"),
                        new TestBid(Bidder: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", Amount: 2m, OperationGroupHash: "ogh-bid-1")
                    },
                    operationGroupHash: "ogh-auction"
                ),
                TestAuction.Get(
                    id: "can-be-settled.auc:0",
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.1),
                    endsAtUtc: GetTime(2000, 2, 3),
                    ownedUntilUtc: GetTime(2200, 2, 3),
                    bids: new TestBid(Bidder: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", Amount: 100m)
                ),
                TestAuction.Get(
                    id: "settlement-expired.auc:0",
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.3),
                    endsAtUtc: GetTime(2000, 2, 1),
                    ownedUntilUtc: GetTime(2000, 2, 3),
                    bids: new TestBid(Bidder: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", Amount: 100m)
                )
            );

            var settled = TestAuction.Get(
                id: "settled.auc:0",
                block: TestBlock.Get(currentBlockLevel, daysOffset: -0.2),
                endsAtUtc: GetTime(2000, 2, 2),
                ownedUntilUtc: GetTime(2200, 2, 3),
                isSettled: true,
                bids: new[]
                {
                    new TestBid(Bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", Amount: 1m),
                    new TestBid(Bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", Amount: 2m),
                    new TestBid(Bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", Amount: 1000m)
                }
            );
            DbContext.Insert(
                settled,
                Historize(
                    settled with { IsSettled = false },
                    block: TestBlock.Get(level: 2, daysOffset: -1),
                    validUntil: settled.Block
                )
            );

            #endregion

            DbContext.Insert(
                TestBlock.Get(level: 1, daysOffset: -2),
                TestBlock.Get(level: 2, daysOffset: -1),
                TestBlock.Get(level: 3, daysOffset: 0.3)
            );

            DbContext.Insert(TestDomain.Get(name: "bidder.tez", address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n"));
            DbContext.Insert(TestReverseRecord.Get(address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", name: "bidder.tez", operationGroupHash: "ogh-rr"));
        }

        #region Auction

        [Test]
        public async Task Auction_ShouldLoadAllFields()
        {
            var auction = await GetAuctionAsync(@"domainName: ""default.auc"", startedAtLevel: 65431");

            VerifyTestAuction(auction!);
        }

        [Test]
        public async Task Auction_ShouldLoadNull_IfNotExist()
        {
            var auction = await GetAuctionAsync(@"domainName: ""nonexisting.auc"", startedAtLevel: 0");

            auction.Should().BeNull();
        }

        [TestCase("default.auc", "IN_PROGRESS")]
        [TestCase("can-be-settled.auc", "CAN_BE_SETTLED")]
        [TestCase("settled.auc", "SETTLED")]
        [TestCase("settlement-expired.auc", "SETTLEMENT_EXPIRED")]
        public async Task Auction_ShouldFilterByState(string domainName, string state)
        {
            var auction = await GetAuctionsAsync($@"where: {{domainName: {{ equalTo: ""{domainName}"" }}, state: {{ in: {state} }} }}");

            (auction.Items?.SingleOrDefault()?.DomainName).Should().Be(domainName);
        }

        [TestCase(1, null)]
        [TestCase(2, "CAN_BE_SETTLED")]
        [TestCase(3, "SETTLED")]
        public async Task Auction_HistoricQuery_ShouldShowCorrectState(int blockLevel, string? state)
        {
            var auctions = await GetAuctionsAsync(
                $@"where: {{ domainName: {{ equalTo: ""settled.auc"" }} }},
                atBlock: {{ level: {blockLevel} }}"
            );
            var auction = await GetAuctionAsync(
                $@"domainName: ""settled.auc"",
                startedAtLevel: 0,
                atBlock: {{ level: {blockLevel} }}"
            );

            if (state == null)
            {
                auctions.Items.Should().BeEmpty();
                auction.Should().BeNull();
            }
            else
            {
                auctions.Items.Should().HaveCount(1);
                (auctions.Items?.First()?.State).Should().Be(state);
                auction.Should().NotBeNull();
                auction!.State.Should().Be(state);
            }
        }

        #endregion

        #region CurrentAuction

        [TestCase("default.auc", true)]
        [TestCase("can-be-settled.auc", true)]
        [TestCase("settled.auc", false)]
        [TestCase("settlement-expired.auc", false)]
        public async Task CurrentAuction_ShouldShowOnlyRunningAuctions(string domainName, bool shouldBeReturned)
        {
            var currentAuction = await SendToGraphQLAsync<AuctionDto?>($@"currentAuction( domainName: ""{domainName}"" ) {AuctionDto.FieldsQuery}");

            (currentAuction?.DomainName).Should().Be(shouldBeReturned ? domainName : null);
        }

        #endregion

        #region Auctions

        [Test]
        public async Task Auctions_ShouldLoadAllFields()
        {
            var auctions = await GetAuctionsAsync(@"where: { domainName: { equalTo: ""default.auc"" }}");

            auctions.Items.Should().HaveCount(1);
            VerifyTestAuction(auctions.Items?.First());
        }

        [Test]
        public Task Auctions_ShouldOrderByEndsAtDescByDefault()
            => RunAuctionsTest(
                "where: {}",
                expectedNames: new[] { "default.auc", "can-be-settled.auc", "settled.auc", "settlement-expired.auc" }
            );

        [TestCase("field: ID direction: ASC", new[] { "can-be-settled.auc", "default.auc", "settled.auc", "settlement-expired.auc" })]
        [TestCase("field: DOMAIN_NAME direction: DESC", new[] { "settlement-expired.auc", "settled.auc", "default.auc", "can-be-settled.auc" })]
        [TestCase("field: ENDS_AT direction: ASC", new[] { "settlement-expired.auc", "settled.auc", "can-be-settled.auc", "default.auc" })]
        [TestCase("field: HIGHEST_BID_AMOUNT direction: DESC", new[] { "settled.auc", "can-be-settled.auc", "settlement-expired.auc", "default.auc" })]
        [TestCase("field: HIGHEST_BID_TIMESTAMP direction: DESC", new[] { "can-be-settled.auc", "settled.auc", "settlement-expired.auc", "default.auc" })]
        [TestCase("field: BID_COUNT direction: DESC", new[] { "settled.auc", "default.auc", "can-be-settled.auc", "settlement-expired.auc" })]
        [TestCase("field: BID_AMOUNT_SUM direction: DESC", new[] { "settled.auc", "can-be-settled.auc", "settlement-expired.auc", "default.auc" })]
        public Task Auctions_ShouldOrderAccordingToQuery(string order, string[] expectedNames)
            => RunAuctionsTest($"order: {{ {order} }}", expectedNames);

        [TestCase("domainName", "equalTo", @"""default.auc""", new[] { "default.auc" })] // Filter should be case-insensitive
        [TestCase("state", "in", @"IN_PROGRESS", new[] { "default.auc" })]
        [TestCase("highestBidder", "equalTo", @"""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""", new[] { "settled.auc" })]
        [TestCase("bidders", "include", @"""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""", new[] { "default.auc", "settled.auc" })]
        [TestCase("endsAtUtc", "greaterThan", @"""2150-02-03T00:00:00Z""", new[] { "default.auc" })]
        [TestCase("bidCount", "greaterThan", "2", new[] { "settled.auc" })]
        public Task Auctions_ShouldFilterByField(string field, string filterType, string value, string[] expectedNames)
            => RunAuctionsTest(@$"where: {{ {field}: {{ {filterType}: {value} }}}}", expectedNames);

        #endregion

        private async Task RunAuctionsTest(string queryArgs, string[] expectedNames)
        {
            var auctions = await GetAuctionsAsync(queryArgs);

            (auctions.Items?.Select(d => d?.DomainName)).Should().Equal(expectedNames);
        }

        private static void VerifyTestAuction(AuctionDto? auction)
        {
            var expectedRecord = ReverseRecordDto.GetExpected(
                address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                name: "bidder.tez",
                owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                hasOwnerReverseRecord: true,
                operationGroupHash: "ogh-rr"
            );
            var expectedAuction = new AuctionDto
            {
                Id = "Auction:default.auc:65431",
                TypeName = "Auction",
                OperationGroupHash = "ogh-auction",
                DomainName = "default.auc",
                EndsAtUtc = GetTime(2150, 2, 4),
                OwnedUntilUtc = GetTime(2200, 2, 3),
                State = "IN_PROGRESS",
                BidCount = 2,
                BidAmountSum = 3,
                CountOfUniqueBidders = 2,
                Bids = new[]
                {
                    new BidDto
                    {
                        Id = "Bid:default.auc:65431:1",
                        TypeName = "Bid",
                        OperationGroupHash = "ogh-bid-1",
                        Bidder = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                        Amount = 2,
                        BidderReverseRecord = expectedRecord
                    },
                    new BidDto
                    {
                        Id = "Bid:default.auc:65431:0",
                        TypeName = "Bid",
                        OperationGroupHash = "ogh-bid-0",
                        Bidder = "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
                        Amount = 1,
                    }
                },
                HighestBid = new BidDto
                {
                    Id = "Bid:default.auc:65431:1",
                    TypeName = "Bid",
                    OperationGroupHash = "ogh-bid-1",
                    Bidder = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    Amount = 2,
                    BidderReverseRecord = expectedRecord
                },
                StartedAtLevel = 65431
            };
            auction.Should().BeEquivalentTo(expectedAuction);
        }

        private Task<AuctionDto?> GetAuctionAsync(string queryArgs)
            => SendToGraphQLAsync<AuctionDto?>($@"auction({queryArgs}) {AuctionDto.FieldsQuery}");

        private Task<Connection<AuctionDto>> GetAuctionsAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<AuctionDto>>($"auctions({queryArgs}) {Edges(AuctionDto.FieldsQuery)}");
    }
}