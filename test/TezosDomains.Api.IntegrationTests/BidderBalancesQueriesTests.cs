using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.TestUtilities;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public class BidderBalancesQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var block0 = TestBlock.Get(level: 0, daysOffset: -2);
            var block1 = TestBlock.Get(level: 1, daysOffset: -1);
            var block2 = TestBlock.Get(level: 2);
            DbContext.Insert(block0, block1, block2);

            var bidderBalances = TestBidderBalances.Get(
                address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                block: block2,
                balances: new[] { ("a1", 10m), ("a2", 100m) },
                operationGroupHash: "ogh-balances"
            );
            DbContext.Insert(
                bidderBalances,
                Historize(
                    bidderBalances with { Balances = new Dictionary<string, decimal> { { "a1", 10m } } },
                    block: block1,
                    validUntil: bidderBalances.Block
                )
            );
        }

        [Test]
        public Task ShouldLoadAllFields()
            => RunFieldsTest(
                address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                expectedBalances: new[]
                {
                    new BalanceDto
                    {
                        Id = "Balance:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:a1",
                        TypeName = "Balance",
                        TldName = "a1",
                        Balance = 10
                    },
                    new BalanceDto
                    {
                        Id = "Balance:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:a2",
                        TypeName = "Balance",
                        TldName = "a2",
                        Balance = 100,
                    }
                },
                expectedOperationGroupHash: "ogh-balances"
            );

        [Test]
        public Task ShouldLoadEmpty_IfNoEntryInDatabase()
            => RunFieldsTest(
                address: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                expectedBalances: Array.Empty<BalanceDto>(),
                expectedOperationGroupHash: null
            );

        private async Task RunFieldsTest(string address, BalanceDto[] expectedBalances, string? expectedOperationGroupHash)
        {
            var item = await GetAsync(@$"address: ""{address}""");

            var expected = new BidderBalancesDto
            {
                Id = $"BidderBalances:{address}",
                TypeName = "BidderBalances",
                Address = address,
                Balances = expectedBalances,
                OperationGroupHash = expectedOperationGroupHash
            };
            item.Should().BeEquivalentTo(expected);
        }

        [TestCase(0, new int[0])]
        [TestCase(1, new[] { 10 })]
        [TestCase(2, new[] { 10, 100 })]
        public async Task HistoricQuery_ShouldShowCorrectBalances(int blockLevel, int[] balances)
        {
            var item = await GetAsync($@"address: ""tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n"", atBlock: {{ level: {blockLevel} }}");

            (item?.Balances?.Select(b => b.Balance)).Should().BeEquivalentTo(balances);
        }

        private Task<BidderBalancesDto?> GetAsync(string queryArgs)
            => SendToGraphQLAsync<BidderBalancesDto?>($@"bidderBalances({queryArgs}) {BidderBalancesDto.FieldsQuery}");
    }
}