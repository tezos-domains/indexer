﻿using FluentAssertions;
using GraphQL.IntrospectionModel;
using GraphQL.IntrospectionModel.SDL;
using NUnit.Framework;
using System;
using System.IO;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public class SchemaTests : TestBase
    {
        [Test]
        public async Task ShouldNotIntroduceBreakingChanges()
        {
            var previousSdlFile = GetFullPath("Expected.sdl");
            var currentSdlFile = GetFullPath("Actual.sdl");

            var previousSdlStr = await File.ReadAllTextAsync(previousSdlFile);
            var currentSdlStr = await DownloadSchemaStringAsync();

            await File.WriteAllTextAsync(currentSdlFile, currentSdlStr);

            try
            {
                currentSdlStr.Should().Be(previousSdlStr);
            }
            catch (Exception ex)
            {
                throw new Exception($"GraphQL schema has changed. If it's intentional then merge changes from {currentSdlFile} to {previousSdlFile}.", ex);
            }
        }

        private async Task<string> DownloadSchemaStringAsync()
        {
            try
            {
                var response = await SendRawToGraphQLAsync<SchemaResponse>(IntrospectionQuery.Classic);

                response.Data.Schema.Should().NotBeNull();
                return SDLBuilder.Build(response.Data.Schema!);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to download GraphQL schema.", ex);
            }
        }

        private sealed class SchemaResponse
        {
            [JsonPropertyName("__schema")]
            public GraphQLSchema? Schema { get; set; }
        }

        private static string GetFullPath(string name)
            => Path.GetFullPath($"../../../Schema/{name}");
    }
}