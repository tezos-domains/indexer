using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.TestUtilities;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public class BuyOfferQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            #region Insert BuyOffers

            const int currentBlockLevel = 3;
            var buyerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n";
            var secondBuyerAddress = "tz1eFX5MRWV88wCvodK2i4Lq2ufSAGHRbhAY";
            var ownerAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7";

            DbContext.Insert(
                TestOffer.Get( //sell offer should be filtered out in buyOffer queries
                    operationGroupHash: "ogh-SellOffer-NOTUSED",
                    id: "1:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:654321",
                    tokenId: 1,
                    initiatorAddress: ownerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 8),
                    endsAtUtc: GetTime(2030, 1, 1),
                    presence: OfferBlockchainPresence.Present,
                    domainData: TestOffer.GetDomainData(name: "valid.buy-offer", owner: ownerAddress, useValidOperatorContract: true),
                    price: 123m
                ),
                TestOffer.GetBuy(
                    operationGroupHash: "ogh-Offer",
                    id: "b:10:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:654321",
                    tokenId: 10,
                    initiatorAddress: buyerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 8),
                    endsAtUtc: GetTime(2030, 1, 1),
                    presence: OfferBlockchainPresence.Present,
                    domainData: TestOffer.GetDomainData(name: "valid.buy-offer", owner: ownerAddress, useValidOperatorContract: true),
                    price: 123m
                ),
                TestOffer.GetBuy(
                    operationGroupHash: "ogh-Offer",
                    id: "b:10:tz1eFX5MRWV88wCvodK2i4Lq2ufSAGHRbhAY:654321",
                    tokenId: 11,
                    initiatorAddress: secondBuyerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 7),
                    endsAtUtc: GetTime(2030, 1, 1),
                    presence: OfferBlockchainPresence.Present,
                    domainData: TestOffer.GetDomainData(name: "valid.buy-offer", owner: ownerAddress, useValidOperatorContract: true),
                    price: 123m
                ),
                TestOffer.GetBuy(
                    id: "invalid-missing-domain.buy-offer:0",
                    tokenId: 20,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 6),
                    endsAtUtc: GetTime(2030, 2, 5),
                    presence: OfferBlockchainPresence.Present,
                    price: 1m
                ),
                TestOffer.GetBuy(
                    id: "offer-expired.buy-offer:0",
                    tokenId: 30,
                    initiatorAddress: buyerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 5),
                    endsAtUtc: GetTime(2000, 2, 6),
                    presence: OfferBlockchainPresence.Present,
                    price: 2m,
                    domainData: TestOffer.GetDomainData(name: "offer-expired.buy-offer", owner: ownerAddress)
                ),
                TestOffer.GetBuy(
                    id: "domain-expired.buy-offer:0",
                    tokenId: 40,
                    initiatorAddress: buyerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 4),
                    endsAtUtc: GetTime(2030, 2, 7),
                    presence: OfferBlockchainPresence.Present,
                    price: 3m,
                    domainData: TestOffer.GetDomainData(
                        name: "domain-expired.buy-offer",
                        owner: ownerAddress,
                        useValidOperatorContract: true,
                        expiresAtUtc: GetTime(2000, 2, 4)
                    )
                ),
                TestOffer.GetBuy(
                    tokenId: 60,
                    id: "removed.buy-offer:0",
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.1),
                    createdAtUtc: GetTime(2021, 08, 3),
                    endsAtUtc: GetTime(2000, 2, 8),
                    presence: OfferBlockchainPresence.Removed,
                    price: 4m,
                    domainData: TestOffer.GetDomainData(name: "removed.buy-offer", owner: ownerAddress)
                ),
                TestOffer.GetBuy(
                    id: "domain-expiring-soon.buy-offer:0",
                    tokenId: 90,
                    initiatorAddress: buyerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 07, 30),
                    endsAtUtc: GetTime(2030, 2, 11),
                    presence: OfferBlockchainPresence.Present,
                    price: 3m,
                    domainData: TestOffer.GetDomainData(
                        name: "domain-expiring-soon.buy-offer",
                        owner: ownerAddress,
                        useValidOperatorContract: true,
                        expiresAtUtc: DateTime.UtcNow.Date.AddDays(3)
                    )
                )
            );

            var executed = TestOffer.GetBuy(
                id: "executed.buy-offer:0",
                tokenId: 50,
                initiatorAddress: buyerAddress,
                createdAtUtc: GetTime(2021, 07, 1),
                endsAtUtc: null,
                block: TestBlock.Get(currentBlockLevel, daysOffset: -0.1),
                presence: OfferBlockchainPresence.Executed,
                domainData: TestOffer.GetDomainData(name: "executed.buy-offer", owner: ownerAddress),
                price: 333m
            );
            DbContext.Insert(
                executed,
                Historize(
                    executed with { Presence = OfferBlockchainPresence.Present },
                    block: TestBlock.Get(level: 2, daysOffset: -1),
                    validUntil: executed.Block
                )
            );

            #endregion

            DbContext.Insert(
                TestBlock.Get(level: 1, daysOffset: -2),
                TestBlock.Get(level: 2, daysOffset: -1),
                TestBlock.Get(level: 3, daysOffset: 0.3)
            );

            DbContext.Insert(TestDomain.Get(name: "valid.buy-offer", owner: ownerAddress));
            DbContext.Insert(TestDomain.Get(name: "executed.buy-offer", owner: ownerAddress));
            DbContext.Insert(TestDomain.Get(name: "offer-expired.buy-offer", owner: ownerAddress));
            DbContext.Insert(TestDomain.Get(name: "domain-expired.buy-offer", owner: ownerAddress, expiresAtUtc: GetTime(2000, 2, 4)));
            DbContext.Insert(TestDomain.Get(name: "domain-expiring-soon.buy-offer", owner: ownerAddress, expiresAtUtc: DateTime.UtcNow.Date.AddDays(3)));

            DbContext.Insert(TestReverseRecord.Get(address: buyerAddress, name: "bidder.tez", operationGroupHash: "ogh-rr"));
        }

        #region BuyOffer

        [Test]
        public async Task BuyOffer_ShouldLoadAllFields()
        {
            var offer = await GetBuyOfferAsync("buyerAddress: \"tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n\",tokenId: 10, startedAtLevel: 654321");

            VerifyTestBuyOffer(offer!);
        }

        [Test]
        public async Task BuyOffer_ShouldLoadNull_IfNotExist()
        {
            var offer = await GetBuyOfferAsync("buyerAddress: \"tz1eFX5MRWV88wCvodK2i4Lq2ufSAGHRbhAY\" tokenId: -1, startedAtLevel: 0");

            offer.Should().BeNull();
        }

        #endregion

        #region CurrentBuyOffer

        [TestCase("valid.buy-offer", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", true)]
        [TestCase("valid.buy-offer", "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7", false)] //wrong buyerAddress
        [TestCase("offer-expired.buy-offer", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", true)]
        [TestCase("domain-expiring-soon.buy-offer", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", true)]
        [TestCase("domain-expired.buy-offer", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", true)]
        [TestCase("removed.buy-offer", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", false)]
        [TestCase("executed.buy-offer", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", false)]
        public async Task CurrentBuyOffer_ShouldShowOnlyValidOffers(string domainName, string buyerAddress, bool shouldBeReturned)
        {
            var currentBuyOffer = await SendToGraphQLAsync<OfferDto?>(
                $@"currentBuyOffer( domainName: ""{domainName}"" buyerAddress: ""{buyerAddress}"" ) {OfferDto.FieldsQuery}"
            );

            (currentBuyOffer?.Domain?.Name).Should().Be(shouldBeReturned ? domainName : null);
        }

        #endregion

        #region BuyOffers

        [Test]
        public async Task BuyOffers_ShouldLoadAllFields()
        {
            var offers = await GetOffersAsync(
                @"where: { domainName: { equalTo: ""valid.buy-offer"" } buyerAddress: {equalTo: ""tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n""}}"
            );

            offers.Items.Should().HaveCount(1);
            VerifyTestBuyOffer(offers.Items?.First());
        }

        [Test]
        public Task BuyOffers_ShouldOrderByCreatedAtDescByDefault()
            => RunBuyOffersTest(
                "where: {}",
                expectedNames: new[]
                {
                    "valid.buy-offer", "valid.buy-offer", null, "offer-expired.buy-offer", "domain-expired.buy-offer", "removed.buy-offer",
                    "domain-expiring-soon.buy-offer", "executed.buy-offer"
                }
            );

        [TestCase(
            "field: ID direction: ASC",
            new[]
            {
                "valid.buy-offer", "valid.buy-offer", "domain-expired.buy-offer", "domain-expiring-soon.buy-offer", "executed.buy-offer",
                null, "offer-expired.buy-offer", "removed.buy-offer"
            }
        )]
        [TestCase(
            "field: DOMAIN_NAME direction: DESC",
            new[]
            {
                "valid.buy-offer", "valid.buy-offer", "removed.buy-offer", "offer-expired.buy-offer",
                "executed.buy-offer", "domain-expiring-soon.buy-offer", "domain-expired.buy-offer", null
            }
        )]
        [TestCase(
            "field: ENDS_AT direction: ASC",
            new[]
            {
                "offer-expired.buy-offer", "removed.buy-offer", "valid.buy-offer", "valid.buy-offer", null, "domain-expired.buy-offer",
                "domain-expiring-soon.buy-offer", "executed.buy-offer"
            }
        )]
        [TestCase(
            "field: TOKEN_ID direction: DESC",
            new[]
            {
                "domain-expiring-soon.buy-offer", "removed.buy-offer",
                "executed.buy-offer", "domain-expired.buy-offer",
                "offer-expired.buy-offer", null, "valid.buy-offer", "valid.buy-offer"
            }
        )]
        [TestCase(
            "field: PRICE direction: DESC",
            new[]
            {
                "executed.buy-offer", "valid.buy-offer", "valid.buy-offer",
                "removed.buy-offer", "domain-expiring-soon.buy-offer", "domain-expired.buy-offer", "offer-expired.buy-offer", null
            }
        )]
        public Task BuyOffers_ShouldOrderAccordingToQuery(string order, string[] expectedNames)
            => RunBuyOffersTest($"order: {{ {order} }}", expectedNames);

        [TestCase(
            "domainName",
            "equalTo",
            @"""valid.buy-offer""",
            new[] { "valid.buy-offer", "valid.buy-offer" }
        )] // Filter should be case-insensitive
        [TestCase("state", "in", @"[EXECUTED,DOMAIN_IS_EXPIRING_SOON]", new[] { "domain-expiring-soon.buy-offer", "executed.buy-offer" })]
        [TestCase("state", "in", @"[ACTIVE]", new[] { "valid.buy-offer", "valid.buy-offer" })]
        [TestCase(
            "buyerAddress",
            "equalTo",
            "\"tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n\"",
            new[]
            {
                "valid.buy-offer", "offer-expired.buy-offer", "domain-expired.buy-offer", "domain-expiring-soon.buy-offer", "executed.buy-offer"
            }
        )]
        [TestCase("createdAtUtc", "greaterThan", @"""2021-08-06T00:00:00Z""", new[] { "valid.buy-offer", "valid.buy-offer" })]
        [TestCase(
            "endsAtUtc",
            "lessThan",
            @"""2030-02-02T00:00:00Z""",
            new[] { "valid.buy-offer", "valid.buy-offer", "offer-expired.buy-offer", "removed.buy-offer" }
        )]
        [TestCase("tokenId", "greaterThan", "70", new[] { "domain-expiring-soon.buy-offer" })]
        [TestCase("price", "greaterThan", "\"100\"", new[] { "valid.buy-offer", "valid.buy-offer", "executed.buy-offer" })]
        [TestCase(
            "id",
            "notIn",
            "[\"BuyOffer:b:10:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:654321\",\"BuyOffer:domain-expired.buy-offer:0\",\"BuyOffer:offer-expired.buy-offer:0\", \"BuyOffer:invalid-missing-domain.buy-offer:0\"]",
            new[]
            {
                "valid.buy-offer", "removed.buy-offer", "domain-expiring-soon.buy-offer", "executed.buy-offer"
            }
        )]
        public Task BuyOffers_ShouldFilterByField(string field, string filterType, string value, string[] expectedNames)
            => RunBuyOffersTest(@$"where: {{ {field}: {{ {filterType}: {value} }}}}", expectedNames);

        [TestCase("valid.buy-offer", "ACTIVE")]
        [TestCase("offer-expired.buy-offer", "OFFER_EXPIRED")]
        [TestCase("domain-expired.buy-offer", "DOMAIN_EXPIRED")]
        [TestCase("domain-expiring-soon.buy-offer", "DOMAIN_IS_EXPIRING_SOON")]
        [TestCase("removed.buy-offer", "REMOVED")]
        [TestCase("executed.buy-offer", "EXECUTED")]
        //state: DOMAIN_DOES_NOT_EXIST tested in following test (BuyOffers_ShouldFilterOfferWithoutExistingDomain)
        public async Task BuyOffers_ShouldFilterByState(string domainName, string state)
        {
            var offer = await GetOffersAsync($@"where: {{domainName: {{ equalTo: ""{domainName}"" }}, state: {{ in: {state} }} }}");

            offer.Items?.Select(i => i?.Domain?.Name).Should().AllBe(domainName);
        }

        [Test]
        public async Task BuyOffers_ShouldFilterOfferWithoutExistingDomain()
        {
            var offer = await GetOffersAsync($@"where: {{tokenId: {{ equalTo: 20 }}, state: {{ in: DOMAIN_DOES_NOT_EXIST }} }}");

            (offer.Items?.SingleOrDefault()?.Id).Should().Be("BuyOffer:invalid-missing-domain.buy-offer:0");
        }

        #endregion

        private async Task RunBuyOffersTest(string queryArgs, string?[] expectedNames)
        {
            var offers = await GetOffersAsync(queryArgs);

            offers.Items?.Select(d => d?.Domain?.Name).Should().Equal(expectedNames);
        }

        private static void VerifyTestBuyOffer(OfferDto? offer)
        {
            var expectedOffer = new OfferDto
            {
                Id = "BuyOffer:b:10:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:654321",
                TypeName = "BuyOffer",
                OperationGroupHash = "ogh-Offer",
                ExpiresAtUtc = GetTime(2030, 1, 1),
                CreatedAtUtc = GetTime(2021, 08, 08),
                BuyerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                SellerAddress = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                TokenId = 10,
                TokenContract = "KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p",
                Price = 123m,
                State = "ACTIVE",
                Domain = new DomainDataDto
                {
                    Id = "DomainData:valid.buy-offer",
                    Name = "valid.buy-offer",
                    Operators = new[] { TestOffer.MARKETPLACE_CONTRACT },
                    Owner = "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    Data = Array.Empty<DataItemDto>(),
                    TypeName = "DomainData"
                },
                BuyerAddressReverseRecord = ReverseRecordDto.GetExpected(
                    address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    hasOwnerReverseRecord: true,
                    operationGroupHash: "ogh-rr"
                )
            };
            offer.Should().BeEquivalentTo(expectedOffer);
        }

        private Task<OfferDto?> GetBuyOfferAsync(string queryArgs)
            => SendToGraphQLAsync<OfferDto?>($@"buyOffer({queryArgs}) {OfferDto.FieldsQuery}");

        private Task<Connection<OfferDto>> GetOffersAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<OfferDto>>($"buyOffers({queryArgs}) {Edges(OfferDto.FieldsQuery)}");
    }
}