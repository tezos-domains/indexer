﻿namespace TezosDomains.Api.IntegrationTests.Dto.Auctions
{
    public class BidderBalancesDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            operationGroupHash
            address
            balances {BalanceDto.FieldsQuery}
        }}";


        public string? OperationGroupHash { get; set; }
        public string? Address { get; set; }
        public BalanceDto[]? Balances { get; set; }
    }
}