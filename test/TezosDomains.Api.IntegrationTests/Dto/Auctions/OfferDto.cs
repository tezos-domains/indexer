﻿using System;

namespace TezosDomains.Api.IntegrationTests.Dto.Auctions
{
    public class OfferDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            operationGroupHash
            domain {DomainDataDto.FieldsQuery}
            tokenId
            tokenContract
            sellerAddress
            sellerAddressReverseRecord {ReverseRecordDto.FieldsQuery}
            buyerAddress
            buyerAddressReverseRecord {ReverseRecordDto.FieldsQuery}
            createdAtUtc
            expiresAtUtc
            price
            state
        }}";

        public string? OperationGroupHash { get; set; }
        public DomainDataDto? Domain { get; set; }
        public DateTime? ExpiresAtUtc { get; set; }
        public DateTime? CreatedAtUtc { get; set; }
        public string? SellerAddress { get; set; }
        public ReverseRecordDto? SellerAddressReverseRecord { get; set; }
        public string? BuyerAddress { get; set; }
        public ReverseRecordDto? BuyerAddressReverseRecord { get; set; }
        public string? TokenContract { get; set; }
        public string? State { get; set; }
        public decimal? Price { get; set; }
        public int? TokenId { get; set; }
    }

    public class DomainDataDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            name
            owner
            expiresAtUtc
            operators
            data {DataItemDto.FieldsQuery}
            operators
        }}";

        public string? Name { get; set; }
        public string? Owner { get; set; }
        public DateTime? ExpiresAtUtc { get; set; }
        public DataItemDto[]? Data { get; set; }
        public string[]? Operators { get; set; }
    }
}