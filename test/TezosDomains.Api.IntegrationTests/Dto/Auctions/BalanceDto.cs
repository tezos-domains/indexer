﻿namespace TezosDomains.Api.IntegrationTests.Dto.Auctions
{
    public sealed class BalanceDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
            {NodeFieldsQuery}
            tldName
            balance
        }}";

        public string? TldName { get; set; }
        public decimal? Balance { get; set; }
    }
}