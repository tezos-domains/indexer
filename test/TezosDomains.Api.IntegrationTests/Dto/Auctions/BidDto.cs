﻿namespace TezosDomains.Api.IntegrationTests.Dto.Auctions
{
    public class BidDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            operationGroupHash
            bidder
            amount
            bidderReverseRecord {ReverseRecordDto.FieldsQuery}
        }}";

        public string? OperationGroupHash { get; set; }
        public string? Bidder { get; set; }
        public decimal? Amount { get; set; }
        public ReverseRecordDto? BidderReverseRecord { get; set; }
    }
}