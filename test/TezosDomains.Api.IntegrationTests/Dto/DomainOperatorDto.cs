﻿namespace TezosDomains.Api.IntegrationTests.Dto
{
    public class DomainOperatorDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            address
            reverseRecord {ReverseRecordDto.FieldsQuery}
        }}";

        public string? Address { get; set; }
        public ReverseRecordDto? ReverseRecord { get; set; }

        public static DomainOperatorDto GetExpected(
            string address,
            ReverseRecordDto? reverseRecord = null
        )
            => new DomainOperatorDto
            {
                Id = $"DomainOperator:{address}",
                TypeName = "DomainOperator",
                Address = address,
                ReverseRecord = reverseRecord
            };
    }
}