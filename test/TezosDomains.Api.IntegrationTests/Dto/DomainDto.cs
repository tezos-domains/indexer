﻿using GraphQL.Types.Relay.DataObjects;
using System;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class DomainDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            operationGroupHash
            name
            label
            address
            owner
            parentName      
            parentOwner
            expiresAtUtc
            level
            tokenId
            data {DataItemDto.FieldsQuery}
            operators {DomainOperatorDto.FieldsQuery}
            lastAuction {AuctionDto.FieldsQuery}


            subdomains {TestBase.Edges("{ name }")}
            reverseRecord {ReverseRecordDto.FieldsQuery}

            addressReverseRecord {ReverseRecordDto.FieldsQuery}
            ownerReverseRecord {ReverseRecordDto.FieldsQuery}
            parentOwnerReverseRecord {ReverseRecordDto.FieldsQuery}
        }}";

        public string? OperationGroupHash { get; set; }
        public string? Name { get; set; }
        public string? Label { get; set; }
        public string? Address { get; set; }
        public string? Owner { get; set; }
        public string? ParentName { get; set; }
        public string? ParentOwner { get; set; }
        public DateTime? ExpiresAtUtc { get; set; }
        public int Level { get; set; }
        public int? TokenId { get; set; }
        public DataItemDto[]? Data { get; set; }
        public Connection<DomainDto> Subdomains { get; set; } = new();
        public ReverseRecordDto? ReverseRecord { get; set; }
        public ReverseRecordDto? AddressReverseRecord { get; set; }
        public ReverseRecordDto? OwnerReverseRecord { get; set; }
        public ReverseRecordDto? ParentOwnerReverseRecord { get; set; }
        public DomainOperatorDto[]? Operators { get; set; }
        public AuctionDto? LastAuction { get; set; }
    }
}