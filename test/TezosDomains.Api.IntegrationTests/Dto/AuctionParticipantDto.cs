﻿namespace TezosDomains.Api.IntegrationTests.Dto
{
    public class AuctionParticipantDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
            {NodeFieldsQuery}
            address
        }}";

        public string? Address { get; set; }
    }
}