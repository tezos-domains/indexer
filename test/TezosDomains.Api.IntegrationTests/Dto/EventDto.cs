﻿using System;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class EventDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
            {NodeFieldsQuery}
            type
            id
            block {BlockDto.FieldsQuery}
            sourceAddress
            sourceAddressReverseRecord {ReverseRecordDto.FieldsQuery}
            ... on AuctionBidEvent {{
                operationGroupHash
                bidAmount
                transactionAmount
                domainName
                previousBidAmount
                previousBidderAddress
                previousBidderAddressReverseRecord {ReverseRecordDto.FieldsQuery}
            }}
            ... on AuctionEndEvent {{
                winningBid
                domainName
                participants {AuctionParticipantDto.FieldsQuery}
            }}
            ... on AuctionWithdrawEvent {{
                operationGroupHash
                tldName
                withdrawnAmount
            }}
            ... on AuctionSettleEvent {{
                operationGroupHash
                domainName
                domainOwnerAddress
                domainForwardRecordAddress
                data {DataItemDto.FieldsQuery}
                registrationDurationInDays
                winningBid
            }}
            ... on DomainBuyEvent {{
                operationGroupHash
                domainName
                price
                durationInDays
                domainOwnerAddress
                domainForwardRecordAddress
                data {DataItemDto.FieldsQuery}
            }}
            ... on DomainRenewEvent {{
                operationGroupHash
                domainName
                durationInDays
                price
            }}
            ... on DomainSetChildRecordEvent {{
                operationGroupHash
                domainName
                domainOwnerAddress
                domainForwardRecordAddress
                isNewRecord
                data {DataItemDto.FieldsQuery}
            }}
            ... on DomainUpdateEvent {{
                operationGroupHash
                domainName
                domainOwnerAddress
                domainForwardRecordAddress
                data {DataItemDto.FieldsQuery}
            }}
            ... on DomainCommitEvent {{
                operationGroupHash
                commitmentHash
            }}
            ... on ReverseRecordUpdateEvent {{
                operationGroupHash
                name
                reverseRecordOwnerAddress
                reverseRecordAddress
            }}
            ... on ReverseRecordClaimEvent {{
                operationGroupHash
                name
                reverseRecordOwnerAddress
            }}
            ... on DomainGrantEvent {{
                operationGroupHash
                domainName
                nullableDurationInDays: durationInDays
                domainOwnerAddress
                domainForwardRecordAddress
                data {DataItemDto.FieldsQuery}
            }}
            ... on DomainClaimEvent {{
                operationGroupHash
                domainName
                domainOwnerAddress
                domainForwardRecordAddress
            }}
            ... on DomainTransferEvent {{
                operationGroupHash
                domainName
                newOwner
                newOwnerReverseRecord {ReverseRecordDto.FieldsQuery}
            }}
            ... on DomainUpdateOperatorsEvent {{
                operationGroupHash
                domainName
                operators
            }}
            ... on OfferPlacedEvent {{
                operationGroupHash
                tokenId
                domainName
                price
                priceWithoutFee
                fee
                expiresAtUtc
            }}
            ... on OfferUpdatedEvent {{
                operationGroupHash
                tokenId
                domainName
                price
                priceWithoutFee
                fee
                expiresAtUtc
            }}
            ... on OfferRemovedEvent {{
                operationGroupHash
                tokenId
                domainName
            }}
            ... on OfferExecutedEvent {{
                operationGroupHash
                tokenId
                domainName
                price
                priceWithoutFee
                fee
                sellerAddress
                sellerAddressReverseRecord {ReverseRecordDto.FieldsQuery}
            }}
            ... on BuyOfferPlacedEvent {{
                operationGroupHash
                tokenId
                domainName
                domainOwner
                price
                priceWithoutFee
                fee
                expiresAtUtc
            }}
            ... on BuyOfferRemovedEvent {{
                operationGroupHash
                tokenId
                domainName
            }}
            ... on BuyOfferExecutedEvent {{
                operationGroupHash
                tokenId
                domainName
                price
                priceWithoutFee
                fee
                buyerAddress
                buyerAddressReverseRecord {ReverseRecordDto.FieldsQuery}
            }}
        }}";

        public string? Type { get; set; }
        public decimal? WithdrawnAmount { get; set; }
        public decimal? WinningBid { get; set; }
        public decimal? BidAmount { get; set; }
        public decimal? TransactionAmount { get; set; }
        public decimal? PreviousBidAmount { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceWithoutFee { get; set; }
        public decimal? Fee { get; set; }
        public int? DurationInDays { get; set; }
        public int? NullableDurationInDays { get; set; }
        public string? DomainOwnerAddress { get; set; }
        public string? DomainForwardRecordAddress { get; set; }
        public string? TldName { get; set; }
        public string? SourceAddress { get; set; }
        public string? DomainName { get; set; }
        public string? DomainOwner { get; set; }
        public string? Name { get; set; }
        public DataItemDto[]? Data { get; set; }
        public BlockDto? Block { get; set; }
        public string? CommitmentHash { get; set; }
        public string? OperationGroupHash { get; set; }
        public int? RegistrationDurationInDays { get; set; }
        public ReverseRecordDto? SourceAddressReverseRecord { get; set; }
        public bool? IsNewRecord { get; set; }
        public AuctionParticipantDto[]? Participants { get; set; }
        public string? ReverseRecordOwnerAddress { get; set; }
        public string? ReverseRecordAddress { get; set; }
        public string? PreviousBidderAddress { get; set; }
        public ReverseRecordDto? PreviousBidderAddressReverseRecord { get; set; }
        public string? NewOwner { get; set; }
        public ReverseRecordDto? NewOwnerReverseRecord { get; set; }
        public string[]? Operators { get; set; }
        public DateTime? ExpiresAtUtc { get; set; }
        public int? TokenId { get; set; }
        public string? SellerAddress { get; set; }
        public ReverseRecordDto? SellerAddressReverseRecord { get; set; }
        public string? BuyerAddress { get; set; }
        public ReverseRecordDto? BuyerAddressReverseRecord { get; set; }
    }
}