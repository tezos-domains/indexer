﻿using System;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class BlockDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
            {NodeFieldsQuery}
            level
            hash
            timestamp
        }}";

        public int Level { get; set; }
        public string? Hash { get; set; }
        public DateTime Timestamp { get; set; }

        public static BlockDto GetExpected(int level, string hash, DateTime timestamp)
            => new BlockDto
            {
                Id = $"Block:{hash}",
                TypeName = "Block",
                Level = level,
                Hash = hash,
                Timestamp = timestamp
            };
    }
}