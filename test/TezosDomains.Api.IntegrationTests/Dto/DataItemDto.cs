﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class DataItemDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
            {NodeFieldsQuery}
            key
            rawValue
            value
        }}";

        public string? Key { get; set; }
        public string? RawValue { get; set; }

        [JsonConverter(typeof(ObjectToInferredTypesConverter))]
        public object? Value { get; set; }

        public static Dictionary<string, string> GetTestInput(string testSeed)
            => new()
            {
                { $"{testSeed}:string", "2268656c6c6f4074657a6f732e646f6d61696e7322" },
                { $"{testSeed}:int", "363030" },
                { $"{testSeed}:invalid-json", "676962626572697368" },
            };

        public static DataItemDto[] GetExpected(string parentId, string testSeed)
            => new[]
            {
                GetExpected(parentId, $"{testSeed}:string", "2268656c6c6f4074657a6f732e646f6d61696e7322", "hello@tezos.domains"),
                GetExpected(parentId, $"{testSeed}:int", "363030", 600),
                GetExpected(parentId, $"{testSeed}:invalid-json", "676962626572697368", null),
            };

        public static DataItemDto GetExpected(string parentId, string key, string rawValue, object? decodedValue)
            => new()
            {
                Id = $"DataItem:{parentId}:{key}",
                TypeName = "DataItem",
                Key = key,
                RawValue = rawValue,
                Value = decodedValue,
            };

        private sealed class ObjectToInferredTypesConverter : JsonConverter<object>
        {
            public override object? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
                => reader.TokenType switch
                {
                    JsonTokenType.Null => null,
                    JsonTokenType.True => true,
                    JsonTokenType.False => false,
                    JsonTokenType.Number => reader.GetInt32(),
                    JsonTokenType.String => reader.GetString(),
                    _ => JsonDocument.ParseValue(ref reader).RootElement.Clone()
                };

            public override void Write(Utf8JsonWriter writer, object objectToWrite, JsonSerializerOptions options)
                => JsonSerializer.Serialize(writer, objectToWrite, objectToWrite.GetType(), options);
        }
    }
}