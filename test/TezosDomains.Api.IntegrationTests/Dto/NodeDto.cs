﻿using System.Text.Json.Serialization;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public abstract class NodeDto
    {
        private const string TypeNameField = "__typename";
        public const string NodeFieldsQuery = "id\r\n" + TypeNameField;

        public string? Id { get; set; }

        [JsonPropertyName(TypeNameField)]
        public string? TypeName { get; set; }
    }
}