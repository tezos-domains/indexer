﻿using System;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class ReverseRecordDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
            {NodeFieldsQuery}
            operationGroupHash
            address
            owner
            ownerReverseRecord {{ address }}
            expiresAtUtc
            domain {{ name address }} 
        }}";

        public string? OperationGroupHash { get; set; }
        public string? Address { get; set; }
        public string? Owner { get; set; }
        public ReverseRecordDto? OwnerReverseRecord { get; set; }
        public DateTime? ExpiresAtUtc { get; set; }
        public DomainDto? Domain { get; set; }

        public static ReverseRecordDto GetExpected(
            string address,
            string owner,
            string operationGroupHash,
            bool hasOwnerReverseRecord = false,
            string? name = null,
            DateTime? expiresAtUtc = null
        )
            => new ReverseRecordDto
            {
                Id = $"ReverseRecord:{address}",
                TypeName = "ReverseRecord",
                OperationGroupHash = operationGroupHash,
                Address = address,
                Domain = name != null ? new DomainDto { Address = address, Name = name } : null,
                Owner = owner,
                OwnerReverseRecord = hasOwnerReverseRecord ? new ReverseRecordDto { Address = owner } : null,
                ExpiresAtUtc = expiresAtUtc
            };
    }
}