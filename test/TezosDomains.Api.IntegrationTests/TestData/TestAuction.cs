﻿using System;
using System.Linq;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public sealed record TestBid(string Bidder, decimal Amount, string? OperationGroupHash = null);

    public static class TestAuction
    {
        public static Auction Get(
            string id,
            DateTime? endsAtUtc = null,
            DateTime? ownedUntilUtc = null,
            DateTime? firstBidAtUtc = null,
            bool isSettled = false,
            BlockSlim? block = null,
            BlockSlim? validUntil = null,
            string? operationGroupHash = null,
            params TestBid[] bids
        )
        {
            block ??= TestBlock.Get();
            var storedBids = bids
                .Select(
                    bid => new Bid(
                        Block: block,
                        OperationGroupHash: bid.OperationGroupHash ?? TestRandom.GetString(),
                        Bidder: bid.Bidder,
                        Amount: bid.Amount
                    )
                )
                .ToList();
            return new Auction(
                Block: block,
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                AuctionId: id,
                DomainName: id.Split(":")[0],
                Bids: storedBids,
                EndsAtUtc: endsAtUtc ?? TestDateTime.Today.AddDays(1),
                OwnedUntilUtc: ownedUntilUtc ?? TestDateTime.Today.AddDays(2),
                IsSettled: isSettled,
                FirstBidAtUtc: firstBidAtUtc ?? TestDateTime.Today.AddDays(-1),
                BidCount: storedBids.Count,
                HighestBid: storedBids.Last(),
                CountOfUniqueBidders: storedBids.Select(b => b.Bidder).Distinct().Count(),
                ValidUntilBlockLevel: (validUntil?.Level).ConvertNullToMax(),
                ValidUntilTimestamp: (validUntil?.Timestamp).ConvertNullToMax()
            );
        }
    }
}