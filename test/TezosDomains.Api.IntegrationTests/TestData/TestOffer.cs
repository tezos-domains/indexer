﻿using System;
using System.Collections.Generic;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestOffer
    {
        public const string TOKEN_CONTRACT = "KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p";
        public const string MARKETPLACE_CONTRACT = "KT1UaPcfphum7ShyndWuC5uKSGACw3HUpjs5";

        public static Offer GetBuy(
            string id,
            string? initiatorAddress = null,
            BlockSlim? block = null,
            BlockSlim? validUntil = null,
            string? operationGroupHash = null,
            DateTime? createdAtUtc = null,
            DateTime? endsAtUtc = null,
            DomainData? domainData = null,
            string? tokenContract = null,
            int? tokenId = null,
            OfferBlockchainPresence presence = OfferBlockchainPresence.Present,
            decimal? price = null
        )
            => Get(
                id: id,
                initiatorAddress: initiatorAddress,
                block: block,
                validUntil: validUntil,
                operationGroupHash: operationGroupHash,
                createdAtUtc: createdAtUtc,
                endsAtUtc: endsAtUtc,
                domainData: domainData,
                tokenContract: tokenContract,
                tokenId: tokenId,
                presence: presence,
                price: price,
                acceptorAddress: domainData?.Owner,
                type: OfferType.BuyOffer
            );

        public static Offer Get(
            string id,
            string? initiatorAddress = null,
            BlockSlim? block = null,
            BlockSlim? validUntil = null,
            string? operationGroupHash = null,
            DateTime? createdAtUtc = null,
            DateTime? endsAtUtc = null,
            DomainData? domainData = null,
            string? tokenContract = null,
            int? tokenId = null,
            OfferBlockchainPresence presence = OfferBlockchainPresence.Present,
            decimal? price = null,
            string? acceptorAddress = null,
            OfferType type = OfferType.SellOffer
        )
        {
            block ??= TestBlock.Get();
            var address = initiatorAddress ?? TestAddress.GetRandom();
            price ??= TestRandom.GetDecimal();
            return new Offer(
                Block: block,
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                OfferId: id,
                Domain: domainData,
                ExpiresAtUtc: endsAtUtc.ConvertNullToMax(),
                CreatedAtUtc: createdAtUtc ?? TestDateTime.Today.AddDays(-1),
                InitiatorAddress: address,
                AcceptorAddress: acceptorAddress,
                TokenContract: tokenContract ?? TOKEN_CONTRACT,
                TokenId: tokenId ?? TestRandom.GetInt(),
                Type: type,
                PriceWithoutFee: price.Value * 0.975m,
                Fee: price.Value * 0.025m,
                Presence: presence,
                Validity: Offer.CalculateValidity(MARKETPLACE_CONTRACT, address, acceptorAddress, domainData, type),
                ValidUntilBlockLevel: (validUntil?.Level).ConvertNullToMax(),
                ValidUntilTimestamp: (validUntil?.Timestamp).ConvertNullToMax()
            );
        }

        public static DomainData GetDomainData(string? name = null, string? owner = null, bool useValidOperatorContract = true, DateTime? expiresAtUtc = null)
            => new DomainData(
                Name: name ?? TestRandom.GetString(),
                Owner: owner ?? TestAddress.GetRandom(),
                Operators: useValidOperatorContract ? new[] { MARKETPLACE_CONTRACT } : new string[0],
                new Dictionary<string, string>(),
                ExpiresAtUtc: expiresAtUtc.ConvertNullToMax()
            );
    }
}