﻿using System;
using System.Collections.Generic;
using TezosDomains.Common;
using TezosDomains.Data;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestDomain
    {
        public static Domain Get(
            string name,
            string? address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
            string owner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
            string? parentOwner = "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
            DateTime? expiresAtUtc = null,
            Dictionary<string, string>? data = null,
            int? tokenId = null,
            int? level = null,
            BlockSlim? block = null,
            string? operationGroupHash = null,
            BlockSlim? validUntil = null,
            string[]? operators = null,
            Auction? lastAuction = null
        )
        {
            var (label, parent, ancestors, domainLevel, nameForOrdering) = DomainNameUtils.Parse(name);
            block ??= TestBlock.Get();
            return new Domain(
                Block: block,
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                Name: name,
                Address: address,
                Owner: owner,
                Data: data ?? new Dictionary<string, string>(),
                TokenId: tokenId ?? (level == 2 ? TestRandom.GetInt() : null),
                Label: label,
                Ancestors: ancestors,
                ParentName: parent,
                ParentOwner: parentOwner,
                ExpiresAtUtc: expiresAtUtc ?? DateTime.MaxValue,
                ValidityKey: ancestors.Length >= 2 ? ancestors[1] : name,
                Level: level ?? domainLevel,
                NameForOrdering: nameForOrdering,
                Operators: operators ?? Array.Empty<string>(),
                LastAuction: lastAuction,
                ValidUntilBlockLevel: (validUntil?.Level).ConvertNullToMax(),
                ValidUntilTimestamp: (validUntil?.Timestamp).ConvertNullToMax()
            );
        }
    }
}