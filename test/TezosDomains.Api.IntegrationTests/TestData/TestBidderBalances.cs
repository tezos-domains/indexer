﻿using System.Linq;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestBidderBalances
    {
        public static BidderBalances Get(
            string address,
            BlockSlim? block = null,
            string? operationGroupHash = null,
            BlockSlim? validUntil = null,
            params (string tldName, decimal balance)[] balances
        )
            => new BidderBalances(
                Block: block ?? TestBlock.Get(),
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                Address: address,
                Balances: balances.ToDictionary(b => b.tldName, b => b.balance),
                ValidUntilBlockLevel: (validUntil?.Level).ConvertNullToMax(),
                ValidUntilTimestamp: (validUntil?.Timestamp).ConvertNullToMax()
            );
    }
}