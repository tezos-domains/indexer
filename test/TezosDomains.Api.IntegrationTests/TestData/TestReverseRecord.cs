﻿using System;
using TezosDomains.Common;
using TezosDomains.Data;
using TezosDomains.Data.Models;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestReverseRecord
    {
        public static ReverseRecord Get(
            string address,
            string? name = null,
            string owner = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
            DateTime? expiresAtUtc = null,
            BlockSlim? block = null,
            string? operationGroupHash = null,
            (DateTime? Timestamp, int? Level) validUntil = default
        )
        {
            var ancestors = name != null ? DomainNameUtils.Parse(name).Ancestors : null;
            return new ReverseRecord(
                Block: block ?? TestBlock.Get(),
                OperationGroupHash: operationGroupHash ?? TestRandom.GetString(),
                Address: address,
                Name: name,
                Owner: owner,
                ExpiresAtUtc: expiresAtUtc.ConvertNullToMax(),
                ValidityKey: ancestors?.Length >= 2 ? ancestors[1] : name,
                ValidUntilBlockLevel: validUntil.Level.ConvertNullToMax(),
                ValidUntilTimestamp: validUntil.Timestamp.ConvertNullToMax()
            );
        }
    }
}