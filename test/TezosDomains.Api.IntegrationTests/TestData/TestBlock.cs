﻿using TezosDomains.Data.Models;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestBlock
    {
        public static Block Get(
            int? level = null,
            string? hash = null,
            double daysOffset = 0,
            string? predecessor = null
        )
            => new(
                Level: level ?? TestCounter.GetNext(),
                Hash: hash ?? TestRandom.GetString(),
                Timestamp: TestDateTime.Today.AddDays(daysOffset),
                Predecessor: predecessor ?? TestRandom.GetString()
            );
    }
}