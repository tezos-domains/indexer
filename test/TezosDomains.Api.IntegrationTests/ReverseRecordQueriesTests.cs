using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.TestUtilities;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public class ReverseRecordQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var records = new[]
            {
                TestReverseRecord.Get(
                    address: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    name: "2.test.tez"
                ),
                TestReverseRecord.Get(
                    address: "KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf",
                    owner: "tz1a6Sxwj5wJKa1oXDGDVBjH2ZLVXk8zVcsb"
                ),
                TestReverseRecord.Get(
                    address: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
                    name: "3.other.tez",
                    expiresAtUtc: GetTime(2150, 2, 3),
                    operationGroupHash: "ogh-3.other.tez"
                ),
                TestReverseRecord.Get(
                    address: "KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD",
                    name: "expired.test.tez",
                    expiresAtUtc: GetTime(2001, 2, 3)
                )
            };
            DbContext.Insert(records);

            var block3 = TestBlock.Get(level: 3);
            var recordWithHistory = TestReverseRecord.Get(
                address: "tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA",
                name: "1.test.tez",
                owner: "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                block: block3
            );
            var block2 = TestBlock.Get(level: 2, daysOffset: -1);
            DbContext.Insert(
                recordWithHistory,
                Historize(
                    recordWithHistory with { Owner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL" },
                    block: block2,
                    validUntil: recordWithHistory.Block
                )
            );

            // Insert corresponding domains.
            foreach (var record in records.Append(recordWithHistory))
                if (record.Name != null)
                    DbContext.Insert(TestDomain.Get(name: record.Name, address: record.Address, expiresAtUtc: record.ExpiresAtUtc));

            DbContext.Insert(TestBlock.Get(level: 1, daysOffset: -2), block2, block3, TestBlock.Get(level: 4, daysOffset: 1));
        }

        [Test]
        public async Task ReverseRecord_ShouldLoadAllFields()
        {
            var record = await GetReverseRecordAsync(@"address: ""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""");

            VerifyTestRecord(record);
        }

        [TestCase("tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7")] // Not exist
        [TestCase("tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU")] // Deleted
        public async Task ReverseRecord_ShouldLoadNull_IfNotExistOrDelete(string address)
        {
            var record = await GetReverseRecordAsync($@"address: ""{address}""");

            record.Should().BeNull();
        }

        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "VALID", true)]
        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "ALL", true)]
        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "EXPIRED", false)]
        [TestCase("tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA", "VALID", true)] // Doesn't expire
        [TestCase("KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD", "VALID", false)]
        [TestCase("KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD", "ALL", true)]
        [TestCase("KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD", "EXPIRED", true)]
        [TestCase("tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU", "ALL", false)] // Deleted
        public async Task ReverseRecord_ShouldFilterByValidity(string address, string validity, bool expectedSuccess)
        {
            var record = await GetReverseRecordAsync(@$"address: ""{address}"", validity: {validity}");

            if (expectedSuccess)
            {
                record.Should().NotBeNull();
                record!.Address.Should().Be(expectedSuccess ? address : null);
            }
            else
            {
                record.Should().BeNull();
            }
        }

        public static readonly IEnumerable HistoryTestCases = new[]
        {
            new object?[] { 1, null, null },
            new object?[] { 2, null, "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL" },
            new object?[] { 3, null, "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi" },
            new object?[] { 4, null, "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi" },
            new object?[] { null, -2, null },
            new object?[] { null, -1, "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL" },
            new object?[] { null, 0, "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi" },
            new object?[] { null, 1, "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi" },
        };

        [TestCaseSource(nameof(HistoryTestCases))]
        public async Task ReverseRecord_ShouldFilterByHistory(int? blockLevel, int? blockDaysToNow, string? expectedOwner)
        {
            var record = await GetReverseRecordAsync(
                @"address: ""tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA"","
                + AtBlockFilter(blockLevel, blockDaysToNow)
            );

            (record?.Owner).Should().Be(expectedOwner);
        }

        [TestCaseSource(nameof(HistoryTestCases))]
        public async Task ReverseRecords_ShouldFilterByHistory(int? blockLevel, int? blockDaysToNow, string? expectedOwner)
        {
            var records = await GetReverseRecordsAsync(
                @"where: { address: { equalTo: ""tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA"" } },"
                + AtBlockFilter(blockLevel, blockDaysToNow)
            );

            records.Items?.Select(i => i?.Owner).SingleOrDefault().Should().Be(expectedOwner);
        }

        [Test]
        public async Task ReverseRecords_ShouldCountHistoricalEntriesCorrectly()
        {
            var records = await GetReverseRecordsAsync(
                @"where: { owner: { startsWith: TZ2 } },
                atBlock: { level: 2 }"
            );

            records.TotalCount.Should().Be(1);
        }

        [Test]
        public async Task ReverseRecords_ShouldLoadAllFields()
        {
            var records = await GetReverseRecordsAsync(@"where: { address: { equalTo: ""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm"" }}");

            VerifyTestRecord(records.Items?.Single());
        }

        private static void VerifyTestRecord(ReverseRecordDto? record)
        {
            var expected = ReverseRecordDto.GetExpected(
                address: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
                name: "3.other.tez",
                owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                expiresAtUtc: GetTime(2150, 2, 3),
                operationGroupHash: "ogh-3.other.tez"
            );
            record.Should().BeEquivalentTo(expected);
        }

        [Test]
        public Task ReverseRecords_ShouldOrderByNameByDefault()
            => RunRecordsTest("where: {}", expectedNames: new[] { null, "1.test.tez", "2.test.tez", "3.other.tez" });

        [TestCase("order: { field: NAME }", new[] { null, "1.test.tez", "2.test.tez", "3.other.tez" })]
        [TestCase("order: { field: NAME direction: DESC }", new[] { "3.other.tez", "2.test.tez", "1.test.tez", null })]
        public Task ReverseRecords_ShouldOrderAccordingToOrderArgument(string queryArgs, string[] expectedNames)
            => RunRecordsTest(queryArgs, expectedNames);

        [TestCase("address", @"""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""", new[] { "3.other.tez" })]
        [TestCase("name", @"""1.TEST.tez""", new[] { "1.test.tez" })] // Filter should be case-insensitive
        [TestCase("owner", @"""tz1a6Sxwj5wJKa1oXDGDVBjH2ZLVXk8zVcsb""", new string?[] { null })]
        [TestCase("expiresAtUtc", @"""2150-02-03T00:00:00.000+00:00""", new[] { "3.other.tez" })]
        public Task ReverseRecords_ShouldFilterByField(string field, string value, string?[] expectedNames)
            => RunRecordsTest($"where: {{ {field}: {{ equalTo: {value} }}}}", expectedNames);

        [TestCase("VALID", new[] { null, "1.test.tez", "2.test.tez", "3.other.tez" })]
        [TestCase("EXPIRED", new[] { "expired.test.tez" })]
        [TestCase("ALL", new[] { null, "1.test.tez", "2.test.tez", "3.other.tez", "expired.test.tez" })]
        public Task ShouldFilterByValidity(string validity, string[] expectedNames)
            => RunRecordsTest($"where: {{ validity: {validity} }}", expectedNames);


        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "3.other.tez")]
        [TestCase("KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf", null)] // ReverseRecord has no name
        public async Task ShouldLoadRelatedDomainRecord_IfNameAndAddressMatched(string address, string? expectedNameOrNull)
        {
            var reverseRecord = await GetReverseRecordAsync($@"address: ""{address}""");

            reverseRecord.Should().NotBeNull();
            if (expectedNameOrNull == null)
            {
                reverseRecord!.Domain.Should().BeNull();
            }
            else
            {
                reverseRecord!.Domain.Should().NotBeNull();
                reverseRecord!.Domain!.Name.Should().Be(expectedNameOrNull);
                reverseRecord.Domain.Address.Should().Be(address);
            }
        }

        private async Task RunRecordsTest(string queryArgs, string?[] expectedNames)
        {
            var records = await GetReverseRecordsAsync(queryArgs);

            records.Items?.Select(d => d?.Domain?.Name).Should().Equal(expectedNames);
        }

        private Task<ReverseRecordDto?> GetReverseRecordAsync(string queryArgs)
            => SendToGraphQLAsync<ReverseRecordDto?>($@"reverseRecord({queryArgs}) {ReverseRecordDto.FieldsQuery}");

        private Task<Connection<ReverseRecordDto>> GetReverseRecordsAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<ReverseRecordDto>>($"reverseRecords({queryArgs}) {Edges(ReverseRecordDto.FieldsQuery)}");
    }
}