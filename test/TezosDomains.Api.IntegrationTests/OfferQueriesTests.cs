using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.Models.Marketplace;
using TezosDomains.TestUtilities;
using TezosDomains.TestUtilities.TestData;

namespace TezosDomains.Api.IntegrationTests
{
    [Parallelizable(ParallelScope.All)]
    public class OfferQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            #region Insert Offers

            const int currentBlockLevel = 3;
            var sellerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n";

            DbContext.Insert(
                TestOffer.GetBuy( //buy offer should be filtered out in offer queries
                    operationGroupHash: "ogh-BuyOffer-NOTUSED",
                    id: "b:1:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:654321",
                    tokenId: 1,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 8),
                    endsAtUtc: GetTime(2030, 1, 1),
                    presence: OfferBlockchainPresence.Present,
                    domainData: TestOffer.GetDomainData(name: "valid.buy-offer", owner: sellerAddress, useValidOperatorContract: true),
                    price: 123m
                ),
                TestOffer.Get(
                    operationGroupHash: "ogh-Offer",
                    id: "10:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:654321",
                    tokenId: 10,
                    initiatorAddress: sellerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 7),
                    endsAtUtc: GetTime(2030, 1, 1),
                    presence: OfferBlockchainPresence.Present,
                    domainData: TestOffer.GetDomainData(name: "valid.off", owner: sellerAddress, useValidOperatorContract: true),
                    price: 123m
                ),
                TestOffer.Get(
                    id: "invalid-missing-domain.off:0",
                    tokenId: 20,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 6),
                    endsAtUtc: GetTime(2030, 2, 5),
                    presence: OfferBlockchainPresence.Present,
                    price: 1m
                ),
                TestOffer.Get(
                    id: "offer-expired.off:0",
                    tokenId: 30,
                    initiatorAddress: sellerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 5),
                    endsAtUtc: GetTime(2000, 2, 6),
                    presence: OfferBlockchainPresence.Present,
                    price: 2m,
                    domainData: TestOffer.GetDomainData(name: "offer-expired.off", owner: sellerAddress)
                ),
                TestOffer.Get(
                    id: "domain-expired.off:0",
                    tokenId: 40,
                    initiatorAddress: sellerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 08, 4),
                    endsAtUtc: GetTime(2030, 2, 7),
                    presence: OfferBlockchainPresence.Present,
                    price: 3m,
                    domainData: TestOffer.GetDomainData(
                        name: "domain-expired.off",
                        owner: sellerAddress,
                        useValidOperatorContract: true,
                        expiresAtUtc: GetTime(2000, 2, 4)
                    )
                ),
                TestOffer.Get(
                    tokenId: 60,
                    id: "removed.off:0",
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.1),
                    createdAtUtc: GetTime(2021, 08, 3),
                    endsAtUtc: GetTime(2000, 2, 8),
                    presence: OfferBlockchainPresence.Removed,
                    price: 4m,
                    domainData: TestOffer.GetDomainData(name: "removed.off", owner: sellerAddress)
                ),
                TestOffer.Get(
                    tokenId: 70,
                    id: "invalid-domain-owner.off:0",
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.1),
                    createdAtUtc: GetTime(2021, 08, 2),
                    endsAtUtc: GetTime(2030, 2, 9),
                    presence: OfferBlockchainPresence.Present,
                    initiatorAddress: TestAddress.GetRandom(),
                    price: 5m,
                    domainData: TestOffer.GetDomainData(name: "invalid-domain-owner.off", owner: sellerAddress)
                ),
                TestOffer.Get(
                    tokenId: 80,
                    id: "invalid-domain-operators.off:0",
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.1),
                    createdAtUtc: GetTime(2021, 08, 1),
                    endsAtUtc: GetTime(2030, 2, 10),
                    presence: OfferBlockchainPresence.Present,
                    initiatorAddress: sellerAddress,
                    price: 6m,
                    domainData: TestOffer.GetDomainData(name: "invalid-domain-operators.off", owner: sellerAddress, useValidOperatorContract: false)
                ),
                TestOffer.Get(
                    id: "domain-expiring-soon.off:0",
                    tokenId: 90,
                    initiatorAddress: sellerAddress,
                    block: TestBlock.Get(currentBlockLevel, daysOffset: -0.4),
                    createdAtUtc: GetTime(2021, 07, 30),
                    endsAtUtc: GetTime(2030, 2, 11),
                    presence: OfferBlockchainPresence.Present,
                    price: 3m,
                    domainData: TestOffer.GetDomainData(
                        name: "domain-expiring-soon.off",
                        owner: sellerAddress,
                        useValidOperatorContract: true,
                        expiresAtUtc: DateTime.UtcNow.Date.AddDays(3)
                    )
                )
            );

            var executed = TestOffer.Get(
                id: "executed.off:0",
                tokenId: 50,
                initiatorAddress: sellerAddress,
                createdAtUtc: GetTime(2021, 07, 1),
                endsAtUtc: null,
                block: TestBlock.Get(currentBlockLevel, daysOffset: -0.1),
                presence: OfferBlockchainPresence.Executed,
                domainData: TestOffer.GetDomainData(name: "executed.off", owner: sellerAddress),
                price: 333m
            );
            DbContext.Insert(
                executed,
                Historize(
                    executed with { Presence = OfferBlockchainPresence.Present },
                    block: TestBlock.Get(level: 2, daysOffset: -1),
                    validUntil: executed.Block
                )
            );

            #endregion

            DbContext.Insert(
                TestBlock.Get(level: 1, daysOffset: -2),
                TestBlock.Get(level: 2, daysOffset: -1),
                TestBlock.Get(level: 3, daysOffset: 0.3)
            );

            DbContext.Insert(TestDomain.Get(name: "valid.off", owner: sellerAddress));
            DbContext.Insert(TestDomain.Get(name: "executed.off", owner: sellerAddress));
            DbContext.Insert(TestDomain.Get(name: "offer-expired.off", owner: sellerAddress));
            DbContext.Insert(TestDomain.Get(name: "domain-expired.off", owner: sellerAddress, expiresAtUtc: GetTime(2000, 2, 4)));
            DbContext.Insert(TestDomain.Get(name: "domain-expiring-soon.off", owner: sellerAddress, expiresAtUtc: DateTime.UtcNow.Date.AddDays(3)));
            DbContext.Insert(TestDomain.Get(name: "invalid-domain-owner.off", owner: sellerAddress, expiresAtUtc: GetTime(2000, 2, 4)));
            DbContext.Insert(TestDomain.Get(name: "invalid-domain-operators.off", owner: sellerAddress, expiresAtUtc: GetTime(2000, 2, 4)));

            DbContext.Insert(TestReverseRecord.Get(address: sellerAddress, name: "bidder.tez", operationGroupHash: "ogh-rr"));
        }

        #region Offer

        [Test]
        public async Task Offer_ShouldLoadAllFields()
        {
            var offer = await GetOfferAsync("sellerAddress: \"tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n\",tokenId: 10, startedAtLevel: 654321");

            VerifyTestOffer(offer!);
        }

        [Test]
        public async Task Offer_ShouldLoadNull_IfNotExist()
        {
            var offer = await GetOfferAsync("sellerAddress: \"tz1eFX5MRWV88wCvodK2i4Lq2ufSAGHRbhAY\" tokenId: -1, startedAtLevel: 0");

            offer.Should().BeNull();
        }

        #endregion

        #region CurrentOffer

        [TestCase("valid.off", true)]
        [TestCase("domain-expiring-soon.off", true)]
        [TestCase("invalid-domain-owner.off", false)]
        [TestCase("invalid-domain-operators.off", false)]
        [TestCase("offer-expired.off", false)]
        [TestCase("domain-expired.off", false)]
        [TestCase("removed.off", false)]
        [TestCase("executed.off", false)]
        public async Task CurrentOffer_ShouldShowOnlyValidOffers(string domainName, bool shouldBeReturned)
        {
            var currentOffer = await SendToGraphQLAsync<OfferDto?>($@"currentOffer( domainName: ""{domainName}"" ) {OfferDto.FieldsQuery}");

            (currentOffer?.Domain?.Name).Should().Be(shouldBeReturned ? domainName : null);
        }

        #endregion

        #region Offers

        [Test]
        public async Task Offers_ShouldLoadAllFields()
        {
            var offers = await GetOffersAsync(@"where: { domainName: { equalTo: ""valid.off"" }}");

            offers.Items.Should().HaveCount(1);
            VerifyTestOffer(offers.Items?.First());
        }

        [Test]
        public Task Offers_ShouldOrderByCreatedAtDescByDefault()
            => RunOffersTest(
                "where: {}",
                expectedNames: new[]
                {
                    "valid.off", null, "offer-expired.off", "domain-expired.off", "removed.off", "invalid-domain-owner.off", "invalid-domain-operators.off",
                    "domain-expiring-soon.off", "executed.off"
                }
            );

        [TestCase(
            "field: ID direction: ASC",
            new[]
            {
                "valid.off", "domain-expired.off", "domain-expiring-soon.off", "executed.off", "invalid-domain-operators.off", "invalid-domain-owner.off", null,
                "offer-expired.off", "removed.off"
            }
        )]
        [TestCase(
            "field: DOMAIN_NAME direction: DESC",
            new[]
            {
                "valid.off", "removed.off", "offer-expired.off", "invalid-domain-owner.off", "invalid-domain-operators.off", "executed.off",
                "domain-expiring-soon.off", "domain-expired.off", null
            }
        )]
        [TestCase(
            "field: ENDS_AT direction: ASC",
            new[]
            {
                "offer-expired.off", "removed.off", "valid.off", null, "domain-expired.off", "invalid-domain-owner.off",
                "invalid-domain-operators.off", "domain-expiring-soon.off", "executed.off"
            }
        )]
        [TestCase(
            "field: TOKEN_ID direction: DESC",
            new[]
            {
                "domain-expiring-soon.off", "invalid-domain-operators.off", "invalid-domain-owner.off", "removed.off", "executed.off", "domain-expired.off",
                "offer-expired.off", null, "valid.off"
            }
        )]
        [TestCase(
            "field: PRICE direction: DESC",
            new[]
            {
                "executed.off", "valid.off", "invalid-domain-operators.off", "invalid-domain-owner.off", "removed.off", "domain-expiring-soon.off",
                "domain-expired.off", "offer-expired.off", null
            }
        )]
        public Task Offers_ShouldOrderAccordingToQuery(string order, string[] expectedNames)
            => RunOffersTest($"order: {{ {order} }}", expectedNames);

        [TestCase("domainName", "equalTo", @"""valid.off""", new[] { "valid.off" })] // Filter should be case-insensitive
        [TestCase("state", "in", @"[EXECUTED,DOMAIN_IS_EXPIRING_SOON]", new[] { "domain-expiring-soon.off", "executed.off" })]
        [TestCase("state", "in", @"[ACTIVE]", new[] { "valid.off" })]
        [TestCase(
            "sellerAddress",
            "equalTo",
            "\"tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n\"",
            new[] { "valid.off", "offer-expired.off", "domain-expired.off", "invalid-domain-operators.off", "domain-expiring-soon.off", "executed.off" }
        )]
        [TestCase("createdAtUtc", "greaterThan", @"""2021-08-06T00:00:00Z""", new[] { "valid.off" })]
        [TestCase("endsAtUtc", "lessThan", @"""2030-02-02T00:00:00Z""", new[] { "valid.off", "offer-expired.off", "removed.off" })]
        [TestCase("tokenId", "greaterThan", "70", new[] { "invalid-domain-operators.off", "domain-expiring-soon.off" })]
        [TestCase("price", "greaterThan", "\"100\"", new[] { "valid.off", "executed.off" })]
        public Task Offers_ShouldFilterByField(string field, string filterType, string value, string[] expectedNames)
            => RunOffersTest(@$"where: {{ {field}: {{ {filterType}: {value} }}}}", expectedNames);

        [TestCase("valid.off", "ACTIVE")]
        [TestCase("invalid-domain-owner.off", "OFFER_SELLER_DOMAIN_OWNER_MISMATCH")]
        [TestCase("invalid-domain-operators.off", "DOMAIN_OPERATORS_CONTRACT_MISSING")]
        [TestCase("offer-expired.off", "OFFER_EXPIRED")]
        [TestCase("domain-expired.off", "DOMAIN_EXPIRED")]
        [TestCase("domain-expiring-soon.off", "DOMAIN_IS_EXPIRING_SOON")]
        [TestCase("removed.off", "REMOVED")]
        [TestCase("executed.off", "EXECUTED")]
        //state: DOMAIN_DOES_NOT_EXIST tested in following test (Offer_ShouldFilterOfferWithoutExistingDomain)
        public async Task Offers_ShouldFilterByState(string domainName, string state)
        {
            var offer = await GetOffersAsync($@"where: {{domainName: {{ equalTo: ""{domainName}"" }}, state: {{ in: {state} }} }}");

            (offer.Items?.SingleOrDefault()?.Domain?.Name).Should().Be(domainName);
        }

        [Test]
        public async Task Offers_ShouldFilterOfferWithoutExistingDomain()
        {
            var offer = await GetOffersAsync($@"where: {{tokenId: {{ equalTo: 20 }}, state: {{ in: DOMAIN_DOES_NOT_EXIST }} }}");

            (offer.Items?.SingleOrDefault()?.Id).Should().Be("Offer:invalid-missing-domain.off:0");
        }

        #endregion

        private async Task RunOffersTest(string queryArgs, string?[] expectedNames)
        {
            var offers = await GetOffersAsync(queryArgs);

            offers.Items?.Select(d => d?.Domain?.Name).Should().Equal(expectedNames);
        }

        private static void VerifyTestOffer(OfferDto? offer)
        {
            var expectedOffer = new OfferDto
            {
                Id = "Offer:10:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n:654321",
                TypeName = "Offer",
                OperationGroupHash = "ogh-Offer",
                ExpiresAtUtc = GetTime(2030, 1, 1),
                CreatedAtUtc = GetTime(2021, 08, 7),
                SellerAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                TokenId = 10,
                TokenContract = "KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p",
                Price = 123m,
                State = "ACTIVE",
                Domain = new DomainDataDto
                {
                    Id = "DomainData:valid.off",
                    Name = "valid.off",
                    Operators = new[] { TestOffer.MARKETPLACE_CONTRACT },
                    Owner = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    Data = Array.Empty<DataItemDto>(),
                    TypeName = "DomainData"
                },
                SellerAddressReverseRecord = ReverseRecordDto.GetExpected(
                    address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    owner: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    hasOwnerReverseRecord: true,
                    operationGroupHash: "ogh-rr"
                )
            };
            offer.Should().BeEquivalentTo(expectedOffer);
        }

        private Task<OfferDto?> GetOfferAsync(string queryArgs)
            => SendToGraphQLAsync<OfferDto?>($@"offer({queryArgs}) {OfferDto.FieldsQuery}");

        private Task<Connection<OfferDto>> GetOffersAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<OfferDto>>($"offers({queryArgs}) {Edges(OfferDto.FieldsQuery)}");
    }
}