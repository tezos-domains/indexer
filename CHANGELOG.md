# Change Log
## All notable changes to this project will be documented in this file. See [versionize](https://github.com/saintedlama/versionize) for commit guidelines.

<a name="8.6.2"></a>
## 8.6.2 (2024-6-12)

### Bug Fixes

* ci

<a name="8.6.1"></a>
## 8.6.1 (2024-6-11)

### Bug Fixes

* fix gitlab-ci.yaml

<a name="8.6.0"></a>
## 8.6.0 (2024-6-11)

### Bug Fixes

* fix parsing of tz4 and sr1 addresses

<a name="8.5.0"></a>
## 8.5.0 (2024-4-17)

### Features

* Extend domain filters with specific character filtering like only digits. Added under new domain's label filtering.

<a name="8.4.0"></a>
## 8.4.0 (2024-2-28)

### Features

* Upgrade MongoDb Driver to version 2.24.0 supporting MongoDB 7.0.

<a name="8.3.2"></a>
## 8.3.2 (2023-5-17)

### Bug Fixes

* **api:** Fix paging using cursor for sorts where DirectionOverride is used as first order field

<a name="8.3.1"></a>
## 8.3.1 (2023-5-17)

### Bug Fixes

* **api:** Fix paging using cursor for sorts where DirectionOverride is used

<a name="8.3.0"></a>
## 8.3.0 (2023-3-15)

### Features

* **indexer:** Add Mumbai protocol

<a name="8.2.0"></a>
## 8.2.0 (2023-2-27)

### Features

* Update mongo driver to support MongoDB6.0

<a name="8.1.0"></a>
## 8.1.0 (2023-1-12)

### Bug Fixes

* **indexer:** Fix DomainGrantEvent.DurationInDays value to null for domains without expiration
* **indexer:** Update mainnet configuration to index DomainClaimEvent

### Features

* Add Lima protocol

<a name="8.0.4"></a>
## 8.0.4 (2022-11-18)

### Bug Fixes

* **api:** fix CurrentBuyOffer returning buyOffer when sellOffer exists

<a name="8.0.3"></a>
## 8.0.3 (2022-11-2)

### Bug Fixes

* **api:** Fix offer cursor when ordering by ExpiresAtUtc

<a name="8.0.2"></a>
## 8.0.2 (2022-11-1)

### Bug Fixes

* **api:** Fix ExpiresAtUtc null value

<a name="8.0.1"></a>
## 8.0.1 (2022-10-31)

<a name="8.0.0"></a>
## 8.0.0 (2022-10-24)

### Bug Fixes

* **api:** Use DateTimeFilter for OffersFilter.CreatedAtUtc (instead of NullableDateTimeFilter). Field cannot be nullable.

### Breaking Changes

* MongoDb performance optimization (store DateTime/int null values as DateTime.Max/int.Max for an upper range bound)

<a name="7.0.1"></a>
## 7.0.1 (2022-10-19)

<a name="7.0.0"></a>
## 7.0.0 (2022-10-14)

### Bug Fixes

* **indexer:** fix BuyOfferExecutedEvent for execute_offer

### Features

* Add DomainOwner to BuyOfferPlacedEvent
* Added BuyOffer (an offer created by a buyer)
* improve metrics description for generic classes (i.e....
* **indexer:** Do not index invalid offer events

### Breaking Changes

* Add DomainOwner to BuyOfferPlacedEvent
* Added BuyOffer (an offer created by a buyer)

<a name="6.0.0"></a>
## 6.0.0 (2022-8-22)

### Features

* Add LastAuction to Domain
* Add UniqueBidderCount to Auction
* **api:** GraphQL shcema is now ordered alphabetically.
* **indexer:** Add ghostnet configuration
* **indexer:** Add Kathmandunet config
* **indexer:** Add support for rollups address type (txr1) introduced in Jakarta
* **indexer:** Optimize creation of indexes

### Breaking Changes

* Add LastAuction to Domain
* Add UniqueBidderCount to Auction

<a name="5.1.0"></a>
## 5.1.0 (2022-6-27)

### Features

* upgrade to .NET6
* **api:** Upgrade to GraphQL.NET v5
* **indexer:** Add Jakartanet configuration
* **indexer:** Add support for rollups address type (txr1) introduced in Jakarta

<a name="5.0.0"></a>
## 5.0.0 (2022-5-17)

### Features

* default "DOMAIN "order changed to ordering by 2nd level domain (alice.org -> bob.com instead of current ordering bob.com -> alice.org)
* Generate DomainClaimEvent when domain is claimed through DNSRegistrar
* **api:** Allow specific sorting direction per field (currently there is only one direction for all fields)
* **indexer:** Improve sorting of ExpiresAtUtc (null as DateTime.Max). Stored additional ExpiresAsUtcForOrder field in db.

<a name="4.0.1"></a>
## 4.0.1 (2022-5-14)

### Bug Fixes

* **api:** Use atBlock for Validity calculation

<a name="4.0.0"></a>
## 4.0.0 (2022-3-22)

### Features

* Added API filter on Price. Offer price is now stored in db.
* **indexer:** Add Db lock info in IndexingProgressHealthCheck. Create /lock route for possible use in LB to select active instance.
* **indexer:** Add Ithaca configuration. The BlockReader can index up to head -n blocks (Configuration value TezosContract.NumberOfBlockConfirmations)

<a name="3.3.0"></a>
## 3.3.0 (2022-1-24)

### Features

* **api:** Filter domains by operators

<a name="3.2.0"></a>
## 3.2.0 (2021-11-9)

### Features

* **indexer:** Replace hangzhounet with hangzhou2net configuration

<a name="3.1.0"></a>
## 3.1.0 (2021-10-14)

### Features

* **api:** Log query variables when error occurs
* **indexer:** Add hangzhounet configuration
* **indexer:** Deprecate florencenet configuration

<a name="3.0.2"></a>
## 3.0.2 (2021-9-29)

### Bug Fixes

* **api:** Fix offers order definition (also fixes the graph cursor)

<a name="3.0.1"></a>
## 3.0.1 (2021-9-26)

### Bug Fixes

* **indexer:** OfferProcessors to only retrieve domains with tokenId

<a name="3.0.0"></a>
## 3.0.0 (2021-9-25)

### Bug Fixes

* Offer fee is stored in db and visible in API
* **indexer:** Fix domain expiresAtUct synchronization in offers domainData
* **indexer:** Fix parsing nullable structs to return null instead of default struct value
* **indexer:** Update domainData in every offer (not only in present offers)

### Features

* Add events for DomainUpdateOperatorsEvent and Offer operations
* added Offer base price and fee
* Added Offers (Indexer/API)
* Index TZIP-12 Operators on domain and list them on API (part of DomainGraphType)

### Breaking Changes

* added Offer base price and fee
* Index TZIP-12 Operators on domain and list them on API (part of DomainGraphType)

<a name="2.0.0"></a>
## 2.0.0 (2021-6-21)

### Bug Fixes

* **api:** Fix unhandled exception when empty query is sent to /graphql endpoint

### Features

* Parent, ParentOwner, ParentOwnerReverseRecord added to Indexer and API
* **indexer:** Add Granadanet network configuration
* **indexer:** Improve tezos node http client to support base url with path

### Breaking Changes

* Parent, ParentOwner, ParentOwnerReverseRecord added to Indexer and API

<a name="1.1.0"></a>
## 1.1.0 (2021-5-31)

### Bug Fixes

* Fixed logging of the current environment.
* **api:** Allow graphql Int arguments be supplied as strings
* **api:** Fixed generation of GraphQL schema by fixing abstract syntax tree of default order input object.
* **indexer:** Refactored DomainUpdateEventProcessor to process DomainUpdateEvent, DomainSetChildRecordEvent, DomainTransferEvent togehter. The refactor fixes reading updated current domainState while processing events.

### Features

* Added check for all properties to be present on Mongo deserialization.
* Added OTP guard to /health.
* Added test which guard breaking changes in GraphQL schema.
* Generate DomainTransferEvent when Domain owner changes and show this new event type in API
* Index domain TZIP-12 Token id. Extend DomainGraphType with tokenId.
* **api:** Add NewOwnerReverseRecord to DomainTransferEvent
* **indexer:** Changed indexing to use `lazy_storage_diff` instead of `big_map_diff`.

### Other

* BREAKING CHANGE(api): Update packages - Api only accepts for DateTime strings in ISO-8601 format ("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK")
* Merge branch 'release/1.0.x' into 'master'
* Added documentation for monitoring (+ added grafana dashboards)
* Added JMeter stress tests.
* How to run tests.
* Replaced Newtonsoft.JSON with System.Text.Json.
* **indexer:** Added scoped logging for block processing with its details.
* **indexer:** AuctionWithdrawEvent generation refactored (withdraw event now correctly reports destination address)
* **indexer:** Improve console logging (add scope properties)
* **indexer:** Subscriber configuration simplified.

<a name="1.0.1"></a>
## 1.0.1 (2021-5-18)

### Bug Fixes

* **api:** Fix CurrentAuction query to not return settled auctions

<a name="1.0.0"></a>
## 1.0.0 (2021-5-7)

### Bug Fixes

* Removed useless and failing `ID` on the root query.
* **api:** Fixed conflict in registration of order indexes.
* **indexer:** Fixed deserialization of `additional_info` from node's /version in order to fix indexer health.

### Features

* Added `Auction.BidAmountSum` and related `BID_AMOUNT_SUM` order.
* Added `decodedValue` property to `DataItem` in GraphQL.
* Automatically creating db indexes for defined combinations of order fields.
* **api:** Added support for multiple order Mongo fields for Tezos API.
* **indexer:** Added health check which checks progress in block indexing. If there is none then it degrades, fails, and ultimately shuts down the app.
* **indexer:** Added timeout config for HTTP requests.

### Other

* Renamed `cancellationToken` variable to `cancellation`.
* Reworked history to store documents in flat collection.

<a name="0.8.2"></a>
## 0.8.2 (2021-4-21)

### Bug Fixes

* **api:** Fixed secondary ordering if primary uses descending direction.
* **api:** Remove unnecessary assertion in Auction.GetState (in edge cases can currently throw before when time synchronization is little bit off)

<a name="0.8.1"></a>
## 0.8.1 (2021-4-20)

### Bug Fixes

* **indexer:** Update OperationGroupHash in Domain and ReverseRecord when ValidityUpdate happens

<a name="0.8.0"></a>
## 0.8.0 (2021-4-19)

### Features

* **indexer:** Added `TezosNode:ReplayDelays`.

### Other

* chore:Increase db timeout to 5s
* fix(api):Extend Indexer's health LatestBlockDelay on production (Mainnet has slower block generation ~1 block/min)
* fix(indexer):Fix mongoDb writeConflict error when indexing bigger blocks (with many changes)
* Merge branch 'release/0.7.x' into 'master'
* Merge branch 'release/0.7.x' into 'master'
* log graphql query on error
* **indexer:** Added smoke tests.

<a name="0.7.3"></a>
## 0.7.3 (2021-4-19)

### Other

* fix(indexer):Improve fix EntrypointValue as an array. Only allow array as...

<a name="0.7.2"></a>
## 0.7.2 (2021-4-18)

### Other

* fix(indexer):Allow EntrypointValue to be represented as an array instead of binary tree object

<a name="0.7.1"></a>
## 0.7.1 (2021-4-16)

### Other

* fix(indexer):Fix generating grant events for multiple domainUpdates for same domain in one block

<a name="0.7.0"></a>
## 0.7.0 (2021-4-7)

### Bug Fixes

* do not fail MongoDb health check when DB is empty
* Fixed `MongoDbFactory` to use the configuration from DI + simplified `DatabaseName` for tests.
* Fixed passing of OneTimePassword environment variable for docker-compose which also fixes schema docs generation.
* **indexer:** Fized subscriber type names in appsettings.json.

### Features

* Added `OperationGroupHash` to remaining documents.
* Added info about host environment to `/config`.
* **api:** Added `OperationGroupHash` on GraphQL to remaining documents.
* **indexer:** add Florencenet contracts

<a name="0.6.2"></a>
## 0.6.2 (2021-3-16)

### Other

* fix docker-compose, removed serilog.CI.serilog.CI.json

<a name="0.6.1"></a>
## 0.6.1 (2021-3-16)

### Other

* improve pipeline runs (added CI configuration, log sink to slack), fix Dockerfile

<a name="0.6.0"></a>
## 0.6.0 (2021-3-16)

### Bug Fixes

* fix race condition for db initialization (settings object was setting twice)
* Fixed binding of `TezosDomains:Cors:AllowedOrigins` configuration.
* Fixed letter-casing of referred `appsettings` in indexer because it's failing on Docker.
* Fixed order of `appsettings.{network}.json` so that environment variables can override it.
* Fixed serialization of exceptions with circular references at /health endpoint.
* **indexer:** Fix AuctionWithdrawEvent generation. TezosBlockTraverser only calls OnOutgoingMoneyTransaction for transaction with amount > 0
* **indexer:** fix settings initialization (do not insert settings object if already exists)

### Features

* Add domain grant operation
* Added descriptions for auction states.
* App configuration is logged on startup and exposed at `/config`.
* Extend TezosAddress encoding  to support addresses with entrypoints (i.e. KT1JJbWfW8CHUY95hG9iq2CEMma1RiKhMHDR%foo)
* Loading indexer configuration from network specific file e.g. appsettings.Development.json.
* Removed deprecated `ReverseRecord.Name` GraphQL in favor of `ReverseRecord.Domain.Name`.
* Replaced `TEZOSDOMAINS_` prefix for host environment variables with default `DOTNET_`.
* **api:** Add Indexer heathcheck to monitor latest indexed block
* **api:** Remove AuctionState NotStarted (this state was not used and wouldn't return any data)
* **indexer:** Validate each block chainId against configuration and last indexed block

<a name="0.5.0"></a>
## 0.5.0 (2021-3-1)

### Bug Fixes

* fix regex to parse an integer coverage result
* Fixed creation of Mongo indexes for *events* collection.
* Fixed Mongo expressions for events by changing `interface IEvent` to `abstract record Event`.
* **indexer:** fix loading contracts from configuration (ensure that the configuration is correct)

### Features

* Added `Node.id` to remaining GraphQL output types.
* **indexer:** switch to new edo contracts

<a name="0.4.0"></a>
## 0.4.0 (2021-2-25)

### Bug Fixes

* application hanging when mongodb connection cannot be established during startup (IndexerDistributedLockService.DisposeAsync won't throw an exception while disposing)
* Fixed `auction.highestBid`.
* serilog slack sink should now correctly send all the messages before the application shutdown
* update to new version of MongoDB.Driver (2.11.6) - should fix a CompositeServerSelector timeout issue
* **indexer:** fix indexing auction bids (was failing on setting bidCount)
* **indexer:** StartFromLevel configuration is now inclusive. The set block number is the first block to be indexed.

### Features

* Add mongoDbClient Timeout configuration setting + Improve mongoDb HealthCheck logging
* add new integration environment - autopublish docker images with 'integration' git tag
* Add remaining events (ClaimReverseRecord, UpdateReverseRecord, UpdateRecord, SetChildRecord,Commit)
* Added `id` GraphQL field on `bid` type.
* Added explicit `Exception` to `TezosClient` if the response isn't successful.
* always log version
* Changed `bidderBalances` GraphQL query to return empty result instead of null if corresponding database entry doesn't exist.
* improve mongoDb logging to allow multiple servers in connectionString
* Index auctions and provide them on API (GraphQL queries: auctions, auction, currentAuction)
* Index events (bid, settle, buy, renew, withdraw, outbid, auction-end) operations and provide them on API
* make mongoClient singleton
* remove purchases from indexer and api
* Renamed `block` history filter to `atBlock`.
* **api:** add additional sort (highest bid timestamp, number of bids) and filter (endsAt) for auctions graphQL
* **api:** Add addressReverseRecord, ownerReverseRecord to DomainGraphType
* **api:** add bidder balances to GraphQL Endpoint
* **api:** Add block graphType to Events
* **api:** Add BlockFilter to EventsFilter to be able to search by timestamp, level, hash
* **api:** Add Type to EventGraphType
* **api:** Changed unique params (id ->domainName + firstBidBlockLevel) for AuctionQuery, added FirstBidBlockLevel field to AuctionGraphType
* **api:** extend AuctionEndEvent with Participants, AuctionBidEvent with PreviousBidder and PreviousBidAmount
* **api:** extend block query to get latest index block (head)
* **api:** Extend reverseRecord graphType with domain graphType (resolved using name)
* **indexer:** Index user balances
* **indexer:** switch configuration to support edonet

<a name="0.3.2"></a>
## 0.3.2 (2020-11-28)

### Bug Fixes

* new buy entrypoint
* new buy entrypoints

<a name="0.3.1"></a>
## 0.3.1 (2020-11-25)

### Other

* fix Reorganization_ShouldRollbackAllOperations  integrationTest - increase wait period

<a name="0.3.0"></a>
## 0.3.0 (2020-11-25)

### Bug Fixes

* add Apollo-specific headers to CORS according to https://github.com/kamilkisiela/apollo-angular/issues/1591
* Dockerfile (after removal of TezosDomains.Indexer.Models project)
* **indexer**: index record/reverse_record data map as hexBytesString (do not decode values)

### Features

* **api**: Upgrade GraphQL packages to stable versions (GraphQL.Server 4.0.1, GraphQL 3.1.0)

<a name="0.2.1"></a>
## 0.2.1 (2020-10-27)

### Bug Fixes

* **api**: Added disclaimer that not all blocks are indexed but only from the configured one.
* **api**: Fixed `Id` of all `Node`-s to be non-nullable.
* **api**: Hardcoded secondary sort direction to be always ascending so that client app gets nice view of domains of ordered by expiresAt desc.
* **indexer**: Temporary quick fix for purchases TLD suffix (switched from 'tez' to 'delphi')
* **Indexer**: Improve rollback logging message

### Features

* Enhance Slack logging - send startup message
* **api**: Added `in` and `notIn` filter operators.
* **api**: Added composite filters `or`, `and` to main document filters.
* **api**: Added custom error if `block(...)` Graph query specifies `level` lower than the one from which we started indexing.
* **indexer**: add lock expiring warning
* **indexer**: Index data field on domains and reverse records
* **indexer**: switch contracts to delphinet
* **Indexer**: ReverseRecords are never removed

<a name="0.2.0"></a>
## 0.2.0 (2020-10-2)

### Bug Fixes

* **api**: Added disclaimer that not all blocks are indexed but only from the configured one.
* **api**: Fixed `Id` of all `Node`-s to be non-nullable.
* **api**: Hardcoded secondary sort direction to be always ascending so that client app gets nice view of domains of ordered by expiresAt desc.
* **indexer**: Improve rollback logging message

### Features

* **api**: Added `in` and `notIn` filter operators.
* **api**: Added composite filters `or`, `and` to main document filters.
* **api**: Added custom error if `block(...)` Graph query specifies `level` lower than the one from which we started indexing.
* **api**: Added `ReverseRecord` property on `Domain` in GraphQL
* **indexer**: Index data field on domains and reverse records
* **Indexer**: ReverseRecords are never removed
* Added Serilog slack integration to notify about production errors

<a name="0.1.0"></a>
## 0.1.0 (2020-9-9)

### Features

* Initial release of Indexer and GraphQL API

